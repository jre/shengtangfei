pageQuery
===
    select #page("*")# from article where is_deleted=0
    @if(isNotEmpty(categoryId) && categoryId!=1){
    and category_id = #categoryId#
    @}
    @if(isNotEmpty(status)){
    and status = #status#
    @}
    @if(isNotEmpty(createBy)){
    and create_by = #createBy#
    @}
    @if(isNotEmpty(publishId)){
    and publish_id = #publishId#
    @}
    @if(isNotEmpty(title) && title != ''){
    and title like #'%'+title+'%'#
    @}
    @if(isNotEmpty(publisher) && publisher != ''){
    and publisher like #'%'+publisher+'%'#
    @}
    @if(isNotEmpty(keyword) && keyword != ''){
    and (title like #'%'+keyword+'%'# or content like #'%'+keyword+'%'# or id like #'%'+keyword+'%'#)
    @}
    @if(recommend!=null){
    and recommend=#recommend#
    @}
    -- 不查找用户拉黑的人
    @if(searchNoBlack!=null){
    and publish_id not in(select black_user_id from user_black_list where user_id=#searchNoBlack#)
    @}
	
    order by create_time desc
findClosely
===
    select * from article where is_deleted=0
    @if(isNotEmpty(categoryId)){
    and category_id = #categoryId#
    @}
    @if(isNotEmpty(status)){
    and status = #status#
    @}
    @if(isNotEmpty(id)){
    and id <> #id#
    @}
    order by id desc
    
    
findById
===    

        select t.*,t1.invitation_code as invitationCode from article t 
        left join user t1 on t.publish_id=t1.id 
        where t.is_deleted=0
        @if(!isEmpty(id)){
         and t.id = #id#
        @}
pageAttentionArticle
===

    select #page("a.*")# from article  a
    inner join attention a1 on a.publish_id = a1.focus_user_id
    where a.is_deleted =0
        and a.status = 1
    @if(!isEmpty(userId)){
      and a1.user_id = #userId#
     @}
    -- 不查找用户拉黑的人
    @if(searchNoBlack!=null){
    and publish_id not in(select black_user_id from user_black_list where user_id=#searchNoBlack#)
    @}
     order by a.create_time desc
pageFavoritesArticle
===
    select #page("*")#  from article where is_deleted=0 and status=1
    and id in(select article_id from  favorites where  user_id=#userId#
    and type=1
    )    
pageAccoladeArticle
===
    select #page("*")#  from article where is_deleted=0 and status=1
    and id in(select article_id from  accolade where  user_id=#userId#
    and type=1
    )   
    
pageToAuditArticle
===
    select #page("*")# from article where is_deleted=0 and status=0 and upload_type=1
    @if(isNotEmpty(categoryId) && categoryId!=1){
        and category_id = #categoryId#
    @}
    @if(isNotEmpty(content)){
        and (publish_id = #content# or publisher like #'%'+content+'%'# or id = #content# or title like #'%'+content+'%'#)
    @}
    @if(isNotEmpty(title)){
    and title like #'%'+title+'%'#
    @}  
    order by create_time desc
newCheck
===
    select * from article where is_deleted=0 and status = 0
    and upload_type=1 and create_time > (CURRENT_TIMESTAMP - INTERVAL 1 MINUTE)
    @if(searchNoBlack!=null){
    and publish_id not in (select black_user_id from user_black_list where user_id=#searchNoBlack#)
    @}
    order by create_time desc     
        
    
getUserArticle
=== 
     select #page("*")# from article where publish_id=#userId# and is_deleted=0 and status in (0,1)
      @if(!isEmpty(keywords)){
         and title like #'%'+keywords+'%'#
      @}
      order by `status` asc ,publish_date desc
        
pageGetComment
===
    select #page("`artc`.`id` AS `comment_id`,`artc`.`article_id` AS `object_id`,`artc`.`user_id` AS `user_id`,`artc`.`user_icon` AS `user_icon`,`artc`.`user_name` AS `user_name`,`artc`.`content` AS `content`,`artc`.`status` AS `status`,0 AS `type`,1 AS `comment_type`,`artc`.`create_time` AS `create_time`,NULL AS `voice_time`,NULL AS `voice`,NULL AS `images`")# from article as artc where is_deleted=0 and status=0
        @if(userId!=null){
            and user_id=#userId#
        @}
        @if(userName!=null){
            and user_name like #'%'+userName+'%'#
        @}
        @if(isNotEmpty(content) && content != ''){
            and content like #'%'+content+'%'#
        @}    
        order by create_time desc 

