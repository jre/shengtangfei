getUserArticleCategory
===   

    select a.* from user_category  u
    inner join article_category a on a.id = u.category_id
    where a.is_deleted =0 and u.category_type = 1 and u.user_id=#userId#

getUserVideoCategory
===   

    select v.* from user_category  u
    inner join video_category v on v.id = u.category_id
    where v.is_deleted =0 and u.user_id=#userId#
    @if(isNotEmpty(type) && type!=1){
        and u.category_type = #type#
    @}
    @if(isNotEmpty(type) && type==1){
        and u.category_type = 3
    @}
    
getUserJYJCategory
=== 
    select s.* from user_category  u
    inner join subject s on s.id = u.category_id
    where s.is_deleted =0 and u.user_id=#userId#
    @if(isNotEmpty(type) && type!=1){
        and u.category_type = #type#
    @}
    @if(isNotEmpty(type) && type==1){
        and u.category_type = 3
    @}    
    
deleteCategory
===

    DELETE FROM user_category WHERE user_id = #userId# and category_type = #categoryType#


