getRankTeacher
===

    select * from user u
    left join class_members c on c.user_id=u.id
    where u.is_deleted=0
    @if(type==1){
    order by u.answer_num desc
    @}else{
    order by u.accurate desc
    @}
    @if(isNotEmpty(classId)){
     and c.class_id = #classId#
    @}
    limit 0 , #size#    
    
findByOpenId
===
    select * from user where is_deleted=0
    @if(openId!=null){
    and open_id=#openId#
    @}else{
    and 1=2
    @}

findByMobile
===
    select * from user where is_deleted = false
    @if(mobile!=null){
    and mobile=#mobile#
    @}else{
    and 1=2
    @}
    
    
    
findByInvitationCode
===
    select * from user where is_deleted = 1
    @if(invitationCode !=null && invitationCode != '' ){
    and invitation_code = #invitationCode#
    @}    

findByLoginName
===
    select * from user where is_deleted = false
    @if(loginName!=null){
    and login_name=#loginName#
    @}else{
    and 1=2
    @}
    
    
plusLikeNum
===
    update `user` set like_num = like_num + '1' where id = #id# 
    
lessLikeNum
===
      update `user` set like_num = like_num -'1' where id = #id#
    

pageQuery
===
    select #page("*")# from user where is_deleted=0 and type=0
    @if(isNotEmpty(name) && name != ''){
    	and name like #'%'+ name +'%'#
    @}
    @if(isNotEmpty(mobile) && mobile != ''){
    	and mobile = #mobile#
    @}
    @if(isNotEmpty(sex)){
    	and sex = #sex#
    @}
    @if(isNotEmpty(level)){
    	and level = #level#
    @}
    order by id desc
getMySpread
===
    select * from user where is_deleted=0
    and spread_id=#spreadId#
pageUserComment
===
     select #page("*")# from question_article_video_comment where user_id=#userId#
pageComment
===
     select #page("*")# from question_article_video_comment where 1=1
     @if(isNotEmpty(userId)){
        and user_id=#userId#
    @}
     @if(isNotEmpty(status)){
        and status=#status#
    @}
     @if(isNotEmpty(type)){
        and type=#type#
    @}
     @if(isNotEmpty(userName)){
        and user_name like #'%'+ userName +'%'#
    @}
     @if(isNotEmpty(content)){
        and content like #'%'+ content +'%'#
    @}
    order by create_time desc
   
searchFriendOrGroup
===
    select * from user where is_deleted=0
        @if(isNotEmpty(parameter)){
           and (name like #'%'+ parameter +'%'# or account like #'%'+ parameter +'%'#)
       @}
        
       order by create_time desc  
       
getNotAttentionRobot
===
    SELECT * FROM `user` where type=1 and is_deleted=0 and id not in (SELECT user_id from attention where is_deleted=0 and focus_user_id=#userId#) order by rand() limit #fansNum#    
