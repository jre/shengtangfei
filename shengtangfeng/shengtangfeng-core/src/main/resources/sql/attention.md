
pageQuery
===
    select #page("*")# from attention  where is_deleted=0 and
    #use("condition")#
    order by create_time desc

pageMyAttention
===
    select #page("u.id,u.head_image,u.name,a.is_attention,u.invitation_code,a.is_stick")# from attention a left join user u on u.id=a.focus_user_id where u.is_deleted=0 and a.user_id=#userId#
    order by a.is_stick desc,a.update_time desc,a.create_time desc

pageMyFans
===
    select #page("u.id,u.head_image,u.name,a.is_stick,a.is_attention,u.invitation_code")# from attention a left join user u on u.id=a.user_id where u.is_deleted=0 and a.focus_user_id=#userId#
    order by a.is_stick desc ,a.create_time desc
    
findByFocusId
===

    select * from attention where is_deleted=0
    @if(!isEmpty(userId)){
        and user_id =#userId#
    @}
    @if(!isEmpty(focusUserId)){
        and focus_user_id =#focusUserId#
    @}
        
    order by create_time asc
 
 
deleteAttention
===    
    delete from attention 
    where user_id = #userId#
    and focus_user_id =#focusUserId#
    
updateAttention
===       
       
    UPDATE attention set is_attention = #isAttention#  where id = #id#
   
    


sample
===
* 注释###

    select #use("cols")# from attention  where  #use("condition")#

cols
===
	id,user_id,focus_user_id,is_attention,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,user_id=#userId#,focus_user_id=#focusUserId#,is_attention=#isAttention#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(focusUserId)){
     and focus_user_id=#focusUserId#
    @}
    @if(!isEmpty(isAttention)){
     and is_attention=#isAttention#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    