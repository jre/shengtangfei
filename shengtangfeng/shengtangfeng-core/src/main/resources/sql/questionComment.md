pageQuery
===
	select #page("*")# from question_comment 
	where  is_deleted=0
    @if(status!=null){
        and status=#status#
    @}
    @if(isNotEmpty(createBy) && createBy!=null){
        and create_by=#createBy#
    @}
    @if(isNotEmpty(userName)){
        and user_name like #'%'+userName+'%'#
    @}
      @if(isNotEmpty(recommend)){
            and recommend like #'%'+recommend+'%'#
        @}
    @if(isNotEmpty(startTime) && startTime != ''){
        and create_time > #startTime#
    @}
     @if(isNotEmpty(recommend) && recommend!=null){
            and recommend=#recommend#
        @}
    @if(isNotEmpty(endTime) && endTime != ''){
        and create_time < #endTime#
    @}
    @if(isNotEmpty(questionId) && questionId != ''){
            and question_id=#questionId#
    @}
    @if(isNotEmpty(userId) && userId != ''){
            and user_id <> #userId#
    @}
	order by create_time desc
pageQueryNotMine
===
	select #page("*")# from question_comment 
	where  is_deleted=0
    @if(status!=null){
        and status=#status#
    @}
    @if(isNotEmpty(createBy) && createBy!=null){
        and create_by=#createBy#
    @}
    @if(isNotEmpty(userName)){
        and user_name like #'%'+userName+'%'#
    @}
      @if(isNotEmpty(recommend)){
            and recommend like #'%'+recommend+'%'#
        @}
    @if(isNotEmpty(startTime) && startTime != ''){
        and create_time > #startTime#
    @}
     @if(isNotEmpty(recommend) && recommend!=null){
            and recommend=#recommend#
        @}
    @if(isNotEmpty(endTime) && endTime != ''){
        and create_time < #endTime#
    @}
    @if(isNotEmpty(questionId) && questionId != ''){
            and question_id=#questionId#
    @}
    @if(isNotEmpty(userId) && userId != ''){
            and user_id <> #userId#
    @}
	order by create_time desc
findByQuestionId
===
	select * from question_comment  where  is_deleted=0
	@if(questionId!=null){
	and question_id=#questionId#
	@}
	@if(status!=null){
	and status=#status#
	@}
	order by create_time desc		
		
findByTemplate
===
	select * from question_comment  where  is_deleted=0
	@if(questionId!=null){
	and question_id=#questionId#
	@}
	@if(status!=null){
	and status=#status#
	@}
	@if(userId!=null){
	and user_id=#userId#
	@}
	order by create_time desc	
	
pageGetComment
===
    select #page("quec.id as comment_id, quec.question_id as object_id ,quec.user_id as user_id, quec.user_icon as user_icon, quec.user_name as user_name,quec.content as content,quec.`status` as `status`,2 as type , quec.type as comment_type,quec.create_time as create_time,quec.voice_time as voice_time ,quec.voice as voice,quec.images as images")# 
    from question_comment AS quec where is_deleted=0 and status=0
    @if(isNotEmpty(content)){
         and (user_id=#content# or user_name like #'%'+content+'%'# or content like #'%'+content+'%'#)
    @}
    order by create_time desc 
        	

pageArticleVideoQuestionAuditComment
===
    select #page("*")# from article_video_question_audit_comment where 1=1
     @if(isNotEmpty(content)){
          and (user_id=#content# or user_name like #'%'+content+'%'# or content like #'%'+content+'%'#)
     @}
     order by create_time desc 
            
getUserComment
===
    select #page("*")# from user_comment where user_id=#userId#
    @if(isNotEmpty(keywords)){
        and content like #'%'+keywords+'%'#
    @}
    order by `status` asc ,create_time desc
           
                    		
		

   