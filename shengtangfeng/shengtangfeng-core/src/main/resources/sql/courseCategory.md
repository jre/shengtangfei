pageQuery
===
    select #page("*")# from course_category where is_deleted=0
    @if(isNotEmpty(name)){
    and name like #'%'+name+'%'#
    @}
    @if(isNotEmpty(type)){
    and type = #type#
    @}
    order by sort 
    
getList
===
    SELECT
        * 
    FROM
        course_category 
    WHERE
        is_deleted=0
    order by sort asc