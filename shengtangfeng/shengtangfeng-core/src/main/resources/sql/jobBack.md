getStudentJobBack
===
     select * from job_back where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    order by create_time desc
        
getTeacherJobBack
===
    select * from job_back where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    order by create_time desc          