
pageQuery
===
    select #page("*")# from user_job  where is_deleted=0 and
    #use("condition")#
    order by create_time desc
scoreRank
===
    select #page("*")# from user_job where is_deleted = 0 and
    status = 2 and class_id=#classId# and job_id = #jobId# order by score desc

speedRank
===
    select #page("*")# from user_job where is_deleted = 0 and
    status = 2 and class_id=#classId#  and job_id = #jobId# and score > #passScore# 
    order by use_time asc
    
submitRank
===
    select #page("*")# from user_job where is_deleted = 0 and
    status = 2 and class_id=#classId# and job_id = #jobId# and score > #passScore# 
    order by finish_time asc
    
getRankTeacher
===
    select #page("uj.*")# from user_job uj 
    left join class_and as c on uj.class_id=c.id 
    where uj.is_deleted = 0 and uj.status=2
    @if(!isEmpty(jobId)){
     and uj.job_id=#jobId#
    @}
    @if(!isEmpty(classId)){
     and uj.class_id=#classId#
    @}
    @if(!isEmpty(type) && type==1){
     order by uj.score desc
    @}
    @if(!isEmpty(type) && type==2){
     and uj.score > #passScore# order by uj.use_time asc
    @}
    @if(!isEmpty(type) && type==3){
     and uj.score > #passScore#  order by uj.finish_time asc
    @}
        
        

sample
===
* 注释###

    select #use("cols")# from user_job  where  #use("condition")#

cols
===
	id,user_id,job_id,create_time,create_by,update_time,update_by,is_deleted,score

updateSample
===
    
	id=#id#,user_id=#userId#,job_id=#jobId#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#,score=#score#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(jobId)){
     and job_id=#jobId#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(score)){
     and score=#score#
    @}
       
    