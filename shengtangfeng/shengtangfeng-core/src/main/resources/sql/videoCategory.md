
pageQuery
===
    select #page("*")# from video_category  where is_deleted=0
    @if(!isEmpty(type)){
         and type=#type#
    @}
    @if(!isEmpty(name)){
         and name like #'%'+name+'%'#
    @}
    order by sort asc


getList
===
    select * from video_category where is_deleted=0
    @if(!isEmpty(type)){
         and type=#type#
    @}
    order by sort asc
    
    
getVideoCategoryDefault
===
    select * from video_category where is_deleted=0
    @if(!isEmpty(type)){
         and type=#type#
    @}
    and is_default =1
    order by sort asc    