
pageQuery
===
    select #page("*")# from user_report  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from user_report  where  #use("condition")#

cols
===
	id,informer_id,informer_name,reasons_id,content,images,create_time,create_by,update_time,update_by,is_deleted,person_id,person_name

updateSample
===
    
	id=#id#,informer_id=#informerId#,informer_name=#informerName#,reasons_id=#reasonsId#,content=#content#,images=#images#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#,person_id=#personId#,person_name=#personName#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(informerId)){
     and informer_id=#informerId#
    @}
    @if(!isEmpty(informerName)){
     and informer_name=#informerName#
    @}
    @if(!isEmpty(reasonsId)){
     and reasons_id=#reasonsId#
    @}
    @if(!isEmpty(content)){
     and content=#content#
    @}
    @if(!isEmpty(images)){
     and images=#images#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(personId)){
     and person_id=#personId#
    @}
    @if(!isEmpty(personName)){
     and person_name=#personName#
    @}
    
toAuditReport
===
    select #page("ur.*,rr.reasons")# from user_report ur left join report_reasons rr on rr.id = ur.reasons_id where ur.is_deleted=0 and ur.status=0
        @if(!isEmpty(reasonId)){
            and ur.reasons_id = #reasonId#
        @}
        @if(!isEmpty(keywords)){
            and (ur.person_id = #keywords# or ur.person_name like #'%'+keywords+'%'# or content like #'%'+keywords+'%'# or ur.informer_id = #keywords# or ur.informer_name like #'%'+keywords+'%'#)
        @}
        order by ur.create_time desc

toAdminReport
===
    select #page("ur.*,rr.reasons")# from user_report ur left join report_reasons rr on rr.id = ur.reasons_id where ur.is_deleted=0 
            @if(!isEmpty(reasonId)){
                and ur.reasons_id = #reasonId#
            @}
            @if(!isEmpty(user)){
                and (ur.person_id = #keywords# or ur.person_name like #'%'+keywords+'%'# )
            @}
            @if(!isEmpty(keywords)){
                and content like #'%'+keywords+'%'#
            @}
            order by ur.create_time desc
            
upStatusById
===
    update user_report set status = #status# where id in #reportId#
    