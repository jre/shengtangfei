findByLevel
===
	select * from permission where is_deleted=0
	@if(level!=null){
	and level=#level#
	@}

findChildByParentId
===
	select * from permission where is_deleted=0 
	@if(parentId!=null){
	and parent_id=#parentId#
	@}
	
selectMenuList
===
    SELECT
    	* 
    FROM
    	permission 
    WHERE
    	id IN (
    	SELECT
    		permission_id 
    	FROM
    		role_permission 
    WHERE
    	role_id = #roleId#)
