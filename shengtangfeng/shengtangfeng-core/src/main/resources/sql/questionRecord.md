findByIds
====
    select * from question_record where is_deleted=0
    	@if(source!=null){
        and source=#source#
    @}
    @if(userId!=null){
        and user_id=#userId#
    @}
     @if(objectId!=null){
        and object_id=#objectId#
    @}
    and question_id in (
        @for(questionId in questionIdList) {
            #questionId#  #text(questionIdLP.last?"":"," )#
        @}
        )
myRecord
===
    select * from question_record where is_deleted=0
    @if(userId!=null){
        and user_id=#userId#
    @}
    @if(jobId!=null){
        and job_id=#jobId#
    @}
    @if(classId!=null){
        and class_id=#classId#
    @}
    order by create_time
    
getStudentAnswer
===
    select #page("id,user_answer,user_id,user_name,create_time,answer_type,`status`")# from question_record where is_deleted=0 and answer_type in(1,2)
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    @if(!isEmpty(type)){
    and answer_type=#type#
    @}
    @if(!isEmpty(content)){
     and user_answer like #'%'+content+'%'#
    @}
    @if(!isEmpty(userNickname)){
     and user_name like #'%'+userNickname+'%'#
    @}
    order by create_time desc         
           
pageUserQuestionRecord
===
      select #page("id,user_answer,user_id,user_name,create_time,answer_type,`status`")# from question_record where is_deleted=0
          @if(!isEmpty(status)){
           and status=#status#
          @}
          @if(!isEmpty(type)){
          and answer_type=#type#
          @}
          @if(!isEmpty(content)){
           and user_answer like #'%'+content+'%'#
          @}
          @if(!isEmpty(userNickname)){
           and user_name like #'%'+userNickname+'%'#
          @}
          order by create_time desc
findByCondition
===
    select * from question_record where is_deleted=0
    @if(userId!=null){
        and user_id=#userId#
    @}
    @if(jobId!=null){
        and job_id=#jobId#
    @}
    @if(questionId!=null){
        and question_id=#questionId#
    @}
    @if(classId!=null){
        and class_id=#classId#
    @}
    order by create_time desc limit 1
    
getJobQuestionPage
===
    select * from question_record where is_deleted=0 and class_id=#classId# and job_id=#jobId# and question_id=#questionId# and user_id in (select user_id from class_members WHERE is_deleted=0 and type=1 and class_id=#classId#)    
    