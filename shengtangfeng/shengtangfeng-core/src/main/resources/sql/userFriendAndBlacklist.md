
pageQuery
===
    select #page("*")# from user_friend_and_blacklist  where is_deleted=0 and
    #use("condition")#
    order by create_time desc

isFriend
===
    select * from user_friend_and_blacklist where is_deleted = 0 and user_id = #userId#
    and relevance_id = #friendUser# and type = 0 order by id asc limit 1
    
isFriend1
===
    select * from user_friend_and_blacklist where is_deleted = 0 and user_id = #friendUser#
    and relevance_id = #userId# and type = 0 order by id asc limit 1    


sample
===
* 注释###

    select #use("cols")# from user_friend_and_blacklist  where  #use("condition")#

cols
===
	id,user_id,relevance_id,create_time,create_by,update_time,update_by,type,is_deleted

updateSample
===
    
	id=#id#,user_id=#userId#,relevance_id=#relevanceId#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,type=#type#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(relevanceId)){
     and relevance_id=#relevanceId#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
getList
===
    select b.* from user_friend_and_blacklist b LEFT JOIN `user` u on b.relevance_id=u.id where b.is_deleted = 0
    @if(!isEmpty(userId)){
         and b.user_id=#userId#
    @}
    @if(!isEmpty(type)){
         and b.type=#type#
    @}
    @if(!isEmpty(keyword)){
         and (u.account like #'%'+keyword+'%'# or u.name like #'%'+keyword+'%'#)
    @}
    
getFriendList
===
    select b.* from user_friend_and_blacklist b LEFT JOIN `user` u on b.relevance_id=u.id where b.is_deleted=0 and b.type=0
        @if(!isEmpty(userId)){
             and b.user_id=#userId#
        @}
        @if(!isEmpty(keyword)){
             and (u.account like #'%'+keyword+'%'# or u.name like #'%'+keyword+'%'#)
        @}    
    