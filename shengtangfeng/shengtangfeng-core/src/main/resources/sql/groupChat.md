
pageQuery
===
    select #page("*")# from group_chat  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from group_chat  where  #use("condition")#

cols
===
	id,class_id,name,head,introduce,notice,add_way,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,class_id=#classId#,name=#name#,head=#head#,introduce=#introduce#,notice=#notice#,add_way=#addWay#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(classId)){
     and class_id=#classId#
    @}
    @if(!isEmpty(name)){
     and name=#name#
    @}
    @if(!isEmpty(head)){
     and head=#head#
    @}
    @if(!isEmpty(introduce)){
     and introduce=#introduce#
    @}
    @if(!isEmpty(notice)){
     and notice=#notice#
    @}
    @if(!isEmpty(addWay)){
     and add_way=#addWay#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
searchFriendOrGroup
===
    select * from group_chat where is_deleted=0 and group_type=0
    @if(isNotEmpty(parameter)){
       and (name like #'%'+ parameter +'%'# or group_num like #'%'+ parameter +'%'#)
    @}
    order by create_time desc         
    