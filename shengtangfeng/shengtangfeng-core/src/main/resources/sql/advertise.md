pageQuery
===
	select #page("*")# from advertise where is_deleted=0
	@if(isNotEmpty(type)){
    and type = #type#
    @}
	@if(isNotEmpty(status)){
    and status = #status#
    @}
	@if(isNotEmpty(linkTo)){
    and link_to = #linkTo#
    @}
    @if(isNotEmpty(title)){
    and title like #'%'+title+'%'#
    @}
	order by create_time desc
