getUseNum
===
    select count(*) from music_use where is_deleted=0
    @if(!isEmpty(musicId)){
     and music_id=#musicId#
    @}
    @if(!isEmpty(startTime) && !isEmpty(endTime)){
     and use_time>#startTime# and use_time<#endTime#
    @}
    @if(!isEmpty(type) && type==1){
     and  to_days(use_time) = to_days(now())
    @}
    @if(!isEmpty(type) && type==2){
     and TO_DAYS( NOW( )) - TO_DAYS(use_time) =1
    @}
    @if(!isEmpty(type) && type==3){
     and YEARWEEK(date_format(use_time,'%Y-%m-%d'))=YEARWEEK(now())
    @}
    @if(!isEmpty(type) && type==4){
     and YEARWEEK(date_format(use_time,'%Y-%m-%d'))=YEARWEEK(now())-1
    @}
    @if(!isEmpty(type) && type==5){
     and DATE_FORMAT(use_time,'%Y%m')=DATE_FORMAT(CURDATE(),'%Y%m')
    @}
    @if(!isEmpty(type) && type==6){
     and PERIOD_DIFF(date_format(now(),'%Y%m'),date_format(use_time,'%Y%m'))=1
    @}