pageQuery
===
	select #page("*")# from question where is_deleted=0
	@if(isNotEmpty(title)){
	and title like #'%'+title+'%'#
	@}
	@if(isNotEmpty(keyword)){
	and (
	title like #'%'+keyword+'%'# 
	)
	@}
	@if(isNotEmpty(content)){
	and content like #'%'+content+'%'#
	@}
    @if(!isEmpty(cate) && '' != cate){
       and category_id1 in (
            @for(id in cate) {
                #id#  #text(idLP.last?"":"," )#
            @}
            )
    @}  
      @if(!isEmpty(cate2) && '' != cate2){
           and category_id2 in (
            @for(id in cate2) {
                #id#  #text(idLP.last?"":"," )#
            @}
            )
    @}   
    @if(isNotEmpty(type)){
    	and type = #type#
    @}
    @if(isNotEmpty(vip)){
      	and vip = #vip#
    @}
    @if(isNotEmpty(accurate)){
      	and accurate = #accurate#
    @}
     
    @if(isNotEmpty(categoryId1)){
    	and category_id1 = #categoryId1#
    @}
    @if(isNotEmpty(categoryId2)){
    	and category_id2 = #categoryId2#
    @}	
    @if(isNotEmpty(ran)&&ran){ 
        order by rand()
       	
    @} 
    @if(isEmpty(ran)||!ran){
       order by update_time desc
    @}
pageQueryHistory
===
	select #page("*")# from question_history where is_deleted=0
	@if(isNotEmpty(questionId)){
    	and question_id = #questionId#
    	@}
	@if(isNotEmpty(title)){
	and title like #'%'+title+'%'#
	@}
	@if(isNotEmpty(keyword)){
	and (
	title like #'%'+keyword+'%'# 
	or content like #'%'+keyword+'%'#
	)
	@}
	@if(isNotEmpty(content)){
	and content like #'%'+content+'%'#
	@}
    @if(!isEmpty(kaodian) && '' != kaodian){
        @for(kaodianItem in strutil.split(kaodian, ",")) {
            and FIND_IN_SET(#kaodianItem#, kaodian)
        @}
    @}
    @if(isNotEmpty(type)){
    	and type = #type#
    @}
    @if(isNotEmpty(vip)){
      	and vip = #vip#
    @}
    @if(isNotEmpty(accurate)){
      	and accurate = #accurate#
    @}
     
    @if(isNotEmpty(categoryId1)){
    	and category_id1 = #categoryId1#
    @}
    @if(isNotEmpty(categoryId2)){
    	and category_id2 = #categoryId2#
    @}	
    @if(isNotEmpty(ran)&&ran){ 
        order by rand(type)
       	
    @} 
    @if(isEmpty(ran)||!ran){
       order by type asc,id desc
    @}   
pageQueryByHitType
===
	SELECT
    	#page("*")# 
    FROM
    	(
    SELECT
    	*,
    	(
    CASE
    	
    	WHEN locate( keyword, title ) > 0 
    	AND locate( keyword, content ) > 0 THEN
    		0 
    		WHEN locate( keyword, title ) > 0 
    		AND locate( keyword, content ) = 0 THEN
    			1 
    			WHEN locate( keyword, title ) = 0 
    			AND locate( keyword, content ) > 0 THEN
    				2 ELSE 3 
    			END 
    			) hitType 
    		FROM
    			question 
    		) a 
    		
    ORDER BY
    a.hitType
	

findByModule
===
    select q.* from question q right join module_question mq on q.id=mq.question_id 
    where mq.module_id=#moduleId# and q.is_deleted=0
    order by q.type,mq.sort

pageQueryByModule   
===
    select 
        #page("q.* ")#
    from question q 
    right join module_question mq on q.id = mq.question_id 
    where mq.module_id=#moduleId# and q.is_deleted=0
        @if(isNotEmpty(type)){
            and q.type = #type#
        @}
        @if(isNotEmpty(accurate)){
            and q.accurate = #accurate#
        @}
        @if(isNotEmpty(title)){
                                and q.title like #'%'+title+'%'#
                        @}
          order by q.type,mq.sort 
pageModuleQuery
===
    select #page("t1.*")# from question t1 
    where 
        not EXISTS 
        (select question_id from module_question t2 where t1.id = t2.question_id and t2.module_id = #moduleId#)
        @if(isNotEmpty(title)){
        and t1.title like #'%'+title+'%'#
        @}
        @if(isNotEmpty(type)){
            and t1.type = #type#
        @}
        and t1.is_deleted = 0
    order by t1.id desc
getIncorrectQuestion
===
    select * from question  where is_deleted=0 and id in
    (select distinct question_id from incorrect_question where user_id=#userId# 
    and subject_id=#subjectId# and type=0)
    order by id desc       
getFavoritesQuestion
===
    select * from question  where is_deleted=0 and id in
    (select distinct question_id from favorites_question where user_id=#userId# 
    and subject_id=#subjectId#)
    order by id desc       

getQuestionGradeCount
====
    select count(*) from question_grade where 
    question_id=#questionId# and grade_id=#gradeId#
insertQuestionGrade
====
    INSERT INTO question_grade (question_id, grade_id) VALUES (#questionId#, #gradeId#)
deleteQuestionGrade
====
    delete from question_grade where question_id=#questionId#

jobQuestion
===
    select q.* from question q inner join job_question j on j.question_id = q.id
    where j.class_job_id = #jobId# and j.is_deleted = 0 order by j.id asc
    
getGradeQuestion
===
    select question_id from question_grade where grade_id=#gradeId#
    
getQuestionGrade
===
    select grade_id from question_grade where question_id=#questionId# limit 0,1
getQuestionGrades
===
    select grade_id from question_grade where question_id=#questionId#
    
getQuestionCount
===
    select count(*) from question q inner join question_grade g on q.id = g.question_id where q.is_deleted=0 and q.status=1
     @if(isNotEmpty(gradeId)){
        and g.grade_id= #gradeId#
     @}
     @if(isNotEmpty(shareType)){
        and q.share_type= #shareType#
     @}
     @if(isNotEmpty(createBy)){
        and q.create_by= #createBy#
     @}
     @if(isNotEmpty(warehouseType)){
         and q.warehouse_type= #warehouseType#
     @}
     @if(isNotEmpty(subjectId)){
         and q.subject_id= #subjectId#
     @}
     @if(isNotEmpty(type)){
         and q.type= #type#
     @}
     
getMyQuestionPage
===
    select  #page("q.*")#from question q left join question_grade g on q.id = g.question_id where q.is_deleted=0 and q.warehouse_type=1 and q.status=1
    @if(!isEmpty(userId)){
     and q.create_by=#userId#
    @}
    @if(!isEmpty(status) && status!=3){
     and q.type=#status#
    @}
    @if(!isEmpty(gradeId)){
     and g.grade_id=#gradeId#
    @}
    @if(!isEmpty(title)){
     and q.title like #'%'+title+'%'#
    @}
    @if(!isEmpty(keyword)){
     and q.title like #'%'+keyword+'%'#
    @}
    @if(!isEmpty(subjectId) && subjectId!=0){
     and q.subject_id=#subjectId#
    @}
    order by q.create_time desc

getTaoxuePage
===
    select  #page("q.*")#from question q inner join question_grade g on q.id = g.question_id where q.is_deleted=0 and q.warehouse_type=0 and q.status=1
    @if(!isEmpty(status) && status!=3){
     and q.type=#status#
    @}
    @if(!isEmpty(gradeId)){
     and g.grade_id=#gradeId#
    @}
    @if(!isEmpty(title)){
     and q.title like #'%'+title+'%'#
    @}
    @if(!isEmpty(subjectId) && subjectId!=0){
     and q.subject_id=#subjectId#
    @}
    order by q.create_time desc
    
getGongxiangPage
===
    select  #page("q.*")# from question q inner join question_grade g on q.id = g.question_id where q.is_deleted=0 and q.share_type=1 and q.status=1
       @if(!isEmpty(status) && status!=3){
        and q.type=#status#
       @}
       @if(!isEmpty(gradeId)){
        and g.grade_id=#gradeId#
       @}
       @if(!isEmpty(title)){
        and q.title like #'%'+title+'%'#
       @}
       @if(!isEmpty(subjectId) && subjectId!=0){
        and q.subject_id=#subjectId#
       @}
       order by q.create_time desc
       
getTaoxueFavorite
===
    select #page("q.*")# from question q LEFT JOIN favorites_question f on f.question_id=q.id LEFT JOIN question_grade g on f.question_id = g.question_id where f.is_deleted=0 and f.back_type=1 and q.warehouse_type=0 and q.status=1            
    @if(!isEmpty(userId)){
     and f.user_id=#userId#
    @}
    @if(!isEmpty(title)){
     and q.title like #'%'+title+'%'#
    @}
    @if(!isEmpty(status) && status!=3){
    and q.type=#status#
    @}
    @if(!isEmpty(gradeId)){
     and g.grade_id=#gradeId#
    @}
    order by f.create_time desc
    
getGongxiangFavorite
===
    select #page("q.*")# from question q LEFT JOIN favorites_question f on f.question_id=q.id LEFT JOIN question_grade g on f.question_id = g.question_id where f.is_deleted=0 and f.back_type=2 and q.share_type=1 and q.status=1           
    @if(!isEmpty(userId)){
     and f.user_id=#userId#
    @}
    @if(!isEmpty(title)){
     and q.title like #'%'+title+'%'#
    @}
    @if(!isEmpty(status) && status!=3){
    and q.type=#status#
    @}
    @if(!isEmpty(gradeId)){
     and g.grade_id=#gradeId#
    @}
    order by f.create_time desc
    
getQuestionPage
===
    SELECT #page("q.*")# FROM `question` q LEFT JOIN user u on q.create_by=u.id where q.is_deleted=0 and q.warehouse_type=1  
    @if(!isEmpty(status)){
     and q.status=#status#
    @}
    @if(!isEmpty(userId)){
     and u.id=#userId#
    @}
    @if(!isEmpty(type)){
    and q.type=#type#
    @}
    @if(!isEmpty(content)){
     and q.title like #'%'+content+'%'#
    @}
    @if(!isEmpty(userNickname)){
     and u.name like #'%'+userNickname+'%'#
    @}
    order by q.create_time desc
    
getJobQuestionPage
===
    SELECT #page("jq.serial_number,q.id,q.type,q.score,q.title,q.content,jq.create_time, (case when  q.warehouse_type=0 then 1 when q.share_type=1 then 2 else  null  end)banktype")# FROM `question` q LEFT JOIN job_question jq on q.id=jq.question_id LEFT JOIN class_job cj on cj.job_id=jq.class_job_id
    where 1=1
    @if(!isEmpty(type) && type==1){
     and q.type=0
    @}
    @if(!isEmpty(type) && type==2){
     and q.type=1
    @}
    @if(!isEmpty(type) && type==3){
     and q.type=2
    @}
    @if(!isEmpty(jobId)){
     and jq.class_job_id=#jobId#
    @}
    @if(!isEmpty(classId)){
     and cj.class_id=#classId#
    @}
    
getJobQuestion
===
     SELECT q.* FROM `question` q LEFT JOIN job_question jq on q.id=jq.question_id where jq.class_job_id=#jobId#
    
    
getUseQuestion
===
    SELECT jq.* FROM job_question jq 
    LEFT JOIN job on jq.class_job_id=job.id
    where job.is_deleted=0 and job.create_by=#userId# and jq.question_id=#questionId# 
           
          
    
