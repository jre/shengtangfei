pageQueryVo
===
	select #page("*")# from view_notice where is_deleted=0
	@if(userId!=null){
        and user_id=#userId#
    @}
	@if(readed!=null){
    and readed=#readed#
    @}
	@if(category!=null){
    and category=#category#
    @}
	@if(type!=null){
    and type=#type#
    @}
	order by readed asc,id desc
pageQuery
===
	select #page("*")# from notice where is_deleted=0
	@if(category!=null){
    and category=#category#
    @}
	@if(type!=null){
    and type=#type#
    @}
	@if(isNotEmpty(title)){
    and title like #'%'+title+'%'#
    @}
	order by id desc

getCount
===
    select count(*) from view_notice where is_deleted=0
    @if(userId!=null){
        and user_id=#userId#
    @}
    @if(readed!=null){
    and readed=#readed#
    @}
    @if(category!=null){
    and category=#category#
    @}
    @if(type!=null){
    and type=#type#
    @}
