
pageQuery
===
    select #page("*")# from report_reasons  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from report_reasons  where  #use("condition")#

cols
===
	id,reasons,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,reasons=#reasons#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(reasons)){
     and reasons=#reasons#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    