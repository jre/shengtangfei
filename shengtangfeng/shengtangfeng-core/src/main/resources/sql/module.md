pageQuery
===
	select #page("*")# from module where is_deleted=0
	order by sort asc
findIdByCondition
===
    select * from module where is_deleted=0
    @if(!isEmpty(subjectId)){
       and subject_id = #subjectId#
    @}
    @if(!isEmpty(gradeId)){
       and grade_id = #gradeId#
    @}
    @if(!isEmpty(parentId)){
       and parent_id = #parentId#
    @}
    order by sort asc
    
findIdByCondition1
===
    select * from module where is_deleted=0
    @if(!isEmpty(parentId)){
       and parent_id = #parentId#
    @}
    order by sort asc
   
        
findByQuestion
===
    select m.* from module m right join module_question mq on m.id=mq.module_id 
    where mq.question_id=#questionId# and m.is_deleted=0
    order by mq.sort
findRowNum
===
    SELECT b.rownum FROM
    (
    SELECT t.*, \@rownum := \@rownum + 1 AS rownum
    FROM (SELECT \@rownum := 0) r,
    (select * from module where is_deleted=0
        and parent_id = #parentId#
        order by sort asc) AS t
    ) AS b WHERE b.id = #moduleId#;

findNext
===
    SELECT b.* FROM
    (
    SELECT t.*, \@rownum := \@rownum + 1 AS rownum
    FROM (SELECT \@rownum := 0) r,
    (select * from module where is_deleted=0
        and parent_id = #parentId#
        order by sort asc) AS t
    ) AS b WHERE b.rownum > #rowNum#;

findFirst
===
    select * from module where is_deleted=0
    and parent_id = #parentId#
    order by sort asc  limit 0,1
    
findIdByModuleName
===
    select id from module where module_name = #moduleName#    	
    
getMaxSequence
===

    select max(sort) from module where is_deleted = 0 
    @if(!isEmpty(parentId)){
       and parent_id = #parentId#
    @}  
    
getModule
===
    select module.* from module LEFT JOIN module_question on module.id=module_question.module_id where module.is_deleted=0
     @if(!isEmpty(questionId)){
       and module_question.question_id=#questionId#
     @}       