
pageQuery
===
    select #page("*")# from school  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from school  where  #use("condition")#

cols
===
	id,provice_id,city_id,count_id,name,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,provice_id=#proviceId#,city_id=#cityId#,count_id=#countId#,name=#name#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(proviceId)){
     and provice_id=#proviceId#
    @}
    @if(!isEmpty(cityId)){
     and city_id=#cityId#
    @}
    @if(!isEmpty(county)){
     and count_id=#countId#
    @}
    @if(!isEmpty(name)){
     and name like #'%'+name'%'#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
findSchoolPage
===
    select  #page("*")# from school where is_deleted=0
        @if(!isEmpty(province)){
         and province=#province#
        @}
        @if(!isEmpty(city)){
         and city=#city#
        @}
        @if(!isEmpty(county)){
         and county=#county#
        @}
        @if(!isEmpty(name)){
         and name like #'%'+name+'%'#
        @}
        order by create_time desc
        
    
getProvince
===
    select name from area where id=#provinceId# and `level`=1 and is_deleted=0
    
getCity
===
    select name from area where id=#cityId# and `level`=2 and is_deleted=0

getCounty
===
    select name from area where id=#countyId# and `level`=3 and is_deleted=0
    
getAllProvince
===
    SELECT id,`name`,adcode FROM `area` where is_deleted=0 and `level`=1


getAllCity
===
    SELECT id,`name`,adcode FROM `area` where is_deleted=0 and `level`=2 and parent_id=#provinceId#


getAllCounty
===
    SELECT id,`name`,adcode FROM `area` where is_deleted=0 and `level`=3 and parent_id=#cityId#
    
    
    
schoolPage
===
    select  #page("s.id,s.`name`,s.province,s.city,s.county,u.id as user_id,u.`name` as user_nickname,u.is_locked,s.create_time,s.type,(SELECT count(1) from class_and where school_id=s.id and is_deleted=0) as class_num")# from school s    
    LEFT JOIN `user` u on s.create_by=u.id WHERE s.is_deleted=0        
    @if(!isEmpty(province)){
     and s.province=#province#
    @}
    @if(!isEmpty(city)){
     and s.city=#city#
    @}
    @if(!isEmpty(county)){
     and s.county=#county#
    @}
    @if(!isEmpty(schoolName)){
     and s.name like #'%'+schoolName+'%'#
    @}
    @if(!isEmpty(userNickname)){
     and u.name like #'%'+userNickname+'%'#
    @}
    order by s.create_time desc