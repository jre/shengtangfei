pageFavorites
===
    select #page("*")# from question where is_deleted=0 
    and id in(select question_id from  favorites_question where user_id=#userId#)
    order by create_time desc
getFavorites
===
    select * from question where is_deleted=0
    and id in(select question_id from  favorites_question where is_deleted=0 and user_id=#userId#
    and type=#type# 
    )
    order by create_time desc
getFavoritesCount
===
    select count(*) from question where is_deleted=0
    and id in(select question_id from  favorites_question where is_deleted=0 and user_id=#userId#
    @if(isNotEmpty(type)){
    and type=#type# 
    @}
    )
 
deleteFavorites
===
    delete from favorites_question where
    user_id=#userId# 
    and question_id=#questionId# 
  
getCorrectQuestion
===
    SELECT * from favorites_question WHERE is_deleted=0 and  user_id=#userId# and question_id=#questionId#
