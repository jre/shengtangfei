outClass
===
    select * from kick_out where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    order by create_time desc  