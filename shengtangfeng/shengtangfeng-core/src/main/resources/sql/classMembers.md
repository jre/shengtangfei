
pageQuery
===
    select #page("*")# from class_members  where is_deleted=0 and
    #use("condition")#
    order by create_time desc

getTeachers
===
    select cm.*,u.head_image from class_members cm inner join user u on u.id = cm.user_id
    where cm.type in (2,3) and cm.is_deleted = 0 and cm.class_id = #classId#
    order by cm.type desc
   
getStudents
===
    select cm.*,u.head_image from class_members cm inner join user u on u.id = cm.user_id
    where cm.type = 1 and cm.is_deleted = 0 and cm.class_id = #classId#
    order by cm.nickname asc,cm.id asc


sample
===
* 注释###

    select #use("cols")# from class_members  where  #use("condition")#

cols
===
	id,class_id,user_id,nick_name,type,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,class_id=#classId#,user_id=#userId#,nick_name=#nickName#,type=#type#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(classId)){
     and class_id=#classId#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(nickName)){
     and nick_name=#nickName#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    