
pageQuery
===
    select #page("*")# from group_members  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from group_members  where  #use("condition")#

cols
===
	id,group_chat_id,user_id,nick_name,type,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,group_chat_id=#groupChatId#,user_id=#userId#,nick_name=#nickName#,type=#type#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(groupChatId)){
     and group_chat_id=#groupChatId#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(nickName)){
     and nick_name=#nickName#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
getAllGroupMembers
===
     SELECT g.* from group_members g LEFT JOIN `user` u  on g.user_id=u.id where g.is_deleted=0
     @if(!isEmpty(groupId)){
         and g.group_chat_id=#groupId#
     @}
     @if(!isEmpty(keyword)){
          and (u.account like #'%'+keyword+'%'# or u.name like #'%'+keyword+'%'#)
     @}
     ORDER BY g.nick_name desc,g.type desc 
    
    