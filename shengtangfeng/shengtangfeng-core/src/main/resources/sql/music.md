
pageQuery
===
    select #page("*")# from music  where is_deleted=0 and
    #use("condition")#
    order by click_num desc

pageCollection
===
    select #page("*")# from music m inner join music_collection c on c.music_id = m.id where m.is_deleted=0 and c.is_deleted=0
    @if(!isEmpty(userId)){
        and c.uid = #userId#
    @}
    order by click_num desc

sample
===
* 注释###

    select #use("cols")# from music  where  #use("condition")#

cols
===
	id,category_id,name,singer,image,duration,content,status,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,category_id=#categoryId#,name=#name#,singer=#singer#,image=#image#,duration=#duration#,content=#content#,status=#status#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(keyword)){
     and (name like #'%'+keyword+'%'# or id = #keyword#)
    @}
    @if(!isEmpty(name)){
     and name like #'%'+name+'%'#
    @}
    @if(!isEmpty(singer)){
     and singer=#singer#
    @}
    @if(!isEmpty(singerId)){
     and singer_id=#singerId#
    @}
    @if(!isEmpty(image)){
     and image=#image#
    @}
    @if(!isEmpty(duration)){
     and duration=#duration#
    @}
    @if(!isEmpty(content)){
     and content=#content#
    @}
    @if(!isEmpty(isHot)){
     and is_hot=#isHot#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
pageQuery1
===
    select #page("*")# from music  where is_deleted=0 
    @if(!isEmpty(keyword)){
     and (name like #'%'+keyword+'%'# or id like #'%'+keyword+'%'#)
    @}
    @if(!isEmpty(singerId)){
     and singer_id=#singerId#
    @}
    @if(!isEmpty(startTime) && !isEmpty(endTime)){
     and create_time>#startTime# and create_time<#endTime#
    @}
    @if(!isEmpty(type) && type==1){
     and  to_days(create_time) = to_days(now())
    @}
    @if(!isEmpty(type) && type==2){
     and TO_DAYS( NOW( )) - TO_DAYS(create_time) =1
    @}
    @if(!isEmpty(type) && type==3){
     and YEARWEEK(date_format(create_time,'%Y-%m-%d'))=YEARWEEK(now())
    @}
    @if(!isEmpty(type) && type==4){
     and YEARWEEK(date_format(create_time,'%Y-%m-%d'))=YEARWEEK(now())-1
    @}
    @if(!isEmpty(type) && type==5){
     and DATE_FORMAT(create_time,'%Y%m')=DATE_FORMAT(CURDATE(),'%Y%m')
    @}
    @if(!isEmpty(type) && type==6){
     and PERIOD_DIFF(date_format(now(),'%Y%m'),date_format(create_time,'%Y%m'))=1
    @}
    order by sort asc    
    