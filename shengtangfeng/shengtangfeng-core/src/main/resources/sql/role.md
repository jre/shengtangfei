findByUserId
===
	select r.* from system_user_role ur left join role r on r.id=ur.role_id where 1=1
	@if(systemUserId!=null){
	and ur.system_user_id=#systemUserId#
	@}else{
	and 1=2
	@}

findSystemRoleByNameAndIsSystemRole
===
	select * from system_user_role  where 1=1
	@if(name!=null){
	and name=#name#
	@}
	@if(isSystemRole!=null){
	and is_system_role=#isSystemRole#
	@}

selectRole
===
	select r.* from system_user_role ur left join role r on ur.role_id=r.id where ur.system_user_id=#systemUserId#

pageQuery
===
	select #page()# from role 
	where 
	is_deleted=0 
	@if(name!=null && name != ''){
        and name like #'%'+name+'%'#
    @}
	order by id

findSysRole
===
	select * from role where is_deleted=0 and is_system_role=1

deleteRolepermission
===
    delete from role_permission where role_id=#roleId#
insertRolePermission
=== 
    insert into role_permission (role_id,permission_id) values (#roleId#,#permissionId#)