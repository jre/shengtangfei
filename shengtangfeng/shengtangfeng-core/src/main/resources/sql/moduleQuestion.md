pageQuery
===
	select #page("*")# from module_question where is_deleted=0 
	and
    #use("condition")#
	order by create_time desc
deleteByQuestionAndModuleId
===
    delete from module_question 
    where 
    @if(isNotEmpty(moduleId)){
        module_id = #moduleId#
    @}
    and question_id in (
    @for(id in questionIdList) {
        #id#  #text(idLP.last?"":"," )#
    @}
    )	
sample
===
* 注释###

    select #use("cols")# from module_question  where  #use("condition")#

cols
===
	id,module_id,question_id,is_deleted,create_by,update_by,create_time,update_time

updateSample
===
    
	id=#id#,module_id=#moduleId#,question_id=#questionId#,is_deleted=#isDeleted#,create_by=#createBy#,update_by=#updateBy#,create_time=#createTime#,update_time=#updateTime#
getMaxSequence
===

    select max(sort) from module_question t1, question t2  where t1.question_id = t2.id and t2.type = #type#
    @if(!isEmpty(moduleId)){
       and module_id = #moduleId#
    @}    
getMaxSequenceC
===

    select max(sort) from module_question where 1 = 1
    @if(!isEmpty(moduleId)){
       and module_id = #moduleId#
    @}       
searchUpOne
===
    select * from module_question where sort =(select max(sort) from module_question  t1, question t2 where t1.question_id = t2.id and t2.type = #type#  and module_id=#moduleId# and sort< #sort1# )and module_id=#moduleId#
searchOne
===
    select * from module_question where sort=(select min(sort) from module_question t1, question t2  where t1.question_id = t2.id and t2.type = #type# and
     module_id=#moduleId#) and module_id=#moduleId#   
searchDownOne
===
    select * from module_question where sort =(select min(sort) from module_question t1, question t2  where t1.question_id = t2.id and t2.type = #type# and 
    sort> #sort1# and module_id=#moduleId#) and module_id=#moduleId# 
searchDown
===
    select * from module_question where sort=(select max(sort) from module_question t1, question t2  where t1.question_id = t2.id and t2.type = #type# and
     module_id=#moduleId#) and module_id=#moduleId#       
findByModuleId
===
    select * from module_question where 1=1
    @if(!isEmpty(moduleId)){
           and module_id = #moduleId#
        @}   
    @if(!isEmpty(questionId)){
           and question_id = #questionId#
        @}   
update
===
    update module_question set  sort = #sort# where  1=1
     @if(!isEmpty(moduleId)){
               and module_id = #moduleId#
            @}   
        @if(!isEmpty(questionId)){
               and question_id = #questionId#
            @}                    
condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(moduleId)){
     and module_id=#moduleId#
    @}
    @if(!isEmpty(questionId)){
     and question_id=#questionId#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
        