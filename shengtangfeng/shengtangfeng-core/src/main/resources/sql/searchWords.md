getList
===
    select * from search_words where is_deleted=0
     @if(!isEmpty(userId)){
         and user_id=#userId#
        @}
     @if(!isEmpty(type)){
         and type=#type#
        @}
    order by create_time asc
    

sample
===
* 注释###

    select #use("cols")# from search_words  where  #use("condition")#

cols
===
	id,keyword,keyword_type,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,keyword=#keyword#,keyword_type=#keywordType#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(keyword)){
     and keyword=#keyword#
    @}
    @if(!isEmpty(keywordType)){
     and keyword_type=#keywordType#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
       
