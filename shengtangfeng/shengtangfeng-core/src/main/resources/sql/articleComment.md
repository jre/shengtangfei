pageQuery
===
	select #page("*")# from article_comment 
	where 1 = 1
    @if(status!=null){
        and status=#status#
    @}
    @if(userId!=null){
        and user_id=#userId#
    @}
    @if(isNotEmpty(userName) && userName != ''){
        and user_name like #'%'+userName+'%'#
    @}
    @if(isNotEmpty(startTime) && startTime != ''){
        and create_time > #startTime#
    @}
    @if(isNotEmpty(endTime) && endTime != ''){
        and create_time < #endTime#
    @}
	order by create_time desc

findByArticleId
===
     select * from article_comment  where  is_deleted=0
        @if(articleId!=null){
        and article_id=#articleId#
        @}
        @if(status!=null){
        and status=#status#
        @}
        @if(userId!=null){
        and user_id = #userId#
        @}
        order by create_time desc   
findByArticleIdNotMine
===
    	select * from article_comment  where  is_deleted=0
    	@if(articleId!=null){
    	and article_id=#articleId#
    	@}
    	@if(status!=null){
    	and status=#status#
    	@}
    	@if(userId!=null){
    	and user_id <> #userId#
    	@}
    	order by create_time desc
getTodayCount
===
    select count(id) from article_comment 
    where is_deleted=0 
    and to_days(create_time) = to_days(now())
    and user_id = #userId#
    
pageGetComment
===
	select #page("artc.id as comment_id,artc.article_id as object_id ,artc.user_id as user_id, artc.user_icon as user_icon, artc.user_name as user_name,artc.content as content,artc.`status` as `status`,0 as type ,1 as comment_type,artc.create_time as create_time")# 
	from article_comment AS artc where is_deleted=0 and status=0
     @if(isNotEmpty(content)){
         and (user_id=#content# or user_name like #'%'+content+'%'# or content like #'%'+content+'%'#)
     @}
     order by create_time desc 

