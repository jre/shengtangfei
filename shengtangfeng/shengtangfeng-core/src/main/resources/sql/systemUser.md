getsystem_userInfo
===
	select * from system_user where  is_deleted = 0
	@if(!isEmpty(categoryType)){
	 and category_type=#categoryType#
	@}
getsystem_userInfoList
===
	select * from system_user where  is_deleted = 0
getHeadquartersInfo
===
	select * from system_user where is_deleted = 0

findByMobile
===
	select * from system_user where 1=1
	@if(mobile!=null){
	and mobile=#mobile#
	@}else{
	and 1=2
	@}

findByLoginName
===
	select * from system_user where 1=1
	@if(loginName!=null){
	and login_name=#loginName#
	@}else{
	and 1=2
	@}
findUserAndRole
===
	select * from system_user where is_deleted=0
	@if(id!=null){
	and id=#id#
	@}
	@orm.many({"id":"systemUserId"},"role.selectRole","Role");	
	
pageQuery
===
	select #page("*")# from system_user where is_deleted = 0
	@if(!isEmpty(name)){
	 and name like #'%'+name+'%'#
	@}
	@if(!isEmpty(loginName)){
	 and login_name like #'%'+loginName+'%'#
	@}
	@if(!isEmpty(departmentId)){
	 and department_id=#departmentId#
	@}
	@if(!isEmpty(mobile)){
	 and mobile=#mobile#
	@}
	@if(!isEmpty(categoryType)){
	 and category_type=#categoryType#
	@}
	@orm.many({"id":"systemUserId"},"role.selectRole","Role");	
	
pageSystemUser
===
    select #page("t.id,t.name,t.login_name,t.mobile,t.head_image,t.employee_number,t.description,t2.name as roleName, t2.id as roleId")# from system_user t 
    left join system_user_role t1 on t.id=t1.system_user_id 
    left join role t2 on t1.role_id=t2.id 
    where 1 = 1
    @if(!isEmpty(loginName)){
     and login_name like #'%'+loginName+'%'#
    @}
    @if(!isEmpty(mobile)){
             and mobile like #'%'+mobile+'%'#
    @}	 
        
findAll
===
    select * from system_user where is_deleted = 0 and type = 1
findCategoryType
===   
	select * from system_user where is_deleted = 0 and type = 1
	@if(!isEmpty(categoryType)){
	 and category_type=#categoryType#
	@}
	
deleteUserRole
===
	delete from system_user_role where system_user_id=#userId#


findByEmployeeNumberOrName
===
    select * from system_user where is_deleted = 0 
    @if(!isEmpty(assistantNo)){
     and employee_number=#assistantNo#
    @}
    @if(!isEmpty(assistantName)){
     and name=#assistantName#
    @}


insertUserRole
===
    insert into system_user_role (system_user_id,role_id) values (#systemUserId#,#roleId#)
	



    
    