pageIncorrect
===
    select #page("*")# from question where is_deleted=0 
    and id in(select question_id from  incorrect_question where user_id=#userId#)
    order by create_time desc
getIncorrect
===
    select * from question where is_deleted=0
    and id in(select question_id from  incorrect_question where is_deleted=0 and user_id=#userId#
    @if(isNotEmpty(type)){
    and type=#type# 
    @}
    )
    order by create_time desc
getIncorrectCount
===
    select count(*) from question where is_deleted=0
    and id in(select question_id from  incorrect_question where is_deleted=0 and user_id=#userId#
    @if(isNotEmpty(type)){
    and type=#type# 
    @}
    )
 
deleteIncorrect
===
    delete from incorrect_question where
    user_id=#userId# 
    and question_id=#questionId# 

removeIncorrectQuestionType
===
    delete from incorrect_question where
    user_id=#userId# 
    and type in (
    @for(type in types){
        #type#  #text(typeLP.last?"":"," )#
    @}
    )
selfUpQuestion
===
    select #page("*")# from incorrect_question where is_deleted=0 and type=2
    @if(isNotEmpty(keywords)){
        and id in(select id from  user where id=#keywords# or name like #'%'+keywords+'%'#)
    @}
    @if(isNotEmpty(userId)){
        and user_id = #userId#
    @}
    @if(isNotEmpty(status)){
        and status = #status#
    @}
    @if(isNotEmpty(dateType) && (dateType==0 || dateType==3)){
        and create_time > #startTime# and create_time < #endTime#
    @}
    @if(isNotEmpty(dateType) && dateType==1){
        and YEARWEEK(date_format(create_time,'%Y-%m-%d')) = YEARWEEK(now())
    @}
    @if(isNotEmpty(dateType) && dateType==2){
        and DATE_FORMAT(create_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )    
    @}
    @if(isNotEmpty(subjectId)){
        and subject_id = #subjectId#
    @}
    order by create_time desc
auditSelfQuestion
===
    select #page("*")# from incorrect_question where is_deleted=0 and type=2
    @if(isNotEmpty(status)){
        and status = #status#
    @}
    @if(isNotEmpty(user)){
        and user_id in (select id from  `user` where id=#user# or name like #'%'+user+'%'#)
    @}
    @if(isNotEmpty(keywords)){
        and content like #'%'+keywords+'%'#
    @}
    @if(isNotEmpty(startTime)){
        and create_time > #startTime#
    @}
    @if(isNotEmpty(endTime)){
        and create_time < #endTime#
    @}
    order by create_time desc