
pageQuery
===
    select #page("*")# from inform  where is_deleted=0 and
    #use("condition")#
    order by create_time desc

pageQueryByClass
===
    select #page("i.*")# from inform i inner join class_inform c on c.inform_id = i.id
    where c.class_id = #classId# order by i.create_time desc

sample
===
* 注释###

    select #use("cols")# from inform  where  #use("condition")#

cols
===
	id,content,images,create_time,create_by,update_time,update_by,is_deleted,status

updateSample
===
    
	id=#id#,content=#content#,images=#images#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#,status=#status#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(content)){
     and content like #"%"+content+"%"#
    @}
    @if(!isEmpty(images)){
     and images=#images#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    
getClassInformById
===
    select  #page("i.*")#from inform i
    LEFT JOIN  class_inform ci on i.id=ci.inform_id
    where ci.is_deleted=0 and i.is_deleted=0 and i.status=1 
    @if(!isEmpty(classId)){
     and ci.class_id=#classId#
    @}
    order by i.create_time desc
    
getMyInform
===
    select  #page("*")#from inform where is_deleted=0 and status=1
    @if(!isEmpty(userId)){
     and create_by=#userId#
    @}
    order by create_time desc
    
getInformPage
===
    SELECT #page("i.*")# FROM `inform` i LEFT JOIN user u on i.create_by=u.id where i.is_deleted=0
    @if(!isEmpty(status)){
     and i.status=#status#
    @}
    @if(!isEmpty(userId)){
     and u.id=#userId#
    @}
    @if(!isEmpty(content)){
     and i.content like #'%'+content+'%'#
    @}
    @if(!isEmpty(userNickname)){
     and u.name like #'%'+userNickname+'%'#
    @}
    order by i.create_time desc       
   
        
        
        
    