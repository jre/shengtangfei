
pageQuery
===
    select #page("*")# from apply_for  where is_deleted=0 and
    #use("condition")#
    order by create_time desc

getNewFriend
===
    select * from apply_for  where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(relevanceId)){
     and relevance_id=#relevanceId#
    @}
    @if(!isEmpty(applyId)){
     and apply_id=#applyId#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    order by status asc,create_time desc

getNewGroup
===
    select * from apply_for  where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(relevanceId)){
     and relevance_id=#relevanceId#
    @}
    @if(!isEmpty(applyId)){
     and apply_id=#applyId#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    order by create_time desc  
    
getNewClass
===
    select * from apply_for  where is_deleted=0 and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(create_time)
    @if(!isEmpty(relevanceId)){
     and relevance_id=#relevanceId#
    @}
    @if(!isEmpty(applyId)){
     and apply_id=#applyId#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    order by create_time desc      

sample
===
* 注释###

    select #use("cols")# from apply_for  where  #use("condition")#

cols
===
	id,relevance_id,apply_id,apply_name,apply_remark,create_time,create_by,update_time,update_by,is_deleted,type,status

updateSample
===
	id=#id#,relevance_id=#relevanceId#,apply_id=#applyId#,apply_name=#applyName#,apply_remark=#applyRemark#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#,type=#type#,status=#status#

getNewFriendNum
===
    select count(*) from apply_for where is_deleted=0 and type=0 and id not in(select apply_table_id from apply_for_deleted where is_deleted=0 and user_id=#userId#)
    @if(!isEmpty(userId)){
       and (apply_id=#userId# or relevance_id=#userId#)
    @}

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(relevanceId)){
     and relevance_id=#relevanceId#
    @}
    @if(!isEmpty(applyId)){
     and apply_id=#applyId#
    @}
    @if(!isEmpty(applyName)){
     and apply_name=#applyName#
    @}
    @if(!isEmpty(applyRemark)){
     and apply_remark=#applyRemark#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
    
        
    