pageQuery
===
	select #page("*")# from module_record where is_deleted=0

getRank
===
    SELECT t.*, \@rownum := \@rownum + 1 AS user_rank
    FROM (SELECT \@rownum := 0) r, (
    select user_name,user_icon,score as user_score from module_record where is_deleted=0
    and module_id=#moduleId# order by score desc,time asc limit 0,50
    ) AS t;
getUserRank
===
    SELECT b.* FROM
    (
    SELECT t.*, \@rownum := \@rownum + 1 AS user_rank
    FROM (SELECT \@rownum := 0) r, (
    select user_id,user_name,user_icon,score as user_score from module_record where is_deleted=0
    and module_id=#moduleId# order by score desc,time asc
    ) AS t
    ) AS b WHERE b.user_id = #userId#;
getRankAll
===
    SELECT t.*, \@rownum := \@rownum + 1 AS user_rank
    FROM (SELECT \@rownum := 0) r, (
    select user_name,user_icon,user_score from all_rank 
    order by user_score desc,time asc limit 0,50
    ) AS t;
getUserRankAll
===
    SELECT b.* FROM
    (
    SELECT t.*, \@rownum := \@rownum + 1 AS user_rank
    FROM (SELECT \@rownum := 0) r, (
    select user_id,user_name,user_icon,user_score from all_rank
    order by user_score desc,time asc
    ) AS t
    ) AS b WHERE b.user_id = #userId#;
    
    
    