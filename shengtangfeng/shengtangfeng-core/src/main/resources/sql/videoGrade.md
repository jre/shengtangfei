
pageQuery
===
    select #page("v.*")# from video_grade v LEFT JOIN `user` u on v.user_id=u.id  where v.is_deleted=0
    @if(!isEmpty(userId)){
         and v.create_by=#userId#
    @}
    @if(!isEmpty(status)){
         and v.status=#status#
    @}
    @if(!isEmpty(type)){
         and v.type=#type#
    @}
    @if(!isEmpty(remark)){
         and (u.name like #'%'+remark+'%'# or v.remark like #'%'+remark+'%'# or  v.user_id=#remark#)
    @}
    order by v.create_time desc



sample
===
* 注释###

    select #use("cols")# from video_grade  where  #use("condition")#

cols
===
	id,user_id,total_score,knowledge_score,image_score,language_score,remark,create_time,create_by,update_time,update_by,type,is_deleted

updateSample
===
    
	id=#id#,user_id=#userId#,total_score=#totalScore#,knowledge_score=#knowledgeScore#,image_score=#imageScore#,language_score=#languageScore#,remark=#remark#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,type=#type#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(totalScore)){
     and total_score=#totalScore#
    @}
    @if(!isEmpty(knowledgeScore)){
     and knowledge_score=#knowledgeScore#
    @}
    @if(!isEmpty(imageScore)){
     and image_score=#imageScore#
    @}
    @if(!isEmpty(languageScore)){
     and language_score=#languageScore#
    @}
    @if(!isEmpty(remark)){
     and remark like #'%'+remark+'%'#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(videoId)){
     and video_id=#videoId#
    @}
    @if(!isEmpty(status)){
     and status=#status#
    @}
avgScore
===
    select avg(total_score) from video_grade where is_deleted = 0 and video_id = #videoId#
    
findUserGrade
===
    select * from video_grade where is_deleted = 0 and user_id = #userId# and video_id = #videoId# limit 1
    
