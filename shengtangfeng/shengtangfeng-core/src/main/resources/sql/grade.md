
pageQuery
===
    select #page("*")# from grade  where is_deleted=0
    #use("condition")#
    order by sort asc,id desc
getList
===
    select * from grade  where is_deleted=0
    #use("condition")#
    order by sort asc,id desc

getListBySubjectId
===
    select * from grade g right join grade_subject gs 
    on g.id=gs.grade_id and gs.subject_id=#subjectId#  where g.is_deleted=0 
    order by sort asc

sample
===
* 注释###

    select #use("cols")# from grade  where  #use("condition")#

cols
===
	id,create_time,create_by,update_time,update_by,name,is_deleted

updateSample
===
    
	id=#id#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,name=#name#,is_deleted=#isDeleted#

condition
===

    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(name!=null&&name!=''){
     and name like #'%'+name+'%'#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    
getGrade
===
    select grade.* from grade LEFT JOIN question_grade on question_grade.grade_id=grade.id where grade.is_deleted=0    
     @if(!isEmpty(questionId)){
       and question_grade.question_id=#questionId#
     @}