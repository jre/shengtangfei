
pageQuery
===
    select #page("*")# from class_and  where is_deleted=0
     @if(!isEmpty(id)){
         and id=#id#
        @}
        @if(!isEmpty(proviceId)){
         and provice_id=#proviceId#
        @}
        @if(!isEmpty(cityId)){
         and city_id=#cityId#
        @}
        @if(!isEmpty(countId)){
         and count_id=#countId#
        @}
        @if(!isEmpty(schoolId)){
         and school_id=#schoolId#
        @}
        @if(!isEmpty(name)){
         and (name like #'%'+name+'%'# or class_num like #'%'+name+'%'#)
        @}
        @if(!isEmpty(classNum)){
         and class_num like #'%'+classNum+'%'#
        @}
        @if(!isEmpty(gradeDivision)){
         and grade_division=#gradeDivision#
        @}
        @if(!isEmpty(schoolYear)){
         and school_year=#schoolYear#
        @}
        @if(!isEmpty(createTime)){
         and create_time=#createTime#
        @}
        @if(!isEmpty(createBy)){
         and create_by=#createBy#
        @}
        @if(!isEmpty(updateTime)){
         and update_time=#updateTime#
        @}
        @if(!isEmpty(updateBy)){
         and update_by=#updateBy#
        @}
        @if(!isEmpty(isDeleted)){
         and is_deleted=#isDeleted#
        @}
    order by create_time desc
    
getClassByUid
===
    select c.*,m.user_id,m.class_id from class_and c 
    left join class_members m on m.class_id = c.id 
    where c.is_deleted = 0 
    and m.user_id = #userId# and m.is_deleted=0 order by c.school_year desc
    
getTeachers
===
    select u.* from user u inner join class_members m on m.user_id = u.id
    where m.type = 2 and m.is_delete = 0
    and m.class_id = #classId# order by m.create_time desc

getStudents
===
    select u.* from user u inner join class_members m on m.user_id = u.id
    where m.type = 1 and m.is_delete = 0
    and m.class_id = #classId# order by m.create_time desc

sample
===
* 注释###

    select #use("cols")# from class_and  where  #use("condition")#

cols
===
	id,provice_id,city_id,count_id,school_id,name,grade_division,school_year,create_time,create_by,update_time,update_by,is_deleted

updateSample
===
    
	id=#id#,provice_id=#proviceId#,city_id=#cityId#,count_id=#countId#,school_id=#schoolId#,name=#name#,grade_division=#gradeDivision#,school_year=#schoolYear#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#

condition
===
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(proviceId)){
     and provice_id=#proviceId#
    @}
    @if(!isEmpty(cityId)){
     and city_id=#cityId#
    @}
    @if(!isEmpty(countId)){
     and count_id=#countId#
    @}
    @if(!isEmpty(schoolId)){
     and school_id=#schoolId#
    @}
    @if(!isEmpty(name)){
     and name like #'%'+name+'%'#
    @}
    @if(!isEmpty(gradeDivision)){
     and grade_division=#gradeDivision#
    @}
    @if(!isEmpty(schoolYear)){
     and school_year=#schoolYear#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    