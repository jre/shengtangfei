
pageQuery
===
    select #page("*")# from accolade  where is_deleted=0 and
    #use("condition")#
    order by create_time desc
    
    
    
getAccolade
===
    select * from accolade 
    where user_id = #userId#
    and article_id = #articleId#
    and type = #type#

deleteAccolade
===

    delete from accolade 
    where user_id = #userId#
    and article_id = #articleId#
    and type = #type#
    


sample
===
* 注释###

    select #use("cols")# from accolade  where  #use("condition")#

cols
===
	id,create_time,create_by,update_time,update_by,user_id,is_deleted,article_id,type

updateSample
===
    
	id=#id#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,user_id=#userId#,is_deleted=#isDeleted#,article_id=#articleId#,type=#type#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(userId)){
     and user_id=#userId#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(articleId)){
     and article_id=#articleId#
    @}
    @if(!isEmpty(type)){
     and type=#type#
    @}
    