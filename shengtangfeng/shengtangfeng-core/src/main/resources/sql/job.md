
pageQuery
===
    select #page("*")# from job  where is_deleted=0 and
    #use("condition")#
    order by create_time desc



sample
===
* 注释###

    select #use("cols")# from job  where  #use("condition")#

cols
===
	id,class_id,subject_id,name,create_time,create_by,update_time,update_by,is_deleted,submit,deadline

updateSample
===
    
	id=#id#,subject_id=#subjectId#,name=#name#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,is_deleted=#isDeleted#,submit=#submit#,deadline=#deadline#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(subjectId)){
     and subject_id=#subjectId#
    @}
    @if(!isEmpty(name)){
     and name=#name#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    @if(!isEmpty(submit)){
     and submit=#submit#
    @}
    @if(!isEmpty(deadline)){
     and deadline=#deadline#
    @}
    
getJobListById
===
    select #page("job.*")# from job LEFT JOIN class_job cj on job.id=cj.job_id LEFT JOIN `subject` s on job.subject_id=s.id where job.is_deleted=0
    @if(!isEmpty(pushTime)){
        @if(pushTime==1){
            and to_days(job.create_time) = to_days(now())
        @}else if(pushTime==2){
            and YEARWEEK(date_format(job.create_time,'%Y-%m-%d')) = YEARWEEK(now())
        @}else if(pushTime==3){
            and DATE_FORMAT(job.create_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )
        @}else if(pushTime==4){
            and job.create_time>#pushTime#      
        @}else if(pushTime==5){
            and job.create_time>#startTime# and job.create_time<#finishTime#   
        @}
    @}
    @if(!isEmpty(entTime)){
        @if(entTime==1){
            and to_days(job.deadline) = to_days(now())
        @}else if(entTime==2){
            and YEARWEEK(date_format(job.deadline,'%Y-%m-%d')) = YEARWEEK(now())
        @}else if(entTime==3){
            and DATE_FORMAT(job.deadline, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )
        @}else if(entTime==4){
            and job.deadline<now()
        @}
    @}
    @if(!isEmpty(classId)){
     and cj.class_id=#classId#
    @}
    @if(!isEmpty(subjectName)){
     and s.`name`=#subjectName#
    @}
    @if(!isEmpty(isMy) && isMy==1){
     and job.create_by=#userId#
    @}
    
    @if(!isEmpty(notComplete) && notComplete==1){
        and job.id in (select job_id from user_job where status=0 and is_deleted=0 and user_id=#userId# and class_id=#classId#)
    @}
    @if(!isEmpty(sort) && sort==0){
     order by job.create_time asc
    @}
    @if(!isEmpty(sort) && sort==1){
     order by job.create_time desc
    @}
    
    
getLast3Jobs
===
     select job.*from job LEFT JOIN class_job cj on job.id=cj.job_id where job.is_deleted=0  and cj.class_id=#classId# order by job.create_time desc
        
    