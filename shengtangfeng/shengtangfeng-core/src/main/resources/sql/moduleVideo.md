pageModuleQuery
===
    select #page("t1.*")# from module_video t1 
        where 
            t1.module_id = 0
            @if(isNotEmpty(title)){
            and t1.title like #'%'+title+'%'#
            @}
            @if(isNotEmpty(type)){
                and t1.type = #type#
            @}
            and t1.is_deleted = 0
        order by t1.id desc
pageQueryByModule   
===
    select #page("q.* ")# from module_video q 
    where q.module_id=#moduleId# and q.is_deleted=0
        @if(isNotEmpty(title)){
            and q.title like #'%'+title+'%'#
        @}
        order by q.sort,q.id