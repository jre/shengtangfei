
pageQuery
===
    select #page("*")# from subject  where is_deleted=0 and
    #use("condition")#
    order by sort asc
    @orm.many({"id":"subjectId"}, "subject.selectGrades", "Grade", {"alias":"grades"});

selectGrades
===
    select * from grade where 
    id in (select grade_id from grade_subject where subject_id=#subjectId#) and is_deleted = 0
getList
===
    select * from subject  where is_deleted=0 and
    #use("condition")#
     order by sort asc
getIncorrectSubject
===
    select * from subject s where is_deleted=0 and id in
    (select distinct subject_id from incorrect_question where user_id=#userId# 
    @if(type==0){
        and question_id <> 0
    @}
    @if(type==1){
        and question_id = 0
    @}
    )
     order by sort asc
getFavoritesSubject
===
    select * from subject s where is_deleted=0 and id in
    (select distinct subject_id from favorites_question where user_id=#userId#)
     order by sort asc
getListByGradeId
===
    select * from subject s right join grade_subject gs 
    on s.id=gs.subject_id and gs.grade_id=#gradeId#  where s.is_deleted=0 
     order by sort asc
getListByGradeIds
===
    select DISTINCT s.* from subject s right join grade_subject gs 
    on s.id=gs.subject_id 
    and gs.grade_id in (
    @for(id in gradeIds){
        #id#  #text(idLP.last?"":"," )#
    @}
    )
    where s.is_deleted=0 
     order by sort asc

sample
===
* 注释###

    select #use("cols")# from subject  where  #use("condition")#

cols
===
	id,create_time,create_by,update_time,update_by,name,is_deleted

updateSample
===
    
	id=#id#,create_time=#createTime#,create_by=#createBy#,update_time=#updateTime#,update_by=#updateBy#,name=#name#,is_deleted=#isDeleted#

condition
===

    1 = 1  
    @if(!isEmpty(id)){
     and id=#id#
    @}
    @if(!isEmpty(createTime)){
     and create_time=#createTime#
    @}
    @if(!isEmpty(createBy)){
     and create_by=#createBy#
    @}
    @if(!isEmpty(updateTime)){
     and update_time=#updateTime#
    @}
    @if(!isEmpty(updateBy)){
     and update_by=#updateBy#
    @}
    @if(name!=null&&name!=''){
     and name like #'%'+name+'%'#
    @}
    @if(!isEmpty(isDeleted)){
     and is_deleted=#isDeleted#
    @}
    