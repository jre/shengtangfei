pageQuery
===
	select #page("*")# from course where is_deleted=0
	@if(isNotEmpty(title)){
	and title like #'%'+title+'%'#
	@}
	@if(isNotEmpty(recommend)){
	and recommend=#recommend#
	@}
	@if(isNotEmpty(status)){
	and status=#status#
	@}
	@if(isNotEmpty(categoryId)){
	and category_id=#categoryId#
	@}
	@if(recommend!=null){
	and recommend=#recommend#
	@}
	order by sort asc, id desc


findAll
===
	select * from course where is_deleted = 0
	order by sort asc, id desc
	
getAmountByDate
===
    SELECT
    	date_format( create_time, '%Y-%m-%d' ) as date,
    	sum( actual_price ) as amount
    FROM
    	course_order 
    WHERE
        status = 1 and
    	date_format( create_time, '%Y-%m-%d' ) IN ( 
    	@for(label in labels) {
            #label#  #text(labelLP.last?"":"," )#
        @}
    	) 
    GROUP BY
    	date_format( create_time, '%Y-%m-%d' ) 
    ORDER BY
    	date_format(
    	create_time,
    	'%Y-%m-%d') desc
autoUpdatePrice
===
    update course set price=original_price,preferential_end_time=null where price<>original_price and (preferential_end_time is null or preferential_end_time <= now())