pageQuery
===
	select #page("*")# from video_comment 
	where 1 = 1
    @if(status!=null){
        and status=#status#
    @}
    @if(userId!=null){
        and user_id=#userId#
    @}
    @if(isNotEmpty(userName) && userName != ''){
        and user_name like #'%'+userName+'%'#
    @}
    @if(isNotEmpty(startTime) && startTime != ''){
        and create_time > #startTime#
    @}
    @if(isNotEmpty(endTime) && endTime != ''){
        and create_time < #endTime#
    @}
	order by create_time desc

findByVideoId
===
    	select * from video_comment  where  is_deleted=0
    	@if(videoId!=null){
    	and video_id=#videoId#
    	@}
    	@if(status!=null){
    	and status=#status#
    	@}
    	@if(userId!=null){
        and user_id=#userId#
        @}
    	order by create_time desc		
findByVideoIdNotMine
===
    	select * from video_comment  where  is_deleted=0
    	@if(videoId!=null){
    	and video_id=#videoId#
    	@}
    	@if(status!=null){
    	and status=#status#
    	@}
    	@if(userId!=null){
      and user_id <> #userId#
      @}
    	order by create_time desc
getTodayCount
===
    select count(id) from video_comment 
    where is_deleted=0 
    and to_days(create_time) = to_days(now())
    and user_id = #userId#	
    
pageGetComment
===
    select #page("vidc.id as comment_id, vidc.video_id as object_id ,vidc.user_id as user_id, vidc.user_icon as user_icon, vidc.user_name as user_name,vidc.content as content,vidc.`status` as `status`,1 as type , 1 as comment_type,vidc.create_time as create_time")# 
    from video_comment AS vidc where is_deleted=0 and status=0
       @if(isNotEmpty(content)){
           and (user_id=#content# or user_name like #'%'+content+'%'# or content like #'%'+content+'%'#)
       @}
       order by create_time desc 