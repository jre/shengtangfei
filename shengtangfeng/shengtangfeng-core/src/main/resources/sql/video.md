pageQuery
===
    select #page("*")# from video where is_deleted=0
    @if(isNotEmpty(categoryId)){
    and category_id = #categoryId#
    @}
    @if(isNotEmpty(type)){
        and type = #type#
    @}
    @if(isNotEmpty(status)){
    and status = #status#
    @}
    @if(isNotEmpty(uploadType)){
    and upload_type = #uploadType#
    @}
    @if(isNotEmpty(createBy)){
    and create_by = #createBy#
    @}
    @if(isNotEmpty(publishId)){
    and publish_id = #publishId#
    @}
    @if(isNotEmpty(title) && title != ''){
    and (title like #'%'+title+'%'# or id like #'%'+title+'%'#)
    @}
    @if(isNotEmpty(publisher) && publisher != ''){
    and publisher like #'%'+publisher+'%'#
    @}
    @if(isNotEmpty(keyword) && keyword != ''){
    and title like #'%'+keyword+'%'# 
    @}
    @if(recommend!=null){
    and recommend=#recommend#
    @}
	-- 不查找用户拉黑的人
    @if(searchNoBlack!=null){
    and publish_id not in(select black_user_id from user_black_list where user_id=#searchNoBlack#)
    @}
    order by create_time desc
findClosely
===
    select * from video where is_deleted=0
    @if(isNotEmpty(categoryId)){
    and category_id = #categoryId#
    @}
    @if(isNotEmpty(status)){
    and status = #status#
    @}
    @if(isNotEmpty(id)){
    and id <> #id#
    @}
    order by id desc
    
    
findById
===    

    select t.*,t1.invitation_code as invitationCode from video t 
    left join user t1 on t.publish_id=t1.id 
    where t.is_deleted=0
    @if(!isEmpty(id)){
     and t.id = #id#
    @}
    
pageAttentionVideo
===

    select #page("a.*")# from video  a
    inner join attention a1 on a.publish_id = a1.focus_user_id
    where a.is_deleted =0
        and a.status = 1
    @if(!isEmpty(userId)){
      and a1.user_id = #userId#
     @}
    -- 不查找用户拉黑的人
    @if(searchNoBlack!=null){
    and publish_id not in(select black_user_id from user_black_list where user_id=#searchNoBlack#)
    @}
     order by a.create_time desc

pageFavoritesVideo
===
    select #page("*")# from video where is_deleted=0  and status=1
    and id in(select article_id from  favorites where user_id=#userId#
    and type=2
    )    
pageAccoladeVideo
===
    select #page("*")# from video where is_deleted=0  and status=1
    and id in(select article_id from  accolade where user_id=#userId#
    and type=2
    )  
    
pageToAuditVideo
===
    select #page("*")# from video where is_deleted=0 and status=0 and upload_type=1
        @if(isNotEmpty(categoryId)  && categoryId!=1){
        and category_id = #categoryId#
        @}
        @if(isNotEmpty(content)){
        and  (publish_id = #content# or publisher like #'%'+content+'%'# or id = #content# or title like #'%'+content+'%'#)
        @}
        @if(isNotEmpty(videoId)){
        and id = #videoId#
        @}
        @if(isNotEmpty(title)){
        and title like #'%'+title+'%'#
        @}  
        @if(isNotEmpty(type)){
        and type = #type#
        @}
        order by create_time desc
                
newCheck
===
    select *  from video where is_deleted=0 and status=0 and upload_type=1
        order by create_time desc         
 
        
getUserVideo
===
     select  #page("*")# from video where publish_id=#userId# and is_deleted=0 and status in (0,1,2)
     @if(isNotEmpty(type)){
        and type = #type#
     @}
     @if(isNotEmpty(keywords)){
        and (id = #keywords# or title like #'%'+keywords+'%'#)
     @}
     order by `status` asc ,publish_date desc
getLast4StudentVideo
===
    select v.* from video v inner join video_class c on c.video_id = v.id
    where v.is_deleted=0 and v.status = 1 and v.type = 1 
    and c.class_id = #classId#  order by c.create_time desc limit 4
    
pageClassTalkVideo
===
    select #page("v.*")# from video v left join video_class c on c.video_id = v.id
    where v.is_deleted = 0 and v.status = 1 and v.type=1 and (v.scope =0 or v.scope=2)  and c.class_id = #classId# order by v.create_time desc
    
    
getNewVideo
===
    select v.* from video v left join video_class c on c.video_id = v.id where v.`status`=1 and v.type=1 and (v.scope =0 or v.scope=2) and v.is_deleted=0
    @if(isNotEmpty(classId)){
    and c.class_id=#classId#
    @}  
    order by v.create_time desc
    
selectRecommendVideo
===
    select #page("*")# from video where is_deleted=0 and type=2 and status=1
      @if(isNotEmpty(userId)){
        and publish_id not in (select black_user_id from user_black_list where user_id = #userId# )
     @}  
 
    order by create_time desc
    
selectAttentionVideo
===
    select #page("*")# from video where is_deleted=0 and type=2 and status=1 and publish_id in (SELECT focus_user_id FROM `attention` where is_deleted=0 and user_id=#userId#)
    @if(isNotEmpty(userId)){
        and publish_id not in (select black_user_id from user_black_list where user_id = #userId# )
    @}
    order by create_time desc

selectLocalVideo
===
     select #page("*")# from video where is_deleted=0 and type=2 and status=1 and address like #'%'+cityName+'%'# 
        @if(isNotEmpty(userId)){
        and publish_id not in (select black_user_id from user_black_list where user_id = #userId# )
        @}

     order by create_time desc
     
updateOSSDNS
===
    update video set content=REPLACE(content,'https://shengtangfeng-public.oss-cn-huhehaote.aliyuncs.com','http://accelerate.boruankeji.net') where is_deleted=0  and type=2 and content like
    '%https://shengtangfeng-public.oss-cn-huhehaote.aliyuncs.com%'
    
updateOSSDNS1
===
    update video set content=REPLACE(content,'https://public.stftt.com','http://accelerate.boruankeji.net') where is_deleted=0  and type=2 and content like
    '%https://public.stftt.com%'    
     

