package com.boruan.shengtangfeng.core.service.impl;

import java.util.HashMap;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IApkDao;
import com.boruan.shengtangfeng.core.dao.IConfigDao;
import com.boruan.shengtangfeng.core.dao.IFeedbackDao;
import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.entity.JPushMessage;
import com.boruan.shengtangfeng.core.entity.Notice;
import com.boruan.shengtangfeng.core.service.IConfigService;
import com.boruan.shengtangfeng.core.utils.Consts;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.PushUtil;

import cn.jpush.api.JPushClient;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class ConfigService implements IConfigService {
	@Autowired
	private IConfigDao configDao;
	@Autowired
	private IApkDao apkDao;
	@Autowired
	private IFeedbackDao feedbackDao;
	@Autowired
	private JPushClient jPushClient;
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<Config> pageQuery, Config config) {
		pageQuery.setParas(config);
		configDao.pageQuery(pageQuery);
	}

	/**
	 * @author: 刘光强
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public Config findById(Long id) {
		return configDao.unique(id);
	}

	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(Config config) {
		if (config.getId() == null) {
			KeyHolder kh = configDao.insertReturnKey(config);
			config.setId(kh.getLong());
		} else {
			configDao.updateById(config);
		}
	}

	@Override
    public Config findConfigByKey(String key) {
        Config search=new Config();
        search.setKeyword(key);
        return configDao.templateOne(search);
    }

    @Override
    public Apk getApk(Long id) {
        return apkDao.single(id);
    }
    
	
}
