package com.boruan.shengtangfeng.core.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "advertise")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Advertise extends IdEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -7812662247157526784L;

    /**
     * 点击数
     **/
    private Integer clickCount;

    /**
     * 排序
     **/
    private Integer sort;

    /**
     * 上下线状态：0->下线；1->上线
     **/
    private Integer status;

    /**
     * 图片地址
     **/
    private String image;

    /**
     * 标题
     **/
    private String title;

    

    /**
     * 轮播位置：0->首页轮播  1->开屏广告
     **/
    private Integer position;

    /**
     * 链接类型 1 富文本  2 webview 3 文章详情  4 视频详情
     **/
    private Integer linkType;
    
    /**
     * 链接地址或者富文本内容
     **/
    private String content;
    /**
     * 发布人头像
     **/
    private String icon;
    /**
     * 发布人
     **/
    private String publisher;
    
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date endTime;

}