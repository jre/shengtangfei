package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.ClassAnd;
import com.boruan.shengtangfeng.core.vo.ClassAndVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClassAndVoMapper extends VoMapper<ClassAndVo, ClassAnd> {
    ClassAndVoMapper MAPPER = Mappers.getMapper(ClassAndVoMapper.class);
}
