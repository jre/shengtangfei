package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassInform;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IClassInformDao extends BaseMapper<ClassInform> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ClassInform> query);

}