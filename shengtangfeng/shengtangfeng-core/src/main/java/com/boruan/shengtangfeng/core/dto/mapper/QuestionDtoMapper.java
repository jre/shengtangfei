package com.boruan.shengtangfeng.core.dto.mapper;


import com.boruan.shengtangfeng.core.dto.QuestionDto;
import com.boruan.shengtangfeng.core.entity.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface QuestionDtoMapper extends DtoMapper<QuestionDto, Question> {
	QuestionDtoMapper MAPPER = Mappers.getMapper(QuestionDtoMapper.class);

	@Mappings({
			@Mapping(target = "content", expression = "java(null == dto.getOptionList() ? null : com.alibaba.fastjson.JSON.toJSON(dto.getOptionList()).toString())")})
	public Question toEntity(QuestionDto dto);

}
