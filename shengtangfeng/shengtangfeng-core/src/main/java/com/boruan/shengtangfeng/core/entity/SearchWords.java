package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="search_words")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SearchWords extends IdEntity {

    /**
     * 关键字 
     **/
     private String keyword ;
             
     /**
     * 类型  1 试题 2 文章 3 视频
     **/
     private Integer type ;
     /**
      * 用户ID
      **/
     private Long userId;


}