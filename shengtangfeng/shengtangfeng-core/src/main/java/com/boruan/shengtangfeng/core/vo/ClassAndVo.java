package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.dto.JobDto;
import com.boruan.shengtangfeng.core.dto.TeacherAndStudentDto;
import com.boruan.shengtangfeng.core.entity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.TailBean;

import java.util.List;


@Data
public class ClassAndVo extends TailBean {

    @ApiModelProperty(value="创建人id")
    private Long createId ;

    @ApiModelProperty(value="创建人姓名")
    private String createName ;

    @ApiModelProperty(value="我在班级的昵称")
    private String myNickname ;

    @ApiModelProperty(value="创建人头像")
    private String createImage ;

    @ApiModelProperty(value="学校id")
    private Long schoolId ;

    @ApiModelProperty(value="省")
    private String province ;

    @ApiModelProperty(value="市")
    private String city ;

    @ApiModelProperty(value="区")
    private String county;

    @ApiModelProperty(value="学校名称")
    private String schoolName ;


    @ApiModelProperty(value="班级id")
    private Long classId ;

    @ApiModelProperty(value="班级群Id")
    private String tencentId ;

    @ApiModelProperty(value="班级名称")
    private String className ;

    @ApiModelProperty(value="班级号")
    private String classNum ;

    @ApiModelProperty(value="学年")
    private String schoolYear ;

    @ApiModelProperty(value="年级")
    private String gradeName ;

    @ApiModelProperty(value="班级状态 0学籍中 1已毕业")
    private Integer classStatus ;

    @ApiModelProperty(value = "班级人数")
    private Long memberNum;

    @ApiModelProperty(value = "学生人数")
    private Long studentNum;

    @ApiModelProperty(value = "教师人数")
    private Long teacherNum;

    @ApiModelProperty(value = "学生列表")
    private List<ClassMembers> studentList;

    @ApiModelProperty(value = "教师列表")
    private List<ClassMembers> teacherList;

    @ApiModelProperty(value = "学生列表后台专用")
    private List<TeacherAndStudentDto> studentDtoList;

    @ApiModelProperty(value = "教师列表 后台专用")
    private List<TeacherAndStudentDto> teacherDtoList;

    @ApiModelProperty(value = "班级最新通知")
    private Inform LastNotice;

    @ApiModelProperty(value = "班级通知是否已读 0未读 1已读")
    private Integer informIsRead;

    @ApiModelProperty(value = "班级作业")
    private List<Job> classJob;

    @ApiModelProperty(value = "班级作业Dto")
    private List<JobDto> classJobDto;

    @ApiModelProperty(value = "讲一讲列表")
    private List<VideoVo> studentVideo;

    @ApiModelProperty(value = "是否加入")
    private Boolean isJoin;

}