package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IArticleCategoryDao;
import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import com.boruan.shengtangfeng.core.service.IArticleCategoryService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author KongXH
 * @title: ArticleCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1315:32
 */
@Service
@Transactional(readOnly = true)
public class ArticleCategoryService implements IArticleCategoryService {


    @Autowired
    private IArticleCategoryDao articleCategoryDao;

    /**
     * @description: 分页
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public void pageQuery(PageQuery<ArticleCategory> query) {

    }

    /**
     * @description: 获取全部
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public List<ArticleCategory> getList(ArticleCategory articleCategory) {
        return articleCategoryDao.getList(articleCategory);
    }


    /**
     * @description: 获取默认
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public List<ArticleCategory> getArticleCategoryDefault() {
        return articleCategoryDao.getArticleCategoryDefault();
    }
}
