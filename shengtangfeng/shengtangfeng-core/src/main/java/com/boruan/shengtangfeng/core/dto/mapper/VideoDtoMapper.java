package com.boruan.shengtangfeng.core.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.dto.VideoDto;
import com.boruan.shengtangfeng.core.entity.Video;

/**
 * @author KongXH
 * @title: VideoCommentDtoMapper
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1416:55
 */
@Mapper
public interface  VideoDtoMapper extends DtoMapper<VideoDto, Video> {
    VideoDtoMapper MAPPER = Mappers.getMapper(VideoDtoMapper.class);

}
