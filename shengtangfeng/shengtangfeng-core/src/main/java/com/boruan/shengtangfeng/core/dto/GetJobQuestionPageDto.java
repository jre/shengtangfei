package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class GetJobQuestionPageDto {

    @ApiModelProperty("班级Id")
    private Long classId ;

    @ApiModelProperty("作业Id")
    private Long JobId ;

    @ApiModelProperty("排序 0升序 1降序")
    private Integer sort;

    @ApiModelProperty("0只看未全对 1只看待批改")
    private Integer status;

    @ApiModelProperty("题目类型 0全部 1选择 2主观 3线下 ")
    private Integer type ;

    @ApiModelProperty("页码")
    private Integer pageNo=1;
}
