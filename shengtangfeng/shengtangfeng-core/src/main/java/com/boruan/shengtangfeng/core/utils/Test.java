package com.boruan.shengtangfeng.core.utils;

import java.text.DecimalFormat;

/**
 * @author:Jessica
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public class Test {

	public static void main(String[] args) {
		double p = 456d / 1000d;
		DecimalFormat df = new DecimalFormat("0.00");
		String per = df.format(p);
		System.out.println(per);
		double num = Double.parseDouble(per);

		System.out.println(num);
//		long i = Math.round(num);
		String percent = (int) (num * 100) + "%";
		System.out.println(percent);
	}
}
