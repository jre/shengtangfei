package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

/**
 * 小视频@好友
 */
@Data
public class VideoFriendVo{

    /**
     * id
     **/
    @ApiModelProperty(value = "记录表里的id")
    private Long id ;

    /**
     * 视频id
     **/
    @ApiModelProperty(value = "视频id")
    private Long videoId ;

    /**
     * 视频封面
     **/
    @ApiModelProperty(value = "视频封面")
    private String image ;

    /**
     * 视频地址
     */
    @ApiModelProperty(value="视频地址")
    private String content;

    /**
     * 用户id
     **/
    @ApiModelProperty("用户id")
    private Long userId ;

    @ApiModelProperty("用户头像")
    private String userImage;

    @ApiModelProperty("用户昵称")
    private String userNickname;

    @ApiModelProperty("用户手机号")
    private String userMobile;

    @ApiModelProperty("创建时间时间戳")
    private Long time;
}
