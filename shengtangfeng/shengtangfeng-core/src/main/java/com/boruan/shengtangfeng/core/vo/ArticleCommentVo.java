package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ArticleCommentVo extends BaseVo {

	/**
	 * 文章ID
	 */
	@ApiModelProperty("文章ID")
	private Long articleId;
	/**
	 * 回复的评论ID
	 */
	@ApiModelProperty("回复的评论ID")
	private Long commentId;
	/**
	 * 发布人ID
	 */
	@ApiModelProperty("发布人ID")
	private Long userId;
	/**
	 * 发布人头像
	 */
	@ApiModelProperty("发布人头像")
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	@ApiModelProperty("发布人姓名")
	private String userName;
	/**
	 * 内容
	 */
	@ApiModelProperty("内容")
	private String content;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("状态 0未审核 1已审核 2审核失败")
	private Integer status;
	
    /**
     *  是否推荐
     */
    @ApiModelProperty("是否推荐")
    private Boolean recommend;
}
