package com.boruan.shengtangfeng.core.utils;

import java.util.Random;

public class PayUtil {

    public static String buildOutTradeNo(String type, Long orderId) {
        return type + "_" + orderId;
    }

    public static String buildTradeNo() {
        String result = "";
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            result += random.nextInt(10);
        }
        return System.currentTimeMillis() + result;
    }

    public static Long splitOutTradeNoOrderId(String outTradeNo) {
        outTradeNo = StringUtils.split(outTradeNo, "_")[1];
        return Long.parseLong(outTradeNo);
    }

    public static String splitOutTradeNoType(String outTradeNo) {
        return StringUtils.split(outTradeNo, "_")[0];
    }

    public static void main(String[] args) {
        System.out.println(buildTradeNo());
    }
}
