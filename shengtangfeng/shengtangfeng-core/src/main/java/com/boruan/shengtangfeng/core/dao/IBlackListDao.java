package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.BlackList;


public interface IBlackListDao extends BaseMapper<BlackList> {
}