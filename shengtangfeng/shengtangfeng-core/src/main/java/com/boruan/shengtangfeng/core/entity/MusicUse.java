package com.boruan.shengtangfeng.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * 音乐使用中间表
 */
@Table(name="music_use")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MusicUse extends IdEntity {

    @ApiModelProperty(value = "音乐id")
    private Long musicId ;

    @ApiModelProperty(value = "使用者id")
    private Long userId ;

    @ApiModelProperty(value = "使用时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date userTime ;

}