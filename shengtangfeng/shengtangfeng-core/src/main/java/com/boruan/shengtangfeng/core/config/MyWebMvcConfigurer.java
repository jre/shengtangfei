package com.boruan.shengtangfeng.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.boruan.shengtangfeng.core.converter.StringToEnumConverterFactory;

@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer{
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new StringToEnumConverterFactory());
    }
}
