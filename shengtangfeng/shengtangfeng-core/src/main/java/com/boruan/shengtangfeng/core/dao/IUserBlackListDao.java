package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.UserBlackList;


public interface IUserBlackListDao extends BaseMapper<UserBlackList> {
}