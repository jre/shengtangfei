package com.boruan.shengtangfeng.core.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.QuestionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 错题记录表
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "incorrect_question")
@EqualsAndHashCode(callSuper = false)
public class IncorrectQuestion extends IdEntity implements Serializable {

	private static final long serialVersionUID = 6335675770371435246L;
	/**
	 *  用户 Id
	 */
	private Long userId;
	/**
	 *  试题Id
	 */
	private Long questionId;
	/**
     * 类型
     */
    private QuestionType type;
    /**
     * 模块ID
     */
    private Long moduleId;
    /**
     * 年级ID
     */
    private Long gradeId;
    /**
     * 年级名
     */
    private String gradeName;
    /**
     * 学科ID
     */
    private Long subjectId;
    /**
     * 学科名
     */
    private String subjectName;

    @ApiModelProperty(value = "手机错题图片")
    private String img;

    @ApiModelProperty(value = "状态")
    private Integer status;
    
}
