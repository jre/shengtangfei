package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.TalkVideoDto;
import com.boruan.shengtangfeng.core.entity.Video;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TalkVideoDtoMapper extends DtoMapper<TalkVideoDto, Video> {
    TalkVideoDtoMapper MAPPER = Mappers.getMapper(TalkVideoDtoMapper.class);
}
