package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserJobVo extends BaseVo {

    @ApiModelProperty(value = "作业ID")
    private Long jobId ;

    @ApiModelProperty(value = "得分")
    private BigDecimal score ;

    @ApiModelProperty(value = "用户id，学生id")
    private Long userId ;

    @ApiModelProperty(value = "用户昵称")
    private String userName;

    @ApiModelProperty(value = "头像")
    private String headImage;

    @ApiModelProperty(value = "耗费时间")
    private Long useTime;

    @ApiModelProperty(value = "耗费时间时分秒")
    private Long elapsedTime;

    @ApiModelProperty(value = "完成时间")
    private String finishTime;

    @ApiModelProperty(value = "状态0未提交1已提交2已完成")
    private Integer status;

    @ApiModelProperty(value = "排名")
    private Long rank;

    @ApiModelProperty(value = "教师评语")
    private String comment;

    @ApiModelProperty(value = "是否全对")
    private Boolean isRight;
}
