package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.enums.Sex;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserDto extends BaseDto {

	/**
	 * 性别
	 * 
	 * @see Sex
	 */
	@ApiModelProperty(value = "1男,2女")
	private Integer sex;
	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	private String name;

	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String headImage;

	/**
	 * 登录名
	 **/
	@ApiModelProperty(value = "登录名")
	private String loginName;

	/**
	 * 密码
	 */
	@ApiModelProperty(value = "密码")
	private String password;

	/**
     * 微信unionId
     **/
    private String unionId;
    /**
     * 微信openId
     **/
    private String openId;

	@ApiModelProperty(value = "邀请码")
	private String invitationCode;//邀请码
	@ApiModelProperty(value = "获赞数")
	private String likeNum;
	@ApiModelProperty(value="用户类型")
	private Integer userType;


}
