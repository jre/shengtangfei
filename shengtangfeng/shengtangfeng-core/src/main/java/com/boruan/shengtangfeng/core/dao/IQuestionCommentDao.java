package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import com.boruan.shengtangfeng.core.vo.CommentVo;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.QuestionComment;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IQuestionCommentDao extends BaseMapper<QuestionComment> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<QuestionComment> pageQuery);
	/**
	 * 分页获取不是我的评论
	 */
	public void pageQueryNotMine(PageQuery<QuestionComment> pageQuery);

	/**
	 * 根据问题ID获取所有的评论
	 */
	public List<QuestionComment> findByQuestionId(Long questionId,Integer status,Integer type);
	/**
	 * 根据条件获取所有的评论
	 */
	public List<QuestionComment> findByTemplate(QuestionComment questionComment);

	/**
	 * 分页查询待审核试题评论
	 * @param pageQuery
	 */
    void pageGetComment(PageQuery<CommentVo> pageQuery);

	/**
	 * 分页查询待审核文章视频试题评论
	 * @param pageQuery
	 */
	void pageArticleVideoQuestionAuditComment(PageQuery<CommentVo> pageQuery);

	/**
	 * 审核端根据用户id查看用户的评论
	 * @param pageQuery
	 */
    void getUserComment(PageQuery<CommentVo> pageQuery);
}
