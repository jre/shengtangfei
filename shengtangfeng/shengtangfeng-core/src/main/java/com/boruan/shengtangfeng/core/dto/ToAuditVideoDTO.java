package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

@Data
public class ToAuditVideoDTO {

    @ApiModelProperty("视频分类id")
    private Long categoryId;

    @ApiModelProperty("用户id,用户昵称，视频id，标题")
    private String content;

    @ApiModelProperty("第几页")
    private Integer pageNo;

    @ApiModelProperty("每页总条数")
    private Integer pageSize;

    @ApiModelProperty(value = "视频类型：0普通视频1讲一讲2小视频")
    private Integer type;
}
