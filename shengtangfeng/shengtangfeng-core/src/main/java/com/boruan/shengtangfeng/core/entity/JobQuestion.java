package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="job_question")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class JobQuestion extends IdEntity {


    /**
     * 序号
     */
    private Long serialNumber ;

    /**
    * 作业id 
    **/
    private Long classJobId ;

    /**
    * 题目id 
    **/
    private Long questionId ;
}