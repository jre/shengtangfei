package com.boruan.shengtangfeng.core.utils;

import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.boruan.shengtangfeng.core.entity.JPushMessage;
import com.boruan.shengtangfeng.core.vo.NoticeVo;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PushUtil {

    public static void push(JPushClient client, NoticeVo noticeVo, JPushMessage... message) {
        PushPayload payload = buildPushObject(noticeVo, message);

        try {
            PushResult result = client.sendPush(payload);
            log.info("推送结果 - " + result);

        } catch (APIConnectionException e) {
            // Connection error, should retry later
            log.error("Connection error, should retry later", e);

        } catch (APIRequestException e) {
            // Should review the error, and fix the request
            log.error("Should review the error, and fix the request", e);
            log.info("HTTP Status: " + e.getStatus());
            log.info("Error Code: " + e.getErrorCode());
            log.info("Error Message: " + e.getErrorMessage());
        }
    }




    private static PushPayload buildPushObject(NoticeVo noticeVo, JPushMessage... message) {
		/**
		 * 推送所有人
		 */
		if (noticeVo.getUserId() == null || noticeVo.getUserId() == 0) {
            return PushPayload.newBuilder().setPlatform(Platform.all()).setAudience(Audience.all())
                    .setMessage(message.length == 0 ? null : Message.newBuilder().setMsgContent(message[0].getMsgContent()).addExtras(message[0].getExtras()).build())
                    .setNotification(noticeVo.getType().equals(404) ? null :
							Notification.newBuilder()
                                    .addPlatformNotification(IosNotification
                                            .newBuilder().setAlert(noticeVo.getTitle()).setBadge(5).setSound("happy")
                                            .addExtra("noticeVo", JSON.toJSONString(noticeVo)).build())
                                    .addPlatformNotification(
                                            AndroidNotification.newBuilder().setAlert(noticeVo.getTitle())
                                                    .addExtra("noticeVo", JSON.toJSONString(noticeVo)).build())
                                    .build())
                    .setOptions(Options.newBuilder()
                            .setApnsProduction(false)
                            .build())
                    .build();
        } else {
			/**
			 * 单个用户
			 */
            return PushPayload.newBuilder().setPlatform(Platform.all())
                    .setAudience(Audience.alias(noticeVo.getUserId().toString()))
                    .setMessage(message.length == 0 ? null : Message.newBuilder().setMsgContent(message[0].getMsgContent()).addExtras(message[0].getExtras()).build())
                    .setNotification(noticeVo.getType().equals(404) ? null : Notification.newBuilder()
                            .addPlatformNotification(
                                    IosNotification.newBuilder().setAlert(noticeVo.getTitle()).setBadge(1)
                                            .setSound("happy").addExtra("noticeVo", JSON.toJSONString(noticeVo)).build())
                            .addPlatformNotification(AndroidNotification.newBuilder().setAlert(noticeVo.getTitle())
                                    .addExtra("noticeVo", JSON.toJSONString(noticeVo)).build())
                            .build())
                    .setOptions(Options.newBuilder()
                            .setApnsProduction(false)
                            .build())
                    .build();
        }
    }

    public static void main(String[] args) throws InterruptedException {
		JPushClient customerClient = new JPushClient("9b80ac1db7007a4a6fd53d5e", "ff8ece3d2566774757824b60", null,
				ClientConfig.getInstance());

		NoticeVo noticeVo = new NoticeVo();
		noticeVo.setUserId(120L);
		noticeVo.setType(403);
		noticeVo.setTitle("账号被锁,强制下线");

		JPushMessage pushMessage = new JPushMessage();
		pushMessage.setMsgContent("账号被锁,强制下线");
		HashMap<String, String> map = new HashMap<String, String>(){{
			put("code", "403");
		}};
		pushMessage.setExtras(map);
		PushUtil.push(customerClient, noticeVo, pushMessage);

    }
}
