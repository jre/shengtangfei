package com.boruan.shengtangfeng.core.service.impl;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IGradeDao;
import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.service.IGradeService;
/**
 * 
 * @author 刘光强
 * @date 2020年8月12日16:10:49
 */
@Service
@Transactional(readOnly = true)
public class GradeService implements IGradeService{

	@Autowired
	private IGradeDao gradeDao;
	
	
	@Override
    public Grade findById(Long id) {
        return gradeDao.single(id);
    }



    @Override
	@Transactional(readOnly = false)
	public void save(Grade grade) {
		if (grade.getId()==null) {
			KeyHolder kh=gradeDao.insertReturnKey(grade);
			grade.setId(kh.getLong());
		}else{
			gradeDao.updateById(grade);
		}
	}



	@Override
	public void pageQuery(PageQuery<Grade> pageQuery, Grade grade) {
		pageQuery.setParas(grade);
		gradeDao.pageQuery(pageQuery);
	}



    @Override
    public List<Grade> getList(Grade grade) {
        return gradeDao.getList(grade);
    }



    @Override
    public List<Grade> getListBySubjectId(Long subjectId) {
        return gradeDao.getListBySubjectId(subjectId);
    }

}
