package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Singer;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface ISingerDao extends BaseMapper<Singer> {

    void pageQuery(PageQuery<Singer> pageQuery);
}
