package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="group_chat")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GroupChat extends IdEntity {

    /**
     * 群号
     **/
    private String groupNum ;

    /**
     * 群号
     **/
    private String tencentId ;

    /**
     * 类型 0公开群 1班级关联群
     **/
    private Integer groupType ;
            
    /**
    * 加群方式 0禁止加群 ，1管理员审核，2不需要审核 
    **/
    private Integer addWay ;
    /**
    * 班级id 
    **/
    private Long classId ;
    /**
    * 群聊头像 
    **/
    private String head ;
            
    /**
    * 群介绍 
    **/
    private String introduce ;
            
    /**
    * 群聊名称 
    **/
    private String name ;
            
    /**
    * 群公告 
    **/
    private String notice ;
}