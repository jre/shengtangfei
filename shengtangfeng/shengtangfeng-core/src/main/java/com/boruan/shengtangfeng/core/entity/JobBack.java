package com.boruan.shengtangfeng.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 作业补交
 */
@Table(name="job_back")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class JobBack extends IdEntity {

    /**
    * 作业id
    **/
    private Long jobId ;
            
    /**
    * 用户id
    **/
    private Long userId ;

    /**
     * 状态 0未补交 1已补交
     */
    private Integer status;

    @ApiModelProperty("发起时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    protected Date riseTime;

}