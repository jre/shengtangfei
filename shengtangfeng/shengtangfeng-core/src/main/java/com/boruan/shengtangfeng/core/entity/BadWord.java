package com.boruan.shengtangfeng.core.entity;
import org.beetl.sql.core.annotatoin.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Table(name="bad_word")
@Setter
@Getter
@NoArgsConstructor
public class BadWord extends IdEntity {
    
    /**
    * 名称 
    **/
    
    private String name;

    /**
     * 0：完全禁止 1：待审核
     */
    private Integer level;

}