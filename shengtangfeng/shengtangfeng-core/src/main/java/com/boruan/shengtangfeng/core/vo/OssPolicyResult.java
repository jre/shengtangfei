package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 获取OSS上传授权返回结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OssPolicyResult {
    @ApiModelProperty("accessKey")
    private String accessKeyId;
    @ApiModelProperty("策略")
    private String policy;
    @ApiModelProperty("签名")
    private String signature;
    @ApiModelProperty("存储目录")
    private String dir;
    @ApiModelProperty("提交节点action")
    private String host;
    @ApiModelProperty("限制大小，整数，单位MB")
    private Integer limit;

}
