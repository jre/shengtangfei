package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassInformMember;
import com.boruan.shengtangfeng.core.entity.ClassVideoMember;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IClassVideoMemberDao extends BaseMapper<ClassVideoMember> {
}
