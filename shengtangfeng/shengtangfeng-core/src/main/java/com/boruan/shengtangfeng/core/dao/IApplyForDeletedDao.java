package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ApplyForDeleted;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IApplyForDeletedDao extends BaseMapper<ApplyForDeleted> {
}
