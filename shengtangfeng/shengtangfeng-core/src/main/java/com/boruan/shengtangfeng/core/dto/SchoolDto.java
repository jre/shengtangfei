package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@Data
public class SchoolDto{

    @ApiModelProperty("省id")
    private String provinceId ;

    @ApiModelProperty("市id")
    private String cityId ;

    @ApiModelProperty("区id")
    private String countyId;

    @ApiModelProperty("学校名称")
    private String schoolName ;

    @ApiModelProperty("用户姓名 在创建学校的时候不需要传值，该字段用于查询")
    private String userNickname ;

}