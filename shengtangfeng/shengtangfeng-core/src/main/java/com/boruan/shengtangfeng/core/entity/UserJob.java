package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;


@Table(name="user_job")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserJob extends IdEntity {
    /**
    * 作业id 
    **/
    private Long jobId ;

    /**
     * 班级id
     **/
    private Long classId ;
            
    /**
    * 得分 
    **/
    private BigDecimal score ;
    /**
    * 学生id 
    **/
    private Long userId ;

    @ApiModelProperty(value = "耗费时间")
    private Long useTime;

    @ApiModelProperty(value = "完成时间")
    private Date finishTime;

    @ApiModelProperty(value = "状态 0未提交1已提交待批改2已完成")
    private Integer status;

    @ApiModelProperty(value = "教师评语")
    private String comment;
}