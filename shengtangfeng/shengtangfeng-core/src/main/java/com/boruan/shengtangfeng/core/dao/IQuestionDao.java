package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import com.boruan.shengtangfeng.core.dto.JobQuestionDto;
import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.JobQuestion;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Question;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IQuestionDao extends BaseMapper<Question> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<Question> pageQuery);
	/**
     * 通过章节ID获取题目
     */
    public List<Question> findByModule(Long moduleId);
	
	void pageQueryByModule(PageQuery<Question> pageData,Question question);

	void pageModuleQuery(PageQuery<Question> pageData);
	/**
	 * 获取错题
	 * @param userId
	 * @param subjectId
	 * @return
	 */
	public List<Question> getIncorrectQuestion(Long userId,Long subjectId);
	/**
	 * 获取收藏的题
	 * @param userId
	 * @param subjectId
	 * @return
	 */
	public List<Question> getFavoritesQuestion(Long userId,Long subjectId);
	
	
	/**
     * 插入试题年级关联
     * @param questionId
     * @param gradeId
     * @return
     */
    public int insertQuestionGrade(Long questionId,Long gradeId);
    /**
     * 查找试题年级关联数量
     * @param questionId
     * @param gradeId
     * @return
     */
    public long getQuestionGradeCount(Long questionId,Long gradeId);
    /**
     * 删除试题的所有年级
     * @param questionId
     * @return
     */
    public void deleteQuestionGrade(Long questionId);
    /**
     * 试题的所有年级
     * @param questionId
     * @return
     */
    public List<Long> getQuestionGrades(Long questionId);

    public List<Question> jobQuestion(Long jobId);

	List<Long> getGradeQuestion(Long gradeId);

	/**
	 * 获取题目数量
	 * @param question
	 * @return
	 */
	long getQuestionCount(Question question,Long gradeId);

	/**
	 * 获取我的题库分页
	 * @param pageQuery
	 */
	void getMyQuestionPage(PageQuery<Question> pageQuery);

	/**
	 * 获取淘学题库分页
	 * @param pageQuery
	 */
	void getTaoxuePage(PageQuery<Question> pageQuery);

	/**
	 * 获取共享题库分页
	 * @param pageQuery
	 */
	void getGongxiangPage(PageQuery<Question> pageQuery);

	/**
	 * 获取淘学题库收藏试题
	 * @param pageQuery
	 */
	void getTaoxueFavorite(PageQuery<Question> pageQuery);

	/**
	 * 获取共享题库收藏试题
	 * @param pageQuery
	 */
	void getGongxiangFavorite(PageQuery<Question> pageQuery);

	/**
	 * 获取试题对应的年级id
	 * @param questionId
	 * @return
	 */
	Long getQuestionGrade(Long questionId);

	/**
	 * 班级内容--多条件查询题目
	 * @param query
	 */
    void getQuestionPage(PageQuery<Question> query);

	/**
	 * 多条件查询作业下题目分页
	 * @param query
	 */
	void getJobQuestionPage(PageQuery<JobQuestionDto> query);

	/**
	 * 获取使用过的题的Id
	 * @param userId
	 * @return
	 */
    Question getUseQuestion(Long userId,Long questionId);

	/**
	 * 获取作业下所有题
	 * @param
	 * @return
	 */
	List<Question> getJobQuestion(Long jobId);
}
