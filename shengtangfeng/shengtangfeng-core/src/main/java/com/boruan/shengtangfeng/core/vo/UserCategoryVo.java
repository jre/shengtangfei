package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCategoryVo extends IdEntity {
    
            
            
    /**
    * 文章类别id 
    **/
    @ApiModelProperty("文章类别id ")
    private Long categoryId ;
            

    /**
    * 用户id 
    **/
    @ApiModelProperty("用户id ")
    private Long userId ;

    @ApiModelProperty("分类类别1文章2视频")
    private String category_type;//分类类别1文章2视频
    @ApiModelProperty("类别名称")
    private String categoryName ;//类别名称

            


}