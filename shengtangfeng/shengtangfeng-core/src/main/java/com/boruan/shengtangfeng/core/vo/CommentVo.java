package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author:guojiang
 * @Description:用户评论
 * @date:2021/2/25
 */
@Data
public class CommentVo{

    /**
     * 评论id
     */
    @ApiModelProperty("评论id")
    private Long commentId;

    /**
     * 文章ID
     */
    @ApiModelProperty("文章,视频,试题id")
    private Long objectId;
    /**
     * 发布人ID
     */
    @ApiModelProperty("发布人ID")
    private Long userId;
    /**
     * 发布人头像
     */
    @ApiModelProperty("发布人头像")
    private String userIcon;
    /**
     * 发布人姓名
     */
    @ApiModelProperty("发布人姓名")
    private String userName;
    /**
     * 内容
     */
    @ApiModelProperty("内容")
    private String content;
    /**
     * 状态 0未审核 1已审核
     */
    @ApiModelProperty("状态 0未审核 1已审核 2审核失败")
    private Integer status;
    /**
     * 类型 0文章 1视频 2试题
     */
    @ApiModelProperty("类型 0文章 1视频 2试题 ")
    private Integer type;

    /**
     * 类型  1文字，2文字+图片，3语音，4语音+图片，5图片
     */
    @ApiModelProperty("类型  1文字，2文字+图片，3语音，4语音+图片，5图片")
    private Integer commentType;

    /**
     * 语音时间，秒数
     */
    @ApiModelProperty("语音时间，秒数")
    private Integer voiceTime;

    /**
     * 语音地址
     */
    @ApiModelProperty("语音地址")
    private String voice;
    /**
     * 逗号分隔的图片
     */
    @ApiModelProperty("逗号分隔的图片")
    private String images;

    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    protected Date createTime;
}
