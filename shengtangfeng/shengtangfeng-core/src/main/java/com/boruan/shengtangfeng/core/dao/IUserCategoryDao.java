package com.boruan.shengtangfeng.core.dao;
import com.boruan.shengtangfeng.core.dto.UserCategoryDto;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;

import java.util.List;


public interface IUserCategoryDao extends BaseMapper<UserCategory> {


    public List<ArticleCategory> getUserArticleCategory(Long userId);
    public List<VideoCategory> getUserVideoCategory(Long userId,Integer type);

    public void deleteCategory(UserCategory userCategory);
    List<Subject> getUserJYJCategory(Long userId, Integer type);
}