package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.JobName;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IJobNameDao extends BaseMapper<JobName> {

    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<JobName> query);
}
