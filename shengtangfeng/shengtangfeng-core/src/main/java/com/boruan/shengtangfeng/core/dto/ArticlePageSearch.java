package com.boruan.shengtangfeng.core.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ArticlePageSearch extends BasePageSearch {

    /**
     * 分类ID，关注分类传0
     */
    @ApiModelProperty("分类ID，关注分类传0")
    private Long categoryId;
    /**
     * 检索词，用于搜索功能，非检索功能可不传
     */
    @ApiModelProperty("检索词，用于搜索功能，非检索功能可不传")
    private String keyword;

    @ApiModelProperty(value = "状态：0待审核1已通过2拒绝")
    private Integer status;

}
