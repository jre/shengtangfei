package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;


@Data
public class JobDto extends TailBean {

    @ApiModelProperty("作业id")
    private Long jobId ;

    @ApiModelProperty("作业状态 0未提交 1已提交待批改 2已完成 -1都是选择题")
    private Integer status ;

    @ApiModelProperty("学科名称")
    private String subjectName ;

    @ApiModelProperty("作业名称")
    private String jobName ;

    @ApiModelProperty("正确率")
    private String accuracy ;

    @ApiModelProperty("已提交人数")
    private Long submit ;

    @ApiModelProperty("班级总人数")
    private Long studentNum ;

    @ApiModelProperty("未提交人数")
    private Long noSubmit ;

    @ApiModelProperty("全对人数")
    private Long correctNum ;

    @ApiModelProperty("未全对人数")
    private Long noCorrectNum ;

    @ApiModelProperty("截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date deadline ;

    @ApiModelProperty("截止时间时间戳")
    private Long deadlineTime ;

    @ApiModelProperty("是否超过截止时间")
    private Boolean isDeadline ;

    @ApiModelProperty("题目总数")
    private Long topicNum ;

    @ApiModelProperty("发布人id")
    private Long pusherId ;

    @ApiModelProperty("发布人头像")
    private String pusherImage ;

    @ApiModelProperty("发布人姓名")
    private String pusherName ;

    @ApiModelProperty("是否为自己发布的")
    private Boolean isMy ;

    @ApiModelProperty("平均用时分秒")
    private Long averageTime ;

    @ApiModelProperty("是否有主观题需要批改")
    private Boolean isCheck ;

    @ApiModelProperty("得分")
    private String score ;

    @ApiModelProperty("分数线")
    private BigDecimal fractionalLine ;


}