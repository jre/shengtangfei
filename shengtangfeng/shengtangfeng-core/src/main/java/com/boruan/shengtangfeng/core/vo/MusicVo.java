package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="音乐主体")
public class MusicVo extends BaseVo{


    @ApiModelProperty(value="音乐时长")
    private Integer duration ;


    @ApiModelProperty(value = "音乐链接地址")
    private String content ;

    @ApiModelProperty(value = "封面图")
    private String image ;

    /**
     * 歌曲名
     **/
    @ApiModelProperty(value = "歌曲名")
    private String name ;

    /**
     * 歌手名
     **/
    @ApiModelProperty(value = "歌手名")
    private String singer ;

    /**
     * 是否热门：1是0否
     */
    @ApiModelProperty(value = "是否热门：0否1是")
    private Integer isHot;

    /**
     * 使用次数
     */
    @ApiModelProperty(value = "使用次数")
    private Long clickNum;

    @ApiModelProperty(value = "是否收藏：0否1是")
    private Integer isCollection;

}
