package com.boruan.shengtangfeng.core.enums;


import org.beetl.sql.core.annotatoin.EnumMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
@author: lihaicheng
@Description: 实名认证是否通过
@date:2020年3月10日 上午11:14:10
*/
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserCheckStatus implements CommonEnum {
	WEITIJIAO(0, "未提交"), 
	YITIJIAO(1, "已提交"),
    YISHENHE(2, "已通过"),
	WEITONGGUO(3, "未通过");

	@ApiModelProperty(value=" 3未通过 2已审核  1已提交 0未提交 ")
	private Integer value;
	private String name;

	private UserCheckStatus(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	/**
	 *将该枚举全部转化成json
	 * @return
	 */
	public static String toJson(){
		JSONArray jsonArray = new JSONArray();
		for (UserCheckStatus e : UserCheckStatus.values()) {
			JSONObject object = new JSONObject();
			object.put("value", e.getValue());
			object.put("name", e.getName());
			jsonArray.add(object);
		}
		return jsonArray.toString();
	}
}
