package com.boruan.shengtangfeng.core.service;

import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IConfigService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param config
	 */
	public void pageQuery(PageQuery<Config> pageQuery, Config config);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public Config findById(Long id);
	/**
	 * 根据key获取
	 * @param key
	 * @return
	 */
	public Config findConfigByKey(String key);
	/**
	 * 保存分类
	 * @param  config
	 */
	public void save(Config config);
	/**
	 * 获取最新版本的APK
	 * */
	public Apk getApk(Long id);

}
