package com.boruan.shengtangfeng.core.enums;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import org.beetl.sql.core.annotatoin.EnumMapping;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
@author: liuguangqiang
@Description: 报名状态
@date:2020年3月10日 上午11:14:10
*/
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum QuestionType implements CommonEnum {
//    DUOXUANTI(1, "多选题")
	DANXUANTI(0, "选择题"),
//	PANDUANTI(2, "判断题")
	ZHUGUANTI(1,"主观题"),
	XIANXIATI(2,"线下题")
	;
	@ApiModelProperty(value="0选择题,1主观题，2线下题")
	private Integer value;
	private String name;

	private QuestionType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static QuestionType valueOf(int ordinal) {
		return QuestionType.class.getEnumConstants()[ordinal];
	}
}

