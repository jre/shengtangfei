package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;


/**
 * 用户拉黑的内容发布者
 * @author liuguangqiang
 *
 */
@Table(name="user_black_list")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserBlackList extends IdEntity {
    
            

    /**
    * 被拉黑人id
    **/
    private Long blackUserId ;
            
            
    /**
    * 用户id
    **/
    private Long userId ;


}