package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="music")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Music extends IdEntity {
    
            
            
    /**
    * 音乐秒数
    **/
    private Integer duration ;
            
    /**
    * 音乐地址
    **/
    @ApiModelProperty(value = "音乐地址")
    private String content ;
    /**
    * 封面图 
    **/
    @ApiModelProperty(value = "音乐封面图")
    private String image ;
            
    /**
    * 歌曲名 
    **/
    @ApiModelProperty(value = "歌曲名")
    private String name ;
            
    /**
    * 歌手名 
    **/
    private String singer ;

    /**
     * 歌手ID
     */
    @ApiModelProperty(value = "歌手ID")
    private Long singerId ;

    /**
     * 是否热门：1是0否
     */
    @ApiModelProperty(value = "是否热门：1是0否")
    private Integer isHot;

    /**
     * 使用次数
     */
    @ApiModelProperty(value = "使用次数")
    private Long clickNum;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

}