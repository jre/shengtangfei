package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 小视频评论
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SmallVideoCommentParentVo extends BaseVo {

	/**
	 * 文章ID
	 */
	@ApiModelProperty("文章ID")
	private Long videoId;
	/**
	 * 回复的评论ID
	 */
	@ApiModelProperty("回复的评论ID")
	private Long commentId;
	/**
	 * 发布人ID
	 */
	@ApiModelProperty("发布人ID")
	private Long userId;
	/**
	 * 发布人头像
	 */
	@ApiModelProperty("发布人头像")
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	@ApiModelProperty("发布人姓名")
	private String userName;
	/**
	 * 内容
	 */
	@ApiModelProperty("内容")
	private String content;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("状态 0未审核 1已审核 2审核失败")
	private Integer status;
	
    /**
     *  是否推荐
     */
    @ApiModelProperty("是否推荐")
    private Boolean recommend;

	/**
	 *  点赞数
	 */
	@ApiModelProperty("点赞数")
	private Integer thumpUpNum;

	/**
	 * 父id
	 */
	@ApiModelProperty("父id")
	private Long parentId;

	/**
	 * 是否点赞
	 */
	@ApiModelProperty("是否点赞")
	private Boolean isThumpUp;

	@ApiModelProperty("二级评论")
	private List<SmallVideoCommentChildVo> childVoList;
}
