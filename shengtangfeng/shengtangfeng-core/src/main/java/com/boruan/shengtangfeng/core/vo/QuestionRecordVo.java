package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionRecordVo extends BaseVo {

	/**
     * 问题ID
     */
    @ApiModelProperty("问题ID")
    private Long questionId;
    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Long userId;
    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String userName;
    /**
     * 来源 1 章节 2 模块 3 知识点 4主观题 5随机练习 6真题模拟 7名师题库 8精选易错 9必做600题 10重要考点
     */
    @ApiModelProperty("来源 1 章节 2 模块 3 知识点 4主观题 5随机练习 6真题模拟 7名师题库 8精选易错 9必做600题 10重要考点")
    private Integer source;
    /**
     * 对象ID，根据source确定为什么ID，不同的练习传不同的ID
     */
    @ApiModelProperty("对象ID，根据source确定为什么ID，不同的练习传不同的ID")
    private Long objectId;
    
    /**
     *  是否正确
     */
    @ApiModelProperty("是否正确")
    private Boolean correct;
    /**
     *  用户的答案
     */
    @ApiModelProperty("用户的答案")
    private String userAnswer;
    
}
