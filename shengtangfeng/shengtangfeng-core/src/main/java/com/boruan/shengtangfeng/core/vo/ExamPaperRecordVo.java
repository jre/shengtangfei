package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 课程
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ExamPaperRecordVo extends BaseVo {

	/**
     * 试卷ID
     **/
    @ApiModelProperty("试卷ID")
    private Long examId;
    /**
     * 试卷名称
     **/
    private String name;
    /**
     * 正确率百分数
     **/
    @ApiModelProperty("正确率百分数")
    private String accurate;
    /**
     * 试卷总分
     **/
    @ApiModelProperty("试卷得分")
    private Double score;
    /**
     * 主观题总分
     **/
    @ApiModelProperty("主观题总分")
    private Double subjectiveScore;
    /**
     * 客观题总分
     **/
    @ApiModelProperty("客观题总分")
    private Double objectiveScore;
    /**
     * 答题卡
     **/
    @ApiModelProperty("答题卡")
    private AnswerCardVo answerCard;
    /**
     * 所有的题
     **/
    @ApiModelProperty("所有的题")
    private List<QuestionVo> allQuestion;
    /**
     * 错误的题
     **/
    @ApiModelProperty("错误的题")
    private List<QuestionVo> incorrect;

    @ApiModelProperty("试卷总分")
    private Double totalScore;
}
