//package com.boruan.shengtangfeng.core.utils;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import IQuestionTypeDao;
//import QuestionType;
//
///**
// * questionType工具类
// */
//@Component
//public class QuestionTypeUtil {
//    @Autowired
//    private IQuestionTypeDao questionTypeDao;
//    private static IQuestionTypeDao questionTypeDaoStatic;
//
//    @PostConstruct
//    public void init() {
//        questionTypeDaoStatic = questionTypeDao;
//    }
//    public static QuestionType praseQuestionType(Integer type) {
//        QuestionType search=new QuestionType();
//        search.setIsDeleted(false);
//        search=questionTypeDaoStatic.templateOne(search);
//        if(search==null) {
//            search=new QuestionType();
//            search.setName("未知");
//            search.setValue(-1);
//        }
//        return search;
//    }
//
//}
