package com.boruan.shengtangfeng.core.config;

import javax.sql.DataSource;

import org.beetl.sql.ext.spring4.BeetlSqlDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataSourceConfig {
	
	@Bean(name = "dataSource")
	public DataSource datasource(Environment env) {
		HikariDataSource ds = new HikariDataSource();
		ds.setJdbcUrl(env.getProperty("spring.datasource.url"));
		ds.setUsername(env.getProperty("spring.datasource.username"));
		ds.setPassword(env.getProperty("spring.datasource.password"));
		ds.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
		ds.setMinimumIdle(Integer.parseInt(env.getProperty("spring.datasource.hikari.minimum-idle")));
		ds.setMaximumPoolSize(Integer.parseInt(env.getProperty("spring.datasource.hikari.maximum-pool-size")));
		ds.setAutoCommit(Boolean.parseBoolean(env.getProperty("spring.datasource.hikari.auto-commit")));
		ds.setIdleTimeout(Integer.parseInt(env.getProperty("spring.datasource.hikari.idle-timeout")));
		ds.setPoolName(env.getProperty("spring.datasource.hikari.pool-name"));
		ds.setMaxLifetime(Integer.parseInt(env.getProperty("spring.datasource.hikari.max-lifetime")));
		ds.setConnectionTimeout(Integer.parseInt(env.getProperty("spring.datasource.hikari.connection-timeout")));
		ds.setConnectionTestQuery(env.getProperty("spring.datasource.hikari.connection-test-query"));
		return ds;
	}
	
	@Bean
	public BeetlSqlDataSource beetlSqlDataSource(DataSource dataSource){
		BeetlSqlDataSource source = new BeetlSqlDataSource();
		source.setMasterSource(dataSource);
		return source;
	}
}