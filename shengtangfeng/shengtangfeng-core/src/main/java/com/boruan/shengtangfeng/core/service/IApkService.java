package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface IApkService {
    public Long save(Apk apk);
    public GlobalReponse<Apk> getApk();
}
