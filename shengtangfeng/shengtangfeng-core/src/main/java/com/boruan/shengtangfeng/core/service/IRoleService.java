package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.entity.Role;
import org.beetl.sql.core.engine.PageQuery;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface IRoleService {
	/**
	 * 通过用户id获取用户的角色列表
	 * @param userId
	 * @return
	 */
	public List<Role> findByUserId(Long userId);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param role
	 */
	public void pageQuery(PageQuery<Role> pageQuery,Role role);
	/**
	 * 通过id获取角色
	 * @param id
	 * @return
	 */
	public Role findById(Long id);
	/**
	 * 保存角色
	 * @param role
	 */
	public void save(Role role);
	/**
	 * 获取所有角色
	 * @return
	 */
	public List<Role> findAll();
	/**
	 * 根据角色名称获取角色
	 * @param name
	 * @param isSystemRole 是否系统默认角色
	 * @return
	 */
	public Role findSystemRoleByNameAndIsSystemRole(String name,boolean isSystemRole);
	/**
	 * 查询所有系统角色
	 * @return
	 */
	public List<Role> findSysRole();

	GlobalReponse delRole(Long id, Long userId);
}
