package com.boruan.shengtangfeng.core.service.impl;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IModuleRecordDao;
import com.boruan.shengtangfeng.core.dao.IQuestionRecordDao;
import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.entity.ModuleRecord;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.service.IModuleRecordService;
import com.boruan.shengtangfeng.core.vo.ModuleRankDetailVo;
import com.boruan.shengtangfeng.core.vo.ModuleRankVo;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class ModuleRecordService implements IModuleRecordService {
	@Autowired
	private IModuleRecordDao moduleRecordDao;
	@Autowired
	private IQuestionRecordDao questionRecordDao;
	@Autowired
	private IUserDao userDao;
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<ModuleRecord> pageQuery, ModuleRecord moduleRecord) {
		pageQuery.setParas(moduleRecord);
		moduleRecordDao.pageQuery(pageQuery);
	}

	/**
	 * @author: 刘光强
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public ModuleRecord findById(Long id) {
		return moduleRecordDao.unique(id);
	}
	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(ModuleRecord moduleRecord) {
		if (moduleRecord.getId() == null) {
			KeyHolder kh = moduleRecordDao.insertReturnKey(moduleRecord);
			moduleRecord.setId(kh.getLong());
		} else {
			moduleRecordDao.updateById(moduleRecord);
		}
	}

    @Override
    public ModuleRecord templateOne(ModuleRecord moduleRecord) {
        return moduleRecordDao.templateOne(moduleRecord);
    }

    @Override
    public Long templateCount(ModuleRecord moduleRecord) {
        return moduleRecordDao.templateCount(moduleRecord);
    }

    @Override
    public ModuleRankVo getRank(Long moduleId, Long userId) {
        ModuleRankVo vo=new ModuleRankVo();
        List<ModuleRankDetailVo> detailVos=moduleRecordDao.getRank(moduleId);
        ModuleRankDetailVo userRankVo=moduleRecordDao.getUserRank(moduleId, userId);
        if(detailVos.size()>=3) {
            vo.setFirst(detailVos.get(0));
            vo.setSecond(detailVos.get(1));
            vo.setThird(detailVos.get(2));
            detailVos.remove(2);
            detailVos.remove(1);
            detailVos.remove(0);
            
        }else if(detailVos.size()>=2){
            vo.setFirst(detailVos.get(0));
            vo.setSecond(detailVos.get(1));
            detailVos.remove(1);
            detailVos.remove(0);
        }else if(detailVos.size()>=1){
            vo.setFirst(detailVos.get(0));
            detailVos.remove(0);
        }
        vo.setDetails(detailVos);
        vo.setMyRank(userRankVo);
        return vo;
    }
    @Override
    public ModuleRankVo getRankAll(Long userId) {
        ModuleRankVo vo=new ModuleRankVo();
        List<ModuleRankDetailVo> detailVos=moduleRecordDao.getRankAll();
        ModuleRankDetailVo userRankVo=moduleRecordDao.getUserRankAll(userId);
        if(userRankVo==null) {
            User user=userDao.single(userId);
            userRankVo=new ModuleRankDetailVo();
            userRankVo.setUserRank(0);
            userRankVo.setUserScore("0");
            userRankVo.setUserIcon(user.getHeadImage());
            userRankVo.setUserName(user.getName());
        }
        if(detailVos.size()>=3) {
            vo.setFirst(detailVos.get(0));
            vo.setSecond(detailVos.get(1));
            vo.setThird(detailVos.get(2));
            detailVos.remove(2);
            detailVos.remove(1);
            detailVos.remove(0);
            
        }else if(detailVos.size()>=2){
            vo.setFirst(detailVos.get(0));
            vo.setSecond(detailVos.get(1));
            detailVos.remove(1);
            detailVos.remove(0);
        }else if(detailVos.size()>=1){
            vo.setFirst(detailVos.get(0));
            detailVos.remove(0);
        }
        vo.setDetails(detailVos);
        vo.setMyRank(userRankVo);
        return vo;
    }

}
