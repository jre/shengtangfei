package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 年级
 * @author 刘光强
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="年级")
public class GradeVo extends BaseVo{

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;
    /**
     *  图标
     **/
    @ApiModelProperty(value="图标")
    private String icon ;
    /**
     * 排序 
     **/
    @ApiModelProperty(value="排序 ")
    private Integer sort ;
	
}
