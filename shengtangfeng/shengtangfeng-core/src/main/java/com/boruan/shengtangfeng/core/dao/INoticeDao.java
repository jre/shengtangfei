package com.boruan.shengtangfeng.core.dao;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.dto.NoticeSearch;
import com.boruan.shengtangfeng.core.entity.Notice;
import com.boruan.shengtangfeng.core.vo.NoticeVo;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */
public interface INoticeDao extends BaseMapper<Notice>{
	/**
	 * 分页查询
	 * @param query
	 */
	public void pageQueryVo(PageQuery<NoticeVo> query) ;
	public void pageQuery(PageQuery<Notice> query) ;
	public long getCount(NoticeSearch noticeSearch) ;
	
}
