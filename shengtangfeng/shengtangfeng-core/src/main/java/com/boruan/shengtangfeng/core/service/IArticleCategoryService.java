package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 * @author KongXH
 * @title: IArticleCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1315:31
 */
public interface IArticleCategoryService {

    public void pageQuery(PageQuery<ArticleCategory> query);

    public List<ArticleCategory> getList(ArticleCategory articleCategory);

    public List<ArticleCategory> getArticleCategoryDefault();
}
