package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionCategoryDto {


    @ApiModelProperty("类型id")
    private Long id;

    @ApiModelProperty("保留字段（名称）")
    private String name ;

    @ApiModelProperty("排序")
    private Integer sort ;
    @ApiModelProperty("类别")
    private Integer type;
}
