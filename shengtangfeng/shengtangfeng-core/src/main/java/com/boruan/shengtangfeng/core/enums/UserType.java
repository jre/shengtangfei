package com.boruan.shengtangfeng.core.enums;


import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
@author: 刘光强
@Description: 用户类型
@date:2020年3月10日 上午11:14:10
*/
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserType implements CommonEnum {
	JIAYOUZHAN_LAOBAN(1, "加油站老板"), 
	JIAYOUZHAN_GUANLI(2, "加油站管理员"),
    CHE_LAOBAN(3, "车老板"),
    CHE_SIJI(4, "司机"),
    YEWUYUAN(5, "业务员");

	@ApiModelProperty(value="  1 加油站老板  2 加油站管理员 3 车老板 4 司机 5 业务员 ")
	private Integer value;
	private String name;

	private UserType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "{'name':"+this.name+",'value':"+this.value+"}";
	}
}
