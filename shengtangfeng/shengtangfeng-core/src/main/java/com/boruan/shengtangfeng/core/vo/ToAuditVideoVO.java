package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.engine.PageQuery;

import java.util.Date;
import java.util.List;

/**
 * @author:guojiang
 * @Description:查询待审核视频返回数据
 * @date:2021/2/25
 */
@Data
public class ToAuditVideoVO{

    /**
     * 视频id
     */
    @ApiModelProperty("视频ID")
    private Long id;
    /**
     * 分类ID
     */
    @ApiModelProperty("分类id")
    private Long categoryId;
    /**
     * 视频标题
     */
    @ApiModelProperty("视频标题")
    private String title;

    /**
     * 视频内容地址
     */
    @ApiModelProperty("视频内容地址")
    private String content;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @ApiModelProperty("发布时间")
    private Date publishDate;
    /**
     * 发布者id
     */
    @ApiModelProperty("发布者id")
    private Long publishId;//发布者id
    /**
     * 发布者账号
     */
    @ApiModelProperty("发布者id")
    private String account;//发布者id
    /**
     * 发布人
     */
    @ApiModelProperty("发布人")
    private String publisher;
    /**
     * 发布人头像
     */
    @ApiModelProperty("发布人头像")
    private String publisherIcon;
    /**
     * 状态 0 审核中 1 通过 2 不通过
     */
    @ApiModelProperty("状态 0 审核中 1 通过 2 不通过")
    private Integer status;

    /**
     *  首页图
     */
    @ApiModelProperty("首页图")
    private String image;
    /**
     *  审核备注
     */
    @ApiModelProperty("审核备注")
    private String remark;

    /**
     *  视频长度（秒）
     */
    @ApiModelProperty(value="视频长度（秒）")
    private Integer duration;

    @ApiModelProperty(value="相关评论")
    private List<VideoCommentVo> comment;

    @ApiModelProperty(value = "视频类型：0普通1讲一讲2小视频")
    private Integer type;

}
