package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: ArticleComment
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 评论
 * @date 2020/8/1416:38
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="评论")
public class ArticleCommentDto{



    /**
     * 文章id
     */
    @ApiModelProperty(value="文章id")
    private Long articleId;

    /**
     * 内容
     */
    @ApiModelProperty(value="内容")
    private String content;
}
