package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 答题卡主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="答题卡主体")
public class AnswerCardVo{

    /**
     * 正确题数
     */
    @ApiModelProperty(value="正确题数")
    private Integer correctNum;
    /**
     * 错误题数
     */
    @ApiModelProperty(value="错误题数")
    private Integer incorrectNum;
    
    @ApiModelProperty(value="答题详情")
    private List<AnswerCardDetailVo> answerCardDetails;
	
}
