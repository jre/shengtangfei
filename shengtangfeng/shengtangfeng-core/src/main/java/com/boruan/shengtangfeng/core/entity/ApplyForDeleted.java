package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;


@Table(name="apply_for_deleted")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApplyForDeleted extends IdEntity {
            
    /**
    * 申请表的id
    **/
    private Long applyTableId ;
            
    /**
    * 用户id
    **/
    private Long userId;

    /**
     * 是否已读
     */
    private Integer isRead ;

    /**
     * 是否删除申请数据
     */
    private Integer isDeletedApply ;

}