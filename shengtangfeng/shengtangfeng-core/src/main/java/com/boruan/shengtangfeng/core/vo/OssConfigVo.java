package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OssConfigVo{

	/**
	 * bucketName
	 **/
	@ApiModelProperty("bucketName")
	private String bucketName;

	/**
	 * endPoint
	 **/
	@ApiModelProperty("endPoint")
	private String endPoint;
	/**
	 * domain
	 **/
	@ApiModelProperty("访问路径，拼到文件名前面就是完整地址")
	private String domain;

}
