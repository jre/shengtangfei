package com.boruan.shengtangfeng.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;


@Table(name="job")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Job extends IdEntity {

    /**
    * 作业名称 
    **/
    private String name ;
            
    /**
    * 学科id 
    **/
    private Long subjectId ;
            

    /**
    * 截止时间 
    **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date deadline ;

    /**
     * 总分
     */
    private BigDecimal score;

    /**
     * 题目总数
     */
    private Long questionNum ;

    @ApiModelProperty(value = "学科名")
    private String subjectName;
}