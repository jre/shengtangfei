package com.boruan.shengtangfeng.core.tencentEntity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FriendAddParam {

    /**
     *需要为该 UserID 添加好友
     * 必填
     */
    @JSONField(name = "From_Account")
    private String From_Account;

    /**
     *好友结构体对象
     * 必填
     */
    @JSONField(name = "AddFriendItem")
    private List<FriendAddParamList> AddFriendItem;

    /**
     * 加好友方式（默认双向加好友方式）：
     * Add_Type_Single 表示单向加好友
     * Add_Type_Both 表示双向加好友
     * 选填
     */
    @JSONField(name = "AddType")
    private String AddType;

    /**
     *管理员强制加好友标记：1表示强制加好友，0表示常规加好友方式
     * 选填
     */
    @JSONField(name = "ForceAddFlags")
    private String ForceAddFlags;

}
