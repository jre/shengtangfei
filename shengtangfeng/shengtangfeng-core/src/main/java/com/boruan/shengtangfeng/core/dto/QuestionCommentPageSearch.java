package com.boruan.shengtangfeng.core.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionCommentPageSearch extends BasePageSearch {

    /**
     * 试题ID
     */
    @ApiModelProperty("试题ID")
    private Long questionId;
}
