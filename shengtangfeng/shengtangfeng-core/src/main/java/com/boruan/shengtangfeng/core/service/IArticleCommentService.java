package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.ArticleComment;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IArticleCommentService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param article
	 */
	public void pageQuery(PageQuery<ArticleComment> pageQuery, ArticleComment articleComment);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public ArticleComment findById(Long id);
	/**
	 * 保存分类
	 * @param  articleComment
	 */
	public void save(ArticleComment articleComment);
	
	/**
	  * 根据问题ID获取回复列表,自己的评论在最前面
	 * @param articleId
	 */
	public List<ArticleComment> findByArticleId(Long userId,Long articleId,Integer status);
	/**
	 * 根据条件查找
	 * @param  search
	 */
	public List<ArticleComment> template(ArticleComment search);
	
    void deleteComment(Long id, Long id1);

	public long getTodayCount(Long userId);

}
