package com.boruan.shengtangfeng.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 封装了一些采用HttpClient发送HTTP请求的方法
 */
public class HttpClientsUtil {

	private static Logger logger = LoggerFactory.getLogger(HttpClientsUtil.class);
    private static HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

	private HttpClientsUtil(){}

	/**
	 * 发送HTTP_GET请求
	 * @see 该方法会自动关闭连接,释放资源
	 * @param requestURL    请求地址(含参数)
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendGetRequest(String reqURL, String decodeCharset){
		long responseLength = 0;       //响应长度
		String responseContent = null; //响应内容
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();

		RequestConfig.Builder builder = RequestConfig.custom();
		RequestConfig config = builder.setSocketTimeout(30000)
				.setConnectTimeout(30000)
				.setConnectionRequestTimeout(30000)
				.setStaleConnectionCheckEnabled(true).build();

		HttpGet httpGet = new HttpGet(reqURL);           //创建org.apache.http.client.methods.HttpGet
		httpGet.setConfig(config);
		try{
			HttpResponse response = httpClient.execute(httpGet); //执行GET请求
			HttpEntity entity = response.getEntity();            //获取响应实体
			if(null != entity){
				responseLength = entity.getContentLength();
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity); //Consume response content
			}
		}catch(ClientProtocolException e){
			logger.error("该异常通常是协议错误导致,比如构造HttpGet对象时传入的协议不对(将'http'写成'htp')或者服务器端返回的内容不符合HTTP协议要求等,堆栈信息如下", e);
		}catch(ParseException e){
			logger.error(e.getMessage(), e);
		}catch(IOException e){
			logger.error("该异常通常是网络原因引起的,如HTTP服务器未启动等,堆栈信息如下", e);
		}finally{
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}


	/**
	 * 发送HTTP_POST请求
	 * @see 该方法为<code>sendPostRequest(String,String,boolean,String,String)</code>的简化方法
	 * @see 该方法在对请求数据的编码和响应数据的解码时,所采用的字符集均为UTF-8
	 * @see 当<code>isEncoder=true</code>时,其会自动对<code>sendData</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,"UTF-8")</code>
	 * @param isEncoder 用于指明请求数据是否需要UTF-8编码,true为需要
	 */
	public static String sendPostRequest(String reqURL, String sendData, boolean isEncoder){
		return sendPostRequest(reqURL, sendData, isEncoder, null, null);
	}


	/**
	 * 发送HTTP_POST请求
	 *  该方法会自动关闭连接,释放资源
	 *  当<code>isEncoder=true</code>时,其会自动对<code>sendData</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @param reqURL        请求地址
	 * @param sendData      请求参数,若有多个参数则应拼接成param11=value11¶m22=value22¶m33=value33的形式后,传入该参数中
	 * @param isEncoder     请求数据是否需要encodeCharset编码,true为需要
	 * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendPostRequest(String reqURL, String sendData, boolean isEncoder, String encodeCharset, String decodeCharset){
		String responseContent = null;
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();

		HttpPost httpPost = new HttpPost(reqURL);
		//httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
		httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
		try{
			if(isEncoder){
				List<NameValuePair> formParams = new ArrayList<NameValuePair>();
				for(String str : sendData.split("&")){
					formParams.add(new BasicNameValuePair(str.substring(0,str.indexOf("=")), str.substring(str.indexOf("=")+1)));
				}
				httpPost.setEntity(new StringEntity(URLEncodedUtils.format(formParams, encodeCharset==null ? "UTF-8" : encodeCharset)));
			}else{
				httpPost.setEntity(new StringEntity(sendData));
			}

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity);
			}
		}catch(Exception e){
			logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
		}finally{
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}

    /**
     * 带身份验证的Post请求
     * Body传参
     * @param url
     * @param charset
     * @param params
     * @return
     */
    public static String sendPostRequestWithAuth(String url, String charset, String params, String auth) {
        String response = null;
        //创建HttpClientBuilder
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        //HttpClient
        CloseableHttpClient client = httpClientBuilder.build();
        HttpPost httpPost = new HttpPost(url);
        if (!StringUtils.isRealBlank(auth)) {
            httpPost.setHeader("Authorization",auth);
        }
        charset = charset == null ? "UTF-8" : charset;
        if (!StringUtils.isRealBlank(params)) {
            StringEntity entity = new StringEntity(params, charset);
            entity.setContentEncoding(charset);
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
        }
        try {
            HttpResponse httpResponse = client.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                response = EntityUtils.toString(httpEntity, charset);
                EntityUtils.consume(httpEntity);
            }
        } catch (IOException e) {
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }


	/**
	 * 发送HTTP_POST请求
	 * @see 该方法会自动关闭连接,释放资源
	 * @see 该方法会自动对<code>params</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @param reqURL        请求地址
	 * @param params        请求参数
	 * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendPostRequest(String reqURL, Map<String, String> params, String encodeCharset, String decodeCharset){
		String responseContent = null;
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();

		HttpPost httpPost = new HttpPost(reqURL);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>(); //创建参数队列
		for(Map.Entry<String,String> entry : params.entrySet()){
			formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}
		try{
			httpPost.setEntity(new UrlEncodedFormEntity(formParams, encodeCharset==null ? "UTF-8" : encodeCharset));
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity);
			}
		}catch(Exception e){
			logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
		}finally{
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}


	/**
	 * 发送HTTPS_POST请求
	 * @see 该方法为<code>sendPostSSLRequest(String,Map<String,String>,String,String)</code>方法的简化方法
	 * @see 该方法在对请求数据的编码和响应数据的解码时,所采用的字符集均为UTF-8
	 * @see 该方法会自动对<code>params</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,"UTF-8")</code>
	 */
	public static String sendPostSSLRequest(String reqURL, Map<String, String> params){
		return sendPostSSLRequest(reqURL, params, null, null);
	}


	/**
	 * 发送HTTPS_POST请求
	 * @see 该方法会自动关闭连接,释放资源
	 * @see 该方法会自动对<code>params</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @param reqURL        请求地址
	 * @param params        请求参数
	 * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendPostSSLRequest(String reqURL, Map<String, String> params, String encodeCharset, String decodeCharset){
		String responseContent = "";
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();
		X509TrustManager xtm = new X509TrustManager(){
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public X509Certificate[] getAcceptedIssuers() {return null;}
		};
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[]{xtm}, null);
			SSLSocketFactory socketFactory = new SSLSocketFactory(ctx);
			httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 443, socketFactory));

			HttpPost httpPost = new HttpPost(reqURL);
			List<NameValuePair> formParams = new ArrayList<NameValuePair>();
			for(Map.Entry<String,String> entry : params.entrySet()){
				formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			httpPost.setEntity(new UrlEncodedFormEntity(formParams, encodeCharset==null ? "UTF-8" : encodeCharset));

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity);
			}
		} catch (Exception e) {
			logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息为", e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}

	/**
	 * 发送HTTPS_POST,类型为JSON的请求
	 * @see 该方法为<code>sendPostSSLRequest(String,String,String,String)</code>方法的简化方法
	 * @see 该方法在对请求数据的编码和响应数据的解码时,所采用的字符集均为UTF-8
	 * @see 该方法会自动对<code>params</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,"UTF-8")</code>
	 */
	public static String sendPostSSLRequest(String reqURL, String params){
		return sendPostSSLRequest(reqURL, params, null, null);
	}

	/**
	 * 发送HTTPS_POST，类型为JSON的请求
	 * @see 该方法会自动关闭连接,释放资源
	 * @see 该方法会自动对<code>params</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @param reqURL        请求地址
	 * @param params        请求参数
	 * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendPostSSLRequest(String reqURL, String params, String encodeCharset, String decodeCharset){
		String responseContent = "";
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();
		X509TrustManager xtm = new X509TrustManager(){
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public X509Certificate[] getAcceptedIssuers() {return null;}
		};
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[]{xtm}, null);
			SSLSocketFactory socketFactory = new SSLSocketFactory(ctx);
			httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 443, socketFactory));

			HttpPost httpPost = new HttpPost(reqURL);
			/*List<NameValuePair> formParams = new ArrayList<NameValuePair>();
			for(Map.Entry<String,String> entry : params.entrySet()){
				formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}*/
			httpPost.setEntity(new StringEntity(params,encodeCharset==null ? "UTF-8" : encodeCharset));
			httpPost.addHeader("content-type", "application/json");
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity);
			}
		} catch (Exception e) {
			logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息为", e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}


	/**
	 * 发送HTTPS_GET请求
	 * @see 该方法会自动关闭连接,释放资源
	 * @param requestURL    请求地址(含参数)
	 * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
	 * @return 远程主机响应正文
	 */
	public static String sendGetSSLRequest(String reqURL, String decodeCharset){
		long responseLength = 0;       //响应长度
		String responseContent = null; //响应内容
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();
		X509TrustManager xtm = new X509TrustManager(){
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
			public X509Certificate[] getAcceptedIssuers() {return null;}
		};

		try{
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[]{xtm}, null);
			SSLSocketFactory socketFactory = new SSLSocketFactory(ctx);
			httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 443, socketFactory));

			HttpGet httpGet = new HttpGet(reqURL);           //创建org.apache.http.client.methods.HttpGet
			HttpResponse response = httpClient.execute(httpGet); //执行GET请求
			HttpEntity entity = response.getEntity();            //获取响应实体
			if(null != entity){
				responseLength = entity.getContentLength();
				responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
				EntityUtils.consume(entity); //Consume response content
			}
		} catch (Exception e) {
			logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息为", e);
		}finally{
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
		}
		return responseContent;
	}


	/**
	 * 发送HTTP_POST请求
	 * @see 若发送的<code>params</code>中含有中文,记得按照双方约定的字符集将中文<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @see 本方法默认的连接超时时间为30秒,默认的读取超时时间为30秒
	 * @param reqURL 请求地址
	 * @param params 发送到远程主机的正文数据,其数据类型为<code>java.util.Map<String, String></code>
	 * @return 远程主机响应正文`HTTP状态码,如<code>"SUCCESS`200"</code><br>若通信过程中发生异常则返回"Failed`HTTP状态码",如<code>"Failed`500"</code>
	 */
	public static String sendPostRequestByJava(String reqURL, Map<String, String> params){
		StringBuilder sendData = new StringBuilder();
		for(Map.Entry<String, String> entry : params.entrySet()){
			sendData.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		if(sendData.length() > 0){
			sendData.setLength(sendData.length() - 1); //删除最后一个&符号
		}
		return sendPostRequestByJava(reqURL, sendData.toString());
	}


	/**
	 * 发送HTTP_POST请求
	 * @see 若发送的<code>sendData</code>中含有中文,记得按照双方约定的字符集将中文<code>URLEncoder.encode(string,encodeCharset)</code>
	 * @see 本方法默认的连接超时时间为30秒,默认的读取超时时间为30秒
	 * @param reqURL   请求地址
	 * @param sendData 发送到远程主机的正文数据
	 * @return 远程主机响应正文`HTTP状态码,如<code>"SUCCESS`200"</code><br>若通信过程中发生异常则返回"Failed`HTTP状态码",如<code>"Failed`500"</code>
	 */
	public static String sendPostRequestByJava(String reqURL, String sendData){
		HttpURLConnection httpURLConnection = null;
		OutputStream out = null; //写
		InputStream in = null;   //读
		int httpStatusCode = 0;  //远程主机响应的HTTP状态码
		try{
			URL sendUrl = new URL(reqURL);
			httpURLConnection = (HttpURLConnection)sendUrl.openConnection();
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setDoOutput(true);        //指示应用程序要将数据写入URL连接,其值默认为false
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setConnectTimeout(30000); //30秒连接超时
			httpURLConnection.setReadTimeout(30000);    //30秒读取超时

			out = httpURLConnection.getOutputStream();
			out.write(sendData.toString().getBytes());

			//清空缓冲区,发送数据
			out.flush();

			//获取HTTP状态码
			httpStatusCode = httpURLConnection.getResponseCode();

			//该方法只能获取到[HTTP/1.0 200 OK]中的[OK]
			//若对方响应的正文放在了返回报文的最后一行,则该方法获取不到正文,而只能获取到[OK],稍显遗憾
			//respData = httpURLConnection.getResponseMessage();

//			//处理返回结果
			BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
//			String row = null;
//			String respData = "";
//			if((row=br.readLine()) != null){ //readLine()方法在读到换行[\n]或回车[\r]时,即认为该行已终止
//				respData = row;              //HTTP协议POST方式的最后一行数据为正文数据
//			}
//			br.close();
			String line=null;
            StringBuffer sb=new StringBuffer();
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();

		}catch(Exception e){
			logger.error(e.getMessage());
			return "Failed`" + httpStatusCode;
		}finally{
			if(out != null){
				try{
					out.close();
				}catch (Exception e){
					logger.error("关闭输出流时发生异常,堆栈信息如下", e);
				}
			}
			if(in != null){
				try{
					in.close();
				}catch(Exception e){
					logger.error("关闭输入流时发生异常,堆栈信息如下", e);
				}
			}
			if(httpURLConnection != null){
				httpURLConnection.disconnect();
				httpURLConnection = null;
			}
		}
	}

	/**
	 * 获取客户端公网IP
	 * @param request
	 * @return
	 * cjh 2015年6月15日 上午10:15:12
	 */
	public static String getIpAddr(HttpServletRequest request) {
	    String ip = request.getHeader("x-forwarded-for");
	    if(ip == null || ip.length() == 0 ||"unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 ||"unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 ||"unknown".equalsIgnoreCase(ip)) {
	        ip = request.getRemoteAddr();
	    }
	    return ip;
	 }

    /**
     * 发送HTTP_POST请求
     *  该方法会自动关闭连接,释放资源
     *  当<code>isEncoder=true</code>时,其会自动对<code>sendData</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
     * @param reqURL        请求地址
     * @param sendData      请求参数,若有多个参数则应拼接成param11=value11¶m22=value22¶m33=value33的形式后,传入该参数中
     * @param isEncoder     请求数据是否需要encodeCharset编码,true为需要
     * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
     * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
     * @return 远程主机响应正文
     */
    public static String postRequest(String reqURL, String paramStr, String encodeCharset, String decodeCharset,String auth){
        String responseContent = null;
		//创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		//HttpClient
		CloseableHttpClient httpClient = httpClientBuilder.build();

        HttpPost httpPost = new HttpPost(reqURL);
        try{
            StringEntity urlEntity = new StringEntity(paramStr, Charset.forName("UTF-8"));
            httpPost.setEntity(urlEntity);

            httpPost.addHeader("Content-Type", "application/json");
            if(auth!= null){
                httpPost.addHeader("Authorization", auth);
            }
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
                EntityUtils.consume(entity);
            }
        }catch(Exception e){
            logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
            return null;
        }finally{
            // 关闭资源
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
        }
        return responseContent;
    }
    /**
     * 发送HTTP_POST请求
     *  该方法会自动关闭连接,释放资源
     *  当<code>isEncoder=true</code>时,其会自动对<code>sendData</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
     * @param reqURL        请求地址
     * @param sendData      请求参数,若有多个参数则应拼接成param11=value11¶m22=value22¶m33=value33的形式后,传入该参数中
     * @param isEncoder     请求数据是否需要encodeCharset编码,true为需要
     * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
     * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
     * @return 远程主机响应正文
     */
    public static String putRequest(String reqURL, String paramStr, String encodeCharset, String decodeCharset,String auth){
        String responseContent = null;
        //创建HttpClientBuilder
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        //HttpClient
        CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpPut httpPut = new HttpPut(reqURL);
        try{
            StringEntity urlEntity = new StringEntity(paramStr, Charset.forName("UTF-8"));
            httpPut.setEntity(urlEntity);

            httpPut.addHeader("Content-Type", "application/json");
            if(auth!= null){
                httpPut.addHeader("Authorization", auth);
            }
            HttpResponse response = httpClient.execute(httpPut);
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
                EntityUtils.consume(entity);
            }
        }catch(Exception e){
            logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
            return null;
        }finally{
            // 关闭资源
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.error("关闭httpClient时异常",e);
            }
        }
        return responseContent;
    }

    /**
     * 发送HTTP_POST请求
     *  该方法会自动关闭连接,释放资源
     *  当<code>isEncoder=true</code>时,其会自动对<code>sendData</code>中的[中文][|][ ]等特殊字符进行<code>URLEncoder.encode(string,encodeCharset)</code>
     * @param reqURL        请求地址
     * @param sendData      请求参数,若有多个参数则应拼接成param11=value11?m22=value22?m33=value33的形式后,传入该参数中
     * @param isEncoder     请求数据是否需要encodeCharset编码,true为需要
     * @param encodeCharset 编码字符集,编码请求数据时用之,其为null时默认采用UTF-8解码
     * @param decodeCharset 解码字符集,解析响应数据时用之,其为null时默认采用UTF-8解码
     * @return 远程主机响应正文
     */
    public static String postRequestBasic(String reqURL, String paramStr, String encodeCharset, String decodeCharset,String auth){
        String responseContent = null;
        //创建HttpClientBuilder
  		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
  		//HttpClient
  		CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpPost httpPost = new HttpPost(reqURL);
        try{
            StringEntity urlEntity = new StringEntity(paramStr, Charset.forName("UTF-8"));
            httpPost.setEntity(urlEntity);

            httpPost.addHeader("Content-Type", "application/json");
            if(auth!= null){
                httpPost.addHeader("Authorization", "Basic " + auth);
            }
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
                EntityUtils.consume(entity);
            }
        }catch(Exception e){
            logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
            return null;
        }finally{
        	// 关闭资源
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("关闭httpClient时异常",e);
			}
        }
        return responseContent;
    }


    /**
     * Get请求
     *
     * @param reqURL 请求地址
     * @param params 请求参数(map)
     * @param
     * @param decodeCharset 解码集（允许为空）
     * @param auth Authorization
     * @return 字符串
     */
    public static String getRequest(String reqURL, Map<String, String> params, String decodeCharset, String auth){
        CloseableHttpClient httpClient = httpClientBuilder.build();
        String responseContent = null;
        // 封装请求数据
        if(params != null && params.size() > 0){
            reqURL = reqURL + "?";
            int size = params.size();
            for(Map.Entry<String,String> entry : params.entrySet()){
                reqURL = reqURL + entry.getKey() + "=" + entry.getValue();
                if(size != 1){
                    reqURL = reqURL + "&";
                    size--;
                }
            }
        }

        // 初始化Get
        HttpGet httpGet = new HttpGet(reqURL);
        try{
            // 头信息
            httpGet.addHeader("Content-Type", "application/json");
            if(auth!= null){
                httpGet.addHeader("Authorization", auth);
            }
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
                EntityUtils.consume(entity);
            }
        }catch(Exception e){
            logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
            return null;
        }finally{
            try {
                // 关闭资源
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return responseContent;
    }
    /**
     * Get请求
     *
     * @param reqURL 请求地址
     * @param params 请求参数(map)
     * @param
     * @param decodeCharset 解码集（允许为空）
     * @param auth Authorization
     * @return 字符串
     */
    public static String getRequestLong(String reqURL, Map<String, Long> params, String decodeCharset, String auth){
        CloseableHttpClient httpClient = httpClientBuilder.build();
        String responseContent = null;
        // 封装请求数据
        if(params != null && params.size() > 0){
            reqURL = reqURL + "?";
            int size = params.size();
            for(Map.Entry<String,Long> entry : params.entrySet()){
                reqURL = reqURL + entry.getKey() + "=" + entry.getValue();
                if(size != 1){
                    reqURL = reqURL + "&";
                    size--;
                }
            }
        }

        // 初始化Get
        HttpGet httpGet = new HttpGet(reqURL);
        try{
            // 头信息
            httpGet.addHeader("Content-Type", "application/json");
            if(auth!= null){
                httpGet.addHeader("Authorization", auth);
            }
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                responseContent = EntityUtils.toString(entity, decodeCharset==null ? "UTF-8" : decodeCharset);
                EntityUtils.consume(entity);
            }
        }catch(Exception e){
            logger.error("与[" + reqURL + "]通信过程中发生异常,堆栈信息如下", e);
            return null;
        }finally{
            try {
                // 关闭资源
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return responseContent;
    }
}
