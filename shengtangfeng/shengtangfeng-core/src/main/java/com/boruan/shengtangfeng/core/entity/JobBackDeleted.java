package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

/**
 * 作业补交
 */
@Table(name="job_back_deleted")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class JobBackDeleted extends IdEntity {

    /**
    * 补交作业表id
    **/
    private Long jobBackId ;
            
    /**
    * 用户id
    **/
    private Long userId ;

    /**
     * 是否已读
     */
    private Integer isRead ;

    /**
     * 是否删除申请数据
     */
    private Integer isDeletedApply ;

}