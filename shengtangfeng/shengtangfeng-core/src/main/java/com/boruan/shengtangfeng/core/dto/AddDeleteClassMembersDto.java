package com.boruan.shengtangfeng.core.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 邀请加入班级和删除班级成员的请求体
 */
@Data
public class AddDeleteClassMembersDto {

    @ApiModelProperty(value="id集合")
    private List<Long> ids;

    @ApiModelProperty(value="班级Id")
    private Long classId;
}
