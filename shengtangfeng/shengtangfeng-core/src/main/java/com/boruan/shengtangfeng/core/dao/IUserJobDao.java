package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.UserJob;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IUserJobDao extends BaseMapper<UserJob> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<UserJob> query);

    void scoreRank(PageQuery<UserJob> pageQuery);

    void speedRank(PageQuery<UserJob> pageQuery);

    void submitRank(PageQuery<UserJob> pageQuery);

    /**
     * 教师端排行榜查询数据
     * @param query
     */
    void getRankTeacher(PageQuery<UserJob> query);

}