package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.enums.QuestionType;

import com.boruan.shengtangfeng.core.utils.QuestionOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionVo extends BaseVo {

	/**
	 * 类型
	 **/
	@ApiModelProperty("类型")
	private QuestionType type;
	/**
	 * 答案
	 **/
	@ApiModelProperty("答案")
	private String answer;


	/**
	 * 选项
	 **/
	@ApiModelProperty(name = "选项,json串，见备注", notes = "[{\"content\":\"1\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"2\",\"isCorrect\":true,\"type\":\"text\"},{\"content\":\"3\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"4\",\"isCorrect\":false,\"type\":\"text\"}]")
	private String content;

	/**
	 * 选择题 题目集合
	 */
	@ApiModelProperty(value = "选择题选项")
	private List<QuestionOption> optionList;

	/**
	 * 文字解释
	 **/
	@ApiModelProperty("文字解释")
	private String textExplain;

	/**
	 * 题干
	 **/
	@ApiModelProperty("题干")
	private String title;


	/**
	 * 正确率
	 **/
	@ApiModelProperty("正确率")
	private String accurate;
	/**
     * 学科ID
     **/
	@ApiModelProperty("学科ID")
    private Long subjectId;
	/**
	 * 模块ID，仅用于搜索试题
	 **/
	@ApiModelProperty("模块ID，仅用于搜索试题")
	private Long moduleId;
	/**
	 * 试题学科
	 **/
	@ApiModelProperty("试题学科")
	private String subjectName;
	/**
	 * 是否收藏
	 **/
	@ApiModelProperty("是否收藏")
	private Boolean collect;
	/**
	 * 分数
	 */
	@ApiModelProperty("分数")
	private Integer score;
	
	@ApiModelProperty(value = "搜索命中类型 0 所有  1 题干 2 内容")
	private Integer hitType;
	@ApiModelProperty(value = "试题评论")
	private List<QuestionCommentVo> comment;
	@ApiModelProperty(value = "年级ID集合")
	private List<Long> gradeIds;
}


