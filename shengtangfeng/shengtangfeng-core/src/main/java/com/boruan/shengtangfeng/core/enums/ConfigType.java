package com.boruan.shengtangfeng.core.enums;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import org.beetl.sql.core.annotatoin.EnumMapping;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
@author: liuguangqiang
@Description: 报名状态
@date:2020年3月10日 上午11:14:10
*/
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ConfigType implements CommonEnum {
	NORMAL(1, "普通文本"),
	FUWENBEN(2, "富文本"),
	IMAGE(3, "图片"),
	ONOFF(4, "开关")
	;
	@ApiModelProperty(value="1 普通文本 2 富文本 3图片 4 开关")
	private Integer value;
	private String name;

	private ConfigType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

