package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SmallVideoPageSearch extends BasePageSearch {

    @ApiModelProperty("市名称")
    private String address;

    @ApiModelProperty(value="经度")
    private String longitude;

    @ApiModelProperty(value=" 纬度")
    private String latitude;

    @ApiModelProperty("0推荐 1关注 2同城")
    private Integer videoType;

    @ApiModelProperty("手机号集合")
    private List<String> mobileList;
}
