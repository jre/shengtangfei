package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="favorites")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Favorites extends IdEntity {


    /**
     * 文章/视频id
     **/
    private Long articleId ;


    /**
     * 类别1文章2视频
     **/
    private Integer type ;


    /**
     * 用户id
     **/
    private Long userId ;
}