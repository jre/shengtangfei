package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@author: 刘光强
@Description: 
@date:2020年3月10日 上午11:14:10
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("获取验证码参数")
public class GetMobileCodeDto{
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("1用户注册验证码, 2登录确认验证码, 3修改密码验证码  4 绑定 5修改手机号")
    private Integer type;
    
    public boolean checkParam(){
        if(type==null||(type!=5&&StringUtils.isBlank(mobile))) {
            return false;
        }else {
            return true;
        }
    }
}
