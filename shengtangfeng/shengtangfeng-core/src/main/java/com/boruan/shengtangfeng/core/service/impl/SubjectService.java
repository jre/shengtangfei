package com.boruan.shengtangfeng.core.service.impl;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IGradeSubjectDao;
import com.boruan.shengtangfeng.core.dao.ISubjectDao;
import com.boruan.shengtangfeng.core.entity.GradeSubject;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.service.ISubjectService;
/**
 * 
 * @author 刘光强
 * @date 2020年8月12日16:10:49
 */
@Service
@Transactional(readOnly = true)
public class SubjectService implements ISubjectService{

	@Autowired
	private ISubjectDao subjectDao;
	@Autowired
	private IGradeSubjectDao gradeSubjectDao;
	
	
	@Override
    public Subject findById(Long id) {
        return subjectDao.single(id);
    }

    @Override
	@Transactional(readOnly = false)
	public void save(Subject subject) {
		if (subject.getId()==null) {
			KeyHolder kh=subjectDao.insertReturnKey(subject);
			subject.setId(kh.getLong());
		}else{
			subjectDao.updateById(subject);
		}
	}



	@Override
	public void pageQuery(PageQuery<Subject> pageQuery, Subject subject) {
		pageQuery.setParas(subject);
		subjectDao.pageQuery(pageQuery);
	}



    @Override
    public List<Subject> getList(Subject subject) {
        return subjectDao.getList(subject);
    }



    @Override
    public List<Subject> getListByGradeId(Long gradeId) {
        return subjectDao.getListByGradeId(gradeId);
    }
    @Override
    public List<Subject> getListByGradeIds(Long[] gradeIds) {
        return subjectDao.getListByGradeIds(gradeIds);
    }



    @Override
    public Subject get(Long id) {
        return subjectDao.single(id);
    }



    @Override
    public List<Subject> getIncorrectSubject(Long userId,int type) {
        return subjectDao.getIncorrectSubject(userId,type);
    }
    @Override
    public List<Subject> getFavoritesSubject(Long userId) {
        return subjectDao.getFavoritesSubject(userId);
    }

    @Override
    @Transactional
    public void addGrade(Long subjectId, Long[] gradeIds) {
        for (Long gradeId : gradeIds) {
            GradeSubject gradeSubject=new GradeSubject();
            gradeSubject.setGradeId(gradeId);
            gradeSubject.setSubjectId(subjectId);
            long count=gradeSubjectDao.templateCount(gradeSubject);
            if(count==0) {
                gradeSubjectDao.insert(gradeSubject);
            }
        }
    }
    @Override
    @Transactional
    public void deleteGrade(Long subjectId, Long gradeId) {
            gradeSubjectDao.deleteGrade(subjectId, gradeId);
    }

}
