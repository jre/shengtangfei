package com.boruan.shengtangfeng.core.dao;
import com.boruan.shengtangfeng.core.entity.ClassAnd;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IClassAndDao extends BaseMapper<ClassAnd> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ClassAnd> query);

    public List<ClassAnd> getClassByUid(Long userId);
}