package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.ModuleVideoDto;
import com.boruan.shengtangfeng.core.entity.ModuleVideo;
import org.beetl.sql.core.engine.PageQuery;

public interface IModuleVideoService {


    public void getModuleVideo(PageQuery<ModuleVideo> pageData);

    public void pageQueryByModule(PageQuery<ModuleVideo> pageData,ModuleVideoDto moduleVideoDto);

    public void addVideo(ModuleVideoDto moduleVideoDto);
}
