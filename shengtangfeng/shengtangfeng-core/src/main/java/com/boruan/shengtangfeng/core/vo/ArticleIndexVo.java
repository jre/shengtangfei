package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.Article;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="文章首页集合")
public class ArticleIndexVo extends BaseVo{

    /**
     * 名称
     */
    @ApiModelProperty(value="文章分页")
    private PageQuery<Article> pageQuery;
    /**
     * 排序
     */
    @ApiModelProperty(value="类别")
    private List<UserCategoryVo> userCategoryVoList;




	
}
