package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassJob;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IClassJobDao extends BaseMapper<ClassJob> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ClassJob> query);
}