package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author: guojiang
 @Description:群资料
 @date:2021/04/07
 */

@Data
public class GroupDataDto {

    /**
     * 群聊头像
     **/
    @ApiModelProperty("群聊头像")
    private String head ;

    /**
     * 群介绍
     **/
    @ApiModelProperty("群介绍")
    private String introduce ;

    /**
     * 群聊名称
     **/
    @ApiModelProperty("群聊名称")
    private String name ;

    /**
     * 群公告
     **/
    @ApiModelProperty("群公告")
    private String notice ;
}
