package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.MusicCategory;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IMusicCategoryDao extends BaseMapper<MusicCategory> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<MusicCategory> query);
}