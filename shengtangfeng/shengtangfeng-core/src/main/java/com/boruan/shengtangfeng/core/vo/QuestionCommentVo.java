package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionCommentVo extends BaseVo {

	/**
	 * 问题ID
	 */
	@ApiModelProperty("问题ID")
	private Long questionId;
	/**
	 * 发布人ID
	 */
	@ApiModelProperty("发布人ID")
	private Long userId;
	/**
	 * 发布人头像
	 */
	@ApiModelProperty("发布人头像")
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	@ApiModelProperty("发布人姓名")
	private String userName;
	/**
	 * 内容
	 */
	@ApiModelProperty("内容")
	private String content;
	/**
	 * 状态 0未审核 1已审核 2审核失败
	 */
	@ApiModelProperty("状态 0未审核 1已审核 2审核失败")
	private Integer status;
	
	/**
     * 语音地址
     */
	@ApiModelProperty("语音地址")
    private String voice;
    /**
     * 逗号分隔的图片
     */
	@ApiModelProperty("逗号分隔的图片")
    private String images;
	
	/**
     * 类型  1文字，2文字+图片，3语音，4语音+图片，5图片
     */
	@ApiModelProperty("类型  1文字，2文字+图片，3语音，4语音+图片，5图片")
    private Integer type;
    /**
     * 语音时间，秒数
     */
	@ApiModelProperty("语音时间，秒数")
    private Integer voiceTime;
	
}
