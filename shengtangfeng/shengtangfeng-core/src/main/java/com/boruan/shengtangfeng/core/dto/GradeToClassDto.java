package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 年级展开
 */
@Data
public class GradeToClassDto {

    @ApiModelProperty("班级id")
    private Long id;

    @ApiModelProperty("班级名称")
    private String name ;

    @ApiModelProperty("班级号")
    private String classNum ;

    @ApiModelProperty("教师人数")
    private Long teacherNum ;

    @ApiModelProperty("学生人数")
    private Long studentNum ;

    @ApiModelProperty("创建人")
    private String userNickname ;
}
