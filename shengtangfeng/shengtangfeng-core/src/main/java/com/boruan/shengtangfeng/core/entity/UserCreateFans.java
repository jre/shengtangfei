package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="user_create_fans")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCreateFans extends IdEntity {
    
            
            
    /**
    * 粉丝数量 
    **/
    private Integer fansNum ;
            
    /**
    * 是否执行 
    **/
    private Integer isExecute ;

            
    /**
    *
    **/
    private Long userId ;

}