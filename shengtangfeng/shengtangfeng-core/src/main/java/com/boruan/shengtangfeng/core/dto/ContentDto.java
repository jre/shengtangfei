package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ContentDto {

    @ApiModelProperty(value="选项内容")
    private String content;

    @ApiModelProperty(value="是否是正确的答案")
    private Boolean  isCorrect;

    @ApiModelProperty(value="类型为text")
    private String type;

}
