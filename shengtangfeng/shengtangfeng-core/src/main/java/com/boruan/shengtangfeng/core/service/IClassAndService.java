package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.*;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

public interface IClassAndService {

    public List<Job> last3Jobs(Long classId,Long userId);

    public ClassAnd findById(Long classId);

    public Long countTeacher(Long classId);

    public Long countStudent(Long classId);

    public List<ClassMembers> teacherList(Long classId);

    public List<ClassMembers> studentList(Long classId);

    public List<Video> last4StudentVideo(Long classId);

    public ClassMembers findClassMember(Long userId,Long classId);

    public void pageQueryClassInform(PageQuery<Inform> pageQuery);

    Job findJobById(Long jobId);

    void scoreRank(PageQuery<UserJob> pageQuery);

    void speedRank(PageQuery<UserJob> pageQuery);

    void submitRank(PageQuery<UserJob> pageQuery);

    void insert(ClassMembers join);
}
