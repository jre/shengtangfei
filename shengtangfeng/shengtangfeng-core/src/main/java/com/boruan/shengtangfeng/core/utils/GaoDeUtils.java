package com.boruan.shengtangfeng.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.math.BigDecimal;
import java.net.URL;

public class GaoDeUtils {

    private static double EARTH_RADIUS = 6371000;//赤道半径(单位m)

    private static double rad(double d) {
        return d * Math.PI / 180.0;

    }

    /**
     * @描述 经纬度获取距离，单位为米
     * @参数  [lat1, lng1, lat2, lng2]
     * @返回值  double
     * @创建人  huyuan
     * @创建时间  2019/3/13 20:33
     **/
    public static double getDistance(double lon1,double lat1,double lon2, double lat2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lon1) - rad(lon2);
        double s = 2 *Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2)+Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        return s/1000;//单位千米
    }

    /**
     *根据经纬度获取省市区
     * @param log
     * @param lat
     * @return
     */
    public static String getAdd(String log, String lat ) {
        //lat 小  log  大
        //参数解释: 纬度,经度 采用高德API可参考高德文档https://lbs.amap.com/
        String key = "1c7aedcb37df86affbd18d17fe9b9ab5";
        String urlString = "https://restapi.amap.com/v3/geocode/regeo?location=" + log + "," + lat + "&extensions=base&batch=false&roadlevel=0&key=" + key;
        String res = "";
        try {
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line + "\n";
            }
            in.close();
            //解析结果
            JSONObject jsonObject = JSONObject.parseObject(res);
            //logger.info(jsonObject.toJSONString());
            JSONObject jsonObject1 = jsonObject.getJSONObject("regeocode");
            res = jsonObject1.getString("formatted_address");
        } catch (Exception e) {
            //logger.error("获取地址信息异常{}", e.getMessage());
            return null;
        }
        return res;
    }

    public static void main(String[] args) {
        double distance;
        //distance = getDistance(120.046190, 35.872664, 120.153728, 35.963228);
        distance = getDistance(120.154896, 35.963673, 120.153728, 35.963228);
        System.out.println(distance);
        System.out.println(new BigDecimal(distance).setScale(2, BigDecimal.ROUND_HALF_UP));
//        BigDecimal bigDecimal = new BigDecimal(distance).setScale(0);
//        System.out.println(bigDecimal);
//        System.out.println("Distance is:" + distance);
//        BigDecimal bigDecimal = new BigDecimal(distance);
//        System.out.println(bigDecimal.multiply(new BigDecimal(1000)).setScale(2));


//        String add = getAdd("120.046190", "35.872664");
//        System.out.println(add);

    }
}
