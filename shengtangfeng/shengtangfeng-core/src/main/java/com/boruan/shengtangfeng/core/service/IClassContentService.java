package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.ClassContentDto;
import com.boruan.shengtangfeng.core.dto.InformDto;
import com.boruan.shengtangfeng.core.dto.QuestionDetailDto;
import com.boruan.shengtangfeng.core.dto.StudentAnswerDto;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

/**
 @author: guojiang
 @Description:班级内容
 @date:2021/4/28
 */
public interface IClassContentService {
    /**
     * 多条件查询题目列表
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    GlobalReponse<PageQuery<QuestionDetailDto>> getQuestionPage(Integer pageNo, Integer pageSize, ClassContentDto dto);

    /**
     * 审核题目 1通过 2拒绝
     * @param questionId
     * @param status
     * @param userId
     * @return
     */
    GlobalReponse auditQuestion(Long questionId, Integer status, Long userId);

    /**
     * 批量或者单个删除题目
     * @param questionIds
     * @param userId
     * @return
     */
    GlobalReponse deletedQuestion(Long[] questionIds, Long userId);

    /**
     * 批量封号或者批量解封
     * @param createIds
     * @param status
     * @param userId
     * @return
     */
    GlobalReponse isLockedUser(Long[] createIds, Integer status, Long userId);

    /**
     * 多条件查询班级通知列表
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    GlobalReponse<PageQuery<InformDto>> getInformPage(Integer pageNo, Integer pageSize, ClassContentDto dto);

    /**
     * 审核通知
     * @param informId
     * @param status
     * @param userId
     * @return
     */
    GlobalReponse auditInform(Long informId, Integer status, Long userId);

    /**
     * 批量或者单个删除班级通知
     * @param informIds
     * @param userId
     * @return
     */
    GlobalReponse deletedInform(Long[] informIds, Long userId);

    /**
     * 多条件查询学生答题列表
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    GlobalReponse<PageQuery<StudentAnswerDto>> getStudentAnswer(Integer pageNo, Integer pageSize, ClassContentDto dto);

    /**
     * 批量或者单个删除学生答题
     * @param questionRecordIds
     * @param userId
     * @return
     */
    GlobalReponse deletedStudentAnswer(Long[] questionRecordIds, Long userId);

    /**
     * 审核学生答题
     * @param questionRecordId
     * @param status
     * @param userId
     * @return
     */
    GlobalReponse auditStudentAnswer(Long questionRecordId, Integer status, Long userId);
}
