package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.GroupMembers;
import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;
import java.util.List;


@Data
public class GroupChatDetailVo{

    @ApiModelProperty(value="群聊名称")
    private String name ;

    @ApiModelProperty(value="群聊头像")
    private String head ;

    @ApiModelProperty(value="群号")
    private String groupNum ;

    @ApiModelProperty(value="类型 0公开群 1班级关联群")
    private Integer groupType ;

    @ApiModelProperty(value="群介绍")
    private String introduce ;

    @ApiModelProperty(value="群公告")
    private String notice ;

    @ApiModelProperty(value="修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    protected Date updateTime;

    @ApiModelProperty(value="修改人")
    private String updateUsername ;

    @ApiModelProperty(value="修改人头像")
    private String updateHead;

    @ApiModelProperty(value="群成员")
    private List<UserRelevanceVo> groupMembers ;

    @ApiModelProperty(value="我在本群的昵称")
    private String nickName ;

    @ApiModelProperty(value="我在本群的身份类型 0普通成员 1管理员 2群主")
    private Integer myType ;

    @ApiModelProperty(value="加群方式 0禁止加群 ，1管理员审核，2不需要审核 ")
    private Integer addWay ;

            

}