package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserAnswerDto {

    /**
     * 问题ID
     */
    @ApiModelProperty("问题ID")
    private Long questionId;
    /**
     *  用户的答案
     */
    @ApiModelProperty("用户的答案")
    private String userAnswer;

    @ApiModelProperty("班级Id")
    private Long classId;

    @ApiModelProperty(value = "用时")
    private Integer useTime;

    @ApiModelProperty(value = "作业id")
    private Long jobId;
}
