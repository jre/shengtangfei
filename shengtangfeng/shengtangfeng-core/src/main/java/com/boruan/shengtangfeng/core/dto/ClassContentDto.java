package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 班级内容的搜索条件
 */
@Data
public class ClassContentDto {

    @ApiModelProperty("用户Id")
    private Long userId ;

    @ApiModelProperty("状态 0待审核 1审核通过 2审核拒绝")
    private Integer status ;

    @ApiModelProperty("题目类型 0单选 1主观 2线下")
    private Integer type ;

    @ApiModelProperty("用户昵称")
    private String userNickname ;

    @ApiModelProperty("内容")
    private String content ;
}
