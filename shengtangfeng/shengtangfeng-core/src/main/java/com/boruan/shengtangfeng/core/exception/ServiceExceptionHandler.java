package com.boruan.shengtangfeng.core.exception;


import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理器
 */
@RestControllerAdvice
public class ServiceExceptionHandler {

        private Logger logger = LoggerFactory.getLogger(getClass());

        /**
         * 处理自定义异常
         */
        @ExceptionHandler(ServicesException.class)
        public GlobalReponse handleRRException(ServicesException e){
            return GlobalReponse.fail(e.getMessage()).setCode(e.getCode());
        }

        @ExceptionHandler(DuplicateKeyException.class)
        public GlobalReponse handleDuplicateKeyException(DuplicateKeyException e){
            logger.error(e.getMessage(), e);
            return GlobalReponse.fail("数据库中已存在该记录");
        }

        @ExceptionHandler(Exception.class)
        public GlobalReponse handleException(Exception e){
            logger.error(e.getMessage(), e);
            return GlobalReponse.fail(e.getMessage());
        }

        @ExceptionHandler(IllegalArgumentException.class)
        public GlobalReponse illegalArgumentException(IllegalArgumentException e){
            logger.error(e.getMessage(), e);
            return GlobalReponse.fail("参数不能为空");
        }
    }

