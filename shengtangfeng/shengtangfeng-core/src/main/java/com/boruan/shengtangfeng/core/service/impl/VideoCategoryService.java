package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IVideoCategoryDao;
import com.boruan.shengtangfeng.core.entity.VideoCategory;
import com.boruan.shengtangfeng.core.service.IVideoCategoryService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author KongXH
 * @title: VideoCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1315:32
 */
@Service
@Transactional(readOnly = true)
public class VideoCategoryService implements IVideoCategoryService {


    @Autowired
    private IVideoCategoryDao videoCategoryDao;

    /**
     * @description: 分页
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public void pageQuery(PageQuery<VideoCategory> query) {

    }

    /**
     * @description: 获取全部
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public List<VideoCategory> getList(VideoCategory videoCategory) {
        return videoCategoryDao.getList(videoCategory);
    }


    /**
     * @description: 获取默认
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 15:33
     */
    @Override
    public List<VideoCategory> getVideoCategoryDefault(Integer type) {
        return videoCategoryDao.getVideoCategoryDefault();
    }

}
