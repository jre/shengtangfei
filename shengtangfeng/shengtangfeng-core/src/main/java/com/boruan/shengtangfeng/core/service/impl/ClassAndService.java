package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.*;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.service.IClassAndService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ClassAndService implements IClassAndService {
    @Autowired
    private IJobDao jobDao;
    @Autowired
    private IClassAndDao classAndDao;
    @Autowired
    private IClassMembersDao classMembersDao;
    @Autowired
    private IVideoDao videoDao;
    @Autowired
    private IInformDao informDao;
    @Autowired
    private IUserJobDao userJobDao;


    @Override
    public List<Job> last3Jobs(Long classId,Long userId) {
        List<Job> list = jobDao.getLast3Jobs(classId,userId);
        return list;
    }

    @Override
    public ClassAnd findById(Long classId) {
        ClassAnd classAnd = classAndDao.unique(classId);
        return classAnd;
    }

    @Override
    public Long countTeacher(Long classId) {
        List<Integer> list = Arrays.asList(2,3);
        Long count = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId,classId).andIn(ClassMembers::getType,list).andEq(ClassMembers::getIsDeleted,false).count();
        return count;
    }

    @Override
    public Long countStudent(Long classId) {
        Long count = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId,classId).andEq(ClassMembers::getType,1).andEq(ClassMembers::getIsDeleted,false).count();
        return count;
    }

    @Override
    public List<ClassMembers> teacherList(Long classId) {
        List<ClassMembers> list = classMembersDao.getTeachers(classId);
        return list;
    }

    /**
     * 查询班级的学生列表
     * @param classId
     * @return
     */
    @Override
    public List<ClassMembers> studentList(Long classId) {
        List<ClassMembers> list = classMembersDao.getStudents(classId);
        return list;
    }

    /**
     * 查询班级最新的四条讲一讲
     * @param classId
     * @return
     */
    @Override
    public List<Video> last4StudentVideo(Long classId) {
        List<Video> list = videoDao.getLast4StudentVideo(classId);
        return list;
    }

    @Override
    public ClassMembers findClassMember(Long userId, Long classId) {
        ClassMembers classMembers = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId,classId).andEq(ClassMembers::getUserId,userId).andEq(ClassMembers::getIsDeleted,false).single();
        return classMembers;
    }

    @Override
    public void pageQueryClassInform(PageQuery<Inform> pageQuery) {
        informDao.pageQueryByClass(pageQuery);
    }

    @Override
    public Job findJobById(Long jobId) {
        return jobDao.unique(jobId);
    }

    @Override
    public void scoreRank(PageQuery<UserJob> pageQuery) {
        userJobDao.scoreRank(pageQuery);
    }

    @Override
    public void speedRank(PageQuery<UserJob> pageQuery) {
        userJobDao.speedRank(pageQuery);
    }

    @Override
    public void submitRank(PageQuery<UserJob> pageQuery) {
        userJobDao.submitRank(pageQuery);
    }

    @Override
    @Transactional(readOnly = false)
    public void insert(ClassMembers join) {
        classMembersDao.insert(join);
    }
}
