package com.boruan.shengtangfeng.core.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="文章主体")
public class ArticleVo extends BaseVo{

    /**
     * 分类ID
     */
    @ApiModelProperty(value="分类ID")
    private Long categoryId;

    /**
     * 用户展示ID
     */
    @ApiModelProperty(value="用户展示ID")
    private String account;

    /**
     * 分类名称
     */
    @ApiModelProperty(value="分类名称")
    private String categoryName;

    /**
     * 文章标题
     */
    @ApiModelProperty(value="文章标题")
    private String title;
    
    /**
     * 文章内容
     */
    @ApiModelProperty(value="文章内容")
    private String content;
    
    /**
     * 发布时间
     */
    @ApiModelProperty(value="发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date publishDate;
    /**
     * 发布人
     */
    @ApiModelProperty(value="发布人")
    private String publisher;
    /**
     * 发布人头像
     */
    @ApiModelProperty(value="发布人头像")
    private String publisherIcon;
    /**
     * 原文链接
     */
    @ApiModelProperty(value="原文链接")
    private String link;
    /**
     * 状态 0 审核中 1 通过 2 不通过
     */
    @ApiModelProperty(value="状态 0 审核中 1 通过 2 不通过")
    private Integer status;
    /**
	 * 已读人数
	 */
    @ApiModelProperty(value="已读人数")
	private Integer readNum;
	/**
	 * 评论人数
	 */
    @ApiModelProperty(value="评论人数")
	private Integer commentNum;
    /**
	 *  是否推荐
	 */
    @ApiModelProperty(value="是否推荐")
	private Boolean recommend;
    /**
     *  相关文章
     */
    @ApiModelProperty(value="相关文章")
    private List<ArticleVo> closely;


    @ApiModelProperty(value="相关评论")
    private List<ArticleCommentVo> comment;

    @ApiModelProperty(value="类别名")
    private List<UserCategoryVo> userCategoryVo;


    /**
     *  内容中的图，用英文逗号分隔
     */
    @ApiModelProperty(value="内容中的图，用英文逗号分隔")
    private String images;
    /**
     *  内容中的图分隔好的
     */
    @ApiModelProperty(value="内容中的图分隔好的")
    private String[] imageList;
    
    /**
     * 新闻摘要，显示在列表
     */
    @ApiModelProperty(value="新闻摘要，显示在列表")
    private String summary;
    
    /**
     *  图数量 1 无图  2 一张图 3 多于一张
     */
    @ApiModelProperty(value="图数量 1 无图  2 一张图 3 多于一张")
    private Integer type;

    @ApiModelProperty(value="发布者id")
    private Long publishId;//

    @ApiModelProperty(value="邀请码")
    private String invitationCode;//

    @ApiModelProperty(value="是否关注")
    private Boolean isAttention;//

    @ApiModelProperty(value="是否收藏")
    private Boolean isFavorites;//

    @ApiModelProperty(value="是否点赞")
    private Boolean isLike;//
    
    /**
     * 点赞人数
     */
    @ApiModelProperty(value="点赞人数")
    private Integer thumpUpNum;
    /**
     *  审核备注
     */
    @ApiModelProperty(value="审核备注")
    private String remark;
    /**
     *  上传类型0 系统  1用户
     */
    @ApiModelProperty(value="上传类型0 系统  1用户")
    private Integer uploadType;
	
}
