package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.QuestionRecord;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.models.auth.In;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IQuestionRecordService {

	/**
	 * 保存
	 * @param  questionRecord
	 */
	public void save(QuestionRecord questionRecord);
	/**
	 * 根据条件查询问题记录
	 * @param  questionRecord
	 */
	public List<QuestionRecord> template(QuestionRecord questionRecord);
	/**
	 * 根据条件查询问题shu
	 * @param  questionRecord
	 */
	public long templateCount(QuestionRecord questionRecord);


	QuestionRecord hasAnswer(QuestionRecord record);

    List<QuestionRecord> myRecordByJob(Long jobId, Long userId, Integer type);

    QuestionRecord findByCondition(QuestionRecord questionRecord);

	/**
	 * 完成作业
	 * @param jobId
	 * @return
	 */
	GlobalReponse finishJobPush(Long jobId,Long userId,Long useTime,Long classId);
}
