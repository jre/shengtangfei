package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentAnswerVo {

    @ApiModelProperty(value = "题目序号")
    private Long serialNumber ;

    @ApiModelProperty(value = "题目Id")
    private Long questionId ;

    @ApiModelProperty(value = "题目类型")
    private QuestionType type ;

    @ApiModelProperty(value = "是否正确")
    private Boolean isRight ;

    @ApiModelProperty(value = "是否已达")
    private Boolean isAnswer ;

    @ApiModelProperty(value = "是否批改")
    private Boolean isCheck ;

    @ApiModelProperty(value = "成绩")
    private BigDecimal score ;

    @ApiModelProperty(value = "用时")
    private Long time ;

}
