package com.boruan.shengtangfeng.core.enums;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import org.beetl.sql.core.annotatoin.EnumMapping;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
@author: liuguangqiang
@Description: 报名状态
@date:2020年3月10日 上午11:14:10
*/
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PermissionLevel implements CommonEnum {
    PARENT(2, "菜单组"),
    CHILD(3, "菜单"),
    BUTTON(4, "按钮")
	;
	@ApiModelProperty(value="2 菜单组 3菜单 4按钮")
	private Integer value;
	private String name;

	private PermissionLevel(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

