package com.boruan.shengtangfeng.core.entity;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import org.beetl.sql.core.annotatoin.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Table(name="subject")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Subject extends IdEntity {
    
            
    /**
    * 学科名 
    **/
    @ApiModelProperty("学科名")
    private String name ;
    /**
     *  图标
     **/
    @ApiModelProperty("图标")
    private String icon ;
    /**
     * 排序 
     **/
    @ApiModelProperty("排序")
    private Integer sort ;

    /**
     * 是否默认1是0否
     **/
    private Boolean isDefault ;
}