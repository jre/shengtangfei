package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.QuestionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 收藏记录表
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "favorites_question")
@EqualsAndHashCode(callSuper = false)
public class FavoritesQuestion extends IdEntity{

	private static final long serialVersionUID = 6335675770371435246L;
	/**
	 *  用户 Id
	 */
	private Long userId;
	/**
	 *  试题Id
	 */
	private Long questionId;
	/**
     * 类型
     */
    private QuestionType type;
    /**
     * 模块ID
     */
    private Long moduleId;
    /**
     * 年级ID
     */
    private Long gradeId;
    /**
     * 年级名
     */
    private String gradeName;
    /**
     * 学科ID
     */
    private Long subjectId;
    /**
     * 学科名
     */
    private String subjectName;

    /**
     * 题库类型 1淘学题库 2共享题库 3作业题库
     */
    private Integer backType;
    
}
