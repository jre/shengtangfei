package com.boruan.shengtangfeng.core.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AdvertiseVo extends BaseVo {

    /**
     * 点击数
     **/
    @ApiModelProperty("点击数")
    private Integer clickCount;

    /**
     * 排序
     **/
    @ApiModelProperty("排序")
    private Integer sort;

    /**
     * 上下线状态：0->下线；1->上线
     **/
    @ApiModelProperty("上下线状态：0->下线；1->上线")
    private Integer status;

    /**
     * 图片地址
     **/
    @ApiModelProperty("图片地址")
    private String image;

    /**
     * 标题
     **/
    @ApiModelProperty("标题")
    private String title;

    

    /**
     * 轮播位置：0->首页轮播
     **/
    @ApiModelProperty("轮播位置：0->首页轮播")
    private Integer position;

    /**
     * 链接类型 1 富文本  2 webview 3 文章详情  4 视频详情
     **/
    @ApiModelProperty("链接类型 1 富文本  2 webview 3 文章详情  4 视频详情")
    private Integer linkType;
    
    /**
     * 链接地址或者富文本内容
     **/
    @ApiModelProperty("链接地址|富文本内容|文章ID|视频ID")
    private String content;
    /**
     * 发布人头像
     **/
    @ApiModelProperty("发布人头像")
    private String icon;
    /**
     * 发布人
     **/
    @ApiModelProperty("发布人")
    private String publisher;
    
    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date endTime;
}
