package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="accolade")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Accolade extends IdEntity {

            
    /**
    * 文章/视频id /评论id
    **/
    private Long articleId ;
            
    /**
    *
    **/
    private String createBy ;
            
    /**
    * 类别1文2视频 /3评论
    **/
    private Integer type ;
            
    /**
    *
    **/
    private String updateBy ;
            
    /**
    * 用户id 
    **/
    private Long userId ;
            
    /**
    *
    **/
    private Date createTime ;
            
    /**
    *
    **/
    private Date updateTime ;

}