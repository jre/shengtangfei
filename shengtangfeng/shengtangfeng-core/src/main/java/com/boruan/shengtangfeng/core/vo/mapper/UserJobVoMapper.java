package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.UserJob;
import com.boruan.shengtangfeng.core.vo.UserJobVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserJobVoMapper extends VoMapper<UserJobVo, UserJob> {
    UserJobVoMapper MAPPER = Mappers.getMapper(UserJobVoMapper.class);
}
