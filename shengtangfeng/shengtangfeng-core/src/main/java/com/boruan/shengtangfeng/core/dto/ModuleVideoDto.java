package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ModuleVideoDto extends BaseDto{

    @ApiModelProperty(value="自增id，更新时传")
    private Long id;

    @ApiModelProperty(value="章节ID")
    private Integer moduleId;
    /**
     * 视频地址
     */
    @ApiModelProperty(value="视频地址")
    private String video;
    /**
     * 视频标题
     */
    @ApiModelProperty(value="视频标题")
    private String title;

    /**
     * 视频的缩略图
     */
    @ApiModelProperty(value="缩略图")
    private String thumb;
}
