package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.SmallVideoDto;
import com.boruan.shengtangfeng.core.entity.Video;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;

@Mapper
public interface SmallVideoDtoMapper extends DtoMapper<SmallVideoDto,Video> {
    SmallVideoDtoMapper MAPPER = Mappers.getMapper(SmallVideoDtoMapper.class);
}
