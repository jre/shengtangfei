package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.ModuleVideo;
import com.boruan.shengtangfeng.core.entity.Question;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.Module;
import com.boruan.shengtangfeng.core.entity.ModuleQuestion;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ModuleVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IModuleService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param module
	 */
	public void pageQuery(PageQuery<Module> pageQuery, Module module);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public Module findById(Long id);

	public ModuleQuestion findByModuleId(ModuleQuestion moduleQuestion);
	/**
	 * 保存分类
	 * @param  module
	 */
	public void save(Module module);

	public void saveQuestion(ModuleQuestion moduleQuestion);
	/**
	 * 根据城市获取章节树
	 * @param city
	 * @return
	 */
    public List<ModuleVo> findTree(Long userId);
    /**
     * 获取同级的下一个模块
     * @return
     */
    public Module findNext(Long moduleId);


	public List<ModuleVo> findModuleTree(Module module);

	/**
	 * 查找最大的序列
	 * @param search
	 */
	public Integer getMaxSequence(Module search);
	/**
	 * 查找一个章节，确保查询条件只有一个结果集，否则异常
	 * @param search
	 */
	public Module templateOne(Module search);
	List<Module> findByCondition(Module module);
	
	public List<Module> findByQuestion(Long questionId);

	/**
	 * 模块内添加试题
	 * @param id
	 * @param moduleId
	 * @param questionIdList
	 */
	void addQuestions(Long id, Long moduleId, List<Long> questionIdList);

	void removeQuestions(Long id,Long moduleId, List<Long> questionIdList);

	GlobalReponse importFormAnother(Long id, Long currentId, Long sourceId);


	void addVideos(Long id, Long moduleId, List<Long> videoIdList);
	void removeVideos(Long id,Long moduleId, List<Long> videoIdList);

    List<Module> findByCondition1(Module search);

}
