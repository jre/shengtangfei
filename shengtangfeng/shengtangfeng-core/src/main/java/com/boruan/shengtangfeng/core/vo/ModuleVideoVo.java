package com.boruan.shengtangfeng.core.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 @author: guojiang
 @Description:章节模块视频
 @date:2021/3/3
 */
@Data
public class ModuleVideoVo extends BaseVo {

    /**
     * 章节模块id
     */
    @ApiModelProperty("章节模块id")
    private Long moduleId;

    /**
     * 视频地址
     */
    @ApiModelProperty("章节模块id")
    private String video;

    /**
     * 排序
     */
    @ApiModelProperty("章节模块id")
    private Long sort;

    /**
     * 视频标题
     */
    private String title;

    /**
     * 视频的缩略图
     */
    private String thumb;

}
