package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="video_grade")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class VideoGrade extends IdEntity {
    
            
            
    /**
    * 形象评分 
    **/
    private Integer imageScore ;

    /**
    * 知识点评分 
    **/
    private Integer knowledgeScore ;
            
    /**
    * 语言评分 
    **/
    private Integer languageScore ;
            
    /**
    * 总评分 
    **/
    private Integer totalScore ;
            
    /**
    * 类型 1学生评分 2老师评分 
    **/
    private Integer type ;
    /**
    * 评语 
    **/
    private String remark ;
    /**
    * 用户id 
    **/
    private Long userId ;

    @ApiModelProperty(value = "视频id")
    private Long videoId;

    @ApiModelProperty(value = "状态0待审核1通过2拒绝")
    private Integer status;

    @ApiModelProperty(value = "拒绝原因")
    private String refusalCause;

}