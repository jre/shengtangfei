package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class VideoGradeDto extends BaseDto {

    /**
     * 形象评分
     **/
    @ApiModelProperty(value = "形象评分")
    private Integer imageScore ;

    /**
     * 知识点评分
     **/
    @ApiModelProperty(value = "知识点评分")
    private Integer knowledgeScore ;

    /**
     * 语言评分
     **/
    @ApiModelProperty(value = "语言评分")
    private Integer languageScore ;

    /**
     * 总评分
     **/
    @ApiModelProperty(value = "总评分")
    private Integer totalScore ;


    /**
     * 评语
     **/
    @ApiModelProperty(value = "评语")
    private String remark ;


    @ApiModelProperty(value = "视频id")
    private Long videoId;
}
