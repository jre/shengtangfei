package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.*;

public interface IMessageService {
    /**
     * 获取新好友列表
     * @param userId
     * @return
     */
    GlobalReponse<NewFriendVo> getNewFriendList(Long userId);

    /**
     * 删除新好友消息
     * @param id
     * @param userId
     * @return
     */
    GlobalReponse deletedNewFriend(Long id, Long userId);

    /**
     * 同意或拒绝好友申请status 1同意 2拒绝
     * @param userId
     * @param id
     * @param status
     * @return
     */
    GlobalReponse agreeOrRefuseApplyFriend(Long userId, Long id,Integer status);

    /**
     * 通过申请人的手机号同意或拒绝好友申请 status 1同意 2拒绝
     * @param userId
     * @param mobile
     * @param status
     * @return
     */
    GlobalReponse agreeByMobile(Long userId, String mobile, Integer status);

    /**
     * 获取加群消息列表
     * @param userId
     * @return
     */
    GlobalReponse<NewGroupVo> getNewGroupList(Long userId);

    /**
     * 删除加群消息
     * @param userId
     * @return
     */
    GlobalReponse deletedNewGroup(Long id,Long userId,Integer apply);

    /**
     * 同意或拒绝群聊申请 status 1同意 2拒绝
     * @param userId
     * @param id
     * @param status
     * @return
     */
    GlobalReponse agreeOrRefuseApplyGroup(Long userId, Long id, Integer status);

    /**
     * 获取班级消息教师端
     * @param userId
     * @return
     */
    GlobalReponse<NewClassMessageVo> getClassMessageTeacher(Long userId);

    /**
     * 删除班级消息
     * @param id
     * @param userId
     * @param apply
     * @return
     */
    GlobalReponse deletedClassMessage(Long id, Long userId, Integer apply);

    /**
     * 同意或拒绝班级申请
     * @param userId
     * @param id
     * @param status
     * @return
     */
    GlobalReponse agreeOrRefuseApplyClass(Long userId, Long id, Integer status);

    /**
     * 获取互动消息
     * @param userId
     * @return
     */
    GlobalReponse<VideoFriendVo> getInteractMessage(Long userId);

    /**
     * 删除互动消息
     * @param userId
     * @return
     */
    GlobalReponse deletedInteractMessage(Long id,Long userId);
}
