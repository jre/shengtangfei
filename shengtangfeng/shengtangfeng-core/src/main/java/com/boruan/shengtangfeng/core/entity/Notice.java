package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="notice")
@Setter
@Getter
@NoArgsConstructor
public class Notice extends IdEntity {
    
            
    /**
    * 变化类型  1 平台通知  2 用户通知
    **/
    
    private Integer category;

    /**
     * 类型，0：,平台通知  1：文章审核通知 2： 视频审核通知 403:用户被锁下线 404 用户互踢下线
     **/
    private Integer type;
    /**
    * 对象ID，根据type判断对应
    **/
    private Long objectId;
            
     /**
      * 标题
      **/
    private String title;
     /**
      * 内容
      **/
    private String content;

}