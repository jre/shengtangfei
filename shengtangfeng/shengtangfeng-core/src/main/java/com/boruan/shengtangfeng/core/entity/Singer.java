package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "singer")
@EqualsAndHashCode(callSuper = false)
public class Singer extends IdEntity {

    @ApiModelProperty(value = "歌手名")
    private String name;

}
