package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 布置作业
 */
@Data
public class HomeworkDto {

    @ApiModelProperty("题目Id的集合")
    private Long[] questionIds ;

    @ApiModelProperty("作业名称")
    private String jobName ;

    @ApiModelProperty("学科id")
    private Long subjectId ;

    @ApiModelProperty("学科名称")
    private String subjectName ;

    @ApiModelProperty("截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08:00")
    private Date deadline ;

    @ApiModelProperty("班级Id的集合")
    private Long[] classIds ;
}
