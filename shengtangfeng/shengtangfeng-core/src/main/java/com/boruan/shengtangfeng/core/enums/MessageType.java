package com.boruan.shengtangfeng.core.enums;

import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MessageType implements CommonEnum {
	TASK(1, "任务"), ROUTINEEXERCISE(2, "日常练习"), TEACHER(3, "老师消息"), NOTICE(4, "系统通知"), ZHANNEITONGZHI(5, "站内通知（管理员消息）");

	@ApiModelProperty(value = "1任务 2日常练习 3老师消息 4系统通知 5站内通知（管理员消息）")
	private Integer value;
	@ApiModelProperty(value = "")
	private String name;

	private MessageType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) {
		System.out.println(Sex.valueOf("男"));
	}
}
