package com.boruan.shengtangfeng.core.utils;

import java.io.Serializable;

import lombok.Data;

@Data
public class ZTree implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2671422004385960947L;
	
	private String id;
	private String pId;
	private String name;
	private String title;
	private Boolean open;
	private Boolean isParent;
	private Boolean chkDisabled=false;
	
}
