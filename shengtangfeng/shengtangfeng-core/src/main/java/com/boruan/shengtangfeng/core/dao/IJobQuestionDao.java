package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.JobQuestion;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IJobQuestionDao extends BaseMapper<JobQuestion> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<JobQuestion> query);
}