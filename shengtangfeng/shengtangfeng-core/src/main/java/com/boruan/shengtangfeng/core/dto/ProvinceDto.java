package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProvinceDto implements Serializable {

    @ApiModelProperty(value="省id")
    private Long provinceId;

    @ApiModelProperty(value="省名称")
    private String provinceName;

    @ApiModelProperty(value="高德编码")
    private String adcode;

    @ApiModelProperty(value="市集合")
    private List<CityDto> cityList;
}
