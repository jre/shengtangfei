package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.UserDto;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
@author: lihaicheng
@Description: 我的 个人中心
@date:2020年3月10日 上午11:14:10
*/
public interface IMyInfoService {

	/**
	 * 修改密码
	 * */
	public GlobalReponse changePassword(String phone, String password);
	/**
	 * 获取用户
	 * */
	public User getInfo(Long userId);
	/**
	 * 修改个人信息 
	 * */
	public GlobalReponse updateInfo(UserDto userDto);
	/**
	 * 意见反馈
	 * */
	public GlobalReponse feedBack(Long userId, String content);
}
