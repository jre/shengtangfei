package com.boruan.shengtangfeng.core.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="视频主体")
public class VideoVo extends BaseVo{

    /**
     * 分类ID
     */
    @ApiModelProperty(value="分类ID")
    private Long categoryId;

    /**
     * 分类名称
     */
    @ApiModelProperty(value="分类名称")
    private String categoryName;

    /**
     * 分类ID
     */
    @ApiModelProperty(value="科目名称")
    private String subjectName;

    /**
     * 文章标题
     */
    @ApiModelProperty(value="视频标题")
    private String title;
    
    /**
     * 视频内容地址
     */
    @ApiModelProperty(value="视频内容地址")
    private String content;
    
    /**
     * 发布时间
     */
    @ApiModelProperty(value="发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date publishDate;
    /**
     * 发布人
     */
    @ApiModelProperty(value="发布人")
    private String publisher;

    /**
     * 发布人
     */
    @ApiModelProperty(value="发布人账号")
    private String account;

    /**
     * 发布人头像
     */
    @ApiModelProperty(value="发布人头像")
    private String publisherIcon;
    /**
     * 状态 0 未发布  1 发布
     */
    @ApiModelProperty(value="状态 0 未发布  1 发布")
    private Integer status;
    /**
	 * 已读人数
	 */
    @ApiModelProperty(value="已读人数")
	private Integer readNum;

    /**
     *  经度
     */
    @ApiModelProperty(value="经度")
    private String longitude;
    /**
     *  纬度
     */
    @ApiModelProperty(value=" 纬度")
    private String latitude;

    /**
     *  距离
     */
    @ApiModelProperty(value="距离")
    private String distance;

	/**
	 * 评论人数
	 */
    @ApiModelProperty(value="评论人数")
	private Integer commentNum;
    /**
     * 点赞人数
     */
    @ApiModelProperty(value="点赞人数")
    private Integer thumpUpNum;
    /**
	 *  是否推荐
	 */
    @ApiModelProperty(value="是否推荐")
	private Boolean recommend;
    /**
     * 分享次数
     */
    @ApiModelProperty(value="分享次数")
    private Integer shareNum;
//    /**
//     *  相关视频
//     */
//    @ApiModelProperty(value="相关文章")
//    private List<VideoVo> closely;
//
//
    @ApiModelProperty(value="相关评论")
    private List<VideoCommentVo> comment;
//
//    @ApiModelProperty(value="类别名")
//    private List<UserCategoryVo> userCategoryVo;

    @ApiModelProperty(value="视频分类 0淘学视频 1讲一讲视频 2小视频")
    private Integer type;


    /**
     *  封面图
     */
    @ApiModelProperty(value="封面图")
    private String image;
    
    @ApiModelProperty(value="发布者id")
    private Long publishId;//

    @ApiModelProperty(value="邀请码")
    private String invitationCode;//

    @ApiModelProperty(value="是否关注")
    private Boolean isAttention;//

    @ApiModelProperty(value="是否收藏")
    private Boolean isFavorites;//

    @ApiModelProperty(value="是否点赞")
    private Boolean isLike;//
    /**
     *  审核备注
     */
    @ApiModelProperty(value="审核备注")
    private String remark;
    /**
     *  上传类型0 系统  1用户
     */
    @ApiModelProperty(value="上传类型0 系统  1用户")
    private Integer uploadType;

    /**
     *  视频长度（秒）
     */
    @ApiModelProperty(value="视频长度（秒）")
    private Integer duration;

    @ApiModelProperty(value = "视频评分")
    private BigDecimal score;

}
