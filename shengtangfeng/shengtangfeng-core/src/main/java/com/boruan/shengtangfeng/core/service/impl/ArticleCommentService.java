package com.boruan.shengtangfeng.core.service.impl;

import java.util.Date;
import java.util.List;

import com.boruan.shengtangfeng.core.dao.IArticleDao;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.service.IArticleCommentService;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IArticleCommentDao;
import com.boruan.shengtangfeng.core.entity.ArticleComment;
import com.boruan.shengtangfeng.core.entity.VideoComment;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class ArticleCommentService implements IArticleCommentService {
    @Autowired
    private IArticleCommentDao articleCommentDao;
    @Autowired
    private IArticleDao articleDao;

    /**
     * @author: 刘光强
     * @Description: 分页获取
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    public void pageQuery(PageQuery<ArticleComment> pageQuery, ArticleComment article) {
        pageQuery.setParas(article);
        articleCommentDao.pageQuery(pageQuery);
    }

    /**
     * @author: 刘光强
     * @Description: 详情
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    public ArticleComment findById(Long id) {
        return articleCommentDao.unique(id);
    }

    /**
     * @author: 刘光强
     * @Description: 添加修改
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    @Transactional(readOnly = false)
    public void save(ArticleComment articleComment) {
        if (articleComment.getId() == null) {
            KeyHolder kh = articleCommentDao.insertReturnKey(articleComment);
            articleComment.setId(kh.getLong());
        } else {
            articleCommentDao.updateById(articleComment);
        }
    }

    @Override
    public List<ArticleComment> findByArticleId(Long userId,Long articleId, Integer status) {
        List<ArticleComment> mine= articleCommentDao.findByArticleId(userId,articleId, status);
        if(userId!=null) {
            List<ArticleComment> notMine= articleCommentDao.findByArticleIdNotMine(userId,articleId, status);
            mine.addAll(notMine);
        }
        return mine;
    }

    @Override
    public List<ArticleComment> template(ArticleComment search) {
        return articleCommentDao.template(search);
    }

    @Override
	@Transactional(readOnly = false)
    public void deleteComment(Long id, Long userId) {

        ArticleComment po = new ArticleComment();
        po.setId(id);
        po.setIsDeleted(true);
        po.setUpdateBy(userId.toString());
        po.setUpdateTime(new Date());
		articleCommentDao.updateTemplateById(po);
        ArticleComment articleComment = articleCommentDao.single(id);
        Article article=articleDao.single(articleComment.getArticleId());
        article.setCommentNum(article.getCommentNum()-1);
        articleDao.updateById(article);
    }

    @Override
    public long getTodayCount(Long userId) {
        return articleCommentDao.getTodayCount(userId);
    }

}
