package com.boruan.shengtangfeng.core.dao;
/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Feedback;

public interface IFeedbackDao extends BaseMapper<Feedback> {

	void pageQuery(PageQuery<Feedback> query);

}
