package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.ModuleRecord;
import com.boruan.shengtangfeng.core.vo.ModuleRankDetailVo;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IModuleRecordDao extends BaseMapper<ModuleRecord> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<ModuleRecord> pageQuery);
	/**
	 * 获取某个模块的前50条排行数据
	 */
	public List<ModuleRankDetailVo> getRank(Long moduleId);
	/**
	 * 获取某个用户某个模块的排行数据
	 */
	public ModuleRankDetailVo getUserRank(Long moduleId,Long userId);
	/**
	 * 获取所有模块的前50条排行数据
	 */
	public List<ModuleRankDetailVo> getRankAll();
	/**
	 * 获取某个用户所有模块的排行数据
	 */
	public ModuleRankDetailVo getUserRankAll(Long userId);


}
