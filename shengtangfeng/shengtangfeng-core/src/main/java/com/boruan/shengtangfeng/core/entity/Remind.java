package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="remind")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Remind extends IdEntity {

    /**
    * 发起人类型 1学生 2老师 
    **/
    private Integer applicatType ;
    /**
    * 状态 0等待验证 1同意 2拒绝 
    **/
    private Integer status ;
            
    /**
    * 类型 0加入班级提醒 1作业提醒 
    **/
    private Integer type ;
            
    /**
    * 发起人头像 
    **/
    private String applicatHead ;
            
    /**
    * 发起人id 
    **/
    private String applicatId ;
            
    /**
    * 发起人昵称 
    **/
    private String applicatName ;
            
    /**
    * 班级id 
    **/
    private Long classId ;
    /**
    * 作业id 
    **/
    private Long jobId ;
            
    /**
    * 接收人id 
    **/
    private Long receiverId ;
            
    /**
    * 备注 
    **/
    private String remark ;
}