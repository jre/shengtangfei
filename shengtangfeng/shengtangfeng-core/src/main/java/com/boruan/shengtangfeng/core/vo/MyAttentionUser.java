package com.boruan.shengtangfeng.core.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="我关注的用户")
public class MyAttentionUser{

    @ApiModelProperty(value="用户ID")
    private Long id;
    
    @ApiModelProperty("头像")
    private String headImage;
    
    @ApiModelProperty("姓名")
    private String name;
    
    @ApiModelProperty("粉丝数")
    private Long fansCount;
    @ApiModelProperty("是否相互关注，用于我的粉丝界面")
    private Boolean isAttention;
    @ApiModelProperty("是否关注，用于我的关注界面")
    private Boolean isFollow;
    @ApiModelProperty("是否置顶")
    private Boolean isStick ;
	
    @ApiModelProperty("邀请码")
    private String invitationCode;
}
