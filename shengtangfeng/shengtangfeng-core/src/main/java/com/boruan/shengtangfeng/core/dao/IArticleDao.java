package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Article;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IArticleDao extends BaseMapper<Article> {
	/**
	 * 分页查询
	 * 
	 * @param query
	 */
	public void pageQuery(PageQuery<Article> query);
	/**
	 * 获取相关文章
	 * @param article
	 * @return
	 */
	public List<Article> findClosely(Article article);


	public Article findById (Long id);

	public void pageAttentionArticle(PageQuery<Article> query);
	/**
	 * 分页获取收藏的文章
	 * @param query
	 */
	public void pageFavoritesArticle(PageQuery<Article> query);
	/**
	 * 分页获取点赞的文章
	 * @param query
	 */
	public void pageAccoladeArticle(PageQuery<Article> query);

	/**
	 * 分页查询待审核文章
	 * @param publishId
	 * @param publisher
	 * @param articleId
	 * @param title
	 * @return
	 */
	List<Article> getToAuditArticle(Long categoryId,Long publishId, String publisher, Long articleId, String title);

    void pageToAuditArticle(PageQuery<Article> pageQuery);


    public List<Article> newCheck(Article article);

	/**
	 * 审核端根据用户id查询用户文章
	 * @param pageQuery
	 */
	void getUserArticle(PageQuery<Article> pageQuery);

}
