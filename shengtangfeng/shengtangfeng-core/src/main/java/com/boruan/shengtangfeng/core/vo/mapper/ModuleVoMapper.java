package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.Module;
import com.boruan.shengtangfeng.core.vo.ModuleVo;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface ModuleVoMapper extends VoMapper<ModuleVo,  Module>  {
	ModuleVoMapper MAPPER = Mappers.getMapper(ModuleVoMapper.class);
}