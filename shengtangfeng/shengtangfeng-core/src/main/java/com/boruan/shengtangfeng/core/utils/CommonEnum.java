package com.boruan.shengtangfeng.core.utils;

import io.swagger.annotations.ApiModel;

/**
 * 枚举接口
 */
@ApiModel
public interface CommonEnum {
    //此处对应枚举的字段,如状态枚举定义了value
    //那么这里定义这个字段的get方法,可以获取到所有的字段
   Integer getValue();
   String getName();
}
