package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="class_job")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClassJob extends IdEntity {
            
    /**
    * 班级id
    **/
    private Long classId ;

    /**
    * 作业id
    **/
    private Long jobId ;

}