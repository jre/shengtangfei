package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.InformDao;
import com.boruan.shengtangfeng.core.entity.Inform;
import com.boruan.shengtangfeng.core.service.IInformService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class InformService implements IInformService {

    @Autowired
    private InformDao informDao;

    @Override
    public void pageQuery(PageQuery<Inform> pageQuery, Inform inform) {
        pageQuery.setParas(inform);
        informDao.pageQuery(pageQuery);
    }
}
