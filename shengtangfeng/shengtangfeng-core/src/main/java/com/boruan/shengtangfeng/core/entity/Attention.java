package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="attention")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Attention extends IdEntity {
    
            

    /**
    * 被关注人id 
    **/
    private Long focusUserId ;
            
    /**
    * 是否互相关注 
    **/
    private Boolean isAttention ;
            
    /**
    * 用户id 
    **/
    private Long userId ;

    /**
     * 是否置顶
     */
    private Boolean isStick ;


}