package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Area;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IAreaDao extends BaseMapper<Area> {

    /**
     * 随机获取省
     * @return
     */
    Area getOneProvince();

    /**
     * 获取省下的随机市
     * @param id
     * @return
     */
    Area getOneCity(Long id);
}
