package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.FavoritesDto;
import com.boruan.shengtangfeng.core.dto.FeedbackDto;
import com.boruan.shengtangfeng.core.entity.Favorites;
import com.boruan.shengtangfeng.core.entity.Feedback;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author KongXH
 * @title: FavoritesDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1714:18
 */
@Mapper
public interface FavoritesDtoMapper extends DtoMapper<FavoritesDto, Favorites> {
    FavoritesDtoMapper MAPPER = Mappers.getMapper(FavoritesDtoMapper.class);

    public Favorites toEntity(FavoritesDto dto);
}
