package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.UserAnswerDto;
import com.boruan.shengtangfeng.core.entity.QuestionRecord;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserAnswerDtoMapper extends DtoMapper<UserAnswerDto, QuestionRecord> {
	UserAnswerDtoMapper MAPPER = Mappers.getMapper(UserAnswerDtoMapper.class);
}
