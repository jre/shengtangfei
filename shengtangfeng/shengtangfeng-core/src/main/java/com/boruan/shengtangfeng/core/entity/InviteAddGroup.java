package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;


@Table(name="invite_add_group")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class InviteAddGroup extends IdEntity {
            
    /**
    * 状态 0为忽略 1忽略
    **/
    @ApiModelProperty("状态 0未忽略 1忽略")
    private Integer status ;
            
    /**
    * 邀请人id
    **/
    @ApiModelProperty("邀请人id")
    private Long inviterId ;
            
    /**
    * 被邀请人
    **/
    @ApiModelProperty("被邀请人Id")
    private Long inviteeId ;

    /**
    * 群id
    **/
    @ApiModelProperty("群id")
    private Long groupId ;


    @ApiModelProperty("是否已读")
    private Integer isRead ;

}