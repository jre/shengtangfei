package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.*;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.*;
import org.beetl.sql.core.engine.PageQuery;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @description: v4E 班级-教师端
 * @date 2021/4/7 9:20
 */
public interface IClassTeacherService {

    /**
     * 获取省
     * @return
     */
    GlobalReponse<AddressDto> getProvince();

    /**
     * 获取市
     * @return
     */
    GlobalReponse<AddressDto> getCity(Long provinceId);

    /**
     * 获取区
     * @return
     */
    GlobalReponse<AddressDto> getCounty(Long cityId);


    /**
     * 根据地址查询学校
     * @return
     */
    GlobalReponse<PageQuery<School>> findSchool(FindSchoolDto dto);


    /**
     * 创建学校
     * @param schoolDto
     * @return
     */
    GlobalReponse<School> createSchool(Long userId,SchoolDto schoolDto);


    /**
     * 创建班级
     * @param userId
     * @param classDto
     * @return
     */
    GlobalReponse<ClassAndVo> createClass(Long userId, ClassAndDto classDto);

    /**
     * 接收前端传递的腾讯群Id
     * @param tencentId
     * @param classId
     * @param userId
     * @return
     */
    GlobalReponse tencentGroupId(String tencentId, Long classId, Long userId);

    /**
     * 查询教师的班级列表
     * @param id
     * @return
     */
    GlobalReponse<ClassAndVo> getTeacherClassAnd(Long id,Integer type);

    /**
     * 根据班级id查询班级详情信息
     * @param classId
     * @return
     */
    GlobalReponse<ClassAndVo> getClassById(Long classId,Long userId);

    /**
     * 修改班级内的昵称
     * @param nickname
     * @param classId
     * @param userId
     * @return
     */
    GlobalReponse changeNickname(String nickname, Long classId, Long userId);

    /**
     * 解散班级
     * @param classId
     * @param userId
     * @return
     */
    GlobalReponse dissolveClass(Long classId, Long userId);

    /**
     * 根据班级Id查看班级通知
     * @param pageQuery
     * @return
     */
    GlobalReponse<PageQuery<InformDto>> getClassInformById(Long userId,PageQuery<Inform> pageQuery, Long classId);

    /**
     * 查看我的通知
     * @param userId
     * @param pageQuery
     * @return
     */
    GlobalReponse<PageQuery<Inform>> getMyInform(Long userId, PageQuery<Inform> pageQuery);

    /**
     * 删除我发布的通知
     * @param userId
     * @param informId
     * @return
     */
    GlobalReponse deleteMyInform(Long userId, Long informId);

    /**
     * 发布通知
     * @param userId
     * @param informPushDto
     * @return
     */
    GlobalReponse pushInform(Long userId, InformPushDto informPushDto);

    /**
     * 获取我的好友列表
     * @param userId
     * @return
     */
    GlobalReponse<FriendDto> getFriendList(Long userId,Long classId,String keyword);

    /**
     * 邀请好友加入班级
     * @param  dto
     * @param userId
     * @return
     */
    GlobalReponse<ClassAndVo> addClassMembers(AddDeleteClassMembersDto dto, Long userId);

    /**
     * 删除班级成员
     * @param dto
     * @param userId
     * @return
     */
    GlobalReponse deletedClassMembers(AddDeleteClassMembersDto dto, Long userId);

    /**
     * 获取全部学科
     * @return
     */
    GlobalReponse<Subject> getSubject();

    /**
     *获取全部年级
     * @return
     */
    GlobalReponse<Grade> getGrade();

    /**
     *获取发布的作业的学科
     * @param userId
     * @return
     */
    GlobalReponse<Subject> getUserJobGrade(Long userId);

    /**
     * 获取题库数量
     * @param type
     * @param subjectId
     * @param userId
     * @return
     */
    GlobalReponse<QuestionCountDto> getQuestionCount(Integer type, Long subjectId,Long gradeId, Long userId);

    /**
     * 多条件查询题库
     * @param userId
     * @return
     */
    GlobalReponse<PageQuery<GetQuestionPageVo>> getQuestionPage(QuestionPageDto dto, Long userId);

    /**
     * 加入或取消收藏
     * @param type
     * @param questionId
     * @param userId
     * @return
     */
    GlobalReponse addOrCancelFavorites(Integer type, Long questionId, Long userId);

    /**
     * 添加题目
     * @param dto
     * @param userId
     * @return
     */
    GlobalReponse addQuestion(AddQuestionDto dto, Long userId);

    /**
     * 删除题目
     * @param questionId
     * @param userId
     * @return
     */
    GlobalReponse deletedQuestion(Long questionId, Long userId);

    /**
     * 获取题目详情
     * @param questionId
     * @param userId
     * @return
     */
    GlobalReponse<QuestionDetailDto> getQuestionDetail(Long questionId, Long userId);

    /**
     * 获取作业名称
     * @return
     */
    GlobalReponse<JobNameDto> getJobName();

    /**
     * 获取截止时间
     * @return
     */
    GlobalReponse getDeadline();

    /**
     * 布置作业
     * @param homeworkDto
     * @param userId
     * @return
     */
    GlobalReponse putHomework(HomeworkDto homeworkDto, Long userId);

    /**
     * 获取题目最大的分数
     * @return
     */
    GlobalReponse getScore();

    /**
     * 通过班级id多条件查看作业列表
     * @param jobConditionDto
     * @param userId
     * @return
     */
    GlobalReponse<PageQuery<JobDto>> getJobListById(JobConditionDto jobConditionDto, Long userId);

    /**
     * 根据id查看作业详情
     * @param jobId
     * @param userId
     * @return
     */
    GlobalReponse<JobDto> getJobDetails(Long classId,Long jobId, Long userId);

    /**
     * 教师端查看排行榜
     * @return
     */
    GlobalReponse<PageQuery<UserJobVo>> getRankTeacher( GetRankDto dto);

    /**
     * 查询作业题目数量
     * @param jobId
     * @return
     */
    GlobalReponse<QuestionCountDto> getJobQuestionCount(Long jobId);

    /**
     * 查询作业题目列表
     * @return
     */
    GlobalReponse<PageQuery<JobQuestionDto>> getJobQuestionPage(GetJobQuestionPageDto dto);

    /**
     * 通过班级和作业获取题目详情
     * @param classId
     * @param questionId
     * @return
     */
    GlobalReponse<QuestionDetailDto> getQuestionDetailFromJob(Long classId,Long jobId, Long questionId);

    /**
     * 通过作业Id查看全部班级
     * @param jobId
     * @return
     */
    GlobalReponse<JobDto> getAllClassForJobId(Long jobId,Long userId);

    /**
     * 通过班级Id和作业Id查看学生列表
     * @param classId
     * @param jobId
     * @return
     */
    GlobalReponse<GetAllStudentListDto> getAllStudentList(Long classId, Long jobId);

    /**
     * 老师添加评语
     * @param userJobId
     * @param remark
     * @return
     */
    GlobalReponse addRemark(Long userJobId, String remark,Long userId );

    /**
     * 查询学生个人答题记录
     * @param jobId
     * @param userId
     * @return
     */
    GlobalReponse<StudentAnswerVoList> getStudentAnswer(Long jobId, Long userId,Long classId);

    /**
     * 给学生打分
     * @param dto
     * @param teacherId
     * @return
     */
    GlobalReponse pullStudentScore(PullStudentScoreDto dto, Long teacherId);

    /**
     * 催交作业
     * @param jobId
     * @param studentId
     * @param teacherId
     * @return
     */
    GlobalReponse callHomeWork(Long jobId, Long studentId, Long teacherId);

}
