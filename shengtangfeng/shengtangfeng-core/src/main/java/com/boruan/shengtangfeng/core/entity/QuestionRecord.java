package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 题库
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question_record")
public class QuestionRecord extends IdEntity {

	/**
	 * 问题ID
	 */
	private Long questionId;
	/**
	 * 答题人ID
	 */
	private Long userId;
	/**
	 * 答题人姓名
	 */
	private String userName;
	/**
	 * 来源 1 模块 
	 */
	private Integer source;
	/**
	 * 对象ID，根据source确定为什么ID，不同的练习传不同的ID
	 */
	private Long objectId;
	
	/**
	 *  是否正确
	 */
	private Boolean correct;
    
    /**
     * 分数
     **/
    private BigDecimal score;
	/**
	 * 用户答案
	 */
	private String userAnswer;
	/**
	 * 答题类型0选择题1主观题2线下题
	 */
	private Integer answerType;
	/**
	 * 答题用时
	 */
	private Integer useTime;
	/**
	 * 作业id
	 */
	private Long jobId;

	/**
	 * 班级ID
	 */
	private Long classId;

	/**
	 * 状态 0待审核 1通过 2拒绝
	 **/
	private Integer status ;

	/**
	 * 是否批改
	 **/
	private Boolean isCheck ;

}
