package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.entity.UserCategory;
import com.boruan.shengtangfeng.core.vo.UserCategoryVo;
import com.boruan.shengtangfeng.core.vo.UserVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface UserCategoryMapper extends VoMapper<UserCategoryVo, UserCategory>  {
	UserCategoryMapper MAPPER = Mappers.getMapper(UserCategoryMapper.class);
	 public UserCategory toEntity(UserCategoryVo vo);
}