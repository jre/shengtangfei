package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 题库
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "article_comment")
//@EqualsAndHashCode(callSuper = true)
public class ArticleComment extends IdEntity {

	private static final long serialVersionUID = -4470773369611955781L;
	/**
	 * 文章ID
	 */
	private Long articleId;
	/**
	 * 回复的评论ID
	 */
	private Long commentId;
	/**
	 * 发布人ID
	 */
	private Long userId;
	/**
	 * 发布人头像
	 */
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	private String userName;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 状态 0未审核 1已审核 2审核失败
	 */
	private Integer status;
	/**
     *  是否推荐
     */
    private Boolean recommend;


	//========= 非数据库字段 ========
	public String target;

}
