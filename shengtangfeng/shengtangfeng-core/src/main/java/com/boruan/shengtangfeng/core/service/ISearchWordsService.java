package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.SearchWords;

/**
 * @author KongXH
 * @title: ISearchWordsService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1317:06
 */
public interface ISearchWordsService {

     /**
         * @description: 查询搜索词
         * @Param
         * @return
         * @author KongXH
         * @date 2020/8/13 17:06
         */
    public List<SearchWords> getList(Long userId,Integer type);
    
    /**
     * 
     * @param userId
     * @param type 类型  1 试题 2 文章 3 视频
     * @param keyword
     */
    public void addSearchWord(Long userId,Integer type,String keyword);
}
