package com.boruan.shengtangfeng.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 班级参数设置
 * @date 2021/4/7 9:20
 */

@Table(name="class_parameter")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClassParameter extends IdEntity {
            
    /**
    * 秋季开学日期
    **/
    protected String autumnTime;

    /**
    * 春季开学日期
    **/
    protected String springTime;
            
    /**
     * 学制：1五四制2六三制
     **/
    private Integer educationSystem ;

}