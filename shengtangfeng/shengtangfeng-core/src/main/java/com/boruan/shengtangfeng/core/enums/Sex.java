package com.boruan.shengtangfeng.core.enums;

import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author: lihaicheng
 * @Description: 性别
 * @date:2020年3月10日 上午11:14:10
 */
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Sex implements CommonEnum {
    UNKNOW(0, "未知"),MALE(1, "男"), FEMALE(2, "女");

	@ApiModelProperty(value = "1:男")
	private Integer value;
	@ApiModelProperty(value = "2:女")
	private String name;

	private Sex(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) {
		System.out.println(Sex.valueOf("男"));
	}
}
