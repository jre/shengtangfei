package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.MusicCollection;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IMusicCollectionDao extends BaseMapper<MusicCollection> {

}
