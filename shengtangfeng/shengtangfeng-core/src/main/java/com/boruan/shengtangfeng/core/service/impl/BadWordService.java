package com.boruan.shengtangfeng.core.service.impl;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IBadWordDao;
import com.boruan.shengtangfeng.core.entity.BadWord;
import com.boruan.shengtangfeng.core.service.IBadWordService;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class BadWordService implements IBadWordService {
	@Autowired
	private IBadWordDao badWordDao;
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<BadWord> pageQuery, BadWord badWord) {
		pageQuery.setParas(badWord);
		badWordDao.pageQuery(pageQuery);
	}

	/**
	 * @author: 刘光强
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public BadWord findById(Long id) {
		return badWordDao.unique(id);
	}

	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(BadWord badWord) {
		if (badWord.getId() == null) {
			KeyHolder kh = badWordDao.insertReturnKey(badWord);
			badWord.setId(kh.getLong());
		} else {
			badWordDao.updateById(badWord);
		}
	}

	@Override
	public List<BadWord> findByTemplate(BadWord badWord) {
		return badWordDao.template(badWord);
	}
}
