package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;


@Table(name="class_inform_member")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClassInformMember extends IdEntity {
            
    /**
    * 班级id
    **/
    private Long classId ;

    /**
    * 通知id
    **/
    private Long informId ;

    /**
     * 用户id
     **/
    private Long userId ;

    /**
     * 是否已读 0未读 1已读
     **/
    private Integer isRead ;


}