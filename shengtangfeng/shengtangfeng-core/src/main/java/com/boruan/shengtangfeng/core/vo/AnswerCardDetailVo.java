package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="答题卡详情")
public class AnswerCardDetailVo{
    
    /**
     * 试题类型名称
     */
    @ApiModelProperty(value="试题类型名称")
    private String questionTypeName;
    
    @ApiModelProperty(value="答题卡")
    private List<AnswerSheet> answerSheet;
    
}