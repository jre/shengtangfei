package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 班级参数设置
 */
@Data
public class ClassParameterDto {

    @ApiModelProperty("id")
    protected Long id;

    /**
     * 秋季开学日期
     **/
    @ApiModelProperty("秋季开学日期")
    protected String autumnTime;

    /**
     * 春季开学日期
     **/
    @ApiModelProperty("春季开学日期")
    protected String springTime;

    /**
     * 学制：1五四制2六三制
     **/
    @ApiModelProperty("小学毕业年级 1是五年级，2是六年级")
    private Integer educationSystem ;
}
