package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.Advertise;
import com.boruan.shengtangfeng.core.vo.AdvertiseVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface AdvertiseVoMapper extends VoMapper<AdvertiseVo,  Advertise>  {
	AdvertiseVoMapper MAPPER = Mappers.getMapper(AdvertiseVoMapper.class);
}