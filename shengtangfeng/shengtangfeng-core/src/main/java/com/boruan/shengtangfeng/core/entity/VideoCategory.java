package com.boruan.shengtangfeng.core.entity;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="video_category")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class VideoCategory extends IdEntity {
            
    /**
    *排序
    **/
    private Integer sort ;

    /**
    * 是否默认1是0否 
    **/
    private Boolean isDefault ;
            
    /**
    *名称
    **/
    private String name ;

    @ApiModelProperty(value = "类型：0普通视频 2小视频分类")
    private Integer type;
}