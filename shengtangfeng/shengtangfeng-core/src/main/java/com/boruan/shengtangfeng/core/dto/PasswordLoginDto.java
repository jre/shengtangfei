package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@author: 刘光强
@Description: 
@date:2020年3月10日 上午11:14:10
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户名密码登录参数")
public class PasswordLoginDto{
    @ApiModelProperty("登录名")
    private String loginName;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("互踢校验位，通过极光推送互踢消息到当前用户所有端，如果time不一样则踢下线")
    private String time;
    
    public boolean checkParam(){
        if(StringUtils.isBlank(password)||StringUtils.isBlank(loginName)) {
            return false;
        }else {
            return true;
        }
    }
}
