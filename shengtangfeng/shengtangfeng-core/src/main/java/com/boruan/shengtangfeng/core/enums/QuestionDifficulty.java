package com.boruan.shengtangfeng.core.enums;

import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author:Jessica
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum QuestionDifficulty implements CommonEnum {
	JIANDAN(0, "简单"), ZHONGDENG(1, "中等"), NAN(2, "难");

	@ApiModelProperty(value = "0简单1中等2难")
	private Integer value;
	private String name;

	private QuestionDifficulty(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
