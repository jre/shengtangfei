package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 首页排行榜实体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="首页排行榜实体")
public class RankingListVo {
    /**
     * 排名列表
     */
    @ApiModelProperty(value="排名列表,按照排名顺序")
    private List<UserVo> users;
    /**
     * 当前用户排名，为空则没上榜
     */
    @ApiModelProperty(value="当前用户，为空则没上榜")
    private UserVo user;
    /**
     * 当前用户排名，当user不为空时有值
     */
    @ApiModelProperty(value="当前用户排名，当user不为空时有值")
    private Integer userRank;
    
}
