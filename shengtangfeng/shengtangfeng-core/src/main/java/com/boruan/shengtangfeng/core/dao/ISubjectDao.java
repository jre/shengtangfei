package com.boruan.shengtangfeng.core.dao;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Subject;


public interface ISubjectDao extends BaseMapper<Subject> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Subject> query);
    
    /**
     * 获取学科列表
     * @param subject
     */
    public List<Subject> getList(Subject subject);
    /**
     * 根据年级ID获取学科列表
     * @param gradeId
     */
    public List<Subject> getListByGradeId(Long gradeId);
    /**
     * 根据年级ID获取学科列表
     * @param gradeId
     */
    public List<Subject> getListByGradeIds(Long[] gradeIds);
    /**
     * 获取用户做错的学科列表
     * @param gradeId
     */
    public List<Subject> getIncorrectSubject(Long userId,int type);
    /**
     * 获取用户收藏题的学科列表
     * @param gradeId
     */
    public List<Subject> getFavoritesSubject(Long userId);

}