package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询学校请求体
 */
@Data
public class FindSchoolDto {

    @ApiModelProperty("省")
    private String province;

    @ApiModelProperty("市")
    private String city;

    @ApiModelProperty("区")
    private String county;

    @ApiModelProperty("学校名称")
    private String name;

    @ApiModelProperty("页码")
    private Integer pageNo=1;

}
