package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassParameter;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @date 2021/4/7 9:20
 */
public interface IClassParameterDao extends BaseMapper<ClassParameter> {

}
