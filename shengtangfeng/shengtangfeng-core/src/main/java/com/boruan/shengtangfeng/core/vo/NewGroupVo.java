package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 @author: guojiang
 @Description:
 @date:2021年5月11日 下午14:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NewGroupVo {

    @ApiModelProperty("id，记录的ID")
    private Long id;

    @ApiModelProperty("0为当前用户申请加入其它群 1其他用户申请加入当前用户为群主/管理员的群 2自己被移出群聊 3别人邀请自己加入群聊")
    private Integer apply;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户头像")
    private String userImage;

    @ApiModelProperty("用户昵称")
    private String userNickname;

    @ApiModelProperty("用户类型")
    private Integer userType;

    @ApiModelProperty("用户手机号")
    private String userMobile;

    @ApiModelProperty("群Id")
    private Long groupId;

    @ApiModelProperty("腾讯的群Id")
    private String tencentId ;

    @ApiModelProperty("群号")
    private String groupNum ;

    @ApiModelProperty("群名称")
    private String groupName;

    @ApiModelProperty("群名称")
    private String groupImage;

    @ApiModelProperty("状态 0等待验证 1同意 2拒绝 当apply=3时0为未忽略1为忽略")
    private Integer status ;

    @ApiModelProperty("申请备注 ")
    private String applyRemark ;

    @ApiModelProperty("处理人Id")
    private Long handlerId;

    @ApiModelProperty("处理人头像")
    private String handlerImage  ;

    @ApiModelProperty("处理人昵称 ")
    private String handlerNickname ;

    @ApiModelProperty("处理人类型")
    private Integer handlerType;

    @ApiModelProperty("处理人手机号")
    private String handlerMobile;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    protected Date createTime;
}
