package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.VideoGrade;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

import java.math.BigDecimal;
import java.util.List;

public interface IVideoGradeService {


    VideoGrade findUserGrade(Long userId, Long videoId);

    List<BigDecimal> getScorePercent(Long videoId);

    List<VideoGrade> getTeacherGrade(Long videoId);

    void getStudentGrade(PageQuery<VideoGrade> pageQuery);

    GlobalReponse save(VideoGrade videoGrade);
}
