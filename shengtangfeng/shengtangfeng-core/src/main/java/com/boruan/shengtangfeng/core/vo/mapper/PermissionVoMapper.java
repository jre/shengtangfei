package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Permission;
import com.boruan.shengtangfeng.core.vo.PermissionVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PermissionVoMapper extends VoMapper<PermissionVo, Permission>  {
	PermissionVoMapper MAPPER = Mappers.getMapper(PermissionVoMapper.class);
}