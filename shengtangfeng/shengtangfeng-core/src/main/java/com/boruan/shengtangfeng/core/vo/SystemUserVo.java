package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SystemUserVo extends BaseVo {
	/**
	 * 头像
	 */
	@ApiModelProperty("头像")
	private String headImage;
	/**
	 * 手机号
	 */
	@ApiModelProperty("手机号")
	private String mobile;
	/**
	 * 姓名
	 */
	@ApiModelProperty("姓名")
	private String name;
	/**
	 * 登录名
	 */
	@ApiModelProperty("登录名")
	private String loginName;

//	/**
//	 * 密码
//	 */
//	@ApiModelProperty(value = "密码")
//	private String password;
//	/**
//	 * 密码盐
//	 */
//	@ApiModelProperty(value = "密码盐")
//	private String salt;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String description;

	/**
	 * 工号
	 */
	@ApiModelProperty(value = "工号")
	private String employeeNumber;

	/**
	 * 院系id
	 */
	@ApiModelProperty(value = "院系id")
	private Long departmentId;

	@Override
	public String toString() {
		return "{'name':" + this.name + ",'mobile':" + this.mobile + "}";
	}

}
