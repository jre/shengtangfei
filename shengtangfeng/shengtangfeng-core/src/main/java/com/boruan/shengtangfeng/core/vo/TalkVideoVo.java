package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.VideoGrade;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="视频主体")
public class TalkVideoVo extends BaseVo{

    /**
     * 分类ID
     */
    @ApiModelProperty(value="分类ID")
    private Long categoryId;
    /**
     * 文章标题
     */
    @ApiModelProperty(value="视频标题")
    private String title;
    
    /**
     * 视频内容地址
     */
    @ApiModelProperty(value="视频内容地址")
    private String content;
    
    /**
     * 发布时间
     */
    @ApiModelProperty(value="发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date publishDate;
    /**
     * 发布人
     */
    @ApiModelProperty(value="发布人")
    private String publisher;
    /**
     * 发布人头像
     */
    @ApiModelProperty(value="发布人头像")
    private String publisherIcon;

    /**
     * 发布人账号
     */
    @ApiModelProperty(value="发布人账号")
    private String account;

    /**
     * 状态 0 未发布  1 发布
     */
    @ApiModelProperty(value="状态 0 未发布  1 发布")
    private Integer status;
    /**
	 * 已读人数
	 */
    @ApiModelProperty(value="已读人数")
	private Integer readNum;
	/**
	 * 评论人数
	 */
    @ApiModelProperty(value="评论人数")
	private Integer commentNum;
    /**
     * 点赞人数
     */
    @ApiModelProperty(value="点赞人数")
    private Integer thumpUpNum;
    /**
	 *  是否推荐
	 */
    @ApiModelProperty(value="是否推荐")
	private Boolean recommend;

    @ApiModelProperty(value="相关评论")
    private List<VideoCommentVo> comment;

    /**
     *  封面图
     */
    @ApiModelProperty(value="封面图")
    private String image;
    
    @ApiModelProperty(value="发布者id")
    private Long publishId;//

    @ApiModelProperty(value="邀请码")
    private String invitationCode;//

    @ApiModelProperty(value="是否关注")
    private Boolean isAttention;//

    @ApiModelProperty(value="是否收藏")
    private Boolean isFavorites;//

    @ApiModelProperty(value="是否点赞")
    private Boolean isLike;//
    /**
     *  审核备注
     */
    @ApiModelProperty(value="审核备注")
    private String remark;
    /**
     *  上传类型0 系统  1用户
     */
    @ApiModelProperty(value="上传类型0 系统  1用户")
    private Integer uploadType;
    /**
     *  视频长度（秒）
     */
    @ApiModelProperty(value="视频长度（秒）")
    private Integer duration;

    @ApiModelProperty(value = "视频评分")
    private BigDecimal score;

    @ApiModelProperty(value = "评分百分比，从5-1依次排序")
    private List<BigDecimal> scorePercent;

    @ApiModelProperty(value = "我的评分")
    private VideoGrade myScore;

}
