package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author:Jessica
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "系统用户主体")
public class SystemUserDto extends BaseDto {
	/**
	 * 头像
	 */
	@ApiModelProperty("头像")
	private String headImage;
	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * 登录名
	 */
	@ApiModelProperty(value = "登录名")
	private String loginName;
	/**
	 * 用户名
	 */
	@ApiModelProperty(value = "用户名")
	private String name;
	/**
	 * 密码
	 */
	@ApiModelProperty(value = "密码")
	private String password;
	/**
	 * 密码盐
	 */
	@ApiModelProperty(value = "密码盐")
	private String salt;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String description;

	/**
	 * 工号
	 */
	@ApiModelProperty(value = "工号")
	private String employeeNumber;
	/**
	 * 院系id
	 */
	@ApiModelProperty(value = "院系id")
	private Long departmentId;
	/**
	 * 权限
	 */
	@ApiModelProperty(value = "权限")
	private Long roleId;
}
