package com.boruan.shengtangfeng.core.vo;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 试卷vo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ExamPaperVo extends BaseVo {

	/**
	 * 类型 历年真题  考前押题
	 **/
	@ApiModelProperty("类型 历年真题0  考前押题1")
	private Integer type;
	/**
	 * 名称
	 **/
	@ApiModelProperty("名称")
	private String name;
	@ApiModelProperty("试卷简称")
	private String abbreviation;
	/**
	 * 总答题人数
	 **/
	@ApiModelProperty("总答题人数")
	private Integer totalPeople;
	
	/**
	 * 题目数量
	 **/
	@ApiModelProperty("题目数量")
	private Integer questionCount;
	/**
	 * 描述
	 **/
	@ApiModelProperty("描述")
	private String description;
	/**
	 * 试卷总分
	 **/
	@ApiModelProperty("试卷总分")
	private Double totalScore;

	
	/**
	 * 学习币数量
	 **/
	@ApiModelProperty("学习币数量")
	private BigDecimal coin;
	@ApiModelProperty("vip学习币数量")
	private BigDecimal vipCoin;
	/**
	 *  是否精选
	 */
    @ApiModelProperty(value="是否精选")
	private String recommend;
    
    /**
     *  是否已经购买
     */
    @ApiModelProperty(value="是否已经购买")
    private Boolean isBuy;
    
    /**
     *  是否已经作答
     */
    @ApiModelProperty(value="是否已经作答")
    private Boolean isAnswer;

	/**
	 *  是否会员
	 */
	@ApiModelProperty(value="是否会员")
	private Boolean vip;

	/**
	 * 发布状态 0 待审核 1已通过 2未通过
	 */
	@ApiModelProperty("发布状态")
	private Integer status;

	@ApiModelProperty("vip是否免费")
	private Boolean isFree;
}
