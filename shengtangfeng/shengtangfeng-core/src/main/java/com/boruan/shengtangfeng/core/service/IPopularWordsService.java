package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.PopularWordsDto;
import com.boruan.shengtangfeng.core.entity.PopularWords;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 * @author KongXH
 * @title: IPopularWordsService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1317:06
 */
public interface IPopularWordsService {

    public void pageQuery(PageQuery<PopularWords> query);


     /**
         * @description: 查询热门词
         * @Param
         * @return
         * @author KongXH
         * @date 2020/8/13 17:06
         */
    public List<PopularWords> getList(Integer type);
}
