package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: 刘光强
 * @Description: 文件上传
 * @date:2020年3月10日 上午11:14:10
 */
public interface IUploadFileService {

	public GlobalReponse uploadImageFile(MultipartFile file, Long userId);

	public GlobalReponse uploadImageFiles(MultipartFile[] files, Long userId);

}
