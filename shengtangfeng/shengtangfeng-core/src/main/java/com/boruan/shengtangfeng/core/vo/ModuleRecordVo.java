package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ModuleRecordVo extends BaseVo {

    /**
     * 用户ID
     **/
    @ApiModelProperty("用户ID")
    private Long userId;
    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String userIcon;
    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String userName;
    /**
     * 父模块ID
     **/
    @ApiModelProperty("父模块ID")
    private Long parentModuleId;
    /**
     * 模块ID
     **/
    @ApiModelProperty("模块ID")
    private Long moduleId;
    
    /**
     * 耗费时间,秒数
     **/
    @ApiModelProperty("耗费时间,秒数")
    private Integer time;

    /**
     * 得分
     */
    @ApiModelProperty("得分")
    private Integer score;
    /**
     * 积分
     */
    @ApiModelProperty("积分")
    private Integer integral;

}
