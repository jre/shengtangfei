package com.boruan.shengtangfeng.core.service;

import java.math.BigDecimal;
import java.util.List;

import com.boruan.shengtangfeng.core.entity.VideoGrade;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.*;
import io.swagger.models.auth.In;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.dto.VideoCategoryDto;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.entity.VideoCategory;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface IVideoService {
	/**
	 * 通过id查找用户，后台使用
	 * @param id
	 * @return
	 */
	public Video findId(Long id);
	public Video findById (Long id);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param video
	 */
	public void pageQuery(PageQuery<Video> pageQuery,Video video);


	 /**
	     * @description: 分页查询关注人的文章
	     * @Param
	     * @return
	     * @author KongXH
	     * @date 2020/8/18 11:05
	     */
	public void pageAttentionVideo(PageQuery<Video> pageQuery,Long userId);
	/**
	 * 保存用户
	 * @param video
	 * @return
	 */
	public boolean save(Video video);

	/**
	 * 保存小视频
	 * @param video 视频实体
	 * @param classes 班级id拼接
	 * @return
	 */
	public boolean saveSmall(Video video,String classes,String friendIds,Long userId,Long classId,Long musicId);



	/**
	 * 通过条件查询用户列表
	 * @param video
	 * @return
	 */
	public List<Video> findByTemplate(Video video);
	/**
	 * 通过条件查询用户列表
	 * @param video
	 * @return
	 */
	public List<Video> findClosely(Video video);
	
	public void deleteVideo(Long videoId,Long userId);
	
	/**
     * 分页查询
     * @param pageQuery
     * @param video
     */
    public void pageQueryCategory(PageQuery<VideoCategory> pageQuery,VideoCategory videoCategory);


	/**
	 * 按条件查询分类
	 * @param videoCategory
	 * @return
	 */
	public List<VideoCategory> getCategoryList(VideoCategory videoCategory);

	void addVideo(Video dto, Long id);

	List<VideoCategoryVo> getCategory(Integer type);


	/**
	 * 获取全部视频分类
	 * @return
	 */
	List<VideoCategoryVo> getAllCategory();

	void updateVideo(Long id, Integer type, Long userId);
	/**
     * 按照id查询分类
     * @param categoryId
     */
    public VideoCategory getCategory(Long categoryId);
    
    /**
     * 保存分类
     * @param videoCategory
     */
    public void saveCategory(VideoCategory videoCategory);
    
    /**
     * 分页查询
     * @param pageQuery
     * @param video
     */
    public void pageQueryCategory(PageQuery<VideoCategory> pageQuery,VideoCategoryDto videoCategoryDto);

    public  Long templateCount(Video video);

    /**
     * 获取收藏的数量
     * @param userId
     * @return
     */
    public Long getFavoritesCount(Long userId);
    
    /**
     * 分页获取收藏的视频
     * @param pageQuery
     * @param userId
     */
    public void pageFavoritesVideo(PageQuery<Video> pageQuery,Long userId);
    /**
     * 分页获取点赞的视频
     * @param pageQuery
     * @param userId
     */
    public void pageAccoladeVideo(PageQuery<Video> pageQuery,Long userId);
    /**
     * 延时计算视频长度
     * @param video
     * @return
     */
    public boolean calculateDuration(Video video);

	public boolean upVideoClasses(Video video,String classes);

	/**
	 * 查看用户待审核视频
	 * @param pageQuery
	 */
	void pageToAuditVideo(PageQuery<Video> pageQuery);




	public List<Video> newCheck(Video video);

	/**
	 * 审核端根据用户id查看用户视频
	 * @param pageIndex
	 * @param pageSize
	 * @param userId
	 * @return
	 */
    GlobalReponse<PageQuery<ToAuditVideoVO>> getUserVideo(int pageIndex, int pageSize, Long userId, Integer type,String keywords);

	/**
	 * 审核端根据视频id获取视频详情
	 * @param videoId
	 * @return
	 */
	GlobalReponse<ToAuditVideoVO> getVideoDetails(Long videoId);

    void pageClassTalkVideo(PageQuery<Video> pageQuery);

	BigDecimal getVideoScore(Long id);

	BigDecimal avgScore(Long videoId);

	void pageVideoGrade(PageQuery<VideoGrade> pageQuery);

    void delGrade(Long gradeId);

	/**
	 * 修改讲一讲点评
	 * @param userId
	 * @param videoGradeId
	 * @param status
	 * @param remark
	 * @return
	 */
    GlobalReponse updateVideoGrade(Long userId, Long videoGradeId, Integer status, String remark);

	/**
	 * 获取讲一讲评论
	 * @param videoId
	 * @return
	 */
	GlobalReponse<List<VideoGrade>> getJyJComment(Long videoId);

	GlobalReponse saveShareNum(Long userId, Long videoId);

	/**
	 * 查询推荐小视频
	 * @param pageQuery
	 */
	void selectRecommendVideo(PageQuery<Video> pageQuery);

	/**
	 * 查询关注小视频
	 * @param pageQuery
	 */
	void selectAttentionVideo(PageQuery<Video> pageQuery);

	/**
	 * 查看同城小视频
	 * @param pageQuery
	 */
	void selectLocalVideo(PageQuery<Video> pageQuery);

	/**
	 * 查看小视频评论
	 * @param userId
	 * @param videoId
	 * @return
	 */
	GlobalReponse<SmallVideoCommentParentVo> getSmallVideoComment(Long userId, Long videoId);

    GlobalReponse<SmallVideoDetailsForIdVo> smallVideoDetailForId(Long userId, Long videoId);
}
