package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IMusicCollectionDao;
import com.boruan.shengtangfeng.core.dao.IMusicDao;
import com.boruan.shengtangfeng.core.dao.IMusicUseDao;
import com.boruan.shengtangfeng.core.dao.ISingerDao;
import com.boruan.shengtangfeng.core.entity.Music;
import com.boruan.shengtangfeng.core.entity.MusicCollection;
import com.boruan.shengtangfeng.core.entity.Singer;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.service.IMusicService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Slf4j
@Service
@Transactional(readOnly = true)
public class MusicService implements IMusicService {

    @Autowired
    private IMusicDao musicDao;
    @Autowired
    private IMusicCollectionDao musicCollectionDao;
    @Autowired
    private ISingerDao singerDao;

    @Override
    public void pageQuery(PageQuery<Music> pageQuery, Music music) {
        pageQuery.setParas(music);
        musicDao.pageQuery(pageQuery);
    }

    @Override
    public void pageQuery1(PageQuery<Music> pageQuery) {
        musicDao.pageQuery1(pageQuery);

    }

    @Override
    public void pageCollectionMusic(PageQuery<Music> pageQuery, Long userId) {
        Music music = new Music();
        music.set("userId",userId);
        pageQuery.setParas(music);
        musicDao.pageCollection(pageQuery);
    }

    @Override
    @Transactional(readOnly = false)
    public GlobalReponse collectMusic(Long musicId,Long userId) {
        MusicCollection has = musicCollectionDao.createLambdaQuery().andEq(MusicCollection::getMusicId,musicId).andEq(MusicCollection::getUid,userId).andEq(MusicCollection::getIsDeleted,false).single();
        if (has==null) {
            //空的话就添加一条数据
            MusicCollection c = new MusicCollection();
            c.setMusicId(musicId);
            c.setUid(userId);
            c.setIsDeleted(false);
            c.setCreateTime(new Date());
            c.setCreateBy(userId.toString());
            musicCollectionDao.insertTemplate(c);
            return GlobalReponse.success("收藏成功");
        }
        has.setIsDeleted(true);
        musicCollectionDao.updateTemplateById(has);
        return GlobalReponse.success("取消成功");
    }

    @Override
    @Transactional(readOnly = false)
    public void save(Music music) {
        if(music.getId()!=null){
            musicDao.updateById(music);
        }else{
            musicDao.insertTemplate(music);
        }
    }

    //删除音乐 type 删除音乐传0，删除热门音乐传1
    @Override
    @Transactional(readOnly = false)
    public void delMusic(Long musicId,Integer type) {
        Music music = musicDao.unique(musicId);
        if (type.equals(0)){
            music.setIsDeleted(true);
        }
        if (type.equals(1)){
            music.setIsHot(0);
        }
        music.setUpdateTime(new Date());
        musicDao.updateById(music);
    }

    @Override
    public Singer findSingerById(Long singerId) {
        Singer singer = singerDao.unique(singerId);
        return singer;
    }

    @Override
    public void pageQuerySinger(PageQuery<Singer> pageQuery) {
        singerDao.pageQuery(pageQuery);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveSinger(Singer singer) {
        if(singer.getId()!=null){
            singerDao.updateById(singer);
        }else{
            singerDao.insert(singer);
        }
    }

    @Override
    public Music findById(Long musicId) {
        Music music = musicDao.unique(musicId);
        return music;
    }

    @Override
    @Transactional(readOnly = false)
    public void delSinger(Long singerId) {
        Singer unique = singerDao.unique(singerId);
        unique.setIsDeleted(true);
        singerDao.updateTemplateById(unique);
    }
}
