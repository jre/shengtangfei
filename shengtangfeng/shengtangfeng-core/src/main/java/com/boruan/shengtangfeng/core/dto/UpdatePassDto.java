package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@author: 刘光强
@Description: 
@date:2020年3月10日 上午11:14:10
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("修改密码参数")
public class UpdatePassDto{
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("手机验证码")
    private String authCode;
    @ApiModelProperty("密码")
    private String password;
    
    public boolean checkParam(){
        if(StringUtils.isBlank(password)||StringUtils.isBlank(mobile)||StringUtils.isBlank(authCode)) {
            return false;
        }else {
            return true;
        }
    }
}
