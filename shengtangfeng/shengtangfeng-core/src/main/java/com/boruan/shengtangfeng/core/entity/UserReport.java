package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="user_report")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserReport extends IdEntity {

    /**
    * 举报内容 
    **/
    private String content ;
    /**
    * 举报图片 
    **/
    private String images ;
            
    /**
    * 举报人id 
    **/
    private Long informerId ;
            
    /**
    * 举报人昵称 
    **/
    private String informerName ;
            
    /**
    * 被举报人id 
    **/
    private Long personId ;
            
    /**
    * 被举报人昵称 
    **/
    private String personName ;
            
    /**
    * 原因id 
    **/
    private Long reasonsId ;

    /**
     * 原因
     */
    private String reasons;

    private String personAvatar;

    private String informerAvatar;

    /**
     * 审核状态：0待审核1忽略2已处理3封号
     */
    private Integer status;

    @ApiModelProperty(value = "被举报人状态")
    private Boolean personStatus;
}