package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: AccoladeDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1815:52
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="关注参数")
public class AttentionDto{


    /**
     * 关注人的ID
     **/
    @ApiModelProperty(value="关注人的ID")
    private Long focusUserId;
    /**
     * 邀请码
     **/
    @ApiModelProperty(value="邀请码")
    private String invitationCode ;



}
