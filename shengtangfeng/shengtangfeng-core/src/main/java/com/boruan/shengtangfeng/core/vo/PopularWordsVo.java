package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: PopularWordsVo
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1317:26
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PopularWordsVo {

    @ApiModelProperty("关键字 ")
    private String keyword;

    @ApiModelProperty("关键字类型1试题2文3视频 ")
    private Integer keywordType;
}
