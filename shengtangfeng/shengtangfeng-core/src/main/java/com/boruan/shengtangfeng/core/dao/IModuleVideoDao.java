package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.dto.ModuleVideoDto;
import com.boruan.shengtangfeng.core.entity.ModuleVideo;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IModuleVideoDao extends BaseMapper<ModuleVideo> {

    void pageModuleQuery(PageQuery<ModuleVideo> pageData);

    void pageQueryByModule(PageQuery<ModuleVideo> pageData, ModuleVideoDto moduleVideoDto);
}
