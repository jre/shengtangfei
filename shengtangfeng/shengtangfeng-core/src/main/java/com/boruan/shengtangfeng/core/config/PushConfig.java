package com.boruan.shengtangfeng.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.jiguang.common.ClientConfig;
import cn.jpush.api.JPushClient;

/**
 */
@Configuration
public class PushConfig {
	@Value("${push.jiguang.appKey}")
	private String appKey;
	@Value("${push.jiguang.appSecret}")
	private String appSecret;

	@Bean(name = "jPushClient")
	public JPushClient jPushClient() {
		return new JPushClient(appSecret, appKey, null, ClientConfig.getInstance());
	}
}
