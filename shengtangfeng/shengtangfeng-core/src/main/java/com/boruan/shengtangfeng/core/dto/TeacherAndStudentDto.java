package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.enums.Sex;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.TailBean;

/**
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TeacherAndStudentDto extends TailBean {

	@ApiModelProperty(value = "id")
	protected Long id;

	@ApiModelProperty(value = "在班级中的姓名")
	private String classUserName;

	@ApiModelProperty(value = "用户昵称")
	private String nickname;

	@ApiModelProperty(value = "用户状态")
	private Boolean isLocked;

	@ApiModelProperty(value = "用户头像")
	private String headImage;

	@ApiModelProperty(value = "用户手机号")
	private String mobile;

	@ApiModelProperty(value = "1男,2女")
	private  Sex sex;

	@ApiModelProperty(value = "出题科目")
	private String subjects;

}
