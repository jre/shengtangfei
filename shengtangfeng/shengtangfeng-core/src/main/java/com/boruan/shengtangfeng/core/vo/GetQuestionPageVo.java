package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GetQuestionPageVo extends BaseVo{

    /**
     * 类型
     **/
    @ApiModelProperty("类型")
    private QuestionType type;
    /**
     * 答案
     **/
    @ApiModelProperty("答案")
    private String answer;

    /**
     * 学科ID
     **/
    @ApiModelProperty("学科id")
    private Long subjectId;


    /**
     * 选项 里面放的是选项集合的JSON串
     **/
    @ApiModelProperty("选项 里面放的是选项集合的JSON串")
    private String content;

    /**
     * 文字解释
     **/
    @ApiModelProperty("文字解释")
    private String textExplain;

    /**
     * 题干
     **/
    @ApiModelProperty("题干")
    private String title;

    /**
     * 图片
     **/
    @ApiModelProperty("图片")
    private String images;

    /**
     * 正确率
     **/
    @ApiModelProperty("正确率")
    private BigDecimal accurate;
    /**
     * 分值
     */
    @ApiModelProperty("分值")
    private BigDecimal score;

    /**
     * 题库类型 0淘学题库 1共享题库
     */
    @ApiModelProperty("题库类型 0淘学题库 1共享题库")
    private Integer houseType;

    /**
     * 状态 0待审核 1审核通过 2审核拒绝
     */
    @ApiModelProperty("状态 0待审核 1审核通过 2审核拒绝")
    private Integer status;

    /**
     * 是否收藏
     **/
    @ApiModelProperty("是否收藏")
    private Boolean collect;

    /**
     * 是否使用
     **/
    @ApiModelProperty("是否使用")
    private Boolean isUse;

    /**
     * 创建人头像
     */
    @ApiModelProperty("创建人头像")
    private String createImage;

    /**
     * 创建人名称
     **/
    @ApiModelProperty("创建人名称")
    private String createName;

}
