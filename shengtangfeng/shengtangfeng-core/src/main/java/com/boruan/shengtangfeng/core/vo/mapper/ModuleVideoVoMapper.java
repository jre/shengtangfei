package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Module;
import com.boruan.shengtangfeng.core.entity.ModuleVideo;
import com.boruan.shengtangfeng.core.vo.ModuleVideoVo;
import com.boruan.shengtangfeng.core.vo.ModuleVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: guojiang
@Description:
@date:2021/3/3
*/
@Mapper
public interface ModuleVideoVoMapper extends VoMapper<ModuleVideoVo, ModuleVideo>  {
	ModuleVideoVoMapper MAPPER = Mappers.getMapper(ModuleVideoVoMapper.class);
}