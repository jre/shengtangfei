package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.BadWord;
import org.beetl.sql.core.engine.PageQuery;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IBadWordService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param badWord
	 */
	public void pageQuery(PageQuery<BadWord> pageQuery, BadWord badWord);
	/**
	 * 根据条件查询
	 * @param id
	 * @return
	 */
	public List<BadWord> findByTemplate(BadWord badWord);
	/**
     * 保存敏感词
     * @param badWord
     */
    public void save(BadWord badWord);
    
    public BadWord findById(Long id);
}
