package com.boruan.shengtangfeng.core.utils;

import org.apache.commons.lang3.StringUtils;

public class UserUtil {
	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;

	/**
	 * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
	 */
	public static String getSalt() {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		return Encodes.encodeHex(salt);

	}

	/**
	 * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
	 */
	public static String entryptPassword(String plainPassword, String salt) {

		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), Encodes.decodeHex(salt), HASH_INTERATIONS);
		return Encodes.encodeHex(hashPassword);
	}

	/**
	 * 根据salt返回密码，用户判断登录
	 */
	public static boolean match(String oldPassword, String password, String salt) {

		byte[] hashPassword = Digests.sha1(oldPassword.getBytes(), Encodes.decodeHex(salt), HASH_INTERATIONS);
		return StringUtils.equals(Encodes.encodeHex(hashPassword), password);
	}
}
