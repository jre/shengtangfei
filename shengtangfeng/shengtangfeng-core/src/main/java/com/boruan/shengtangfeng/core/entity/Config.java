package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.ConfigType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "config")
@Setter
@Getter
@NoArgsConstructor
public class Config extends IdEntity {

	/**
	 * 值类型 1 普通文本 2 富文本 3 图片地址
	 **/

	private ConfigType type;

	/**
	 * 描述
	 **/

	private String description;

	/**
	 * 配置key
	 **/

	private String keyword;

	/**
	 * 更新人
	 **/

	private String updateBy;

	/**
	 * 配置value
	 **/

	private String value;
	/**
	 * 分类
	 **/
	
	private String category;

}