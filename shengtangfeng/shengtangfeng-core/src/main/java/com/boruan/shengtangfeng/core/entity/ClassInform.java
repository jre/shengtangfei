package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="class_inform")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClassInform extends IdEntity {
            
    /**
    * 班级id
    **/
    private Long classId ;

    /**
    * 通知id
    **/
    private Long informId ;


}