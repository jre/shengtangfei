package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IFeedbackDao;
import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.service.IFeedBackService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class FeedBackService implements IFeedBackService {

	@Autowired
	private IFeedbackDao feedbackDao;
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<Feedback> pageQuery, Feedback feedback) {
		pageQuery.setParas(feedback);
		feedbackDao.pageQuery(pageQuery);
	}

    /**
     * @author: 刘光强
     * @Description:删除意见反馈 真删
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    @Transactional(readOnly = false)
    public GlobalReponse deleteFeedback(Long id) {
        feedbackDao.deleteById(id);
        return GlobalReponse.success();
        
    }
    
    /**
     * @author: 刘光强
     * @Description: 添加修改
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    @Transactional(readOnly = false)
    public void saveFeedback(Feedback feedback) {
        if (feedback.getId() == null) {
            KeyHolder kh = feedbackDao.insertReturnKey(feedback);
            feedback.setId(kh.getLong());
        } else {
            feedbackDao.updateById(feedback);
        }
    }
	
}
