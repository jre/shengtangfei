package com.boruan.shengtangfeng.core.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QuestionPageDto {


    @ApiModelProperty("0我的题库,1淘学题库,2共享题库")
    private Integer type;

    @ApiModelProperty("学科id 我的收藏传0")
    private Long subjectId;

    @ApiModelProperty("年级id")
    private Long gradeId;

    @ApiModelProperty("题目类型 0单选 1主观 2线下 3全部")
    private Integer status;

    @ApiModelProperty("题干")
    private String title;

    @ApiModelProperty("题干关键字")
    private String keyword;

    @ApiModelProperty("页数")
    private Integer pageNo=1;

}
