package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassInformMember;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IClassInformMemberDao extends BaseMapper<ClassInformMember> {
}
