package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 给学生打分参数
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PullStudentScoreDto {

    @ApiModelProperty("classId")
    private Long classId;

    @ApiModelProperty("作业id")
    private Long jobId;

    @ApiModelProperty("题目id")
    private Long questionId;

    @ApiModelProperty("学生id")
    private Long userId;

    @ApiModelProperty("分数")
    private BigDecimal score;
}
