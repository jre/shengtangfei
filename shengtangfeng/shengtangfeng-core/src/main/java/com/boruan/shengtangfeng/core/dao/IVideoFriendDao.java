package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.VideoFriend;
import com.boruan.shengtangfeng.core.vo.VideoFriendVo;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

public interface IVideoFriendDao extends BaseMapper<VideoFriend> {

    /**
     * 获取互动消息
     * @param userId
     * @return
     */
    List<VideoFriend> getInteractMessage(Long userId);
}
