package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 新闻主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="学院主体")
public class DepartmentVo extends BaseVo{

    /**
     */
    @ApiModelProperty(value="学院名")
    private String name;
    
    /**
     * 描述
     */
    @ApiModelProperty(value="描述")
    private String  description;
	
}
