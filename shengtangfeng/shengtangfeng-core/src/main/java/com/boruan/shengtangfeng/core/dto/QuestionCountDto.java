package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 题库数量
 */

@Data
public class QuestionCountDto {

    @ApiModelProperty("全部数量")
    private Long allNum;

    @ApiModelProperty("选择题数量")
    private Long choiceNum;

    @ApiModelProperty("主观题数量")
    private Long subjectiveNum;

    @ApiModelProperty("线下题数量")
    private Long offlineNum;
}
