package com.boruan.shengtangfeng.core.dao;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Log;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface ILogDao extends BaseMapper<Log> {
	/**
	 * 分页查询
	 * 
	 * @param query
	 */
	public void pageQuery(PageQuery<Log> query);
}
