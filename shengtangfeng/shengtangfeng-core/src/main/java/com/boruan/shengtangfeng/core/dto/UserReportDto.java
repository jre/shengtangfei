package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserReportDto extends BaseDto {

    @ApiModelProperty(value = "举报理由Id")
    private Long reasonId;

    @ApiModelProperty(value = "举报内容")
    private String content;

    @ApiModelProperty(value = "举报图片，多张用逗号拼接")
    private String images;

    @ApiModelProperty(value = "被举报人id")
    private Long personId;
}
