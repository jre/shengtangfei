package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.dto.StudentAnswerDto;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.QuestionRecord;

import java.util.List;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IQuestionRecordDao extends BaseMapper<QuestionRecord> {

	public List<QuestionRecord> findByIds(QuestionRecord record, List<Long> questionIdList);

    public List<QuestionRecord> myRecord(Long jobId, Long userId,Integer answerType);

    /**
     * 后台查询学生答题
     * @param query
     */
    void getStudentAnswer(PageQuery<StudentAnswerDto> query);

    void pageUserQuestionRecord(PageQuery<QuestionRecord> pageQuery);

    QuestionRecord findByCondition(QuestionRecord questionRecord);

    QuestionRecord hasAnswer(QuestionRecord record);

    List<QuestionRecord> getJobQuestionPage(Long classId, Long jobId, Long questionId);
}
