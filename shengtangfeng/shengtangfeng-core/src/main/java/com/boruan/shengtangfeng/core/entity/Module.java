package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 模块
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "module")
//@EqualsAndHashCode(callSuper = true)
public class Module extends IdEntity {

	private static final long serialVersionUID = -4470773369622255781L;

	/**
	 * 父模块ID
	 **/
	private Long parentId;
	/**
	 * 年级ID
	 **/
	private Long gradeId;
	/**
	 * 学科ID
	 **/
	private Long subjectId;
	/**
	 * 模块名
	 **/
	private String name;
	/**
	 * 题目数量
	 **/
	private Long questionCount;
	/**
	 * 模块描述
	 **/
	private String description;
	/**
	 * 分类图标
	 **/
	private String icon;

	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 难度 0-5
	 */
	private Integer difficulty;
	/**
	 * 完成人数
	 */
	private Integer completeCount;
	/**
	 * 模块下单题分数
	 */
	private Integer score;

	/**
	 * 1试题模块2视频模块
	 */
	private Integer type;
}
