package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;


public interface IAccoladeDao extends BaseMapper<Accolade> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Accolade> query);

    public Accolade getAccolade(Long userId,Long articleId,Integer type);

    public void  deleteAccolade(Long userId,Long articleId,Integer type);
}