package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.ArticleDto;
import com.boruan.shengtangfeng.core.dto.FeedbackDto;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Feedback;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author KongXH
 * @title: ArticleCommentDtoMapper
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1416:55
 */
@Mapper
public interface  ArticleDtoMapper extends DtoMapper<ArticleDto, Article> {
    ArticleDtoMapper MAPPER = Mappers.getMapper(ArticleDtoMapper.class);

    public Article toEntity(ArticleDto dto);

}
