package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="答题卡下标")
public class AnswerSheet{
    /**
     * 下标
     */
    @ApiModelProperty(value="下标")
    private Integer index;
    /**
     * 用户答案
     */
    @ApiModelProperty(value="用户答案")
    private String userAnswer;
    /**
     * 是否正确  未做过: -1, 做错: 0, 做对: 1 
     */
    @ApiModelProperty(value="是否正确  未做过: -1, 做错: 0, 做对: 1 ")
    private int isCorrect;

    @ApiModelProperty(value="实体类型")
    private Integer questionType;
}