package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Module;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IModuleDao extends BaseMapper<Module> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<Module> pageQuery);

	/**
	 *
	 * @Descrition:根据条件查找
	 * @return
	 */
	public List<Module> findIdByCondition(Module module);
	/**
	 *
	 * @Descrition:根据问题ID查找
	 * @return
	 */
	public List<Module> findByQuestion(Long questionId);

	/**
	 * 查找最大的序列
	 * @param search
	 */
	public Integer getMaxSequence(Module search);

	/**
	 *
	 * @Descrition:获取下一个模块
	 * @return
	 */
	public Module findNext(Long parentId,Long rowNum);
	/**
	 *
	 * @Descrition:获取模块的实际排序
	 * @return
	 */
	public Long findRowNum(Long parentId,Long moduleId);
	/**
	 *
	 * @Descrition:获取第一个模块
	 * @return
	 */
	public Module findFirst(Long parentId);

    List<Module> findIdByCondition1(Module search);

	/**
	 * 获取试题的模块
	 * @param questionId
	 */
	Module getModule(Long questionId);
}
