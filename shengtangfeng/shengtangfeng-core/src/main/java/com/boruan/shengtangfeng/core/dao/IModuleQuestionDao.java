package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.ModuleQuestion;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IModuleQuestionDao extends BaseMapper<ModuleQuestion> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<ModuleQuestion> pageQuery);

	/**
	 * 删除章节中的试题
	 * @param moduleId
	 * @param questionIdList
	 */
	void deleteByQuestionAndModuleId(Long moduleId, List<Long> questionIdList);

	/**
	 * 查找最大的序列
	 * @param moduleId
	 */
	public Integer getMaxSequence(Long moduleId,Integer type);
	public Integer getMaxSequenceC(Long moduleId);
	public ModuleQuestion searchUpOne(Integer sort1,Long moduleId,Integer type);
	public ModuleQuestion searchOne(Long moduleId,Integer type);
	public ModuleQuestion searchDownOne(Integer sort1,Long moduleId,Integer type);
	public ModuleQuestion searchDown(Long moduleId,Integer type);
	public ModuleQuestion findByModuleId(ModuleQuestion moduleQuestion);

	void update(ModuleQuestion moduleQuestion);
}
