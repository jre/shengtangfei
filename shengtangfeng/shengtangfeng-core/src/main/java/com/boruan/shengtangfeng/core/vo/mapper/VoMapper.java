package com.boruan.shengtangfeng.core.vo.mapper;

import java.util.List;

/**
 * Contract for a generic dto to entity mapper.
 @param <D> - DTO type parameter.
 @param <E> - Entity type parameter.
 */
public interface VoMapper <D, E> {

    public E toEntity(D vo);

    public D toVo(E entity);
    
    public List <E> toEntity(List<D> voList);

    public List <D> toVo(List<E> entityList);
}
