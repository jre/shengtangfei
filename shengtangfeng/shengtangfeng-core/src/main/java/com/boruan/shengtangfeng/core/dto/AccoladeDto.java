package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: AccoladeDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1815:52
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="点赞表")
public class AccoladeDto extends BaseDto{


    /**
     * 文章/视频id
     **/
    @ApiModelProperty(value="文章/视频id")
    private Long articleId ;



    /**
     * 类别1文2视频
     **/
    @ApiModelProperty(value="类别1文2视频")
    private Integer type ;



    /**
     * 用户id
     **/
    @ApiModelProperty(value="用户id")
    private Long userId ;


    @ApiModelProperty(value="发布人id")
    private Long publishId;//发布人id
}
