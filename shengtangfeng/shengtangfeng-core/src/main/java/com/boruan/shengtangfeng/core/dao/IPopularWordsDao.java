package com.boruan.shengtangfeng.core.dao;
import com.boruan.shengtangfeng.core.dto.PopularWordsDto;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;

import java.util.List;


public interface IPopularWordsDao extends BaseMapper<PopularWords> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<PopularWords> query);


    public List<PopularWords> getList(Integer type);
}