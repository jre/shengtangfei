package com.boruan.shengtangfeng.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;


@Table(name="system_user_role")
@Setter
@Getter
@NoArgsConstructor
public class SystemUserRole {
    
    private Long systemUserId;

    private Long roleId;

}