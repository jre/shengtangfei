package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Advertise;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IAdvertiseDao extends BaseMapper<Advertise> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<Advertise> pageQuery);

}
