package com.boruan.shengtangfeng.core.dao;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Config;

/**
 * @author: lihaicheng
 * @Description: 用来设置注册送积分
 * @date:2020年3月10日 上午11:14:10
 */
public interface IConfigDao extends BaseMapper<Config> {

	/**
	 * 获取值，后台配置死
	 */
	public Config getConfig(Long id);

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<Config> pageQuery);

	/**
	 * 根据key获取值
	 */
	Config getKey(String key);
}
