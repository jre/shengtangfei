package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ToAuditArticleDTO {

    @ApiModelProperty("文章分类id")
    private Long categoryId;

    @ApiModelProperty("用户id，昵称，文章Id，文章标题")
    private String content;

    @ApiModelProperty("第几页")
    private Integer pageNo;

    @ApiModelProperty("每页总条数")
    private Integer pageSize;

}
