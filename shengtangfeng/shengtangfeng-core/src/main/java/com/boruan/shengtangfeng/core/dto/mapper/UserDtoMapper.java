package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.UserDto;
import com.boruan.shengtangfeng.core.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Mapper
public interface UserDtoMapper extends DtoMapper<UserDto, User> {
    UserDtoMapper MAPPER = Mappers.getMapper(UserDtoMapper.class);

    @Mappings({
            @Mapping(target = "sex", expression = "java(com.boruan.shengtangfeng.core.utils.EnumUtil.getEnumByValue(com.boruan.shengtangfeng.core.enums.Sex.class,dto.getSex()))") })
    public User toEntity(UserDto dto);
    
    @Mappings({
        @Mapping(target = "sex", expression = "java(student.getSex().getValue())") })
    public UserDto toDto(User student);
    
}