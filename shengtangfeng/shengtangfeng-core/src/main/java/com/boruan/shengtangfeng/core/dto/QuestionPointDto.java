package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "章节")
public class QuestionPointDto extends BaseDto {
    

    /**
    * 考点名字 
    **/
    @ApiModelProperty(value = "考点名称")
    private String name ;

    /**
     * 父级id
     */
    @ApiModelProperty(value = "父级id，最高级节点为0")
    private Long parentId ;


}