package com.boruan.shengtangfeng.core.entity;

import java.io.Serializable;
import java.util.List;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 权限
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "permission")
@EqualsAndHashCode(callSuper = false)
public class Permission extends IdEntity implements Serializable {

	private static final long serialVersionUID = 8448235416305126624L;

	private String title;

	private String icon;

	private String index;

	private Long parentId;

	private List<Permission> subs;



	// =============== 以下暂时废弃 ===============
	/**
	 * 权限级别,第一级为2
	 * 
	 * @see Level
	 */
	private Integer level;
	/**
	 * 权限的名字，中文，例 用户创建，用于显示
	 */
	private String displayName;
	/**
	 * 权限的描述(中文)
	 */
	private String dscn;
	/**
	 * 权限的名字，英文，例 order:user:create
	 */
	private String name;

	/**
	 * 权限全路径
	 */
	private String permission;
	/**
	 * 权限对应的url
	 */
	private String url;

}
