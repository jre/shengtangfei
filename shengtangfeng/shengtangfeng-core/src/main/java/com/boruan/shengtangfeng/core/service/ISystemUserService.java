package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.SystemUser;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface ISystemUserService {
	/**
	 * 通过id查找用户
	 * @param id
	 * @return
	 */
	public SystemUser findById(Long id);
	/**
	 * 保存用户
	 * @param user
	 * @return
	 */
	public boolean save(SystemUser systemUser, Long roleId);
	/**
	 * 保存用户，附带保存用户密码
	 * @param user
	 */
	public void saveWithPassword(SystemUser systemUser);
	/**
	 * 根据用户手机号查找用户
	 * @param mobile
	 * @return
	 */
	public SystemUser findByMobile(String mobile);
	/**
	 * 根据用户手机号查找用户
	 * @param mobile
	 * @return
	 */
	public SystemUser findByLoginName(String loginName);
	/**
	 * 后台分页查询
	 * */
	public void pageQuery(PageQuery<SystemUser> query, SystemUser systemUser);

	public void pageSystemUser(PageQuery<SystemUser> query, SystemUser systemUser);
	/**
	 * 删除加盟商
	 * */
	public GlobalReponse delete(Long id);
	/**
	 * 获取全部加盟商
	 * */
	public List<SystemUser> findAll();
	/**
	 * 管理员修改密码
	 * */
	public GlobalReponse changePassword(SystemUser user, String oldPassword, String password);
	public SystemUser findId(Long id);
	
	/**
     * 通过id获取用户并查询关联角色
     * @param id
     * @return
     */
    public SystemUser findUserAndRole(Long id);
    /**
     * 删除用户的所有角色
     * @param userId
     */
    public void deleteUserRole(Long userId);
    /**
     * 更新用户的角色信息
     * @param user
     * @param roleIds
     * @return
     */
    public Boolean updateUserRole(SystemUser user,List<Long> roleIds);
    
    /**
     * 根据工号或者姓名查询老师
     * @param assistantNo
     * @param assistantName
     * @return
     */
    public List<SystemUser> findByEmployeeNumberOrName(String assistantNo,String assistantName);
}
