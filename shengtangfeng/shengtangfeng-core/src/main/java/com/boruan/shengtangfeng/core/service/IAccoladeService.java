package com.boruan.shengtangfeng.core.service;

import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
 * @author KongXH
 * @title: ILikeService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1715:30
 */
public interface IAccoladeService {

    public void pageQuery(PageQuery<Accolade> query);

    public GlobalReponse  saveAccolade(Accolade accolade);

    public Accolade getAccolade(Long userId, Long articleId,Integer type);

//    public void  deleteAccolade(Long userId,Long articleId,Integer type);
}
