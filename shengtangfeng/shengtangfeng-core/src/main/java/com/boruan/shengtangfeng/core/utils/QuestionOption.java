package com.boruan.shengtangfeng.core.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QuestionOption {
	@ApiModelProperty("选项类型")
	private String type;
	@ApiModelProperty("选项内容")
	private String content;
	@ApiModelProperty("选项是否正确")
	private Boolean isCorrect;
	@ApiModelProperty("选项是否正确")
	private Boolean ischecked;

	private Integer optionNo;

}
