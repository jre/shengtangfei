package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Remind;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IRemindDao extends BaseMapper<Remind> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Remind> query);
}