package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.QuestionRecord;
import com.boruan.shengtangfeng.core.utils.Global;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.QuestionVo;
import org.beetl.sql.core.engine.PageQuery;
import com.boruan.shengtangfeng.core.entity.FavoritesQuestion;
import com.boruan.shengtangfeng.core.entity.IncorrectQuestion;
import com.boruan.shengtangfeng.core.entity.Question;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IQuestionService {
    
    /**
     * 根据章节ID获取题目列表
     * 
     * @param moduleId
     */
    public List<Question> findByModule(Long moduleId);
    /**
     * 根据条件查询试题
     * 
     * @param 
     */
    public List<Question> templateQuestion(Question question);
    /**
     * 分页查询
     * 
     * @param pageQuery
     * @param question
     */
    public void pageQuery(PageQuery<Question> pageQuery, Question question);
    /**
     * 获取已添加到章节下的题目
     * @param pageData
     */
    void pageQueryByModule(PageQuery<Question> pageData,Question question);
    /**
     * 根据id获取
     * 
     * @param id
     * @return
     */
    public Question findById(Long id);
    /**
     * 插入试题年级的关系
     * 
     * @return
     */
    public void insertQuestionGrade(Long questionId, Long[] gradeIds);
    /**
     * 插入试题年级的关系
     *
     * @return
     */
    public List<Long> getQuestionGrades(Long questionId);

    /**
     * 根据题目数量
     * 
     * @return
     */
    public long getQuestionCount();

    /**
     * 保存分类
     * 
     * @param question
     */
    public void save(Question question);

    /**
     * 保存分类
     *
     * @param question
     */
    public void saveHistory(Question question);


    /**
     * 添加错题记录
     * 
     * @param incorrectQuestion
     */
    public void saveIncorrect(IncorrectQuestion incorrectQuestion);

    /**
     * 分页查询错题本
     * 
     * @param pageQuery
     * @return
     */
    public void pageIncorrect(PageQuery<Question> pageQuery, IncorrectQuestion incorrectQuestion);

    /**
     * 查询错题本
     * 
     * @param userId categoryId
     * @return
     */
    public List<Question> getIncorrectList(Long userId, Integer type);

    /**
     * 查询错题本数量
     * 
     * @param userId categoryId
     * @return
     */
    public Integer getIncorrectCount(Long userId, Integer type);
    /**
     * 查看可添加到章节的试题列表
     * @param pageData
     */
    void getModuleQuestions(PageQuery<Question> pageData);
    /**
     * 分页查询收藏
     * 
     * @param pageQuery
     * @return
     */
    public void pageFavorites(PageQuery<Question> pageQuery, FavoritesQuestion favoritesQuestion);

    /**
     * 查询收藏
     * 
     * @param userId categoryId
     * @return
     */
    public List<Question> getFavoritesList(Long userId, Integer type);
    
    /**
     * 查询做错的题
     * 
     * @param userId subject
     * @return
     */
    public List<Question> getIncorrectQuestion(Long userId,Long subjectId);
    /**
     * 查询收藏的题
     * 
     * @param userId subject
     * @return
     */
    public List<Question> getFavoritesQuestion(Long userId,Long subjectId);

    /**
     * 查询收藏数量
     * 
     * @param userId categoryId
     * @return
     */
    public Long getFavoritesCount(Long userId, Integer type);

    /**
     * 收藏试题
     * 
     * @param questionId userId
     * @return
     */
    public void addFavorites(Long moduleId,Long questionId, Long userId);

    /**
     * 取消收藏试题
     * 
     * @param questionId userId
     * @return
     */
    public void removeFavorites(Long questionId, Long userId);

    /**
     * 校验用户是否收藏
     * 
     * @param userId
     * @param questionId
     * @return
     */
    public boolean isFavorites(Long userId, Long questionId);

    /**
     * 添加错题
     * 
     * @param questionId userId
     * @return
     */
    public void addIncorrect(Long moduleId,Long questionId, Long userId);
    /**
     * 删除错题
     * 
     * @param questionId userId
     * @return
     */
    public void delIncorrect(Long questionId, Long userId);

    /**
     * 删除错题
     * 
     * @param questionId userId
     * @return
     */
    public void removeIncorrect(Long questionId, Long userId);



    /**
     * 根据类型删除错题
     * 
     * @return
     */
    public boolean removeIncorrectQuestionType(Integer[] types, Long userId);

    public Question getQuestionClass(Long id);

    /**
     * 根据题目id删除题目
     */
    public int delById(String id, Long adminId);
    
    /**
     * 获取收藏的数量
     * @param userId
     * @return
     */
    public Long getFavoritesCount(Long userId);

    /**
     * 审核端根据评论id查询题目
     * @param commentId
     * @return
     */
    GlobalReponse<QuestionVo> getQuestionDetail(Long commentId);

    public void getSelfUpQuestion(PageQuery<IncorrectQuestion> pageQuery);

    public void auditSelfQuestion(PageQuery<IncorrectQuestion> pageQuery);

    public IncorrectQuestion findInQuestion(Long inId);

    List<Question> jobQuestion(Long jobId);

    void pageQuestionRecord(PageQuery<QuestionRecord> pageQuery, QuestionRecord search);

    void auditInQuestion(IncorrectQuestion incorrectQuestion);
}