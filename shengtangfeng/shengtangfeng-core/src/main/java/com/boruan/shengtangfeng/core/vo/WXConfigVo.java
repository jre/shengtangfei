package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@author: lihaicheng
@Description: 微信订单返回参数
@date:2020年3月10日 上午11:14:10
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class WXConfigVo {
	
    @ApiModelProperty(value = "应用APPID")
    private String appId;
   
    @ApiModelProperty(value = "随机字符串")
    private String nonceStr;
    
    @ApiModelProperty(value = "时间戳")
    private String timeStamp;
    
    @ApiModelProperty(value = "签名")
    private String sign;
    
    @ApiModelProperty(value = "预支付交易会话ID")
    private String prepayId;
    
    @ApiModelProperty(value = "商户号")
    private String partnerId;
    
    @ApiModelProperty(value = "扩展字段")
    private String packageStr;
    
    @ApiModelProperty(value = "订单id, 查看支付结果需要用到")
    private Long orderId;
}
