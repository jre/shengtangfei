package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Role;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IRoleDao extends BaseMapper<Role> {
	/**
	 * 获取用户的角色列表
	 * 
	 * @param userId
	 * @return
	 */
	public List<Role> findByUserId(Long systemUserId);

	/**
	 * 分页查询
	 * 
	 * @param query
	 */
	public void pageQuery(PageQuery<Role> query);

	/**
	 * 根据名称获取系统默认的角色
	 * 
	 * @param name
	 * @param isSystemRole
	 * @return
	 */
	public Role findSystemRoleByNameAndIsSystemRole(@Param("name") String name,
			@Param("isSystemRole") boolean isSystemRole);

	/**
	 * 获取系统角色列表
	 * 
	 * @return
	 */
	public List<Role> findSysRole();

	/**
	 * 删除角色的所有权限
	 * 
	 * @param userId
	 */
	public void deleteRolePermission(@Param("roleId") Long roleId);

	/**
	 * 插入角色权限的关联关系
	 * 
	 * @param userId
	 * @param roleId
	 */
	public void insertRolePermission(@Param("roleId") Long roleId, @Param("permissionId") Long permissionId);
}
