package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.dto.AddressDto;
import com.boruan.shengtangfeng.core.dto.SchoolListDto;
import com.boruan.shengtangfeng.core.entity.School;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface ISchoolDao extends BaseMapper<School> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<School> query);

    /**
     * 分页查询学校
     * @param query
     */
    void findSchoolPage(PageQuery<School> query);

    /**
     * 获取省名称
     * @return
     */
    String getProvince(String provinceId);

    /**
     * 获取市名称
     * @return
     */
    String getCity(String cityId);

    /**
     * 获取区名称
     * @return
     */
    String getCounty(String countyId);

    /**
     * 获取所有的省
     * @return
     */
    List<AddressDto> getAllProvince();

    /**
     * 获取所有的市
     * @return
     */
    List<AddressDto> getAllCity(Long provinceId);

    /**
     * 获取所有的区
     * @return
     */
    List<AddressDto> getAllCounty(Long cityId);

    /**
     * 分页查询学校列表
     * @param query
     */
    void schoolPage(PageQuery<SchoolListDto> query);



}