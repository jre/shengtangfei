package com.boruan.shengtangfeng.core.service.impl;


import com.boruan.shengtangfeng.core.dao.*;
import com.boruan.shengtangfeng.core.dto.ClassContentDto;
import com.boruan.shengtangfeng.core.dto.InformDto;
import com.boruan.shengtangfeng.core.dto.QuestionDetailDto;
import com.boruan.shengtangfeng.core.dto.StudentAnswerDto;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.service.IClassContentService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author: guojiang
 * @Description:班级内容
 * @date:2021/4/28
 */
@Service
@Transactional(readOnly = false)
public class ClassContentServiceImpl implements IClassContentService {
    @Autowired
    private IQuestionDao questionDao;
    @Autowired
    private IGradeDao gradeDao;
    @Autowired
    private ISubjectDao subjectDao;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private InformDao informDao;
    @Autowired
    private IQuestionRecordDao questionRecordDao;

    /**
     * 多条件查询题目列表
     *
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    @Override
    public GlobalReponse<PageQuery<QuestionDetailDto>> getQuestionPage(Integer pageNo, Integer pageSize, ClassContentDto dto) {
        PageQuery<Question> query = new PageQuery<>();
        query.setPageNumber(pageNo);
        query.setPageSize(pageSize);
        query.setPara("status", dto.getStatus());
        query.setPara("userId", dto.getUserId());
        query.setPara("type", dto.getType());
        query.setPara("userNickname", dto.getUserNickname());
        query.setPara("content", dto.getContent());
        questionDao.getQuestionPage(query);
        ArrayList<QuestionDetailDto> list = new ArrayList<>();
        for (Question question : query.getList()) {
            QuestionDetailDto detailDto = new QuestionDetailDto();
            BeanUtils.copyProperties(question, detailDto);
            Long questionGrade = questionDao.getQuestionGrade(question.getId());
            Grade grade = gradeDao.unique(questionGrade);
            Subject subject = subjectDao.unique(question.getSubjectId());
            User user = userDao.unique(question.getCreateBy());
            detailDto.setGradeName(grade.getName());
            detailDto.setSubjectName(subject.getName());
            detailDto.setCreateId(user.getId());
            detailDto.setCreateName(user.getName());
            detailDto.setCreateStatus(user.getIsLocked());
            detailDto.setCreateImage(user.getHeadImage());
            detailDto.set("user", user);
            list.add(detailDto);
        }
        query.setList(list);
        return GlobalReponse.success(query);
    }

    /**
     * 审核题目 1通过 2拒绝
     *
     * @param questionId
     * @param status
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse auditQuestion(Long questionId, Integer status, Long userId) {
        Question question = questionDao.unique(questionId);
        question.setStatus(status);
        question.setUpdateBy(userId.toString());
        question.setUpdateTime(new Date());
        questionDao.updateTemplateById(question);
        return GlobalReponse.success("审核成功");
    }

    /**
     * 批量或者单个删除题目
     *
     * @param questionIds
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedQuestion(Long[] questionIds, Long userId) {
        for (Long questionId : questionIds) {
            Question question = questionDao.unique(questionId);
            question.setIsDeleted(true);
            question.setUpdateBy(userId.toString());
            question.setUpdateTime(new Date());
            questionDao.updateTemplateById(question);
        }
        return GlobalReponse.success("删除成功");
    }

    /**
     * 批量封号或者批量解封
     *
     * @param createIds
     * @param status
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse isLockedUser(Long[] createIds, Integer status, Long userId) {
        if (status == 0) {
            for (Long createId : createIds) {
                User user = userDao.unique(createId);
                user.setIsLocked(false);
                user.setUpdateBy(user.toString());
                user.setUpdateTime(new Date());
                userDao.updateTemplateById(user);
            }
            return GlobalReponse.fail("解封成功");
        }
        if (status == 1) {
            for (Long createId : createIds) {
                User user = userDao.unique(createId);
                user.setIsLocked(true);
                user.setUpdateBy(user.toString());
                user.setUpdateTime(new Date());
                userDao.updateTemplateById(user);
            }
            return GlobalReponse.fail("封号成功");
        }

        return GlobalReponse.fail("操作失败");
    }

    /**
     * 多条件查询班级通知列表
     *
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    @Override
    public GlobalReponse<PageQuery<InformDto>> getInformPage(Integer pageNo, Integer pageSize, ClassContentDto dto) {
        PageQuery<Inform> query = new PageQuery<>();
        query.setPageNumber(pageNo);
        query.setPageSize(pageSize);
        query.setPara("status", dto.getStatus());
        query.setPara("userId", dto.getUserId());
        query.setPara("userNickname", dto.getUserNickname());
        query.setPara("content", dto.getContent());
        informDao.getInformPage(query);
        ArrayList<InformDto> list = new ArrayList<>();
        for (Inform inform : query.getList()) {
            InformDto informDto = new InformDto();
            BeanUtils.copyProperties(inform, informDto);
            User user = userDao.unique(inform.getCreateBy());
            informDto.setCreateId(user.getId());
            informDto.setCreateNickname(user.getName());
            informDto.setCreateStatus(user.getIsLocked());
            informDto.setCreateImage(user.getHeadImage());
            informDto.set("user", user);
            list.add(informDto);
        }
        PageQuery<InformDto> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(query.getPageNumber());
        pageQuery.setPageSize(query.getPageSize());
        pageQuery.setTotalRow(query.getTotalRow());
        pageQuery.setList(list);
        return GlobalReponse.success(pageQuery);
    }

    /**
     * 审核通知
     *
     * @param informId
     * @param status
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse auditInform(Long informId, Integer status, Long userId) {
        Inform inform = informDao.unique(informId);
        inform.setStatus(status);
        inform.setUpdateBy(userId.toString());
        inform.setUpdateTime(new Date());
        informDao.updateTemplateById(inform);
        return GlobalReponse.success("审核成功");
    }

    /**
     * 批量或者单个删除班级通知
     *
     * @param informIds
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedInform(Long[] informIds, Long userId) {
        for (Long informId : informIds) {
            Inform inform = informDao.unique(informId);
            inform.setIsDeleted(true);
            inform.setUpdateBy(userId.toString());
            inform.setUpdateTime(new Date());
            informDao.updateTemplateById(inform);
        }
        return GlobalReponse.success("删除成功");
    }

    /**
     * 多条件查询学生答题列表
     *
     * @param pageNo
     * @param pageSize
     * @param dto
     * @return
     */
    @Override
    public GlobalReponse<PageQuery<StudentAnswerDto>> getStudentAnswer(Integer pageNo, Integer pageSize, ClassContentDto dto) {
        PageQuery<StudentAnswerDto> query = new PageQuery<>();
        query.setPageNumber(pageNo);
        query.setPageSize(pageSize);
        query.setPara("status", dto.getStatus());
        query.setPara("userId", dto.getUserId());
        query.setPara("userNickname", dto.getUserNickname());
        query.setPara("content", dto.getContent());
        questionRecordDao.getStudentAnswer(query);
        for (StudentAnswerDto answerDto : query.getList()) {
            User user = userDao.unique(answerDto.getUserId());
            answerDto.setUserStatus(user.getIsLocked());
            answerDto.set("user", user);
        }
        return GlobalReponse.success(query);
    }

    /**
     * 审核学生答题
     *
     * @param questionRecordId
     * @param status
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse auditStudentAnswer(Long questionRecordId, Integer status, Long userId) {
        QuestionRecord questionRecord = questionRecordDao.unique(questionRecordId);
        questionRecord.setStatus(status);
        questionRecord.setUpdateBy(userId.toString());
        questionRecord.setUpdateTime(new Date());
        questionRecordDao.updateTemplateById(questionRecord);
        return GlobalReponse.success("审核成功");
    }

    /**
     * 批量或者单个删除学生答题
     *
     * @param questionRecordIds
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedStudentAnswer(Long[] questionRecordIds, Long userId) {
        for (Long questionRecordId : questionRecordIds) {
            QuestionRecord questionRecord = questionRecordDao.unique(questionRecordId);
            questionRecord.setIsDeleted(true);
            questionRecord.setUpdateBy(userId.toString());
            questionRecord.setUpdateTime(new Date());
            questionRecordDao.updateTemplateById(questionRecord);
        }
        return GlobalReponse.success("删除成功");
    }
}
