package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Log;
import org.beetl.sql.core.engine.PageQuery;

/**
 * 
 * @author liuguangqiang
 * @date 2016年10月31日 上午8:21:35
 */
public interface ILogService {

	/**
	 * 保存日志到日志队列中
	 * @param log
	 * @return
	 */
	public boolean save(Log log);
	/**
	 * 保存日志到数据库
	 * @param log
	 * @return
	 */
	public void saveImmediately(Log log);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param log
	 */
	public void pageQuery(PageQuery<Log> pageQuery,Log log);
}
