package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.AccoladeDto;
import com.boruan.shengtangfeng.core.dto.FavoritesDto;
import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.entity.Favorites;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author KongXH
 * @title: FavoritesDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1714:18
 */
@Mapper
public interface AccoladeDtoMapper extends DtoMapper<AccoladeDto, Accolade> {
    AccoladeDtoMapper MAPPER = Mappers.getMapper(AccoladeDtoMapper.class);

    public Accolade toEntity(AccoladeDto dto);
}
