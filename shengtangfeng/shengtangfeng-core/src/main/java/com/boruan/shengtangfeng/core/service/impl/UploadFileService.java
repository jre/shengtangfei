package com.boruan.shengtangfeng.core.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boruan.shengtangfeng.core.service.IUploadFileService;
import com.boruan.shengtangfeng.core.utils.AudioConvertUtils;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: lihaicheng
 * @Description: 上传文件
 * @date:2020年3月10日 上午11:14:10
 */
@SuppressWarnings("all")
@Service
@Transactional(readOnly = true)
public class UploadFileService implements IUploadFileService {
	@Value("${system.baseUrl}")
	private String baseUrl;
	@Value("${upload.file.public.basedir}")
	private String baseDir;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @author: lihaicheng
	 * @Description: 上传文件
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public GlobalReponse uploadImageFile(MultipartFile file, Long userId) {
		try {
			if (file.isEmpty()) {
				return GlobalReponse.fail();
			}
			String fileName = file.getOriginalFilename();
			String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
			File dest = new File(baseDir + "resources/" + userId + "_" + UUIDUtil.getUUID() + "." + suffix);
			File destMp3 = new File(baseDir + "resources/" + userId + "_" + UUIDUtil.getUUID() + ".mp3");
			if (!dest.getParentFile().exists()) {
				dest.getParentFile().mkdirs();
			}
			try {
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
				out.write(file.getBytes());
				out.flush();
				out.close();
				if(StringUtils.containsAny(suffix, "wav", "amr", "m4a", "WAV", "AMR", "M4A")) {
				    AudioConvertUtils.toMp3(dest, destMp3);
				}
			} catch (IOException e) {
			    e.printStackTrace();
				return GlobalReponse.fail();
			}
			GlobalReponse reponse= GlobalReponse.success("上传成功");
			if(StringUtils.containsAny(suffix, "wav", "amr", "m4a", "WAV", "AMR", "M4A")) {
                reponse.setData(baseUrl + "/publicFile/resources/" + destMp3.getName());
            }else {
                reponse.setData(baseUrl + "/publicFile/resources/" + dest.getName());
            }
			return reponse;
		} catch (Exception e) {
			e.printStackTrace();
			return GlobalReponse.fail("上传失败");
		}
	}

	/**
	 * @author: lihaicheng
	 * @Description: 批量上传
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public GlobalReponse uploadImageFiles(MultipartFile[] files, Long userId) {
		try {
			if (files.length == 0) {
				return GlobalReponse.fail("没有文件");
			}
			List<String> result=new ArrayList<String>();
			BufferedOutputStream out = null;
			for (MultipartFile file : files) {
				try {
					String fileName = file.getOriginalFilename();
					String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
					File dest = new File(baseDir + "resources/" + userId + "_" + UUIDUtil.getUUID() + "." + suffix);
					if (!dest.getParentFile().exists()) {
						dest.getParentFile().mkdirs();
					}
					out = new BufferedOutputStream(new FileOutputStream(dest));
					out.write(file.getBytes());
					out.flush();

					result.add(baseUrl + "/publicFile/resources/" + dest.getName());
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return GlobalReponse.success("上传成功", result);
		} catch (Exception e) {
			e.printStackTrace();
			return GlobalReponse.fail("上传失败");
		}
	}

}
