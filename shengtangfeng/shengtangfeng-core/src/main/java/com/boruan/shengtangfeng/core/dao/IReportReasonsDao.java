package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ReportReasons;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IReportReasonsDao extends BaseMapper<ReportReasons> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ReportReasons> query);
}