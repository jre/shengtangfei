package com.boruan.shengtangfeng.core.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 @author: guojiang
 @Description:
 @date:2021年3月31日 下午14:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserRelevanceVo {

    @ApiModelProperty("关联id")
    private Long relevanceId;

    @ApiModelProperty("关联头像")
    private String relevanceImage;

    @ApiModelProperty("关联人员名称")
    private String relevanceNickName;

    @ApiModelProperty("8位数账号")
    private String account;

    @ApiModelProperty("是否为好友")
    private Boolean isFriend;

    @ApiModelProperty("身份 0普通成员 1管理员 2群主")
    private Integer type;

}
