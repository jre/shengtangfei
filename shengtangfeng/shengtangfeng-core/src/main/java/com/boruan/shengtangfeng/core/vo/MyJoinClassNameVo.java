package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MyJoinClassNameVo {

    @ApiModelProperty("班级Id")
    private Long id;

    @ApiModelProperty("班级名称")
    private String className;
}
