package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;

/**
 * 作业参数
 */
@Data
public class JobParameterDto {

    @ApiModelProperty("作业参数Id")
    private Long id ;

    @ApiModelProperty("最低正确率")
    private BigDecimal accuracy ;

    @ApiModelProperty("截止时间迟于提交时间多少分钟")
    private String minute ;

    @ApiModelProperty("设置题目分数范围")
    private Integer score ;
}