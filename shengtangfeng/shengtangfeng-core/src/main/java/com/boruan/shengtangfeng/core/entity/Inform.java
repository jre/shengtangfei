package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="inform")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Inform extends IdEntity {
    

            
    /**
    * 状态 0待审核 1通过 2拒绝 
    **/
    private Integer status ;
            
    /**
    * 通知内容 
    **/
    private String content ;
    /**
    * 通知图片 
    **/
    private String images ;


}