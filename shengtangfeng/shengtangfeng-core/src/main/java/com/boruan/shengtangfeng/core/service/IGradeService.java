package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Subject;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

/**
 * 
 * @author 刘光强
 * @date 2020年8月12日16:08:21
 */
public interface IGradeService {

	/**
	 * 根据id查找
	 * @return
	 */
	public Grade findById(Long id);
	/**
	 * 保存
	 * @param grade
	 * @return
	 */
	public void save(Grade grade);
	/**
	 * 获取年级列表
	 * @return
	 */
	public List<Grade> getList(Grade grade);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param grade
	 */
	public void pageQuery(PageQuery<Grade> pageQuery,Grade grade);
	
	/**
     * 根据学科ID获取年级列表
     * @return
     */
    public List<Grade> getListBySubjectId(Long subjectId);
}
