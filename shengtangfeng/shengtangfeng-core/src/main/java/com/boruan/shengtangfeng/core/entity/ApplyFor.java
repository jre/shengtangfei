package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="apply_for")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApplyFor extends IdEntity {
            
    /**
    * 状态 0等待验证 1同意 2拒绝 
    **/
    private Integer status ;
            
    /**
    * 类型 0个人申请 1群聊申请 2班级申请
    **/
    private Integer type ;
            
    /**
    * 申请人id 
    **/
    private Long applyId ;
            
    /**
    * 申请备注 
    **/
    private String applyRemark ;

    /**
    * 申请加入的id 
    **/
    private Long relevanceId ;

    /**
     * 是否已读
     **/
    private Integer isRead ;

}