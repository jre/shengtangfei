package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="class_members")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClassMembers extends IdEntity {
    

    /**
    * 类型 1学生 2老师 3创建人
    **/
    private Integer type ;
            
    /**
    * 班级id 
    **/
    private Long classId ;
            
    /**
    * 昵称 
    **/
    private String nickname ;
    /**
    * 成员id 
    **/
    private Long userId ;

    /**
     * 头像
     */
    private String headImage;
//
//    /**
//     * 状态：0待审核1通过2拒绝
//     */
//    private Integer status;

}