package com.boruan.shengtangfeng.core.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.boruan.shengtangfeng.core.dao.*;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.service.IQuestionRecordService;

/**
 * @author: 刘光强
 * @Description: 答题记录管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class QuestionRecordService implements IQuestionRecordService {
	@Autowired
	private IQuestionRecordDao questionRecordDao;
	@Autowired
	private IJobQuestionDao jobQuestionDao;
	@Autowired
	private IUserJobDao userJobDao;
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IJobBackDao jobBackDao;


	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(QuestionRecord questionRecord) {
		if (questionRecord.getId() == null) {
			KeyHolder kh = questionRecordDao.insertReturnKey(questionRecord);
			questionRecord.setId(kh.getLong());
		} else {
			questionRecordDao.updateById(questionRecord);
		}

	}


    @Override
    public List<QuestionRecord> template(QuestionRecord questionRecord) {
        return questionRecordDao.template(questionRecord);
    }


    @Override
    public long templateCount(QuestionRecord questionRecord) {
        return questionRecordDao.templateCount(questionRecord);
    }

	@Override
	public QuestionRecord hasAnswer(QuestionRecord record) {
		return questionRecordDao.findByCondition(record);
	}

	@Override
	public List<QuestionRecord> myRecordByJob(Long jobId,Long userId,Integer type) {
		List<QuestionRecord> list = questionRecordDao.myRecord(jobId,userId,type);
		return list;
	}

	@Override
	public QuestionRecord findByCondition(QuestionRecord questionRecord) {
		return questionRecordDao.findByCondition(questionRecord);
	}

	/**
	 * 完成作业
	 *
	 * @param jobId
	 * @return
	 */
	@Override
	@Transactional(readOnly = false)
	public GlobalReponse finishJobPush(Long jobId,Long userId,Long useTime,Long classId) {
		List<JobQuestion> list = jobQuestionDao.createLambdaQuery().andEq(JobQuestion::getClassJobId, jobId).andEq(JobQuestion::getIsDeleted, false).select();
		QuestionRecord record = new QuestionRecord();
		record.setUserId(userId);
		record.setJobId(jobId);
		record.setClassId(classId);
		record.setIsDeleted(false);
		List<QuestionRecord> template = questionRecordDao.template(record);
		BigDecimal zongScore = new BigDecimal(0);
		Boolean isCorrect=false;
		for (QuestionRecord questionRecord : template) {
			if (questionRecord.getAnswerType()==0){
				if (questionRecord.getScore()!=null){
					zongScore=questionRecord.getScore().add(zongScore);
				}
			}
			if (questionRecord.getAnswerType()==1 || questionRecord.getAnswerType()==2){
				isCorrect=true;
			}
		}
		UserJob userJob = userJobDao.createLambdaQuery().andEq(UserJob::getClassId,classId).andEq(UserJob::getJobId, jobId).andEq(UserJob::getUserId, userId).andEq(UserJob::getIsDeleted, false).single();
		if (userJob!=null){
			userJob.setScore(zongScore);
			userJob.setFinishTime(new Date());
			userJob.setUpdateTime(new Date());
			userJob.setUseTime(useTime);
			if (isCorrect==true){
				userJob.setStatus(1);
			}else {
				userJob.setStatus(2);
			}
		}else {
			UserJob job = new UserJob();
			job.setClassId(classId);
			job.setJobId(jobId);
			job.setUserId(userId);
			job.setScore(zongScore);
			job.setFinishTime(new Date());
			job.setCreateBy(userId.toString());
			job.setCreateTime(new Date());
			job.setStatus(1);
			job.setUseTime(useTime);
			if (isCorrect==true){
				job.setStatus(1);
			}else {
				job.setStatus(2);
			}
			userJobDao.insertTemplate(job);
			JobBack jobBack = jobBackDao.createLambdaQuery().andEq(JobBack::getJobId, jobId).andEq(JobBack::getUserId, userId).andEq(JobBack::getIsDeleted, false).single();
			if (jobBack!=null){
				jobBack.setStatus(1);
				jobBack.setUpdateBy(userId.toString());
				jobBack.setUpdateTime(new Date());
				jobBackDao.updateTemplateById(jobBack);
			}
			if (isCorrect==true){
				return GlobalReponse.success("提交成功,等待老师批改");
			}else {
				return GlobalReponse.success("提交成功");
			}
		}
		userJob.setUpdateBy(userId.toString());
		userJobDao.updateTemplateById(userJob);
		JobBack jobBack = jobBackDao.createLambdaQuery().andEq(JobBack::getJobId, jobId).andEq(JobBack::getUserId, userId).andEq(JobBack::getIsDeleted, false).single();
		if (jobBack!=null){
			jobBack.setStatus(1);
			jobBack.setUpdateBy(userId.toString());
			jobBack.setUpdateTime(new Date());
			jobBackDao.updateTemplateById(jobBack);
		}
		if (isCorrect==true){
			return GlobalReponse.success("提交成功,等待老师批改");
		}else {
			return GlobalReponse.success("提交成功");
		}
	}
}
