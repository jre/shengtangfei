package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: FavoritesDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 收藏
 * @date 2020/8/1714:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FavoritesDto {



    /**
     * 文章/视频id
     **/
    @ApiModelProperty("章/视频id")
    private Long articleId ;


    /**
     * 类别1文2视频3试题
     **/
    @ApiModelProperty("类别1文章2视频")
    private String type ;


}
