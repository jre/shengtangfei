package com.boruan.shengtangfeng.core.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.dto.FeedbackDto;
import com.boruan.shengtangfeng.core.entity.Feedback;

/**
 * @author: lihaicheng
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Mapper
public interface FeedbackDtoMapper extends DtoMapper<FeedbackDto, Feedback> {
    FeedbackDtoMapper MAPPER = Mappers.getMapper(FeedbackDtoMapper.class);

    public Feedback toEntity(FeedbackDto dto);
}