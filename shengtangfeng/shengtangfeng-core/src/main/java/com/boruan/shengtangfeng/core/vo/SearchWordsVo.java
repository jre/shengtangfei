package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 答题卡主体
 * 
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "搜索历史词主体")
public class SearchWordsVo {

    /**
     * 关键字
     **/
    @ApiModelProperty("关键字 ")
    private String keyword;

    /**
     * 类型 1 试题 2 文章 3 视频
     **/
    @ApiModelProperty("类型  1 试题 2 文章 3 视频")
    private Integer type;
    /**
     * 用户ID
     **/
    @ApiModelProperty("用户ID")
    private Long userId;

}
