package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 文章主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="视频主体")
public class SmallVideoDetailsForIdVo{

	/**
	 * 评论人数
	 */
    @ApiModelProperty(value="评论人数")
	private Integer commentNum;
    /**
     * 点赞人数
     */
    @ApiModelProperty(value="点赞人数")
    private Integer thumpUpNum;
    /**
     * 分享次数
     */
    @ApiModelProperty(value="分享次数")
    private Integer shareNum;

    @ApiModelProperty(value="是否关注")
    private Boolean isAttention;//

    @ApiModelProperty(value="是否点赞")
    private Boolean isLike;//

}
