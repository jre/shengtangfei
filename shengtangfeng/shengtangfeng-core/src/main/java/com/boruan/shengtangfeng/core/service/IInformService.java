package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Inform;
import org.beetl.sql.core.engine.PageQuery;

public interface IInformService {
    void pageQuery(PageQuery<Inform> pageQuery, Inform inform);
}
