package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.boruan.shengtangfeng.core.utils.QuestionOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionHistoryVo extends BaseVo {

	/**
	 * 类型
	 **/
	@ApiModelProperty("类型")
	private QuestionType type;
	/**
	 * 是否主观题
	 **/
	@ApiModelProperty("是否主观题")
	private Boolean subjective;

	/**
	 * 答案
	 **/
	@ApiModelProperty("答案")
	private String answer;

	/**
	 * 分类ID1
	 **/
	@ApiModelProperty("分类ID1")
	private Long categoryId1;

	/**
	 * 分类ID2
	 **/
	@ApiModelProperty("分类ID2")
	private Long categoryId2;

	/**
	 * 选项
	 **/
	@ApiModelProperty(name = "选项,json串，见备注", notes = "[{\"content\":\"一年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"二年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"三年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"四年\",\"isCorrect\":true,\"type\":\"text\"}]")
	private String content;

	/**
	 * 选择题 题目集合
	 */
	@ApiModelProperty(value = "选择题选项")
	private List<QuestionOption> optionList;
	/**
	 * 考点
	 **/
	@ApiModelProperty("考点")
	private String kaodian;

	/**
	 * 文字解释
	 **/
	@ApiModelProperty("文字解释")
	private String textExplain;

	/**
	 * 题干
	 **/
	@ApiModelProperty("题干")
	private String title;

	/**
	 * 语音解释地址
	 **/
	@ApiModelProperty("语音解释地址")
	private String voiceExplain;

	/**
	 * 用户答案
	 **/
	@ApiModelProperty("用户答案")
	private String userAnswer;
	/**
	 * 用户答案
	 **/
	@ApiModelProperty("用户答案")
	private String userAnswer1;

	/**
	 * 正确率
	 **/
	@ApiModelProperty("正确率")
	private String accurate;
	/**
	 * 是否收藏
	 **/
	@ApiModelProperty("是否收藏")
	private Boolean collect;
	/**
	 * 用户自己打的主观题分数
	 */
	@ApiModelProperty("用户自己打的主观题分数")
	private Double score;
	/**
	 * 是否显示纠错按钮
	 */
	@ApiModelProperty("是否显示纠错按钮")
	private Boolean showCorrecting;
	/**
	 * 是否显示解析按钮
	 */
	@ApiModelProperty("是否显示解析按钮")
	private Boolean showAnalytics;

	@ApiModelProperty(value = "视频地址")
	private String url;

	@ApiModelProperty(value = "分类名称1")
	private String category1;
	@ApiModelProperty(value = "分类名称2")
	private String category2;
	@ApiModelProperty(value = "考点名称")
	private List<String> kaodianN;
	@ApiModelProperty(value = "考点ID")
	private List<Long> kaodiansId;
	
	@ApiModelProperty(value = "搜索命中类型 0 所有  1 题干 2 内容")
	private Integer hitType;
	
	/**
     * 是否打开视频解析开关，优先级最高
     **/
	@ApiModelProperty(value = "是否打开视频解析开关，优先级最高")
    private Boolean videoSwitch;
	/**
	 * 没有视频是否显示视频解析开关
	 **/
	@ApiModelProperty(value = "没有视频是否显示视频解析开关")
	private Boolean emptyVideoSwitch;
	/**
	 * 是否打开视频解析开关
	 **/
	@ApiModelProperty(value = "是否打开视频上传开关")
	private Boolean videoUploadSwitch;

	@ApiModelProperty(value = "高级或者普通题")
	private Boolean vip;
	@ApiModelProperty(value = "被使用次數")
	private Long beMadeCount;
	@ApiModelProperty(value = "關聯視頻數量")
	private Long videoCount;
	@ApiModelProperty(value = "相似度，前端用不到")
	private double similarity;
	@ApiModelProperty(value = "章节题ID")
	private Long cid;
	@ApiModelProperty(value ="多选题答案")
	private String answerN;
}


