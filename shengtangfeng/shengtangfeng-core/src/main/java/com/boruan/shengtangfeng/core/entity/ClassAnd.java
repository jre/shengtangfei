package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * 班级
 */
@Table(name="class_and")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClassAnd extends IdEntity {

    /**
     * 班级号
     **/
    private String classNum;

    /**
    * 学校id
    **/
    private Long schoolId ;

    /**
    * 年级分部 
    **/
    private String gradeDivision ;
            
    /**
    * 学年 
    **/
    private String schoolYear ;

    /**
     * 班级名称
     **/
    private String name ;

    /**
     * 班级状态 0学籍中 1已毕业
     **/
    private Integer status ;

}