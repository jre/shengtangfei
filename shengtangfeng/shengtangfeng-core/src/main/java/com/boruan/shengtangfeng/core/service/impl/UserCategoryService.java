package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IUserCategoryDao;
import com.boruan.shengtangfeng.core.dto.UserCategoryDto;
import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.entity.UserCategory;
import com.boruan.shengtangfeng.core.entity.VideoCategory;
import com.boruan.shengtangfeng.core.service.IUserCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author KongXH
 * @title: UserCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1314:18
 */
@Service
@Transactional(readOnly = true)
public class UserCategoryService implements IUserCategoryService {

    @Resource
    private IUserCategoryDao userCategoryDao;


     /**
         * @description: 保存类别
         * @Param
         * @return
         * @author KongXH
         * @date 2020/8/13 16:01
         */
     @Transactional(readOnly = false)
    public void saveUserCategory(UserCategoryDto userCategoryDto,Integer type,Long userId){
        UserCategory userCategory = new UserCategory();
        userCategory.setCategoryType(type);
        userCategory.setUserId(userId);
        userCategoryDao.deleteCategory(userCategory);

        userCategory.setCreateTime(new Date());
        userCategory.setIsDeleted(false);
        String[] split = userCategoryDto.getCategoryId().split(",");
        for (int i = 0; i < split.length; i++) {
            userCategory.setCategoryId(Long.valueOf(split[i]).longValue());
            userCategoryDao.insertReturnKey(userCategory);
        }

    }



    @Override
    public List<ArticleCategory> getUserArticleCategory(Long userId) {
        return userCategoryDao.getUserArticleCategory(userId);
    }



    @Override
    public List<VideoCategory> getUserVideoCategory(Long userId,Integer type) {
        return userCategoryDao.getUserVideoCategory(userId,type);
    }

    @Override
    public List<Subject> getUserJYJCategory(Long userId, Integer type) {
        return userCategoryDao.getUserJYJCategory(userId,type);
    }
}
