package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.core.lab.TailBean;

import java.util.Date;


@Data
public class SchoolListDto extends TailBean {

    @ApiModelProperty("学校id")
    private Long id ;

    @ApiModelProperty("学校名称")
    private String name ;

    @ApiModelProperty("省")
    private String province ;

    @ApiModelProperty("市")
    private String city ;

    @ApiModelProperty("区")
    private String county;

    @ApiModelProperty("班级数")
    private Long classNum;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户昵称")
    private String userNickname;

    @ApiModelProperty("用户状态")
    private Boolean isLocked;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @ApiModelProperty("提交时间")
    protected Date createTime;

    @ApiModelProperty("创建人类型 0为用户 1为后台管理员")
    private Integer type;

}