package com.boruan.shengtangfeng.core.utils;

public class CommentUtil {
    //完全禁止词汇
    public static String[] level0BadWord = new String[]{};
    //可放行审核的词汇
    public static String[] level1BadWord = new String[]{};

    /**
     * 返回 值 0，禁止发布  1 ，需要审核  2，不需审核
     * @param content
     * @return Integer
     */
    public static Integer checkComment(String content) {
        if (StringUtils.containsAny(content, level0BadWord)) {
            return 0;
        } else if (StringUtils.containsAny(content, level1BadWord)) {
            return 1;
        } else {
            return 2;
        }
    }
}