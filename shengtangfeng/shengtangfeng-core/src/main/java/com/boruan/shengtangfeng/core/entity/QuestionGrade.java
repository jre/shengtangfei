package com.boruan.shengtangfeng.core.entity;

import java.math.BigDecimal;
import java.util.List;

import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.QuestionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 题库-年级关联
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question_grade")
public class QuestionGrade{


    /**
     * 试题ID
     **/
    private Long questionId;
	/**
	 * 年级ID
	 **/
	private Long gradeId;

}
