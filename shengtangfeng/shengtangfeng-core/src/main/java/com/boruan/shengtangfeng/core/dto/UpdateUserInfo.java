package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* @author:twy
* @Description:
*
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "更新用户信息")
public class UpdateUserInfo{
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String headImage;
	/**
	 * 昵称
	 */
	@ApiModelProperty(value = "昵称")
	private String nickName;
	/**
	 * 性别 1男 2女
	 */
	@ApiModelProperty(value = "性别 1男 2女")
	private Integer sex;
	/**
	 * 生日
	 */
	@ApiModelProperty(value = "生日")
	private String birthDay;
	/**
	 * 省
	 */
	@ApiModelProperty(value = "省")
	private String province;
	/**
	 * 市
	 */
	@ApiModelProperty(value = "市")
	private String city;
	/**
	 * 邀请码
	 */
	@ApiModelProperty(value = "邀请码")
	private String invitationCode;

	@ApiModelProperty(value = "是否显示定位：0否1是")
	private Integer showLocation;

	@ApiModelProperty(value = "家长手机号")
	private String parentMobile;

}
