package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.vo.MyAttentionUser;


public interface IAttentionDao extends BaseMapper<Attention> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Attention> query);
    /**
     * 分页查询我关注的用户
     * @param query
     */
    public void pageMyAttention(PageQuery<MyAttentionUser> query);
    /**
     * 分页查询我的粉丝
     * @param query
     */
    public void pageMyFans(PageQuery<MyAttentionUser> query);


    /**
     * @description: 保存关注
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 14:05
     */
    public void saveAttention(Attention iAttention);




     /**
         * @description: 根据对方id查找
         * @Param
         * @return  
         * @author KongXH
         * @date 2020/8/14 14:47 
         */
    public Attention  findByFocusId(Long userId,Long focusUserId);


 /**
     * @description: 根据id更新状态
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 15:17
     */
    public  void updateAttention(Attention attention);



    public  void deleteAttention(Long userId,Long focusUserId);
}