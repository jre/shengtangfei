package com.boruan.shengtangfeng.core.dao;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Grade;


public interface IGradeDao extends BaseMapper<Grade> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Grade> query);
    /**
     * 获取年级列表
     * @param grade
     */
    public List<Grade> getList(Grade grade);
    
    /**
     * 根据学科ID获取年级列表
     * @param subjectId
     */
    public List<Grade> getListBySubjectId(Long subjectId);

    /**
     * 获取题目的年级
     * @param questionId
     */
    Grade getGrade(Long questionId);
}