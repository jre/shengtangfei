package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.Permission;
import com.boruan.shengtangfeng.core.entity.Role;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.ZTree;
import org.beetl.sql.core.engine.PageQuery;

/**
 * 
 * @author liuguangqiang
 * @date 2016年10月31日 上午8:33:38
 */
public interface IPermissionService {

	/**
	 * 构建ztree的对象
	 * @return
	 */
	public List<ZTree> buildZTree();

	List<Permission>  buildMenuTree(Long id);

	void pageQuery(PageQuery<Role> pageData);

	GlobalReponse getRolePermissionTree(Long roleId);

	/**
	 *
	 * @param roleId
	 * @param addPermissions 新增的权限
	 * @param delPermissions 删除的权限
	 * @return
	 */
	GlobalReponse editRolePermission(Long roleId, List<Long> addPermissions, List<Long> delPermissions);
}
