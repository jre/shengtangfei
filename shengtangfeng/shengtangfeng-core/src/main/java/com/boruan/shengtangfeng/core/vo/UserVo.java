package com.boruan.shengtangfeng.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.boruan.shengtangfeng.core.enums.Sex;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserVo extends BaseVo {

    @ApiModelProperty("性别 1男 2女")
    private Sex sex;

    @ApiModelProperty("头像")
    private String headImage;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("登录名")
    private String loginName;
    /**
     * 微信unionId
     **/
    @ApiModelProperty("微信unionId")
    private String unionId;
    /**
     * 微信openId
     **/
    @ApiModelProperty("微信openId")
    private String openId;

    /**
     * 答题数
     **/
    @ApiModelProperty("答题数")
    private Integer answerNum;
    /**
     * 正确率  百分比
     **/
    @ApiModelProperty("正确率  百分比数")
    private Integer accurate;

    @ApiModelProperty("邀请码")
    private String invitationCode;

    @ApiModelProperty("获赞数")
    private Integer likeNum;
    
    /**
     * 积分
     **/
    @ApiModelProperty("积分")
    private Integer integral;
    
//    /**
//	 * 总打卡天数
//	 */
//    @ApiModelProperty("总打卡天数")
//	private Integer totalPunch;
//	/**
//	 * 连续打卡天数 到21 清0
//	 */
//    @ApiModelProperty("连续打卡天数 到21 清0")
//	private Integer punchNum;
    
    
//    /**
//     * 此用户的推广人ID
//     */
//    @ApiModelProperty("此用户的推广人ID")
//    private Long spreadId;
    /**
     * 是否设置了密码
     **/
    @ApiModelProperty("是否设置了密码")
    private Boolean havePassword;
    
    /**
     * 生日
     */
    @ApiModelProperty("生日")
    private String birthDay;
    /**
     * 省
     */
    @ApiModelProperty("省")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty("市")
    private String city;

    /**
     * 用户类型1学生2老师
     */
    @ApiModelProperty("用户类型1学生2老师3普通用户")
    private Integer userType;//用户类型

    @ApiModelProperty("腾讯userSig")
    private String userSig ;

    @Override
    public String toString() {
        return "{'name':" + this.name + ",'mobile':" + this.mobile + "}";
    }

}
