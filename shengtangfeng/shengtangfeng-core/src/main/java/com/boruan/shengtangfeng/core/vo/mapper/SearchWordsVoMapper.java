package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.SearchWords;
import com.boruan.shengtangfeng.core.vo.SearchWordsVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface SearchWordsVoMapper extends VoMapper<SearchWordsVo,  SearchWords>  {
	SearchWordsVoMapper MAPPER = Mappers.getMapper(SearchWordsVoMapper.class);
}