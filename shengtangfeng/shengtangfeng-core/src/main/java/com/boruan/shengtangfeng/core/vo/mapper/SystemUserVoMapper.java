package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.SystemUser;
import com.boruan.shengtangfeng.core.vo.SystemUserVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface SystemUserVoMapper extends VoMapper<SystemUserVo, SystemUser>  {
	SystemUserVoMapper MAPPER = Mappers.getMapper(SystemUserVoMapper.class);
}