package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IFeedbackDao;
import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.dto.UserDto;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.service.IMyInfoService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.GlobalReponseResultEnum;
import com.boruan.shengtangfeng.core.utils.UserUtil;
import com.boruan.shengtangfeng.core.vo.UserVo;
import com.boruan.shengtangfeng.core.vo.mapper.UserVoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.boruan.shengtangfeng.core.entity.Feedback;

/**
 * @author: lihaicheng
 * @Description: 我的 个人中心
 * @date:2020年3月10日 上午11:14:10
 */
@SuppressWarnings("all")
@Service
@Transactional(readOnly = true)
public class MyInfoService implements IMyInfoService {
	private static Logger logger = LoggerFactory.getLogger(MyInfoService.class);
	@Autowired
	private IFeedbackDao feedBackDao;
	@Autowired
	private IUserDao userDao;
	@Autowired
    private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @author: lihaicheng
	 * @Description: 修改密码
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public GlobalReponse changePassword(String phone, String password) {
		try {
			User user = userDao.findByMobile(phone);
			if (user == null) {
				return new GlobalReponse(GlobalReponseResultEnum.USER_NOT_EXIST);
			}
			String newPassword = UserUtil.entryptPassword(password, user.getSalt()); // 获取加密后的密码
			user.setPassword(newPassword);
			userDao.updateById(user);
			return GlobalReponse.success();
		} catch (Exception e) {
			logger.error("修改密码异常phone：" + phone + ",password" + password, e);
			return GlobalReponse.fail();
		}
	}

	/**
	 * @author: lihaicheng
	 * @Description: 修改个人信息
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public GlobalReponse updateInfo(UserDto userDto) {
		try {
			User user = userDao.single(userDto.getId());
			if (user == null) {
				return GlobalReponse.fail("修改失败");
			}
			if(userDto.getHeadImage() != null) {
				user.setHeadImage(userDto.getHeadImage());
			}
			if(userDto.getName() != null) {
				user.setName(userDto.getName());
			}
			userDao.updateById(user);
			UserVo userVo = UserVoMapper.MAPPER.toVo(user);
			return GlobalReponse.success(userVo);
		} catch (Exception e) {
			logger.error("修改个人信息异常", e);
			return GlobalReponse.fail();
		}
	}

	/**
	 * @author: lihaicheng
	 * @Description: 意见反馈
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public GlobalReponse feedBack(Long userId, String content) {
		JSONObject json = JSONObject.parseObject(content);
		try {
			Feedback feedBack = new Feedback();
			feedBack.setUserId(userId);
			feedBack.setContent(json.getString("content"));
			feedBackDao.insertReturnKey(feedBack);
		} catch (Exception e) {
			logger.error("意见反馈异常", e);
			return GlobalReponse.fail();
		}
		return GlobalReponse.success();
	}

    @Override
    public User getInfo(Long userId) {
        return userDao.single(userId);
    }

}
