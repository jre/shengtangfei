package com.boruan.shengtangfeng.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;


@Table(name="notice_user")
@Setter
@Getter
@NoArgsConstructor
public class NoticeUser {

    private Long noticeId ;

    private Long userId ;
    
    private Boolean readed ;

    private Boolean isDeleted ;
    
    public Boolean getReaded() {
        return this.readed;
    }
}