package com.boruan.shengtangfeng.core.utils;

import io.swagger.models.auth.In;
import org.apache.commons.codec.binary.Hex;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by on 2017/6/10.
 */
public class UUIDUtil {

	public static final String allChar = "123456789abcdefghijkmnpqrstuvwxyz";

	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * 返回时间样式的id (201706121614xxxxx)
	 * 
	 * @Title: getTimeId
	 * @Description:
	 * @date 2017年6月12日 下午4:11:20
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws @version V1.0
	 */
	public static String getTimeId() {
		SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
		Date da = new Date();
		return si.format(da) + (int) ((Math.random() * 9 + 1) * 100000);
	}

	/**
	 * 返回一个定长的随机字符串(只包含大小写字母、数字)
	 * 
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static String getCode(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(allChar.charAt(random.nextInt(allChar.length())));
		}
		return sb.toString();
	}

	/**
	 * 返回六位随机数
	 * @return
	 */
	public static String sixRandom() {
		SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
		Date da = new Date();
		String random = si.format(da) + (int) ((Math.random() * 9 + 1) * 100000);
		return random.substring(random.length() - 6);
	}

	/**
	 * 返回九位随机数
	 * @return
	 */
	public static String nineRandom() {
		SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
		Date da = new Date();
		String random = si.format(da) + (int) ((Math.random() * 9 + 1) * 100000);
		return random.substring(random.length() - 9);
	}

	/**
	 * 返回三位随机数
	 * @return
	 */
	public static String threeRandom() {
		SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
		Date da = new Date();
		String random = si.format(da) + (int) ((Math.random() * 9 + 1) * 100000);
		return random.substring(random.length() - 3);
	}

	/**
	 * 返回指定长度的随机数
	 * @param len
	 * @return
	 */
	public static String differentDigitsRandom(Integer len){
		int digit = (int) Math.pow(10, len - 1);
		int rs = new Random().nextInt(digit * 10);
		if (rs < digit) {
			rs += digit;
		}
		return String.valueOf(rs);
	}

	/**
	 * 生成腾讯随机密码
	 * @return
	 */
	public static String getRandom() {
		SimpleDateFormat si = new SimpleDateFormat("HHmmss");
		Date da = new Date();
		String random = si.format(da);
		return random+threeRandom();
	}


	public static void main(String[] args) {
		//System.out.println(threeRandom());
		SimpleDateFormat si = new SimpleDateFormat("HHmmss");
		Date da = new Date();
		for (int i = 0; i < 10; i++) {
			System.out.println(si.format(da) + threeRandom());
		}
	}

}
