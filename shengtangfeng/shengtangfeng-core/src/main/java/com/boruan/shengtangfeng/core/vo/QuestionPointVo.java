package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "考点")
public class QuestionPointVo extends BaseVo {

	/**
	 * 考点名字
	 **/
	@ApiModelProperty(value = "考点名称")
	private String name ;

	/**
	 * 父菜单Id
	 **/
	@ApiModelProperty(value = "父菜单Id")
	private Long parentId ;

	/**
	 * 下级章节
	 */
	@ApiModelProperty(value = "下级考点")
	private List<QuestionPointVo> childs;

}
