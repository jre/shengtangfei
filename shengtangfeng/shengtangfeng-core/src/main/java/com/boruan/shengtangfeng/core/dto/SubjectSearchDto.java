package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: AccoladeDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1815:52
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="表")
public class SubjectSearchDto{


    /**
     * 
     **/
    @ApiModelProperty(value="")
    private Long[] subjectIds;
}
