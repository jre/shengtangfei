package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 系统用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "system_user")
@EqualsAndHashCode(callSuper = false)
public class SystemUser extends IdEntity {

	private static final long serialVersionUID = 6196812525781663760L;
	/**
	 * 头像
	 */
	private String headImage;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 用户名
	 */
	private String name;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 密码盐
	 */
	private String salt;

	/**
	 * 备注
	 */
	private String description;
	/**
	 * 工号
	 */
	private String employeeNumber;
	/**
	 * 院系id
	 */
	private Long departmentId;



	private String roleName;

	private Long roleId;

}
