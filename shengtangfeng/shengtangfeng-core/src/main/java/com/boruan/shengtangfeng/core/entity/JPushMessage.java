package com.boruan.shengtangfeng.core.entity;

import lombok.Data;

import java.util.HashMap;

/**
 * extras主要为了终端跟服务端进行协定操作，最好只能只内置一个指令参数code即可
 */
@Data
public class JPushMessage {

    String msgContent;

    HashMap<String, String> extras;
}
