package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 首页数量vo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FavoritesCount{

	/**
	 * 类别，试题 文章 视频
	 */
	@ApiModelProperty("类别，试题 文章 视频")
	private String type;
	/**
	 * 数量
	 */
	@ApiModelProperty("数量")
	private Long count;
	
}
