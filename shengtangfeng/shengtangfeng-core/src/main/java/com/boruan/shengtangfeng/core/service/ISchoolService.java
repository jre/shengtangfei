package com.boruan.shengtangfeng.core.service;


import com.boruan.shengtangfeng.core.dto.ClassAndDto;
import com.boruan.shengtangfeng.core.dto.JobConditionDto;
import com.boruan.shengtangfeng.core.dto.JobDto;
import com.boruan.shengtangfeng.core.entity.ClassAnd;
import com.boruan.shengtangfeng.core.entity.Inform;
import com.boruan.shengtangfeng.core.entity.School;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.utils.Global;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ClassAndVo;
import com.boruan.shengtangfeng.core.vo.MyJoinClassNameVo;
import org.beetl.sql.core.engine.PageQuery;

import java.util.Date;
import java.util.List;

public interface ISchoolService {

    public School findById(Long id);

    public GlobalReponse<PageQuery<School>> getSchools(PageQuery<School> pageQuery);

    public GlobalReponse<PageQuery<ClassAndVo>> getClassAnd(PageQuery<ClassAnd> pageQuery,Long userId);

    public List<ClassAnd> myClassAnd(Long userId);

    GlobalReponse<PageQuery<JobDto>> getJobListById(JobConditionDto jobConditionDto, Long userId);

    /**
     * 发布讲一讲时候获取我加入的班级
     * @param userId
     * @return
     */
    GlobalReponse<List<MyJoinClassNameVo>> getMyJoinClassName(Long userId);

}
