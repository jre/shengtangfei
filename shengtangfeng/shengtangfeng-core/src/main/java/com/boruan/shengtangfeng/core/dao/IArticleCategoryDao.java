package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.ArticleCategory;

/*
 *
 * gen by beetlsql mapper 2018-12-13
 */
public interface IArticleCategoryDao extends BaseMapper<ArticleCategory> {
	/**
	 * 分页查询
	 *
	 * @param query
	 */
	public void pageQuery(PageQuery<ArticleCategory> query);
	/**
	 * 查询
	 *
	 * @param articleCategory
	 */
	public List<ArticleCategory> getList(ArticleCategory articleCategory);



	/**
	 * @description:获取默认类别
	 * @Param
	 * @return
	 * @author KongXH
	 * @date 2020/8/13 10:54
	 */
	public List<ArticleCategory> getArticleCategoryDefault();
}
