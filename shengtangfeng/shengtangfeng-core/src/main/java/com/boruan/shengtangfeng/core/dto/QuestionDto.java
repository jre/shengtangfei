package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.boruan.shengtangfeng.core.utils.QuestionOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionDto extends BaseDto {

	/**
	 * 类型
	 **/
	@ApiModelProperty("类型")
	private QuestionType type;

	/**
	 * 答案
	 **/
	@ApiModelProperty("答案")
	private String answer;

	/**
	 * 年级ID
	 **/
	@ApiModelProperty("年级ID集合")
	private Long[] gradeIds;

	/**
	 * 学科ID集合
	 **/
	@ApiModelProperty("学科ID")
	private Long subjectId;

	/**
	 * 选项
	 **/
	@ApiModelProperty(name="选项,json串，见备注",notes="[{\"content\":\"一年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"二年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"三年\",\"isCorrect\":false,\"type\":\"text\"},{\"content\":\"四年\",\"isCorrect\":true,\"type\":\"text\"}]")
	private String content;

	/**
	 * 文字解释
	 **/
	@ApiModelProperty("文字解释")
	private String textExplain;

	/**
	 * 题干
	 **/
	@ApiModelProperty("题干")
	private String title;

	/**
     * 用户答案
     **/
    @ApiModelProperty("用户答案")
    private String userAnswer;
    /**
     * 正确率
     **/
    @ApiModelProperty("正确率")
    private String accurate;
	/**
	 * 选择题 题目集合
	 */
	@ApiModelProperty(value = "选项列表")
	private List<QuestionOption> optionList;
	/**
	 * 分值
	 */
	@ApiModelProperty("分值")
	private BigDecimal score;


}
