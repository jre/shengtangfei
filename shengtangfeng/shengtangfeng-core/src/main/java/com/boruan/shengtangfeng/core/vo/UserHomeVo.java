package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.enums.Sex;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserHomeVo extends BaseVo{

    @ApiModelProperty("是否在线 前台运行状态（Online）后台运行状态（PushOnline）未登录状态（Offline)")
    private String isOnline;

    @ApiModelProperty("头像")
    private String headImage;

    @ApiModelProperty("名字")
    private String name;

    @ApiModelProperty("邀请码")
    private String invitationCode;

    @ApiModelProperty("是否显示定位 0否1是")
    private Integer showLocation;

    @ApiModelProperty("家长手机号")
    private String parentMobile;

    @ApiModelProperty("用户ID")
    private Long id;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("性别 1男 2女")
    private Sex sex;

    @ApiModelProperty("市")
    private String city;

    @ApiModelProperty("用户类型：1学生2老师")
    private Integer userType;

    @ApiModelProperty("积分")
    private Integer integral;

    @ApiModelProperty("是否关注 0否 1是")
    private Integer isConcern;

    @ApiModelProperty("是否是好友 0否 1是")
    private Integer isFriend;

    @ApiModelProperty("关注的人数量")
    private Long followCount;

    @ApiModelProperty("粉丝数量")
    private Long fansCount;

    @ApiModelProperty("获赞数")
    private Integer likeNum;

    @ApiModelProperty("发布的文章数量")
    private Long publishArticle;

    @ApiModelProperty("发布的视频数量")
    private Long publishVideo;

    @ApiModelProperty("发布的文章集合")
    private List<Article> articleList;

    @ApiModelProperty("发布的视频集合")
    private List<Video> videoList;

}
