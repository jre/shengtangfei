package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 文章分类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "article_category")
@EqualsAndHashCode(callSuper = false)
public class ArticleCategory extends IdEntity {

	/**
	 * 名称
	 */
	private String name;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 是否默认  1是0否
	 */
	private Boolean isDefault;


}
