package com.boruan.shengtangfeng.core.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 统一定义id的entity基类.
 * 
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class IdEntity extends TailBean implements Serializable{

	@AutoID
	@ApiModelProperty("id")
	protected Long id;

	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	protected String createBy;

	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date createTime;

	/**
	 * 最后更新人
	 */
	@ApiModelProperty("最后更新人")
	protected String updateBy;

	/**
	 * 最后更新时间
	 */
	@ApiModelProperty("最后更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date updateTime;

	/**
	 * 是否删除
	 */
	@ApiModelProperty("是否删除")
	protected Boolean isDeleted;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
