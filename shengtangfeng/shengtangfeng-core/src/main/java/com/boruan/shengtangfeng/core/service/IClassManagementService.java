package com.boruan.shengtangfeng.core.service;


import com.boruan.shengtangfeng.core.dto.*;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ClassAndVo;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 @author: guojiang
 @Description:班级管理
 @date:2021/4/28
 */
public interface IClassManagementService {

    /**
     * 获取省市区
     * @return
     */
    GlobalReponse<List<ProvinceDto>> getAddress();

    /**
     * 新建学校
     * @param schoolDto
     * @param userId
     * @return
     */
    GlobalReponse createSchool(SchoolDto schoolDto, Long userId);

    /**
     * 查询学校分页
     * @param pageNo
     * @param pageSize
     * @param schoolDto
     * @return
     */
    GlobalReponse<PageQuery<SchoolListDto>> schoolPage(Integer pageNo, Integer pageSize, SchoolDto schoolDto);

    /**
     * 批量或者单个删除学校
     * @param schoolIds
     * @return
     */
    GlobalReponse deletedSchool(String schoolIds,Long userId);

    /**
     * 学校展开，即查询学校的年级
     * @param schoolId
     * @return
     */
    GlobalReponse getGradeBySchoolId(Long schoolId);

    /**
     * 年级展开，即查询年级下的班级
     * @param schoolYear
     * @return
     */
    GlobalReponse<GradeToClassDto> getClassBySchoolYear(Long schoolId,String schoolYear);

    /**
     * 后台删除班级
     * @param classId
     * @return
     */
    GlobalReponse deletedClass(Long classId,Long userId);

    /**
     * 查看班级详情
     * @param classId
     * @return
     */
    GlobalReponse<ClassAndVo> getClassDetails(Long classId);

    /**
     * 获取班级参数
     * @return
     */
    GlobalReponse<ClassParameterDto> getClassParameter();

    /**
     * 班级参数设置
     * @param dto
     * @param userId
     * @return
     */
    GlobalReponse setClassParameter(ClassParameterDto dto, Long userId);

    /**
     * 获取作业名称
     * @param pageNo
     * @param pageSize
     * @return
     */
    GlobalReponse<PageQuery<JobNameDto>> getJobName(Integer pageNo, Integer pageSize);

    /**
     * 新增作业名称
     * @param dto
     * @param userId
     * @return
     */
    GlobalReponse createJobName(JobNameDto dto, Long userId);

    /**
     * 删除作业名称
     * @param jobNameId
     * @param userId
     * @return
     */
    GlobalReponse deletedJobName(String jobNameId, Long userId);

    /**
     * 获取作业参数
     * @return
     */
    GlobalReponse<JobParameterDto> getJobParameter();

    /**
     * 获取作业参数
     * @param dto
     * @param userId
     * @return
     */
    GlobalReponse setJobParameter(JobParameterDto dto, Long userId);
}
