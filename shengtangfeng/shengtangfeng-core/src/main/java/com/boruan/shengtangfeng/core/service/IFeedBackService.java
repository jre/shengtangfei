package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import org.beetl.sql.core.engine.PageQuery;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IFeedBackService {
    /**
     * 删除意见反馈
     * */
    public GlobalReponse deleteFeedback(Long id);
    /**
     * 保存意见反馈
     * */
    public void saveFeedback(Feedback feedback);
	/**
	  * 分页查询
	 * @param pageQuery
	 * @param feedback
	 */
	public void pageQuery(PageQuery<Feedback> pageQuery, Feedback feedback);

}
