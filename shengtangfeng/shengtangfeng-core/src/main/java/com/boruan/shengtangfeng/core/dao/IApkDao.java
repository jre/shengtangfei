package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Apk;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IApkDao extends BaseMapper<Apk> {

    void pageQuery(PageQuery<Apk> pageQuery);

}
