package com.boruan.shengtangfeng.core.service.impl;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IAccoladeDao;
import com.boruan.shengtangfeng.core.dao.IArticleDao;
import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.dao.IVideoDao;
import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.service.IAccoladeService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
 * @author KongXH
 * @title: LikeService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1715:31
 */

@Service
@Transactional(readOnly = true)
public class AccoladeService implements IAccoladeService {

    @Autowired
    private IAccoladeDao accoladeDao;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IArticleDao articleDao;
    @Autowired
    private IVideoDao videoDao;


    /**
     * @description: 分页查询
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/17 15:33
     */

    @Override
    public void pageQuery(PageQuery<Accolade> query) {
        accoladeDao.pageQuery(query);
    }


    /**
     * @description: 保存点赞
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/17 15:33
     */
    @Override
    @Transactional(readOnly = false)
    public GlobalReponse saveAccolade(Accolade accolade) {
        try {
            Accolade accolades = accoladeDao.getAccolade(accolade.getUserId(),accolade.getArticleId(),accolade.getType());
//            Article article=articleDao.findById(accolade.getArticleId());
            if (null == accolades) {
                KeyHolder kh=accoladeDao.insertReturnKey(accolade);
                //视频文章点赞需要去加点赞数
                if(accolade.getType()==2) {
                    Video video=videoDao.single(accolade.getArticleId());
                    video.setThumpUpNum(video.getThumpUpNum()+1);
                    userDao.plusLikeNum(video.getPublishId());
                    videoDao.updateById(video);
                }else if(accolade.getType()==1){
                    Article article=articleDao.single(accolade.getArticleId());
                    article.setThumpUpNum(article.getThumpUpNum()+1);
                    articleDao.updateById(article);
                    userDao.plusLikeNum(article.getPublishId());
                }
                return GlobalReponse.success(true).setMessage("点赞成功");
//                favorites.setId(kh.getLong());
            }else{
                accoladeDao.deleteAccolade(accolade.getUserId(),accolade.getArticleId(),accolade.getType());
                //视频点赞需要减点赞数
                if(accolade.getType()==2) {
                    Video video=videoDao.single(accolade.getArticleId());
                    video.setThumpUpNum(video.getThumpUpNum()-1);
                    videoDao.updateById(video);
                    userDao.lessLikeNum(video.getPublishId());
                }else if(accolade.getType()==1){
                    Article article=articleDao.single(accolade.getArticleId());
                    article.setThumpUpNum(article.getThumpUpNum()-1);
                    articleDao.updateById(article);
                    userDao.lessLikeNum(article.getPublishId());
                }
                return GlobalReponse.success(false).setMessage("取消点赞成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return GlobalReponse.fail();
        }
    }




    /**
     * @description: 是否点赞
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/18 14:51
     */
    @Override
    public Accolade getAccolade(Long userId, Long articleId,Integer type) {
        return accoladeDao.getAccolade(userId,articleId,type);
    }


//    /**
//     * @description: 删除
//     * @Param
//     * @return
//     * @author KongXH
//     * @date 2020/8/18 15:07
//     */
//    public void  deleteAccolade(Long userId,Long articleId,Integer type){
//        accoladeDao.deleteAccolade(userId,articleId,type);
//
//    }
}
