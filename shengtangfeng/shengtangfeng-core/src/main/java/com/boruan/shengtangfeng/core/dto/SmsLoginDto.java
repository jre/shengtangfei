package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@author: 刘光强
@Description: 
@date:2020年3月10日 上午11:14:10
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("短信登录参数")
public class SmsLoginDto{
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("手机验证码")
    private String authCode;
    @ApiModelProperty("互踢校验位，通过极光推送互踢消息到当前用户所有端，如果time不一样则踢下线")
    private String time;
    
    public boolean checkParam(){
        if(StringUtils.isBlank(mobile)||StringUtils.isBlank(authCode)) {
            return false;
        }else {
            return true;
        }
    }
}
