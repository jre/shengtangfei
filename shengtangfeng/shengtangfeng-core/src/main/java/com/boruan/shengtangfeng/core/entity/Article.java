package com.boruan.shengtangfeng.core.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "article")
@EqualsAndHashCode(callSuper = false)
public class Article extends IdEntity {

	/**
	 * 分类ID
	 */
	private Long categoryId;
	/**
	 * 新闻标题
	 */
	private String title;
	/**
	 * 新闻摘要，显示在列表
	 */
	private String summary;

	/**
	 * 新闻内容
	 */
	private String content;

	/**
	 * 发布时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	private Date publishDate;
	/**
	 * 发布人
	 */
	private String publisher;
	/**
	 * 发布人头像
	 */
	private String publisherIcon;
	/**
	 * 原文链接
	 */
	private String link;
	/**
	 * 状态 0 审核中 1 通过 2 不通过
	 */
	private Integer status;
	/**
	 * 已读人数
	 */
	private Integer readNum;
	/**
	 * 评论人数
	 */
	private Integer commentNum;
	/**
	 *  是否推荐
	 */
	private Boolean recommend;
	/**
	 *  内容中的图，用英文逗号分隔
	 */
	private String images;
	/**
	 *  图数量 1 无图  2 一张图 3 多于一张
	 */
	private Integer type;


	private Long publishId;//发布者id

	private Long invitationCode;//邀请码


	/** 放值字段*/
	private Long userId;//用户id
	/**
     * 点赞人数
     */
    private Integer thumpUpNum;
    
    /**
     *  审核备注
     */
    private String remark;
    /**
     *  上传类型0 系统  1用户
     */
    private Integer uploadType;
}
