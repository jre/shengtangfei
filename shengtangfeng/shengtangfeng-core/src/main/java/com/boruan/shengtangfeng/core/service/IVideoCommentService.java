package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.VideoComment;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IVideoCommentService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param video
	 */
	public void pageQuery(PageQuery<VideoComment> pageQuery, VideoComment videoComment);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public VideoComment findById(Long id);
	/**
	 * 保存分类
	 * @param  videoComment
	 */
	public void save(VideoComment videoComment);
	
	/**
	  * 根据问题ID获取回复列表
	 * @param videoId
	 */
	public List<VideoComment> findByVideoId(Long userId,Long videoId,Integer status);
	/**
	 * 根据条件查找
	 * @param  search
	 */
	public List<VideoComment> template(VideoComment search);
	
    void deleteComment(Long id, Long id1);

	public long getTodayCount(Long userId);

	GlobalReponse<Boolean> saveAccolade(Accolade accolade);

}
