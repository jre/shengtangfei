package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
*
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "模块做题记录")
public class ModuleRecordDto{
    /**
     * 模块ID
     **/
    @ApiModelProperty("模块ID")
    private Long moduleId;
    
    /**
     * 耗费时间,秒数
     **/
    @ApiModelProperty("耗费时间,秒数")
    private Integer time;

    /**
     * 得分
     */
    @ApiModelProperty("得分")
    private Integer score;
    /**
     * 积分
     */
    @ApiModelProperty("积分")
    private Integer integral;

}
