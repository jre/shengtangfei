package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Inform;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IInformDao extends BaseMapper<Inform> {

    public Inform getLast(Long classId);

    public void pageQueryByClass(PageQuery<Inform> pageQuery);
}
