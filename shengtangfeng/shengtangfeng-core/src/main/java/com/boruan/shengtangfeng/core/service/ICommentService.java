package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.ToAuditCommentDTO;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.CommentVo;
import org.beetl.sql.core.engine.PageQuery;

/**
 @author: guojiang
 @Description:
 @date:2021/2/25
 */
public interface ICommentService {

    /**
     * 多条件查询评论 type评论类型 0文章 1视频 2试题
     * @return
     */
    GlobalReponse<PageQuery<CommentVo>> getComment(ToAuditCommentDTO toAuditCommentDTO);

    /**
     *  @param userId
     * @param commentId
     * @param type
     * @return
     */
    GlobalReponse deleteComment(Long userId, Long commentId, Integer type);

    /**
     * 根据id和评论类型修改评论 type 0文章 1视频 2试题
     * @param userId
     * @param commentId
     * @param type
     * @return
     */
    GlobalReponse updateComment(Long userId, Long commentId, Integer type,Integer status,String remark);

    /**
     * 根据用户id查看用户的评论
     * @param pageIndex
     * @param pageSize
     * @param userId
     * @return
     */
    GlobalReponse<PageQuery<CommentVo>> getUserComment(int pageIndex, int pageSize, Long userId,String keywords);
}
