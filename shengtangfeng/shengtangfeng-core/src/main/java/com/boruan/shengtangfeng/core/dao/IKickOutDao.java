package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.KickOut;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

public interface IKickOutDao  extends BaseMapper<KickOut> {

    /**
     * 我被移出班级
     * @param out
     * @return
     */
    List<KickOut> outClass(KickOut out);
}
