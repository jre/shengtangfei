package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.vo.ArticleVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface ArticleVoMapper extends VoMapper<ArticleVo, Article>  {
	ArticleVoMapper MAPPER = Mappers.getMapper(ArticleVoMapper.class);
}