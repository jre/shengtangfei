package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.boruan.shengtangfeng.core.utils.QuestionOption;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.TailBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionDetailDto extends TailBean {

	@ApiModelProperty("id")
	protected Long id;

	@ApiModelProperty("年级名称")
	private String gradeName;

	@ApiModelProperty("学科名称")
	private String subjectName;

	@ApiModelProperty("题干")
	private String title;

	@ApiModelProperty("选项 里面放的是选项集合的JSON串")
	private String content;

	@ApiModelProperty("答案")
	private String answer;

	@ApiModelProperty("选项")
	private List<ContentDto> contentDtos;

	@ApiModelProperty("解析")
	private String textExplain;

	@ApiModelProperty("图片")
	private String images;

	@ApiModelProperty("类型")
	private QuestionType type;

	@ApiModelProperty("分值")
	private BigDecimal score;

	@ApiModelProperty("题库 0淘学题库 1作业题库")
	private Integer warehouseType;

	@ApiModelProperty("是否加入共享题库 0否 1是")
	private Integer shareType;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date createTime;

	@ApiModelProperty(value = "创建人Id")
	protected Long createId;

	@ApiModelProperty(value = "创建人名称")
	protected String createName;

	@ApiModelProperty(value = "创建人头像")
	protected String createImage;

	@ApiModelProperty(value = "创建人状态")
	protected Boolean createStatus;

	@ApiModelProperty("是否为自己创建")
	protected Boolean isMy;

	@ApiModelProperty("是否收藏")
	protected Boolean isFavorites;

	@ApiModelProperty("状态 0待审核 1审核通过 2审核拒绝")
	private Integer status;

	@ApiModelProperty("未答集合/空题集合")
	private List unansweredList;

	@ApiModelProperty("答错集合/错误集合")
	private List wrongList;

	@ApiModelProperty("答对集合/全对")
	private List<User> answerList;

	@ApiModelProperty("未打分集合")
	private List notScoreList;

	@ApiModelProperty("半对结合")
	private List halfList;


}
