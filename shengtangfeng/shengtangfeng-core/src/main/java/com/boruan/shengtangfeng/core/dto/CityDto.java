package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CityDto implements Serializable {

    @ApiModelProperty(value="市id")
    private Long cityId;

    @ApiModelProperty(value="市名称")
    private String cityName;

    /**
     * 高德编码
     */
    @ApiModelProperty(value="高德编码")
    private String adcode;

    @ApiModelProperty(value="区集合")
    private List<AddressDto> countyList;
}
