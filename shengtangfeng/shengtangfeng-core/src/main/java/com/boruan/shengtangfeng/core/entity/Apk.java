package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
@Description: 用来存放apk版本号
@date:2019年3月22日 上午10:13:09
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "apk")
public class Apk extends IdEntity {
    @ApiModelProperty("路径")
	private String path;
    @ApiModelProperty("介绍")
	private String introduce;
    @ApiModelProperty("版本号")
    private String code;
    @ApiModelProperty("版本名")
	private String name;
    @ApiModelProperty("是否强制更新")
	private Boolean force;
}
