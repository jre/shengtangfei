package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Music;
import com.boruan.shengtangfeng.core.vo.MusicVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MusicVoMapper extends VoMapper<MusicVo, Music>{

    MusicVoMapper MAPPER = Mappers.getMapper(MusicVoMapper.class);
}
