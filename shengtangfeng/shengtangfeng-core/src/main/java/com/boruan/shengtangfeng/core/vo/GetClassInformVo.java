package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class GetClassInformVo {

    @ApiModelProperty(value="班级id")
    private Long id ;

    @ApiModelProperty(value="班级名称")
    private String name ;

    @ApiModelProperty(value="班级编号")
    private String classNum ;

    @ApiModelProperty(value="群成员人数")
    private Long memberNum ;

}