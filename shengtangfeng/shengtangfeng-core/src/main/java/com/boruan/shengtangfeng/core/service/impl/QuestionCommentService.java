package com.boruan.shengtangfeng.core.service.impl;

import java.util.Date;
import java.util.List;

import com.boruan.shengtangfeng.core.dao.IConfigDao;
import com.boruan.shengtangfeng.core.entity.VideoComment;
import com.boruan.shengtangfeng.core.service.IQuestionCommentService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.mapper.QuestionCommentVoMapper;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IQuestionCommentDao;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.entity.QuestionComment;
import com.boruan.shengtangfeng.core.utils.Consts;
import com.boruan.shengtangfeng.core.vo.QuestionCommentVo;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = false)
public class QuestionCommentService implements IQuestionCommentService {
	@Autowired
	private IQuestionCommentDao questionCommentDao;
	@Autowired
	private IConfigDao configDao;

	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<QuestionComment> pageQuery, QuestionComment question) {
		pageQuery.setParas(question);
		questionCommentDao.pageQuery(pageQuery);
	}
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQueryNotMine(PageQuery<QuestionComment> pageQuery, QuestionComment question) {
	    pageQuery.setParas(question);
	    questionCommentDao.pageQueryNotMine(pageQuery);
	}

	/**
	 * @author: 刘光强
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public QuestionComment findById(Long id) {
		return questionCommentDao.unique(id);
	}

	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(QuestionComment questionComment) {
		if (questionComment.getId() == null) {
			KeyHolder kh = questionCommentDao.insertReturnKey(questionComment);
			questionComment.setId(kh.getLong());
		} else {
			questionCommentDao.updateById(questionComment);
		}
	}

	@Override
	public List<QuestionComment> findByQuestionId(Long questionId,Integer status,Integer type) {
		return questionCommentDao.findByQuestionId(questionId,status,type);
	}
	@Override
	public List<QuestionComment> template(QuestionComment search) {
		return questionCommentDao.findByTemplate(search);
	}

	/**
	 * 审核端根据题目id查看全部评论
	 *
	 * @param questionId
	 * @return
	 */
	@Override
	public GlobalReponse<QuestionCommentVo> getQuestionDetail(Long questionId) {
		List<QuestionComment> select = questionCommentDao.createLambdaQuery().andEq(QuestionComment::getQuestionId, questionId).andEq(QuestionComment::getIsDeleted, false).andEq(QuestionComment::getStatus, 1).select();
		List<QuestionCommentVo> questionCommentVos = QuestionCommentVoMapper.MAPPER.toVo(select);
		return GlobalReponse.success(questionCommentVos);
	}

	/**
	 * 审核端根据试题评论id删除评论
	 *
	 * @param commentId
	 * @param adminId
	 */
	@Override
	public GlobalReponse deleteComment(Long commentId, Long adminId) {
		QuestionComment single = questionCommentDao.createLambdaQuery().andEq(QuestionComment::getId, commentId).single();
		single.setIsDeleted(true);
		single.setUpdateBy(adminId.toString());
		single.setUpdateTime(new Date());
		questionCommentDao.updateTemplateById(single);
		return GlobalReponse.success("删除成功");
	}
}
