package com.boruan.shengtangfeng.core.entity;

import com.boruan.shengtangfeng.core.vo.QuestionVo;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 题库
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question_comment")
//@EqualsAndHashCode(callSuper = true)
public class QuestionComment extends IdEntity {

	private static final long serialVersionUID = -4470773369611955781L;
	/**
	 * 问题ID
	 */
	private Long questionId;
	/**
	 * 发布人ID
	 */
	private Long userId;
	/**
	 * 发布人头像
	 */
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	private String userName;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 语音地址
	 */
	private String voice;
	/**
	 * 逗号分隔的图片
	 */
	private String images;
	/**
	 * 状态 0未审核 1已审核 2审核失败
	 */
	private Integer status;
	/**
	 * 类型  1文字，2文字+图片，3语音，4语音+图片，5图片
	 */
	private Integer type;
	/**
	 * 语音时间，秒数
	 */
	private Integer voiceTime;
	
	public void autoSetType() {
	    if (StringUtils.isNotBlank(content)&&StringUtils.isAllBlank(images,voice)) {
            this.type=1;
        }else if (StringUtils.isNoneBlank(content,images)&&StringUtils.isAllBlank(voice)) {
            this.type=2;
        }
        else if (StringUtils.isNoneBlank(voice)&&StringUtils.isAllBlank(content,images)) {
            this.type=3;
        }
        else if (StringUtils.isNoneBlank(voice,images)&&StringUtils.isAllBlank(content)) {
            this.type=4;
        }
        else if (StringUtils.isNoneBlank(images)&&StringUtils.isAllBlank(voice,content)) {
            this.type=5;
        }
	}
}
