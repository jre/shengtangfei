package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Data
public class InformDto extends TailBean {

    @ApiModelProperty("通知Id")
    protected Long id;

    @ApiModelProperty("通知内容")
    private String content ;

    @ApiModelProperty("通知图片")
    private String images ;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08:00")
    @ApiModelProperty("创建时间")
    protected Date createTime;

    @ApiModelProperty("创建人id")
    protected Long createId;

    @ApiModelProperty("创建人头像")
    protected String createImage;

    @ApiModelProperty("创建人昵称")
    protected String createNickname;

    @ApiModelProperty(value = "创建人状态")
    protected Boolean createStatus;

    @ApiModelProperty("是否为自己发布")
    protected Boolean isMy;

    @ApiModelProperty("审核状态 0待审核 1通过 2拒绝")
    private Integer status ;

}