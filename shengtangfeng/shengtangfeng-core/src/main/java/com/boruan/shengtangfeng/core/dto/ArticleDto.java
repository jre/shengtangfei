package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * 日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="文章")
public class ArticleDto{

	/**
	 * ID
	 */
	@ApiModelProperty(value="ID,编辑更新时传")
	private Long id;
	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="分类ID")
	private Long categoryId;
	/**
	 * 文章标题
	 */
	@ApiModelProperty(value="文章标题")
	private String title;

	/**
	 * 文章内容
	 */
	@ApiModelProperty(value="文章内容")
	private String content;

	
}
