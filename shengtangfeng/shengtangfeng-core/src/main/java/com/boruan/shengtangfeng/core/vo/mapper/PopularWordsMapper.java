package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.PopularWords;
import com.boruan.shengtangfeng.core.vo.ArticleVo;
import com.boruan.shengtangfeng.core.vo.PopularWordsVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface PopularWordsMapper extends VoMapper<PopularWordsVo, PopularWords>  {
	PopularWordsMapper MAPPER = Mappers.getMapper(PopularWordsMapper.class);
}