package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.vo.QuestionVo;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface QuestionVoMapper extends VoMapper<QuestionVo, Question>  {
	QuestionVoMapper MAPPER = Mappers.getMapper(QuestionVoMapper.class);
	@Mappings({
			@Mapping(target = "optionList", expression = "java(entity.getContent() == null ? null : com.boruan.shengtangfeng.core.utils.JsonUtils.jsonToList(entity.getContent(), (com.boruan.shengtangfeng.core.utils.QuestionOption.class)))")})
	public QuestionVo toVo(Question entity);
}