package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 @author: guojiang
 @Description:
 @date:2021年3月31日 下午14:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NewFriendVo {

    @ApiModelProperty("id，记录的ID")
    private Long id;

    @ApiModelProperty("0为当前用户添加其他用户 1为其他用户添加当前用户")
    private Integer apply;

    @ApiModelProperty("关联id")
    private Long relevanceId;

    @ApiModelProperty("关联头像")
    private String relevanceImage;

    @ApiModelProperty("关联昵称")
    private String relevanceNickname;

    @ApiModelProperty("手机号")
    private String relevanceMobile;


    @ApiModelProperty("状态 0等待验证 1同意 2拒绝 ")
    private Integer status ;

    @ApiModelProperty("申请备注 ")
    private String applyRemark ;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    protected Date createTime;
}
