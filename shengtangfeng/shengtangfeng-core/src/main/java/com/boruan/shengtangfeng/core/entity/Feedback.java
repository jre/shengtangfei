package com.boruan.shengtangfeng.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

/**
 * @author: 刘光强
 * @Description:  用户反馈
 * @date:2020年3月10日 上午11:14:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "feed_back")
@EqualsAndHashCode(callSuper = false)
public class Feedback extends IdEntity {

	private Long userId;

	private String os;
	private String version;
	private String content;
}
