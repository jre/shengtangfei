package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;

/**
 * 查询作业列表条件
 */
@Data
public class JobConditionDto {

    @ApiModelProperty("班级Id")
    private Long classId ;

    @ApiModelProperty("发布时间 1今天 2本周 3本月 4本学期 5自定义,需要传startTime和finishTime")
    protected Integer pushTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("开始时间")
    protected Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("结束时间")
    protected Date finishTime;

    @ApiModelProperty("截止时间 1今天 2本周 3本月 4已截止")
    protected Integer entTime;

    @ApiModelProperty("科目")
    private String subjectName ;

    @ApiModelProperty("排序 0正序 1倒序")
    private Integer sort ;

    @ApiModelProperty("只看我发布的 0否 1是")
    private Integer isMy ;

    @ApiModelProperty("页数")
    private Integer pageNo=1 ;

    @ApiModelProperty(value = "未完成0否1是")
    private Integer notComplete;
}
