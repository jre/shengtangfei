package com.boruan.shengtangfeng.core.dto.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.dto.ModuleDto;
import com.boruan.shengtangfeng.core.entity.Module;

/**
* @author:twy
* @Description:
*/
@Mapper
public interface ModuleDtoMapper extends DtoMapper<ModuleDto, Module>{

	ModuleDtoMapper MAPPER = Mappers.getMapper(ModuleDtoMapper.class);

}
