package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
public class FriendDto {

    @ApiModelProperty(value="id")
    private Long id;

    @ApiModelProperty(value="头像")
    private String image;

    @ApiModelProperty(value="昵称")
    private String nickname;

    @ApiModelProperty(value="是否已在班级中")
    private Boolean exists;

}
