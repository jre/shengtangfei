package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Music;
import com.boruan.shengtangfeng.core.entity.MusicCollection;
import com.boruan.shengtangfeng.core.entity.Singer;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

public interface IMusicService {

    public void pageQuery(PageQuery<Music> pageQuery, Music music);


    public void pageCollectionMusic(PageQuery<Music> pageQuery,Long userId);

    public GlobalReponse collectMusic(Long musicId,Long userId);

    void save(Music music);

    //删除音乐 type 删除音乐传0，删除热门音乐传1
    void delMusic(Long musicId,Integer type);

    Singer findSingerById(Long singerId);

    void pageQuerySinger(PageQuery<Singer> pageQuery);

    void saveSinger(Singer old);

    Music findById(Long musicId);

    void delSinger(Long singerId);

    void pageQuery1(PageQuery<Music> pageQuery);
}
