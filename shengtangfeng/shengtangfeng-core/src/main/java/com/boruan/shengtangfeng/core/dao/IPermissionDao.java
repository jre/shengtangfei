package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.Permission;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IPermissionDao extends BaseMapper<Permission> {
	/**
	 * 根据级别获取所有权限
	 * 
	 * @param level
	 * @return
	 */
	public List<Permission> findByLevel(@Param("level") Integer level);

	/**
	 * 获取下级权限
	 * 
	 * @param parentId
	 * @return
	 */
	public List<Permission> findChildByParentId(@Param("parentId") Long parentId);

    List<Permission> selectMenuList(Long roleId);
}
