package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.Advertise;
import org.beetl.sql.core.engine.PageQuery;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IAdvertiseService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param advertise
	 */
	public void pageQuery(PageQuery<Advertise> pageQuery, Advertise advertise);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public Advertise findById(Long id);
	/**
	 * 根据条件获取列表
	 * @return
	 */
	public List<Advertise> findByTemplate(Advertise advertise);
	/**
	 * 根据条件获取列表
	 * @return
	 */
	public List<Advertise> getList(Integer status,Integer position);
	/**
	 * 保存分类
	 * @param  advertise
	 */
	public void save(Advertise advertise);

	void delAdvertise(String ids);
}
