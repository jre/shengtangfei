package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ModuleVo extends BaseVo {

    /**
	 * 父章节ID
	 **/
	@ApiModelProperty("父章节ID")
	private Long parentId;
	/**
	 * 章节名
	 **/
	@ApiModelProperty("章节名")
	private String name;
	/**
	 * 已做题目数量
	 **/
	@ApiModelProperty("已做题目数量，如果等于题目数量，则算已完成")
	private Long answerCount;
	/**
	 * 题目数量
	 **/
	@ApiModelProperty("题目数量")
	private Long questionCount;
	/**
	 * 章节描述
	 **/
	@ApiModelProperty("章节描述")
	private String description;
	/**
	 * 子章节
	 **/
	@ApiModelProperty("章节描述")
	private List<ModuleVo> childs;

	/**
	 * 视频模块集合
	 **/
	@ApiModelProperty("视频模块集合")
	private List<ModuleVideoVo> moduleVideoVos;

	/**
	 * 是否是视频模块
	 **/
	@ApiModelProperty("是否是视频模块")
	private Boolean isHeader;

	/**
	 * 1试题模块2视频模块
	 */
	@ApiModelProperty("模块分类1试题模块2视频模块")
	private Integer type;
	
	/**
     * 难度 0-5
     */
    @ApiModelProperty(value = "难度 0-5")
    private Integer difficulty;
    
    /**
     * 完成人数
     */
    @ApiModelProperty(value = "完成人数")
    private Integer completeCount;
    
    /**
     * 分类图标
     **/
    @ApiModelProperty(value = "分类图标")
    private String icon;
    
    /**
     * 模块下单题分数
     */
    @ApiModelProperty(value = "模块下单题分数")
    private Integer score;
    /**
     * 年级ID
     **/
    @ApiModelProperty(value = "年级ID")
    private Long gradeId;
    /**
     * 学科ID
     **/
    @ApiModelProperty(value = "年级ID")
    private Long subjectId;

	/**
	 * 排序
	 **/
	@ApiModelProperty(value = "排序")
	private Integer sequence;

}
