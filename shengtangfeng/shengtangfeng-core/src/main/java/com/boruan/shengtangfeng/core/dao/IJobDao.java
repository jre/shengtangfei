package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.dto.InformDto;
import com.boruan.shengtangfeng.core.entity.Job;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IJobDao extends BaseMapper<Job> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Job> query);

    public List<Job> getLast3Jobs(Long classId,Long userId);

    /**
     * 教师端多条件查询作业列表
     * @param pageQuery
     */
    void getJobListById(PageQuery<Job> pageQuery);
}