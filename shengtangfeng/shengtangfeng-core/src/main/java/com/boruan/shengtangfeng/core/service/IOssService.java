package com.boruan.shengtangfeng.core.service;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.springframework.web.multipart.MultipartFile;

import com.boruan.shengtangfeng.core.vo.OssCallbackResult;
import com.boruan.shengtangfeng.core.vo.OssPolicyResult;

/**
 * oss上传管理Service
 * Created by macro on 2018/5/17.
 */
public interface IOssService {
    OssPolicyResult policy();
    OssCallbackResult callback(HttpServletRequest request);
    GlobalReponse uploadFile(MultipartFile file);
    GlobalReponse uploadFiles(MultipartFile[] files);
    public Map<String, String> getSts(Long userId);
    String uploadObject2OSS(File file);
}
