package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.vo.ArticleVideoCommentVo;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IUserDao extends BaseMapper<User> {
	/**
	 * 通过微信的OpenID获取用户
	 *
	 * @param openId
	 * @return
	 */
	public User findByOpenId(@Param("openId") String openId);

	/**
	 * 通过手机号获取用户
	 * 
	 * @param mobile
	 * @return
	 */
	public User findByMobile(@Param("mobile") String mobile);

	/**
	 * 通过登录名（学号）获取用户
	 * 
	 * @param mobile
	 * @return
	 */
	public User findByLoginName(@Param("loginName") String loginName);

//	/**
//	 * 通过id获取用户
//	 * 
//	 * @param id
//	 * @return
//	 */
//	public User findById(@Param("id") Long id);

	/**
	 * 分页查询
	 * 
	 * @param query
	 */
	public void pageQuery(PageQuery<User> query);
	
	/**
     * 获取排名用户
     * @param type 排行类型  1 按照做题数 2 按照正确率
     * @return
     */
	public List<User> getRank(Integer type, Integer size);
	
	/**
	 * 获取我邀请的用户
	 * @param userId
	 * @param isVip
	 * @return
	 */
	public List<User> getMySpread(Long spreadId, Boolean isVip);


	 /**
	     * @description: 获取用户邀请码
	     * @Param
	     * @return
	     * @author KongXH
	     * @date 2020/8/14 13:41
	     */
	public User findByInvitationCode(Long id);
	/**
	 * 获取用户的评论，包含文章和视频
	 * @param userId
	 * @return
	 */
	public void pageUserComment(PageQuery<ArticleVideoCommentVo> pageQuery);

	/**
	 * 获取评论，包含文章和视频
	 * @param userId
	 * @return
	 */
	public void pageComment(PageQuery<ArticleVideoCommentVo> pageQuery);


	public void plusLikeNum(Long id);

	public void lessLikeNum(Long id);

	/**
	 * 根据id或名称搜索好友
	 * @param parameter
	 * @return
	 */
	List<User> searchFriendOrGroup(String parameter);

    List<User> getNotAttentionRobot(Long userId,Integer fansNum);
}
