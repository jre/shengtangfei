package com.boruan.shengtangfeng.core.dao;


import com.boruan.shengtangfeng.core.entity.SystemUserRole;
import org.beetl.sql.core.mapper.BaseMapper;

public interface ISystemUserRoleDao extends BaseMapper<SystemUserRole> {
}
