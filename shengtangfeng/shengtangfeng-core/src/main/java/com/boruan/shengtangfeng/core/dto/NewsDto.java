package com.boruan.shengtangfeng.core.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 新闻主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="新闻主体")
public class NewsDto extends BaseDto{

    /**
     * 新闻标题
     */
    @ApiModelProperty(value="新闻标题")
    private String title;
    
    /**
     * 新闻内容
     */
    @ApiModelProperty(value="新闻内容")
    private String content;
    
    /**
     * 发布时间
     */
    @ApiModelProperty(value="发布时间")
    private Date publishDate;
    /**
     * 发布人
     */
    @ApiModelProperty(value="发布人")
    private String publisher;
    /**
     * 原文链接
     */
    @ApiModelProperty(value="原文链接")
    private String link;
    
    /**
     * 状态 0 未发布  1 发布
     */
    @ApiModelProperty(value="状态 0 未发布  1 发布")
    private Integer status;
}
