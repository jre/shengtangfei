package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ModuleRankVo {
    
    /**
     * 前50名排行
     */
    @ApiModelProperty("前50名排行")
    private List<ModuleRankDetailVo> details;
    /**
     * 我的排名信息
     */
    @ApiModelProperty("我的排名信息")
    private ModuleRankDetailVo myRank;
    /**
     * 第一名
     */
    @ApiModelProperty("第一名")
    private ModuleRankDetailVo first;
    /**
     * 第二名
     */
    @ApiModelProperty("第二名")
    private ModuleRankDetailVo second;
    /**
     * 第三名
     */
    @ApiModelProperty("第三名")
    private ModuleRankDetailVo third;
    
    
}
