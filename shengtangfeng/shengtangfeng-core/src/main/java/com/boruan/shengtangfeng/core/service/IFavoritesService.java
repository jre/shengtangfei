package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Favorites;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

/**
 * @author KongXH
 * @title: IFavoritesService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1714:11
 */
public interface IFavoritesService {


    public Favorites getFavorites(Long userId,Long articleId,Integer type);

    public GlobalReponse   saveFavorites(Favorites favorites);

    public void deleteFavorites(Long userId,Long articleId,Integer type);
}
