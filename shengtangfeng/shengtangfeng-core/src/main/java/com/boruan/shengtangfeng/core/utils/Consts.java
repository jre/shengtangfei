package com.boruan.shengtangfeng.core.utils;

/**
 * @author: lihaicheng
 * @Description: 常量
 * @date:2020年3月10日 上午11:14:10
 */
public class Consts {
    
 // 目前只有"cn-hangzhou"这个region可用, 不要使用填写其他region的值
    public static final String REGION_CN_HANGZHOU = "cn-hangzhou";
    public static final String STS_API_VERSION = "2015-04-01";

    /**
     * 统计题目数量定时任务锁主键
     */
    public static final String SCHEDULE_KEY_QUESTIONCOUNT = "shengtangfeng:schedule:questioncount:";
    /**
     * 统计题目数量定时任务锁主键
     */
    public static final String SCHEDULE_KEY_CALCULATEDURATION = "shengtangfeng:schedule:calculateDuration:";
    /**
     * 角色权限缓存 格式：key+roleId
     */
    public static final String ROLE_PERMISSION = "shengtangfeng:permission:";

    /**
     * 后台获取省市区 格式：key
     */
    public static final String ADMIN_ADDRESS = "shengtangfeng:address";

    /**
     * 所有权限菜单缓存
     */
    public static final String ROLE_PERMISSION_ALL = "shengtangfeng:permission:all";

    /**
     * 保存设备和token
     */
    public static final String DEVICE_KEY = "shengtangfeng:device:";
    /**
     * 保存用户的token
     */
    public static final String USER_TOKEN_KEY = "shengtangfeng:token:";
    /**
     * 用户扫码看题次数
     */
    public static final String REDIS_USER_SCAN_NUM = "shengtangfeng:user:scan:";
    /**
     * 保存微信授权accessToken的key
     */
    public static final String REDIS_ACCECC_TOKEN_KEY = "shengtangfeng:accessToken:";
    /**
     * 保存验证码
     */
    public static final String sendCode = "shengtangfeng:send:code:";
   /**
    * 轮播图redis_key
    */
   public static final String BANNER_CACHE = "shengtangfeng:banner:cache:";

    /**
     * 客服h5地址
     */
    public static final String CONFIG_CUSTOMER_SERVICE_URL = "config_customer_service_url";
    /**
     * 客服工作时间
     */
    /**
     * OSS bucket配置KEY
     */
    public static final String CONFIG_OSS_BUCKET = "config_oss_bucket";


    /**
     * OSS bucket配置KEY
     */
    public static final String CONFIG_EXPORT_INCORRECT_FAVORITES = "config_export_incorrect_favorites";

    /**
     * 苹果上架标记
     */
    public static final String CONFIG_APPLE_ONLINE = "config_apple_online";
    /**
     *用户端教学视频
     */
    public static final String CONFIG_LEARN_VIDEO_USER = "config_learn_video_user";
    
    /**
     * 分享的二维码地址
     */
    public static final String CONFIG_SHARE_QRCODE = "config_share_qrcode";
    /**
     * 隐私协议
     */
    public static final String CONFIG_PRIVACY_AGREEMENT = "config_privacy_agreement";
    /**
     * 注册协议
     */
    public static final String CONFIG_REGISTER_AGREEMENT = "config_register_agreement";
    /**
     * 帮助文档
     */
    public static final String CONFIG_HELP_DOCUMENT = "config_help_document";
    /**
     * 帮助文档
     */
    public static final String CONFIG_USER_ICON = "config_user_icon";

    /**
     * 用户注册后赠送粉丝总个数
     */
    public static final String CONFIG_FANS_NUM = "config_fans_num";

    /**
     * 用户注册后赠送粉丝总共几天送完
     */
    public static final String CONFIG_FANS_DAY = "config_fans_day";
}
