package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TalkVideoDto extends BaseDto {

	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="分类ID")
	private Long categoryId;
	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="ID，编辑更新时传")
	private Long id;
	/**
	 * 视频标题
	 */
	@ApiModelProperty(value="视频标题")
	private String title;

	/**
	 * 当前班级Id
	 */
	@ApiModelProperty(value="当前班级Id")
	private Long classId;
	/**
	 * 视频地址
	 */
	@ApiModelProperty(value="视频地址")
	private String content;

	@ApiModelProperty(value = "发布范围：0公开1好友可见2班级可见3仅自己可见")
	private Integer scope;

	@ApiModelProperty(value = "可见班级id逗号拼接，班级可见时必填")
	private String classes;

}
