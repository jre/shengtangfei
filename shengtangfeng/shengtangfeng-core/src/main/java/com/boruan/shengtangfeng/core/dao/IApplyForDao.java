package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ApplyFor;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IApplyForDao extends BaseMapper<ApplyFor> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ApplyFor> query);

    /**
     * 查询七天的好友请求
     * @param applyFor
     * @return
     */
    List<ApplyFor> getNewFriend(ApplyFor applyFor);

    /**
     * 查询七天内的群请求
     * @param applyFor
     * @return
     */
    List<ApplyFor> getNewGroup(ApplyFor applyFor);

    /**
     * 查询七天内的班级请求
     * @param applyFor
     * @return
     */
    List<ApplyFor> getNewClass(ApplyFor applyFor);

    Long getNewFriendNum(Long userId);
}