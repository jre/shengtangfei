package com.boruan.shengtangfeng.core.dao;
import java.util.List;

import com.boruan.shengtangfeng.core.entity.FavoritesQuestion;
import com.boruan.shengtangfeng.core.entity.Question;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IFavoritesQuestionDao extends BaseMapper<FavoritesQuestion> {
    
	public void pageFavorites(PageQuery<Question> pageQuery);
	public List<Question> getFavorites(@Param("userId") Long userId,@Param("type") Integer type);
	public Long getFavoritesCount(@Param("userId") Long userId,@Param("type") Integer type);
	public Integer deleteFavorites(@Param("userId") Long userId,@Param("questionId") Long questionId);

	/**
	 * 试题是否收藏
	 * @param userId
	 * @param questionId
	 * @return
	 */
    FavoritesQuestion getCorrectQuestion(Long userId, Long questionId);
}
