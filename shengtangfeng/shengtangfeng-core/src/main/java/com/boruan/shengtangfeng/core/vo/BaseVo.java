package com.boruan.shengtangfeng.core.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.beetl.core.lab.TailBean;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel
public abstract class BaseVo extends TailBean implements Serializable{

	@ApiModelProperty(value="id")
	protected Long id;

	@ApiModelProperty(value="创建人")
	protected String createBy;
	
	
	@ApiModelProperty(value="创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date createTime;
	
	@ApiModelProperty(value="最后更新人")
	protected String updateBy;
	
	
	@ApiModelProperty(value="最后更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date updateTime;
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
