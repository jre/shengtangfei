package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.vo.ToAuditVideoVO;
import com.boruan.shengtangfeng.core.vo.VideoVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: 郭江
@Description:
@date:2021/2/25
*/
@Mapper
public interface ToAuditVideoVoMapper extends VoMapper<ToAuditVideoVO, Video>  {
	ToAuditVideoVoMapper MAPPER = Mappers.getMapper(ToAuditVideoVoMapper.class);
}