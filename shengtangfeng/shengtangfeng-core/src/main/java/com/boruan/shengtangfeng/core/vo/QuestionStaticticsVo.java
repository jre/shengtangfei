package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionStaticticsVo {
    /**
     * 答题数量
     */
    @ApiModelProperty("答题数量")
    private int answerNum;
    /**
     * 错题数量
     */
    @ApiModelProperty("错题数量")
    private Integer incorrectNum;
    /**
     * 正确数量
     */
    @ApiModelProperty("正确数量")
    private Integer correctNum;
    /**
     *总题数
     */
    @ApiModelProperty("总题数")
    private Long questionNum;
    /**
     * 正确率  百分比
     **/
    @ApiModelProperty("正确率  百分比")
    private Integer accurate;
}
