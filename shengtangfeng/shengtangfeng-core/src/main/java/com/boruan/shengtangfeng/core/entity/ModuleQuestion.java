package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 模块-习题关系
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "module_question")
public class ModuleQuestion {

	/**
	 * 模块ID
	 **/
	private Long moduleId;
	
	/**
	 * 题目ID
	 **/
	private Long questionId;

	/**
	 * 排序
	 */
	private Integer sort;

}
