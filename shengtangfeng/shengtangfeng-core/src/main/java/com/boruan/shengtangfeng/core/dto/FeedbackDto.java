package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 反馈
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FeedbackDto{

	@ApiModelProperty("系统 ios 还是android")
	private String os;
	@ApiModelProperty("版本号")
    private String version;
	@ApiModelProperty("内容")
    private String content;

}
