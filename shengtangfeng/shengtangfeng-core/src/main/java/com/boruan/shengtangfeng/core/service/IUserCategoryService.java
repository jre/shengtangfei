package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.dto.UserCategoryDto;
import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.entity.VideoCategory;

/**
 * @author KongXH
 * @title: IUserCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1314:16
 */
public interface IUserCategoryService {

    public void saveUserCategory(UserCategoryDto userCategoryDto,Integer type,Long userId);
    public List<ArticleCategory> getUserArticleCategory(Long userId);
    public List<VideoCategory> getUserVideoCategory(Long userId,Integer type);

    List<Subject> getUserJYJCategory(Long id, Integer type);
}
