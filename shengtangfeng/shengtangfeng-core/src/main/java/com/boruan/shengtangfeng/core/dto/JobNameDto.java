package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;

@Data
public class JobNameDto {

    @ApiModelProperty("作业名称Id,新建时候不需要传递")
    private Long id ;

    @ApiModelProperty("作业名称")
    private String name ;

}