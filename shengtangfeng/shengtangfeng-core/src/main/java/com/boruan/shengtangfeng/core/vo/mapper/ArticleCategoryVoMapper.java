package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import com.boruan.shengtangfeng.core.vo.ArticleCategoryVo;

/**
@author: lihaicheng
@Description:ArticleCategoryVo
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface ArticleCategoryVoMapper extends VoMapper<ArticleCategoryVo,  ArticleCategory>  {
	ArticleCategoryVoMapper MAPPER = Mappers.getMapper(ArticleCategoryVoMapper.class);
}