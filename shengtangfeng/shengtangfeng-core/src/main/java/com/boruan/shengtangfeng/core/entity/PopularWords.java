package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="popular_words")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PopularWords extends IdEntity {

    /**
    * 关键字 
    **/
    private String keyword ;
            
    /**
    * 关键字类型 
    **/
    private Integer keywordType ;
            


}