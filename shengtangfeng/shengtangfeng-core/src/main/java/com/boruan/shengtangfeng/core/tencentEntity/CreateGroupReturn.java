package com.boruan.shengtangfeng.core.tencentEntity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateGroupReturn {

    /**
     *返回的群成员 UserID
     */
    @JSONField(name = "Member_Account")
    private String Member_Account;

    /**
     *Result
     */
    @JSONField(name = "Result")
    private String Result;

}
