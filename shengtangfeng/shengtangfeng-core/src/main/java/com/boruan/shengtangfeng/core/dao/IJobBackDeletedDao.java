package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.JobBackDeleted;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IJobBackDeletedDao extends BaseMapper<JobBackDeleted> {
}
