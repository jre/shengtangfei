package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.SystemUser;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface ISystemUserDao extends BaseMapper<SystemUser> {
	/**
	 * 通过手机号获取用户
	 * 
	 * @param mobile
	 * @return
	 */
	public SystemUser findByMobile(@Param("mobile") String mobile);

	/**
	 * 通过登录名获取用户
	 * 
	 * @param mobile
	 * @return
	 */
	public SystemUser findByLoginName(@Param("loginName") String loginName);

//	/**
//	 * 通过id获取用户
//	 * @param id
//	 * @return
//	 */
//	public SystemUser findById(@Param("id") Long id);
	/**
	 * 获取总部信息
	 */
	public SystemUser getHeadquartersInfo();

	/**
	 * 后台分页
	 */
	public void pageQuery(PageQuery<SystemUser> query);

	/**
	 * 添加权限
	 */
	public void insertRole(@Param("systemUserId") Long systemUserId, Long roleId);

	/**
	 * 获取全部加盟商
	 */
	public List<SystemUser> findAll();

	/**
	 * 获取用户，关联获取用户的角色
	 * 
	 * @param id
	 * @return
	 */
	public SystemUser findUserAndRole(@Param("id") Long id);

	/**
	 * 删除用户的所有角色
	 * 
	 * @param userId
	 */
	public void deleteUserRole(@Param("userId") Long userId);

	/**
	 * 插入用户角色的关联关系
	 * 
	 * @param userId
	 * @param roleId
	 */
	public void insertUserRole(@Param("systemUserId") Long userId, @Param("roleId") Long roleId);

	/**
	 * 根据工号或者姓名查询老师
	 * 
	 * @param assistantNo
	 * @param assistantName
	 * @return
	 */
	public List<SystemUser> findByEmployeeNumberOrName(String assistantNo, String assistantName);

	public void pageSystemUser(PageQuery<SystemUser> query);

}
