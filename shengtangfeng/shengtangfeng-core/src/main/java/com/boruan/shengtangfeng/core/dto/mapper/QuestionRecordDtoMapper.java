package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.QuestionRecordDto;
import com.boruan.shengtangfeng.core.entity.QuestionRecord;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface QuestionRecordDtoMapper extends DtoMapper<QuestionRecordDto, QuestionRecord> {
	QuestionRecordDtoMapper MAPPER = Mappers.getMapper(QuestionRecordDtoMapper.class);
}
