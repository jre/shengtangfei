package com.boruan.shengtangfeng.core.dao;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.Video;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IVideoDao extends BaseMapper<Video> {
	/**
	 * 分页查询
	 * 
	 * @param query
	 */
	public void pageQuery(PageQuery<Video> query);
	/**
	 * 获取相关文章
	 * @param video
	 * @return
	 */
	public List<Video> findClosely(Video video);


	public Video findById (Long id);

	public void pageAttentionVideo(PageQuery<Video> query);
	
	/**
	 * 分页获取收藏的视频
	 * @param query
	 */
	public void pageFavoritesVideo(PageQuery<Video> query);
	/**
	 * 分页获取点赞的视频
	 * @param query
	 */
	public void pageAccoladeVideo(PageQuery<Video> query);

	/**
	 * 查看用户待审核视频
	 * @param pageQuery
	 */

    void pageToAuditArticle(PageQuery<Video> pageQuery);

    public List<Video> newCheck(Video video);

    void pageToAuditVideo(PageQuery<Video> pageQuery);

	/**
	 * 审核端根据用户id查看视频
	 * @param pageQuery
	 */
	void getUserVideo(PageQuery<Video> pageQuery);

	public List<Video> getLast4StudentVideo(Long classId);

    void pageClassTalkVideo(PageQuery<Video> pageQuery);

	/**
	 * 获取最新的讲一讲视频
	 * @param classId
	 * @return
	 */
    List<Video> getNewVideo(Long classId);

	/**
	 * 查询推荐小视频
	 * @param pageQuery
	 */
	void selectRecommendVideo(PageQuery<Video> pageQuery);

	void selectAttentionVideo(PageQuery<Video> pageQuery);

	void selectLocalVideo(PageQuery<Video> pageQuery);

	void updateOSSDNS();

	void updateOSSDNS1();
}
