package com.boruan.shengtangfeng.core.vo;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SpreadStatistics {
    /**
     * 推广总收益
     */
    @ApiModelProperty(value = "推广总收益")
    private BigDecimal allReward;
    /**
     * 推广总人数
     */
    @ApiModelProperty(value = "推广总人数")
    private Integer count;
}
