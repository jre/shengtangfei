package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.vo.VideoVo;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface VideoVoMapper extends VoMapper<VideoVo, Video>  {
	VideoVoMapper MAPPER = Mappers.getMapper(VideoVoMapper.class);
}