package com.boruan.shengtangfeng.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.functors.FalsePredicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.ISearchWordsDao;
import com.boruan.shengtangfeng.core.entity.SearchWords;
import com.boruan.shengtangfeng.core.service.ISearchWordsService;

/**
 * @author KongXH
 * @title: SearchWordsService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1317:07
 */
@Service
@Transactional(readOnly = true)
public class SearchWordsService implements ISearchWordsService {

    @Autowired
    private ISearchWordsDao searchWordsDao;


    /**
     * @description: 查询搜索词
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 17:08
     */
    @Override
    public List<SearchWords> getList(Long userId,Integer type) {
        return searchWordsDao.getList(userId,type);
    }
    /**
     * @description: 查询搜索词
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 17:08
     */
    @Override
    @Transactional(readOnly = false)
    public void addSearchWord(Long userId,Integer type,String keyword) {
        SearchWords search=new SearchWords();
        search.setUserId(userId);
        search.setKeyword(keyword);
        search.setType(type);
        search.setIsDeleted(false);
        long count=searchWordsDao.templateCount(search);
        if(count==0) {
            search.setCreateTime(new Date());
            searchWordsDao.insert(search);
        }
    }
}
