package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.VideoClass;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IVideoClassDao extends BaseMapper<VideoClass> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<VideoClass> query);
}