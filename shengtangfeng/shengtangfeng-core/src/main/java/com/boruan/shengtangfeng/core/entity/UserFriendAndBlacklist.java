package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="user_friend_and_blacklist")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserFriendAndBlacklist extends IdEntity {
    /**
    * 关联的id 
    **/
    private Long relevanceId ;
            
    /**
    * 类型 0好友 1群聊 2黑名单 
    **/
    private Integer type ;
    /**
    * 用户id 
    **/
    private Long userId ;
}