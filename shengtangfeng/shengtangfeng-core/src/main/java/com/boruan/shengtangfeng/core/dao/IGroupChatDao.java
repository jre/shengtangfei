package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.GroupChat;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IGroupChatDao extends BaseMapper<GroupChat> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<GroupChat> query);

    /**
     * 根据id或名称查询群聊
     * @param parameter
     * @return
     */
    List<GroupChat> searchFriendOrGroup(String parameter);
}