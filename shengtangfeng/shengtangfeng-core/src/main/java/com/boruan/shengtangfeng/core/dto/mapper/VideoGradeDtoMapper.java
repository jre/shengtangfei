package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.VideoGradeDto;
import com.boruan.shengtangfeng.core.entity.VideoGrade;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VideoGradeDtoMapper extends DtoMapper<VideoGradeDto, VideoGrade> {
    VideoGradeDtoMapper MAPPER = Mappers.getMapper(VideoGradeDtoMapper.class);
}
