package com.boruan.shengtangfeng.core.entity;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.Sex;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
@EqualsAndHashCode(callSuper = false)
public class User extends IdEntity {

	private static final long serialVersionUID = -1566405435590239910L;

	/**
	 * 是否锁定
	 **/
	private Boolean isLocked;
	/**
	 * 是否能修改手机号
	 **/
	private Boolean changeMobile;

	/**
	 * 性别 1男 2女
	 **/
	private Sex sex;

	/**
	 *头像
	 **/
	private String headImage;

	/**
	 *账号
	 **/
	private String account;

	/**
	 * 手机号
	 **/
	private String mobile;

	/**
	 * 姓名
	 **/
	private String name;


	/**
	 * 登录名
	 **/
	private String loginName;

	/**
	 * 密码
	 **/
	private String password;

	/**
	 * 密码盐
	 **/
	private String salt;

	/**
	 * 微信unionId
	 **/
	private String unionId;
	/**
	 * 微信openId
	 **/
	private String openId;
	
    /**
     * 答题数
     **/
    private Integer answerNum;
    /**
     * 正确数
     **/
    private Integer correctNum;
    /**
     * 错误数
     **/
    private Integer incorrectNum;
    /**
     * 正确率  百分比
     **/
    private BigDecimal accurate;
    /**
     * 积分
     **/
    private Integer integral;
//    /**
//	 * 总打卡天数
//	 */
//	private Integer totalPunch;
//	/**
//	 * 连续打卡天数 到21 清0
//	 */
//	private Integer punchNum;
	

	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 生日
	 */
	private String birthDay;
	/**
	 * 省
	 */
	private String province;
	/**
	 * 市
	 */
	private String city;


	private String invitationCode;//邀请码

	private Integer likeNum;//获赞数

	/**
	 * 用户类型1学生2老师
	 */
	private Integer userType;//用户类型

	/**
	 * 登陆时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
	private Date loginTime;//登陆时间

	/**
	 * 登出时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
	private Date logoutTime;//登出时间

	/**
	 * 家长手机号
	 */
	private String parentMobile;

	/**
	 * 是否显示定位：0否1是
	 */
	private Integer showLocation;

	@ApiModelProperty("腾讯userSig")
	private String userSig ;

	@ApiModelProperty("用户类型 0正常用户 1机器人用户")
	private Integer type;
}
