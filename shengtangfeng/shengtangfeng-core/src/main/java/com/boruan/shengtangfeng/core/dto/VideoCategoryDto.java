package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 新闻主体
 * @author liuguangqiang
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="视频分类主体")
public class VideoCategoryDto extends BaseDto{

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 是否默认1是0否
     */
    @ApiModelProperty(value="是否默认1是0否")
    private String isDefault;

    @ApiModelProperty(value = "0普通视频1讲一讲2小视频")
    private Integer type;
}
