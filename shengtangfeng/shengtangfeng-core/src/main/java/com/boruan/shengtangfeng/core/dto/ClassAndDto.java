package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.beetl.sql.core.annotatoin.Table;


@Data
public class ClassAndDto {

    @ApiModelProperty(value="学校id")
    private Long schoolId ;

    @ApiModelProperty(value="年级分部")
    private String gradeDivision ;

    @ApiModelProperty(value="学年")
    private String schoolYear ;

    @ApiModelProperty(value="班级名称")
    private String name ;

    @ApiModelProperty(value = "班级编号")
    private String classNum;

    @ApiModelProperty(value = "页码")
    private Integer pageNo=1;

    @ApiModelProperty(value = "每页数量")
    private Integer pageSize=10;
}