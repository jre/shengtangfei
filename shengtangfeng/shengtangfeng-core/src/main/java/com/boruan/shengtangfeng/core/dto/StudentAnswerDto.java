package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class StudentAnswerDto extends TailBean {

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "用户答案")
	private String userAnswer;

	@ApiModelProperty(value = "答题人ID")
	private Long userId;

	@ApiModelProperty(value = "答题人姓名")
	private String userName;

	@ApiModelProperty(value = "答题人状态")
	protected Boolean userStatus;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	protected Date createTime;

	@ApiModelProperty(value = "答题类型0选择题1主观题2线下题")
	private Integer answerType;

	@ApiModelProperty(value = "状态 0待审核 1通过 2拒绝")
	private Integer status ;
}
