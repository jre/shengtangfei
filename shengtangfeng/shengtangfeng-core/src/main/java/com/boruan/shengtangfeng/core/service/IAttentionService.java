package com.boruan.shengtangfeng.core.service;

import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.Attention;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.MyAttentionUser;

import java.util.List;

/**
 * @author KongXH
 * @title: IAttentionService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1414:06
 */
public interface IAttentionService {

    public GlobalReponse<Boolean> saveAttention(Attention attention,String invitationCode);


    public Attention  findByFocusId(Long userId,Long focusUserId);

    public  void updateAttention(Attention attention);

    public  void deleteAttention(Long userId,Long focusUserId);
    public void deleteFans(Long userId, Long fansId);
    public  Long templateCount(Attention attention);
    /**
     * 分页获取我关注的所有人
     * @param pageQuery
     */
    public  void pageMyAttention(PageQuery<MyAttentionUser> pageQuery,Long userId);
    /**
     * 分页获取我的粉丝
     * @param pageQuery
     */
    public  void pageMyFans(PageQuery<MyAttentionUser> pageQuery,Long userId);

    /**
     * 获取我关注的所有人
     * @return
     */
    List<Attention> findMyAttention(Long userId );

    void saveOrCaleStick(Long userId, Long fansId);
}
