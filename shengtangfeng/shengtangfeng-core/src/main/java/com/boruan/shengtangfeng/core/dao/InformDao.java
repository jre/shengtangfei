package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.dto.InformDto;
import com.boruan.shengtangfeng.core.entity.Inform;
import com.boruan.shengtangfeng.core.entity.Question;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface InformDao extends BaseMapper<Inform> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Inform> query);

    /**
     * 根据班级id查询班级通知
     * @param pageQuery
     */
    void getClassInformById(PageQuery<Inform> pageQuery);

    /**
     * 查看我发布的通知
     * @param pageQuery
     */
    void getMyInform(PageQuery<Inform> pageQuery);

    /**
     * 后台分页查询通知
     * @param query
     */
    void getInformPage(PageQuery<Inform> query);
}