package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.SystemUserDto;
import com.boruan.shengtangfeng.core.entity.SystemUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author:Jessica
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Mapper
public interface SystemUserDtoMapper extends DtoMapper<SystemUserDto, SystemUser> {
	SystemUserDtoMapper MAPPER = Mappers.getMapper(SystemUserDtoMapper.class);
}
