package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.GroupMembers;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IGroupMembersDao extends BaseMapper<GroupMembers> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<GroupMembers> query);

    /**
     * 获取全部群成员
     * @return
     */
    List<GroupMembers> getAllGroupMembers(Long groupId,String keyword);
}