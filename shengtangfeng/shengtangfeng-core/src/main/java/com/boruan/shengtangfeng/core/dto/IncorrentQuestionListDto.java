package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IncorrentQuestionListDto extends BaseDto {

    @ApiModelProperty(value = "学科id")
    private Long subjectId;

    @ApiModelProperty(value = "筛选类型：0自定义；1本周；2本月；3本学期")
    private Integer dateType;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "查询关键词")
    private String keywords;

    @ApiModelProperty(value = "页码")
    private Integer pageNo=1;

    @ApiModelProperty(value = "每页数量")
    private Integer pageSize=10;

    @ApiModelProperty(value = "状态0待审核1通过2拒绝")
    private Integer status;

    @ApiModelProperty(value = "user")
    private String user;
}
