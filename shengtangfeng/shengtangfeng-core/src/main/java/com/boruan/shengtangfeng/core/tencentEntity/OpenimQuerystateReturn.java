package com.boruan.shengtangfeng.core.tencentEntity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OpenimQuerystateReturn {

    /**
     *返回的群成员 UserID
     */
    @JSONField(name = "To_Account")
    private String To_Account;

    /**
     *Result
     */
    @JSONField(name = "Status")
    private String Status;

}
