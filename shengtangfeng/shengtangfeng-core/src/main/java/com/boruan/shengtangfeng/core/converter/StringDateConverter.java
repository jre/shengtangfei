package com.boruan.shengtangfeng.core.converter;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;


@Configuration
public class StringDateConverter implements Converter<String, Date> {

	@Override
	public Date convert(String source) {
		if (source==null) {
			return null;
		}
		Date date=null;
		try {
			date=DateUtils.parseDate(source, "yyyy-MM-dd");
			return date;
		} catch (Exception e) {
		}
		try {
			date=DateUtils.parseDate(source, "yyyy-MM-dd HH:mm:ss");
			return date;
		} catch (Exception e) {
		}
		try {
			date=DateUtils.parseDate(source, "yyyyMMdd");
			return date;
		} catch (Exception e) {
		}
		try {
			date=DateUtils.parseDate(source, "yyyyMMddhhmmss");
			return date;
		} catch (Exception e) {
		}
		return date;
	}
}
