package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="user_category")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCategory extends IdEntity {
    
            
            
    /**
    * 文章类别id 
    **/
    private Long categoryId ;

            
    /**
    * 用户id 
    **/
    private Long userId;
            
    /**
    * 分类类别1文章2视频 3讲一讲
    **/
    private Integer categoryType ;

}