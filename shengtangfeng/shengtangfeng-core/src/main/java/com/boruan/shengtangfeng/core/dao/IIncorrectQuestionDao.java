package com.boruan.shengtangfeng.core.dao;
import java.util.List;

import com.boruan.shengtangfeng.core.entity.IncorrectQuestion;
import com.boruan.shengtangfeng.core.entity.Question;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/*
* 
* gen by beetlsql mapper 2018-12-13
*/
public interface IIncorrectQuestionDao extends BaseMapper<IncorrectQuestion> {
    
	public void pageIncorrect(PageQuery<Question> pageQuery);
	public List<Question> getIncorrect(@Param("userId") Long userId,@Param("type") Integer type);
	public Integer getIncorrectCount(@Param("userId") Long userId,@Param("type") Integer type);
	public Integer deleteIncorrect(@Param("userId") Long userId,@Param("questionId") Long questionId);
	public Integer removeIncorrectQuestionType(Integer[] types,Long userId);
	public void selfUpQuestion(PageQuery<IncorrectQuestion> pageQuery);
	public void auditSelfQuestion(PageQuery<IncorrectQuestion> pageQuery);
}
