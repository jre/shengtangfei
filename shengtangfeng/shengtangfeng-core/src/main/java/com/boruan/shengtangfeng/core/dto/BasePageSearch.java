package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 分页检索基础类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BasePageSearch{

    /**
     * 页码
     **/
    @ApiModelProperty("分页页码")
    private long pageNo=1;
    /**
     * 页码
     **/
    @ApiModelProperty("单页条数")
    private long pageSize=20;

    /**
     * 用户Id
     **/
    @ApiModelProperty("用户ID")
    private Long userId;

}
