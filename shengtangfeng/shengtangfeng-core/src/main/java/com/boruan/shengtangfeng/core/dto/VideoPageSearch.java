package com.boruan.shengtangfeng.core.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class VideoPageSearch extends BasePageSearch {

    /**
     * 分类ID，关注分类传0
     */
    @ApiModelProperty("分类ID，关注分类传0")
    private Long categoryId;
    /**
     * 检索词，用于搜索功能，非检索功能可不传
     */
    @ApiModelProperty("检索词，用于搜索功能，非检索功能可不传")
    private String keyword;

    @ApiModelProperty("省市拼接")
    private String address;
    /**
     *  经度
     */
    @ApiModelProperty(value="经度")
    private String longitude;
    /**
     *  纬度
     */
    @ApiModelProperty(value=" 纬度")
    private String latitude;

    @ApiModelProperty("视频类别：0国学1讲一讲2小视频")
    private Integer type;
}
