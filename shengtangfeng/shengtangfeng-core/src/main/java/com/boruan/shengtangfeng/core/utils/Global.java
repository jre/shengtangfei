package com.boruan.shengtangfeng.core.utils;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Video;
import com.google.common.collect.Maps;

public class Global {

	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;
	/**
	 * 定义默认分页数量
	 */
	public static final int PAGE_SIZE = 20;
	// 是否启用权限验证
	public static final boolean RIGHT_ENABLE = true;
	public static boolean LOG_THREAD_RUN = false;
	public static boolean VIDEO_THREAD_RUN = false;
	public static long LOG_THREAD_SLEEP_TIME = 10000;
	public static long VIDEO_THREAD_SLEEP_TIME = 10000;
	public static int SAVE_COUNT = 1000;
	public static int VIDEO_SAVE_COUNT = 1000;
	public static Queue<Log> logQueue = new LinkedList<Log>();
	public static Queue<Video> videoQueue = new LinkedList<Video>();
	private static Map<String, String> sortTypes = Maps.newLinkedHashMap();
	static {
		sortTypes.put("auto", "自动");
	}
}
