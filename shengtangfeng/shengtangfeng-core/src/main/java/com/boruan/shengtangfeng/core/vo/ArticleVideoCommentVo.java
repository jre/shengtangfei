package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 文章视频评论的集合
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ArticleVideoCommentVo extends BaseVo {

	/**
	 * 文章ID
	 */
	@ApiModelProperty("文章或者视频ID")
	private Long objectId;
	/**
	 * 发布人ID
	 */
	@ApiModelProperty("发布人ID")
	private Long userId;
	/**
	 * 发布人头像
	 */
	@ApiModelProperty("发布人头像")
	private String userIcon;
	/**
	 * 发布人姓名
	 */
	@ApiModelProperty("发布人姓名")
	private String userName;
	/**
	 * 内容
	 */
	@ApiModelProperty("内容")
	private String content;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("状态 0未审核 1已审核 2审核失败")
	private Integer status;
	/**
	 * 类型 1文章 2视频
	 */
	@ApiModelProperty("类型 0 试题 1 文章 2视频")
	private Integer type;
	
	/**
     * 类型  1文字，2文字+图片，3语音，4语音+图片，5图片
     */
	@ApiModelProperty("类型  1文字，2文字+图片，3语音，4语音+图片，5图片")
    private Integer commentType;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("type 为1时此字段有值")
	private ArticleVo article;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("type 为2时此字段有值")
	private VideoVo video;
	/**
	 * 状态 0未审核 1已审核
	 */
	@ApiModelProperty("type 为0时此字段有值")
	private QuestionVo question;
	
	/**
     * 语音时间，秒数
     */
    @ApiModelProperty("语音时间，秒数")
    private Integer voiceTime;
    
    /**
     * 语音地址
     */
    @ApiModelProperty("语音地址")
    private String voice;
    /**
     * 逗号分隔的图片
     */
    @ApiModelProperty("逗号分隔的图片")
    private String images;
	
}
