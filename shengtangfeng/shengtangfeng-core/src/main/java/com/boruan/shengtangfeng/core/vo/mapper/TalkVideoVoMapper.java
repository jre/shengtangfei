package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.vo.TalkVideoVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TalkVideoVoMapper extends VoMapper<TalkVideoVo, Video> {
    TalkVideoVoMapper MAPPER = Mappers.getMapper(TalkVideoVoMapper.class);
}
