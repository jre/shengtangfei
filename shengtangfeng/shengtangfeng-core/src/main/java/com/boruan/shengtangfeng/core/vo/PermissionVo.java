package com.boruan.shengtangfeng.core.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PermissionVo {

    private String title;

    private String icon;

    private String index;

    private List<PermissionVo> subs;

}
