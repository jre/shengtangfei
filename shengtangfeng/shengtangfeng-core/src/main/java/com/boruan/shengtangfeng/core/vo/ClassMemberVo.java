package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClassMemberVo extends BaseVo {

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "头像")
    private String headImage;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "是否是好友")
    private Boolean isFriend;

    @ApiModelProperty(value = "首字母")
    private String initials;

    @ApiModelProperty(value = "类型 1学生 2老师 3创建人")
    private Integer type ;
}
