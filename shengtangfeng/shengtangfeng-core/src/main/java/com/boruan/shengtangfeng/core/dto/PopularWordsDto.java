package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: PopularWordsDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 关键字
 * @date 2020/8/1317:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="关键字")
public class PopularWordsDto {

    @ApiModelProperty("关键字 ")
    private String keyword;

    @ApiModelProperty("关键字类型1试题2文3视频 ")
    private String keywordType;
}
