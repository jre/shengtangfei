package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateVideoGradeVo {

    @ApiModelProperty("讲一讲点评id")
    private Long videoGradeId;

    @ApiModelProperty("status 1通过 2拒绝")
    private Integer status;

    @ApiModelProperty("理由")
    private String remark;
}
