package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: ArticleComment
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 评论
 * @date 2020/8/1416:38
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="视频评论")
public class VideoCommentDto{



    /**
     * 视频id
     */
    @ApiModelProperty(value="视频id")
    private Long videoId;

    /**
     * 内容
     */
    @ApiModelProperty(value="内容")
    private String content;

    /**
     * 评论id
     */
    @ApiModelProperty(value="评论id（二级评论时候传）")
    private Long commentId;
}
