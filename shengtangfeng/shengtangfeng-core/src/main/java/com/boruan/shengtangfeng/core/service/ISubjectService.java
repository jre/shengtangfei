package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Subject;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

/**
 * 
 * @author 刘光强
 * @date 2020年8月12日16:08:21
 */
public interface ISubjectService {

	/**
	 * 保存
	 * @param subject
	 * @return
	 */
	public void save(Subject subject);
	/**
	 * 通过ID查询
	 * @return
	 */
	public Subject findById(Long id);
	/**
	 * 根据ID获取
	 * @return
	 */
	public Subject get(Long id);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param subject
	 */
	public void pageQuery(PageQuery<Subject> pageQuery,Subject subject);
	
	/**
     * 获取学科列表
     * @return
     */
    public List<Subject> getList(Subject subject);
    /**
     * 根据年级ID获取学科列表
     * @return
     */
    public List<Subject> getListByGradeId(Long gradeId);
    /**
     * 根据年级ID获取学科列表
     * @return
     */
    public List<Subject> getListByGradeIds(Long[] gradeIds);
    
    /**
     * 获取有过错题的学科列表
     * @return
     */
    public List<Subject> getIncorrectSubject(Long userId,int type);
    /**
     * 获取有过收藏的学科列表
     * @return
     */
    public List<Subject> getFavoritesSubject(Long userId);
    
    /**
     * 添加学科关联的年级
     * @return
     */
    public void addGrade(Long subjectId,Long[] gradeIds);
    /**
     * 删除学科关联的年级
     * @return
     */
    public void deleteGrade(Long subjectId,Long gradeId);
    
}
