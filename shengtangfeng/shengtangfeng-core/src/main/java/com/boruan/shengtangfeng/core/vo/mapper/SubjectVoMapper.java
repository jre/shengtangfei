package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.vo.SubjectVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface SubjectVoMapper extends VoMapper<SubjectVo, Subject>  {
	SubjectVoMapper MAPPER = Mappers.getMapper(SubjectVoMapper.class);
}