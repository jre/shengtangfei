package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="小视频")
public class SmallVideoDto {

	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="分类ID")
	private Long categoryId;

	/**
	 * @好友
	 */
	@ApiModelProperty(value=" @好友")
	private String friendIds;

	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="ID，编辑更新时传")
	private Long id;
	/**
	 * 视频标题
	 */
	@ApiModelProperty(value="视频标题")
	private String title;

	/**
	 * 视频地址
	 */
	@ApiModelProperty(value="视频地址")
	private String content;

	@ApiModelProperty(value = "发布范围：0公开1好友可见2班级可见3仅自己可见")
	private Integer scope;

	@ApiModelProperty(value = "可见班级id逗号拼接，班级可见时必填")
	private String classes;

	@ApiModelProperty(value = "发布地址")
	private String Address;

	@ApiModelProperty(value="经度")
	private String longitude;

	@ApiModelProperty(value=" 纬度")
	private String latitude;

	@ApiModelProperty(value = "高德城市编码")
	private String cityCode;

	@ApiModelProperty(value = "封面图")
	private String Image;

	@ApiModelProperty(value = "音乐Id")
	private Long musicId;

}
