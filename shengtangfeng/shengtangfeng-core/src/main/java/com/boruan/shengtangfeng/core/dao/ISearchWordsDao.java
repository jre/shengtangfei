package com.boruan.shengtangfeng.core.dao;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.SearchWords;


public interface ISearchWordsDao extends BaseMapper<SearchWords> {


    public List<SearchWords> getList(Long userId,Integer type);
}