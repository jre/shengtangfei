package com.boruan.shengtangfeng.core.enums;

import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PassedStatus implements CommonEnum {
    MINGCI(0, "名词n."),
    DONGCI(1, "动词v."),
    XINGRONGCI(2, "形容词adj."),
	FUCI(3, "副词adv.");
    
	@ApiModelProperty(value="0名词n. 1动词v. 2形容词adj. 3副词adv.")
    private Integer value;
    private String name;

    private PassedStatus(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

