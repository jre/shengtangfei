package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.dao.IVideoGradeDao;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.entity.VideoGrade;
import com.boruan.shengtangfeng.core.service.IVideoGradeService;
import com.boruan.shengtangfeng.core.utils.CommentUtil;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.mapper.ArticleCommentVoMapper;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class VideoGradeService implements IVideoGradeService {

    @Autowired
    private IVideoGradeDao videoGradeDao;
    @Autowired
    private IUserDao userDao;

    @Override
    public VideoGrade findUserGrade(Long userId, Long videoId) {
        VideoGrade videoGrade = videoGradeDao.findUserGrade(userId, videoId);
        return videoGrade;
    }

    @Override
    public List<BigDecimal> getScorePercent(Long videoId) {
        Long allCount = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).count();
        Long score1 = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).andBetween(VideoGrade::getTotalScore, 1, 2).count();
        Long score2 = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).andBetween(VideoGrade::getTotalScore, 3, 4).count();
        Long score3 = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).andBetween(VideoGrade::getTotalScore, 5, 6).count();
        Long score4 = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).andBetween(VideoGrade::getTotalScore, 7, 8).count();
        Long score5 = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getIsDeleted, false).andBetween(VideoGrade::getTotalScore, 9, 10).count();

        if (allCount != null && allCount != 0) {
            BigDecimal percent1 = BigDecimal.valueOf(score1 / allCount).setScale(2);
            BigDecimal percent2 = BigDecimal.valueOf(score2 / allCount).setScale(2);
            BigDecimal percent3 = BigDecimal.valueOf(score3 / allCount).setScale(2);
            BigDecimal percent4 = BigDecimal.valueOf(score4 / allCount).setScale(2);
            BigDecimal percent5 = BigDecimal.valueOf(score5 / allCount).setScale(2);
            List<BigDecimal> percent = Arrays.asList(percent1, percent2, percent3, percent4, percent5);
            return percent;
        }
        return null;
    }

    @Override
    public List<VideoGrade> getTeacherGrade(Long videoId) {
        List<VideoGrade> list = videoGradeDao.createLambdaQuery().andEq(VideoGrade::getVideoId, videoId).andEq(VideoGrade::getType, 2).andEq(VideoGrade::getIsDeleted, false).select();
        for (VideoGrade videoGrade : list) {
            User user = userDao.unique(videoGrade.getUserId());
            videoGrade.set("user",user);
        }
        return list;
    }

    @Override
    public void getStudentGrade(PageQuery<VideoGrade> pageQuery) {
        videoGradeDao.pageQuery(pageQuery);
    }

    @Override
    @Transactional(readOnly = false)
    public GlobalReponse save(VideoGrade videoGrade) {
        Integer check= CommentUtil.checkComment(videoGrade.getRemark());
        if(check==2) {
            videoGrade.setStatus(1);
            videoGradeDao.insertTemplate(videoGrade);
            return GlobalReponse.success("评价成功");
        }else if(check==1){
            videoGrade.setStatus(0);
            videoGradeDao.insertTemplate(videoGrade);
            GlobalReponse.fail("您的评论包含敏感词，需要等待客服审核");
        } else if(check==0){
            return GlobalReponse.fail("您的评论包含敏感词禁止评论");
        }
        return GlobalReponse.fail("评价失败");
    }
}
