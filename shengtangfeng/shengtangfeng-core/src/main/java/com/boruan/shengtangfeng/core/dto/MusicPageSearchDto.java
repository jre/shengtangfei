package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MusicPageSearchDto extends BasePageSearch  {

    @ApiModelProperty("后台参数含义：时间:1今天2昨天3本周4本月5上月6自定义 APP传值含义我的收藏传0，其他传1")
    private Long type;
    /**
     * 检索词，用于搜索功能，非检索功能可不传
     */
    @ApiModelProperty("检索词，用于搜索功能，非检索功能可不传")
    private String keyword;


    @ApiModelProperty(value = "歌手ID")
    private Long singerId;

    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "是否热门0否1是")
    private Integer isHot;
}
