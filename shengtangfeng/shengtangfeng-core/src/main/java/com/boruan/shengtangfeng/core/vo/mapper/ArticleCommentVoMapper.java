package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.ArticleComment;
import com.boruan.shengtangfeng.core.vo.ArticleCommentVo;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface ArticleCommentVoMapper extends VoMapper<ArticleCommentVo,  ArticleComment>  {
	ArticleCommentVoMapper MAPPER = Mappers.getMapper(ArticleCommentVoMapper.class);
}