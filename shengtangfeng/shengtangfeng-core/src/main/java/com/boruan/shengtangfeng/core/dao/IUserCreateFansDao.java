package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;


public interface IUserCreateFansDao extends BaseMapper<UserCreateFans> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<UserCreateFans> query);
}