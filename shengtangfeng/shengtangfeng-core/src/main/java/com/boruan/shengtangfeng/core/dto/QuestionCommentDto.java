package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.boruan.shengtangfeng.core.utils.QuestionOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionCommentDto{

	/**
	 * 试题ID
	 **/
	@ApiModelProperty("试题ID")
	private Long questionId;

	/**
	 * 内容
	 **/
	@ApiModelProperty("内容")
	private String content;
	/**
	 * 逗号分隔的图片地址
	 **/
	@ApiModelProperty("逗号分隔的图片地址")
	private String images;
	/**
	 * 音频地址
	 **/
	@ApiModelProperty("音频地址")
	private String voice;
	
	/**
     * 语音时间，秒数
     */
    @ApiModelProperty("语音时间，秒数")
    private Integer voiceTime;

}
