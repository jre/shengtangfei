package com.boruan.shengtangfeng.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.oss.OSSClient;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.boruan.shengtangfeng.core.utils.Consts;

/**
 * Created by macro on 2018/5/17.
 */
@Configuration
public class OssConfig {
    @Value("${aliyun.oss.publicEndpoint}")
    private String PUBLIC_ALIYUN_OSS_ENDPOINT;
    @Value("${aliyun.oss.privateEndpoint}")
    private String PRIVATE_ALIYUN_OSS_ENDPOINT;
    @Value("${aliyun.oss.accessKeyId}")
    private String ALIYUN_OSS_ACCESSKEYID;
    @Value("${aliyun.oss.accessKeySecret}")
    private String ALIYUN_OSS_ACCESSKEYSECRET;
    
    @Bean(name="privateOssClient")
    public OSSClient privateOssClient(){
        return new OSSClient(PRIVATE_ALIYUN_OSS_ENDPOINT,ALIYUN_OSS_ACCESSKEYID,ALIYUN_OSS_ACCESSKEYSECRET);
    }
    @Bean(name="publicOssClient")
    public OSSClient publicOssClient(){
        return new OSSClient(PUBLIC_ALIYUN_OSS_ENDPOINT,ALIYUN_OSS_ACCESSKEYID,ALIYUN_OSS_ACCESSKEYSECRET);
    }
    @Bean(name="acsClient")
    public DefaultAcsClient acsClient(){
        // 创建一个 Aliyun Acs Client, 用于发起 OpenAPI 请求
        IClientProfile profile = DefaultProfile.getProfile(Consts.REGION_CN_HANGZHOU, ALIYUN_OSS_ACCESSKEYID, ALIYUN_OSS_ACCESSKEYSECRET);
        return new DefaultAcsClient(profile);
    }
    
}
