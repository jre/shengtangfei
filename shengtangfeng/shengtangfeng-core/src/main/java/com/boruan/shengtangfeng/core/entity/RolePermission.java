package com.boruan.shengtangfeng.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role_permission")
@EqualsAndHashCode(callSuper = false)
public class RolePermission {

    private Long roleId;

    private Long permissionId;

}
