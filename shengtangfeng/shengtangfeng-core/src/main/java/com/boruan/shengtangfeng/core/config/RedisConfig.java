package com.boruan.shengtangfeng.core.config;
import com.boruan.shengtangfeng.core.redis.utils.RedisAtomicClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.boruan.shengtangfeng.core.redis.utils.RedisObjectSerializer;


/**
 * redis 配置
 */
@Configuration
public class RedisConfig {

    
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new RedisObjectSerializer());
        return template;
    }
    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory factory) {
    	StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(factory);
        stringRedisTemplate.afterPropertiesSet();
    	return stringRedisTemplate;
    }
    @Bean
    public RedisAtomicClient redisAtomicClient() {
    	return new RedisAtomicClient();
    }
}