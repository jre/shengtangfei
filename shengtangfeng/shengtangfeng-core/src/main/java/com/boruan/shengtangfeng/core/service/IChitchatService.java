package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.dto.GroupDataDto;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.GroupChatDetailVo;
import com.boruan.shengtangfeng.core.vo.UserRelevanceVo;

import java.util.ArrayList;

/**
 @author: guojiang
 @Description:
 @date:2021年3月31日 下午14:08
 */
public interface IChitchatService {

    /**
     * 获取好友列表
     * @param userId
     * @return
     */
    GlobalReponse<UserRelevanceVo> getList(Long userId,Integer type, String keyword);


    /**
     * 加入或移出黑名单 type 0移出 1加入
     * @param userId
     * @param friendId
     * @param type
     * @return
     */
    GlobalReponse addOrRemoveBlack(Long userId, Long friendId, Integer type);

    /**
     * 删除好友
     * @param userId
     * @param friendId
     * @return
     */
    GlobalReponse deletedFriend(Long userId, Long friendId);

    /**
     * 根据id或名称搜索好友或群聊 type 0好友 1群聊
     * @param parameter
     * @param type
     * @return
     */
    GlobalReponse searchFriendOrGroup(Long userId,String parameter, Integer type);

    /**
     * 添加好友或申请加群 type 0好友 1群聊
     * @param userId
     * @param id
     * @param type
     * @return
     */
    GlobalReponse addFriendOrGroup(Long userId, Long id, String remark,Integer type);

    /**
     * 创建群聊
     * @param ids
     * @return
     */
    GlobalReponse createGroup(Long userId,String ids);

    /**
     * 查看群详情
     * @param userId
     * @param groupId
     * @return
     */
    GlobalReponse<GroupChatDetailVo> getGroupChatDetail(Long userId, Long groupId);

    /**
     * 修改昵称
     * @param userId
     * @param groupId
     * @param nickName
     * @return
     */
    GlobalReponse updateNickName(Long userId, Long groupId, String nickName);

    /**
     * 编辑群资料
     * @param userId
     * @param groupDataDto
     * @return
     */
    GlobalReponse updateGroupData(Long userId, Long groupId,GroupDataDto groupDataDto);

    /**
     * 获取群成员列表
     * @param groupId
     * @return
     */
    GlobalReponse getAllGroupMembers(Long userId,Long groupId,String keyword);

    /**
     * 批量删除群成员
     * @param userId
     * @param ids
     * @return
     */
    GlobalReponse deletedGroupMembers(Long userId,Long groupId,String ids);

    /**
     * 获取为加入群的好友列表
     * @param userId
     * @param groupId
     * @return
     */
    GlobalReponse<ArrayList<User>> getFriendList(Long userId, Long groupId,String keyword);

    /**
     * 批量邀请成员
     * @param userId
     * @param groupId
     * @param idArr
     * @return
     */
    GlobalReponse addGroupMembers(Long userId, Long groupId, Long[] idArr);

    /**
     * 设置群管理员
     * @param userId
     * @param groupId
     * @param addIds
     * @param deleteIds
     * @return
     */
    GlobalReponse setGroupAdministrator(Long userId, Long groupId, String addIds, String deleteIds);

    /**
     * 设置加群方式
     * @param userId
     * @param groupId
     * @param type
     * @return
     */
    GlobalReponse updateAddGroupWay(Long userId, Long groupId, Integer type);

    /**
     * 转让群
     * @param userId
     * @param groupId
     * @param memberId
     * @return
     */
    GlobalReponse transferGroup(Long userId, Long groupId, Long memberId);

    /**
     * 解散群
     * @param userId
     * @param groupId
     * @return
     */
    GlobalReponse dissolveGroup(Long userId, Long groupId);

    /**
     * 退出群聊
     * @param userId
     * @param groupId
     * @return
     */
    GlobalReponse quitGroup(Long userId, Long groupId);

}
