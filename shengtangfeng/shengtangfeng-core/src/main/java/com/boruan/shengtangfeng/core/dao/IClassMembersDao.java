package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.ClassMembers;
import com.boruan.shengtangfeng.core.entity.User;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IClassMembersDao extends BaseMapper<ClassMembers> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<ClassMembers> query);

    public List<ClassMembers> getTeachers(Long classId);

    public List<ClassMembers> getStudents(Long classId);
}