package com.boruan.shengtangfeng.core.entity;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import org.beetl.sql.core.annotatoin.Table;

import com.boruan.shengtangfeng.core.enums.QuestionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 题库
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question")
public class Question extends IdEntity {

	private static final long serialVersionUID = -4470773369611955781L;
	/**
	 * 类型
	 **/
	@ApiModelProperty("类型")
	private QuestionType type;

	/**
	 * 答案
	 **/
	@ApiModelProperty("答案")
	private String answer;

	/**
	 * 学科ID
	 **/
	@ApiModelProperty("学科id")
	private Long subjectId;


	/**
	 * 选项 里面放的是选项集合的JSON串
	 **/
	@ApiModelProperty("选项 里面放的是选项集合的JSON串")
	private String content;

	/**
	 * 文字解释
	 **/
	@ApiModelProperty("文字解释")
	private String textExplain;

	/**
	 * 题干
	 **/
	@ApiModelProperty("题干")
	private String title;

	/**
	 * 图片
	 **/
	@ApiModelProperty("图片")
	private String images;

	/**
	 * 正确率
	 **/
	@ApiModelProperty("正确率")
	private BigDecimal accurate;
	/**
	 * 分值
	 */
	@ApiModelProperty("分值")
	private BigDecimal score;

	/**
	 * 题库类型 0淘学题库 1作业题库
	 */
	@ApiModelProperty("题库类型 0淘学题库 1作业题库")
	private Integer warehouseType;

	/**
	 * 是否加入共享题库 0否 1是
	 */
	@ApiModelProperty("是否加入共享题库 0否 1是")
	private Integer shareType;

	/**
	 * 状态 0待审核 1审核通过 2审核拒绝
	 */
	@ApiModelProperty("状态 0待审核 1审核通过 2审核拒绝")
	private Integer status;

	/**
	 * 发布人类型
	 */
	@ApiModelProperty("0后台发布  1教师发布")
	private Integer pushStatus;

}
