package com.boruan.shengtangfeng.core.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.dto.FeedbackDto;
import com.boruan.shengtangfeng.core.dto.QuestionCommentDto;
import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.entity.QuestionComment;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Mapper
public interface QuestionCommentDtoMapper extends DtoMapper<QuestionCommentDto, QuestionComment> {
    QuestionCommentDtoMapper MAPPER = Mappers.getMapper(QuestionCommentDtoMapper.class);
}