package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.VideoCategory;
import com.boruan.shengtangfeng.core.vo.VideoCategoryVo;

/**
@author: lihaicheng
@Description:ArticleCategoryVo
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface VideoCategoryVoMapper extends VoMapper< VideoCategoryVo,  VideoCategory>  {
	VideoCategoryVoMapper MAPPER = Mappers.getMapper(VideoCategoryVoMapper.class);
}