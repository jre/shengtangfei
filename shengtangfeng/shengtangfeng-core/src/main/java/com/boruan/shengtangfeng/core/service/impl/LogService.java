package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.ILogDao;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.utils.Global;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.utils.WriteLogThread;
/**
 * 
 * @author lixuwei
 * @date 2016年10月31日 上午8:22:35
 */
@Service
@Transactional(readOnly = true)
public class LogService implements ILogService{

	@Autowired
	private ILogDao logDao;
	
	/**
	 * 只保存到序列中 
	 */
	@Override
	@Transactional(readOnly = false)
	public boolean save(Log log) {
		if (!Global.LOG_THREAD_RUN) {
			System.out.println("日志线程未启动，start");
			WriteLogThread writeLogThread=new WriteLogThread(logDao);
			writeLogThread.start();
			Global.LOG_THREAD_RUN=true;
		}
		return Global.logQueue.offer(log);
	}
	
	
	
	@Override
	@Transactional(readOnly = false)
	public void saveImmediately(Log log) {
		if (log.getId()==null) {
			KeyHolder kh=logDao.insertReturnKey(log);
			log.setId(kh.getLong());
		}else{
			logDao.updateById(log);
		}
	}



	@Override
	public void pageQuery(PageQuery<Log> pageQuery, Log log) {
		pageQuery.setParas(log);
		logDao.pageQuery(pageQuery);
	}

}
