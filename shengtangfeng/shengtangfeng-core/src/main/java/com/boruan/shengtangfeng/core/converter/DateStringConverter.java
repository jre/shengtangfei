package com.boruan.shengtangfeng.core.converter;

import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;


@Configuration
public class DateStringConverter implements Converter<Date, String> {

	@Override
	public String convert(Date source) {
		if (source==null) {
			return null;
		}
		return DateFormatUtils.format(source, "yyyy-MM-dd HH:mm:ss");
	}
}
