package com.boruan.shengtangfeng.core.service.impl;

import java.util.Date;
import java.util.List;

import com.boruan.shengtangfeng.core.dao.IAccoladeDao;
import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IVideoCommentDao;
import com.boruan.shengtangfeng.core.entity.VideoComment;
import com.boruan.shengtangfeng.core.service.IVideoCommentService;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class VideoCommentService implements IVideoCommentService {
    @Autowired
    private IVideoCommentDao videoCommentDao;
    @Autowired
    private IAccoladeDao accoladeDao;

    /**
     * @author: 刘光强
     * @Description: 分页获取
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    public void pageQuery(PageQuery<VideoComment> pageQuery, VideoComment video) {
        pageQuery.setParas(video);
        videoCommentDao.pageQuery(pageQuery);
    }

    /**
     * @author: 刘光强
     * @Description: 详情
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    public VideoComment findById(Long id) {
        return videoCommentDao.unique(id);
    }

    /**
     * @author: 刘光强
     * @Description: 添加修改
     * @date:2020年3月10日 上午11:14:10
     */
    @Override
    @Transactional(readOnly = false)
    public void save(VideoComment videoComment) {
        if (videoComment.getId() == null) {
            KeyHolder kh = videoCommentDao.insertReturnKey(videoComment);
            videoComment.setId(kh.getLong());
        } else {
            videoCommentDao.updateById(videoComment);
        }
    }

    @Override
    public List<VideoComment> findByVideoId(Long userId, Long videoId, Integer status) {
        List<VideoComment> mine = videoCommentDao.findByVideoId(userId, videoId, status);
        if (userId != null) {
            List<VideoComment> notMine = videoCommentDao.findByVideoIdNotMine(userId, videoId, status);
            mine.addAll(notMine);
        }
        return mine;
    }

    @Override
    public List<VideoComment> template(VideoComment search) {
        return videoCommentDao.template(search);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteComment(Long id, Long userId) {

        VideoComment po = new VideoComment();
        po.setId(id);
        po.setIsDeleted(true);
        po.setUpdateBy(userId.toString());
        po.setUpdateTime(new Date());
        videoCommentDao.updateTemplateById(po);
    }

    @Override
    public long getTodayCount(Long userId) {
        return videoCommentDao.getTodayCount(userId);
    }

    @Override
    @Transactional(readOnly = false)
    public GlobalReponse saveAccolade(Accolade accolade) {
        try {
            Accolade accolades = accoladeDao.getAccolade(accolade.getUserId(), accolade.getArticleId(), accolade.getType());
            if (null == accolades) {
                KeyHolder kh = accoladeDao.insertReturnKey(accolade);
                if (accolade.getType() == 3){
                    VideoComment videoComment = videoCommentDao.single(accolade.getArticleId());
                    videoComment.setThumpUpNum(videoComment.getThumpUpNum() + 1);
                    videoCommentDao.updateById(videoComment);
                }
                return GlobalReponse.success(true).setMessage("点赞成功");
            } else {
                accoladeDao.deleteAccolade(accolade.getUserId(), accolade.getArticleId(), accolade.getType());
                //视频点赞需要减点赞数
                if (accolade.getType() == 3) {
                    VideoComment videoComment = videoCommentDao.single(accolade.getArticleId());
                    videoComment.setThumpUpNum(videoComment.getThumpUpNum() - 1);
                    videoCommentDao.updateById(videoComment);
                }
                return GlobalReponse.success(false).setMessage("取消点赞成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return GlobalReponse.fail();
        }
    }
}
