package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="school")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class School extends IdEntity {


    /**
     * 省
     **/
    @ApiModelProperty("省")
    private String province ;

    /**
    * 市
    **/
    @ApiModelProperty("市")
    private String city ;
            
    /**
    * 区
    **/
    @ApiModelProperty("区")
    private String county;
    /**
    * 学校名称 
    **/
    @ApiModelProperty("学校名称")
    private String name ;

    /**
     * 创建人类型 0为用户 1为后台管理员
     */
    @ApiModelProperty("创建人类型 0为用户 1为后台管理员")
    private Integer type;

}