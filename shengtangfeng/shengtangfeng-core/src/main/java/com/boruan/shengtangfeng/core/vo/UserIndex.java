package com.boruan.shengtangfeng.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.boruan.shengtangfeng.core.enums.Sex;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserIndex{

    @ApiModelProperty("用户ID")
    private Long id;
    @ApiModelProperty("性别 1男 2女")
    private Sex sex;

    @ApiModelProperty("头像")
    private String headImage;

    @ApiModelProperty("我的Id")
    private String account;

    @ApiModelProperty("是否显示定位 0否1是")
    private Integer showLocation;

    @ApiModelProperty("家长手机号")
    private String parentMobile;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("名字")
    private String name;

    @ApiModelProperty("用户类型")
    private Integer userType;//用户类型
    
    @ApiModelProperty("邀请码")
    private String invitationCode;
    
    @ApiModelProperty("密码是否设置")
    private Boolean passwordSet;
    
    @ApiModelProperty("获赞数")
    private Integer likeNum;
    /**
     * 积分
     **/
    @ApiModelProperty("积分")
    private Integer integral;
    /**
     * 发布的文章和视频数量
     **/
    @ApiModelProperty("发布的文章和视频数量")
    private Long publishCount;
    /**
     * 关注的人数量
     **/
    @ApiModelProperty("关注的人数量")
    private Long followCount;
    /**
     * 粉丝数量
     **/
    @ApiModelProperty("粉丝数量")
    private Long fansCount;
    /**
     * 生日
     */
    @ApiModelProperty("生日")
    private String birthDay;
    /**
     * 省
     */
    @ApiModelProperty("省")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty("市")
    private String city;
//    /**
//     * 客服H5地址
//     **/
//    @ApiModelProperty("客服H5地址")
//    private String customerServiceUrl;
//    /**
//     * 客服工作时间
//     **/
//    @ApiModelProperty("客服工作时间")
//    private String customerServiceTime;
//    /**
//     * 未读消息数
//     **/
//    @ApiModelProperty("未读消息数")
//    private Long noticeCount;
    
    @ApiModelProperty("苹果上架标记（true已上架 false 上架中）")
    private String online;
    
    @ApiModelProperty("分享二维码地址")
    private String qrcode;

    @ApiModelProperty("腾讯userSig")
    private String userSig ;


}

