package com.boruan.shengtangfeng.core.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NoticeVo extends BaseVo {

	/**
	    * 分类  1 平台通知  2 用户通知
	    **/
	    
    @ApiModelProperty("分类  1 平台通知  2 用户通知")
    private Integer category;
    /**
     * 类型，0：,平台通知  1：文章审核通知 2： 视频审核通知 403:用户被锁下线 404 用户互踢下线
     **/
    @ApiModelProperty("类型，0：,平台通知  1：文章审核通知 2： 视频审核通知 403:用户被锁下线 404 用户互踢下线")
    private Integer type;
    /**
     * 对象ID，根据type判断对应
     **/
    @ApiModelProperty("对象ID，根据type判断对应")
    private Long objectId;

    /**
     * 标题
     **/
    @ApiModelProperty("标题")
    private String title;
    /**
     * 内容
     **/
    @ApiModelProperty("内容")
    private String content;

    
    /**
     * 用户ID
     **/
    @ApiModelProperty("用户ID")
    private Long userId;
    
    /**
     * 是否已读
     **/
    @ApiModelProperty("是否已读")
    private Boolean readed;
    
    public Boolean getReaded() {
        return this.readed;
    }
}
