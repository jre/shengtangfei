package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.annotatoin.*;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.vo.FavoritesCount;


public interface IFavoritesDao extends BaseMapper<Favorites> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Favorites> query);

    public Favorites getFavorites(Long userId,Long articleId,Integer type);

    /**
     * 获取收藏的文章数量
     * @param userId
     * @return
     */
    public Long getFavoritesArticleCount(Long userId);
    /**
     * 获取收藏的视频数量
     * @param userId
     * @return
     */
    public Long getFavoritesVideoCount(Long userId);

    public void deleteFavorites(Long userId,Long articleId,Integer type);


}