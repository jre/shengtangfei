package com.boruan.shengtangfeng.core.enums;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.beetl.sql.core.annotatoin.EnumMapping;

@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BannerType implements CommonEnum {

    TEXT(0, "加载富文本"),
    LINK(1, "跳转链接"),
    ;

    @ApiModelProperty(value = "0 富文本 1 连接")
    private Integer value;
    private String name;

    private BannerType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
