package com.boruan.shengtangfeng.core.service;

import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.ModuleRecord;
import com.boruan.shengtangfeng.core.vo.ModuleRankVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IModuleRecordService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param moduleRecord
	 */
	public void pageQuery(PageQuery<ModuleRecord> pageQuery, ModuleRecord moduleRecord);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public ModuleRecord findById(Long id);

	/**
	 * 保存
	 * @param  moduleRecord
	 */
	public void save(ModuleRecord moduleRecord);
	/**
	 * 根据条件查询单个对象
	 * @param  moduleRecord
	 */
	public ModuleRecord templateOne(ModuleRecord moduleRecord);
	/**
	 * 查询符合条件的数量
	 * @param  moduleRecord
	 */
	public Long templateCount(ModuleRecord moduleRecord);
	/**
	 * 获取某个模块的排名数据
	 * @param  moduleRecord
	 */
	public ModuleRankVo getRank(Long moduleId,Long userId);
	/**
	 * 获取总的排名数据
	 * @param  moduleRecord
	 */
	public ModuleRankVo getRankAll(Long userId);

}
