package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.TailBean;

import java.util.Date;


@Data
public class InformPushDto {

    @ApiModelProperty("班级id数组")
    protected Long[] classIds;

    @ApiModelProperty("通知内容")
    private String content ;

    @ApiModelProperty("通知图片")
    private String images ;


}