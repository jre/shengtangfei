package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


@Table(name="group_members")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GroupMembers extends IdEntity {
    

    /**
    * 类型 0普通成员 1管理员 2群主
    **/
    private Integer type ;
    /**
    * 群聊id 
    **/
    private Long groupChatId ;

    /**
    * 成员id 
    **/
    private Long userId ;
    /**
     * 成员昵称
     **/
    private String nickName ;

}