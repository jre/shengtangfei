package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="black_list")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BlackList extends IdEntity {
    
            

    /**
    * 被拉黑人id 
    **/
    private Long blackUserId ;
            
            
    /**
    * 用户id 
    **/
    private Long userId ;


}