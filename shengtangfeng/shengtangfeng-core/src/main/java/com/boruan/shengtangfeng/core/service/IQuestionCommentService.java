package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.QuestionComment;
import com.boruan.shengtangfeng.core.vo.QuestionCommentVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
public interface IQuestionCommentService {

	/**
	  * 分页查询
	 * @param pageQuery
	 * @param question
	 */
	public void pageQuery(PageQuery<QuestionComment> pageQuery, QuestionComment questionComment);
	/**
	 * 分页查询不是我的评论
	 * @param pageQuery
	 * @param question
	 */
	public void pageQueryNotMine(PageQuery<QuestionComment> pageQuery, QuestionComment questionComment);
	/**
	 * 根据id获取
	 * @param id
	 * @return
	 */
	public QuestionComment findById(Long id);
	/**
	 * 保存分类
	 * @param  questionComment
	 */
	public void save(QuestionComment questionComment);
	
	/**
	  * 根据问题ID获取回复列表
	 * @param questionId
	 */
	public List<QuestionComment> findByQuestionId(Long questionId,Integer status,Integer type);
	/**
	 * 根据条件查找
	 * @param  search
	 */
	public List<QuestionComment> template(QuestionComment search);

	/**
	 * 审核端根据题目id查看全部评论
	 * @param questionId
	 * @return
	 */
    GlobalReponse<QuestionCommentVo> getQuestionDetail(Long questionId);

	/**
	 * 审核端根据试题评论id删除评论
	 * @param commentId
	 * @param adminId
	 */
	GlobalReponse deleteComment(Long commentId, Long adminId);
}
