package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

/**
 * 章节模块视频
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="module_video")
public class ModuleVideo extends IdEntity{

    /**
     * 章节模块id
     */
    private Long moduleId;

    /**
     * 视频地址
     */
    private String video;

    /**
     * 排序
     */
    private Long sort;


    /**
     * 视频标题
     */
    private String title;

    /**
     * 视频的缩略图
     */
    private String thumb;

}
