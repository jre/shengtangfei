package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.ClassMembers;
import com.boruan.shengtangfeng.core.vo.ClassMemberVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClassMemberVoMapper extends VoMapper<ClassMemberVo, ClassMembers> {
    ClassMemberVoMapper MAPPER = Mappers.getMapper(ClassMemberVoMapper.class);
}
