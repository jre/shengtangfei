package com.boruan.shengtangfeng.core.dao;

import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.NoticeUser;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */
public interface INoticeUserDao extends BaseMapper<NoticeUser>{
    public void insertAllUser(Long noticeId);
    public void setRead(Long noticeId, Long userId);

    void deletedNotice(Long id, Long userId);
}
