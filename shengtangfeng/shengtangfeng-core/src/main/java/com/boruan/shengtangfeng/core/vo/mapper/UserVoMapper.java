package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.vo.UserVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface UserVoMapper extends VoMapper<UserVo, User>  {
	UserVoMapper MAPPER = Mappers.getMapper(UserVoMapper.class);
	 @Mappings({
	        @Mapping(target = "sex", ignore = true)
	    })
	 public UserVo toVo(User entity);
	 @Mappings({
	        @Mapping(target = "sex", ignore = true)
	    })
	 public User toEntity(UserVo vo);
}