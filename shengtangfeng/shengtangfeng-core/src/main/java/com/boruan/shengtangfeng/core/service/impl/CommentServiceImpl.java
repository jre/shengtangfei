package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.*;
import com.boruan.shengtangfeng.core.dto.ToAuditCommentDTO;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.service.ICommentService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.CommentVo;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author: 郭江
 * @Description: 用户评论
 * @date:2021/2/25
 */
@Service
@Transactional(readOnly = false)
public class CommentServiceImpl implements ICommentService {

    @Autowired
    private IArticleCommentDao articleCommentDao;
    @Autowired
    private IVideoCommentDao videoCommentDao;
    @Autowired
    private IQuestionCommentDao questionCommentDao;
    @Autowired
    private IArticleDao iArticleDao;
    @Autowired
    private IVideoDao iVideoDao;
    @Autowired
    private IQuestionDao iQuestionDao;

    /**
     * 多条件查询评论 type评论类型 0文章 1视频 2试题
     * @return
     */
    @Override
    public GlobalReponse<PageQuery<CommentVo>> getComment(ToAuditCommentDTO toAuditCommentDTO) {
        PageQuery<CommentVo> pageQuery = new PageQuery<>();
        Integer pageNo = toAuditCommentDTO.getPageNo();
        Integer pageSize = toAuditCommentDTO.getPageSize();
        if (pageNo==null){
            pageNo=1;
        }
        if (pageSize==null){
            pageSize=10;
        }
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);

        if (toAuditCommentDTO.getContent()!=null){
            pageQuery.setPara("content", toAuditCommentDTO.getContent());
        }
        if (toAuditCommentDTO.getType() == null) {
            questionCommentDao.pageArticleVideoQuestionAuditComment(pageQuery);
            return GlobalReponse.success(pageQuery);
        } else if (toAuditCommentDTO.getType() != null) {
            if (toAuditCommentDTO.getType() == 0) {
                articleCommentDao.pageGetComment(pageQuery);
                return GlobalReponse.success(pageQuery);
            } else if (toAuditCommentDTO.getType() == 1) {
                videoCommentDao.pageGetComment(pageQuery);
                return GlobalReponse.success(pageQuery);
            } else if (toAuditCommentDTO.getType() == 2) {
                questionCommentDao.pageGetComment(pageQuery);
                return GlobalReponse.success(pageQuery);
            }
        }
        return GlobalReponse.success("查询失败");
    }

    /**
     * @param userId
     * @param commentId
     * @param type
     * @return
     */
    @Override
    public GlobalReponse deleteComment(Long userId, Long commentId, Integer type) {
        if (type == 0) {
            ArticleComment single = articleCommentDao.createLambdaQuery().andEq(ArticleComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setIsDeleted(true);
            articleCommentDao.updateTemplateById(single);
            return GlobalReponse.success("修改成功");
        } else if (type == 1) {
            VideoComment single = videoCommentDao.createLambdaQuery().andEq(VideoComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setIsDeleted(true);
            videoCommentDao.updateTemplateById(single);
            return GlobalReponse.success("修改成功");
        } else if (type == 2) {
            QuestionComment single = questionCommentDao.createLambdaQuery().andEq(QuestionComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setIsDeleted(true);
            questionCommentDao.updateTemplateById(single);
            return GlobalReponse.success("修改成功");
        }
        return GlobalReponse.fail("修改失败");
    }

    /**
     * 根据id和评论类型修改评论 type 0文章 1视频 2试题
     *
     * @param userId
     * @param commentId
     * @param type
     * @param status
     * @return
     */
    @Override
    public GlobalReponse updateComment(Long userId, Long commentId, Integer type, Integer status,String remark) {
        if (type==0) {
            ArticleComment single = articleCommentDao.createLambdaQuery().andEq(ArticleComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setStatus(status);
            articleCommentDao.updateTemplateById(single);
            if (status==1){
                Article article = iArticleDao.createLambdaQuery().andEq(Article::getId, single.getArticleId()).single();
                article.setCommentNum(article.getCommentNum()+1);
            }
            return GlobalReponse.success("修改成功");
        } else if (type==1) {
            VideoComment single = videoCommentDao.createLambdaQuery().andEq(VideoComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setStatus(status);
            videoCommentDao.updateTemplateById(single);
            if (status==1){
                Video video = iVideoDao.createLambdaQuery().andEq(Video::getId, single.getVideoId()).single();
                video.setCommentNum(video.getCommentNum()+1);
            }
            return GlobalReponse.success("修改成功");
        } else if (type==2) {
            QuestionComment single = questionCommentDao.createLambdaQuery().andEq(QuestionComment::getId, commentId).single();
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            single.setStatus(status);
            questionCommentDao.updateTemplateById(single);
            return GlobalReponse.success("修改成功");
        }
        return GlobalReponse.fail("修改失败");
    }

    /**
     * 根据用户id查看用户的评论
     *
     * @param pageIndex
     * @param pageSize
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse<PageQuery<CommentVo>> getUserComment(int pageIndex, int pageSize, Long userId,String keywords) {
        PageQuery<CommentVo> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageIndex);
        pageQuery.setPageSize(pageSize);
        pageQuery.setPara("userId", userId);
        pageQuery.setPara("keywords",keywords);
        questionCommentDao.getUserComment(pageQuery);
        return GlobalReponse.success(pageQuery);
    }
}


