package com.boruan.shengtangfeng.core.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IAttentionDao;
import com.boruan.shengtangfeng.core.dao.IBlackListDao;
import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.entity.Attention;
import com.boruan.shengtangfeng.core.entity.BlackList;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.service.IAttentionService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import com.boruan.shengtangfeng.core.vo.MyAttentionUser;

/**
 * @author KongXH
 * @title: AttentionService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1414:06
 */

@Service
@Transactional(readOnly = true)
public class AttentionService implements IAttentionService {


    @Autowired
    private IAttentionDao attentionDao;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IBlackListDao blackListDao;

    /**
     * @description: 关注保存
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 14:07
     */
    @Override
    @Transactional(readOnly = false)
    public GlobalReponse<Boolean> saveAttention(Attention attention,String invitationCode) {
        //校验是不是被拉黑了
        BlackList search=new BlackList();
        search.setBlackUserId(attention.getUserId());
        search.setUserId(attention.getFocusUserId());
        long count=blackListDao.templateCount(search);
        if(count>0) {
            return GlobalReponse.fail("您无权关注对方",false);
        }
        Attention attentions = attentionDao.findByFocusId(attention.getUserId(),attention.getFocusUserId());
        //之前没关注过
        if (null ==attentions ) {
            
            User user=userDao.single(attention.getFocusUserId());
            if(StringUtils.isNotBlank(user.getInvitationCode())&&!StringUtils.equals(invitationCode, user.getInvitationCode())) {
                return GlobalReponse.fail("邀请码错误");
            }
            attention.setIsStick(false);
            KeyHolder  aa  =attentionDao.insertReturnKey(attention);
            attention.setId(aa.getLong());
            Attention attention1 = attentionDao.findByFocusId(attention.getFocusUserId(),attention.getUserId());
            if (null != attention1) {
                attention.setIsAttention(true);
                attentionDao.updateAttention(attention);
                attention1.setIsAttention(true);
                attentionDao.updateAttention(attention1);
            }
            return GlobalReponse.success(true).setMessage("关注成功");
        }else{
            Attention attention1 = attentionDao.findByFocusId(attention.getFocusUserId(),attention.getUserId());
            if (null !=attention1) {
                attention1.setIsAttention(false);
                attentionDao.updateAttention(attention1);
            }
            attentionDao.deleteAttention(attention.getUserId(),attention.getFocusUserId());
            return GlobalReponse.success(false).setMessage("取消关注成功");
        }

    }

    /**
     * @description: 根据对方id查找
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 14:49
     */
    @Override
    public Attention findByFocusId(Long userId,Long focusUserId) {
        return attentionDao.findByFocusId(userId,focusUserId);
    }


    /**
     * @description: 根据id更新状态
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 15:19
     */
    @Override
    @Transactional(readOnly = false)
    public void updateAttention(Attention attention) {
        attentionDao.updateAttention(attention);
    }


    /**
     * @description: 删除
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/18 16:25
     */
    @Override
    @Transactional
    public void deleteAttention(Long userId, Long focusUserId) {

        attentionDao.deleteAttention(userId,focusUserId);
    }
    /**
     * @description: 删除
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/18 16:25
     */
    @Override
    @Transactional
    public void deleteFans(Long userId, Long fansId) {
        //删除双方的关注信息
        attentionDao.deleteAttention(userId,fansId);
        attentionDao.deleteAttention(fansId,userId);
        //添加到 黑名单
        BlackList search=new BlackList();
        search.setBlackUserId(fansId);
        search.setUserId(userId);
        search.setCreateBy(userId+"");
        search.setCreateTime(new Date());
        blackListDao.insert(search);
    }

    @Override
    public Long templateCount(Attention attention) {
        return attentionDao.templateCount(attention);
    }

    @Override
    public void pageMyAttention(PageQuery<MyAttentionUser> pageQuery,Long userId) {
        pageQuery.setPara("userId", userId);
        attentionDao.pageMyAttention(pageQuery);
    }
    @Override
    public void pageMyFans(PageQuery<MyAttentionUser> pageQuery,Long userId) {
        pageQuery.setPara("userId", userId);
        attentionDao.pageMyFans(pageQuery);
    }

    /**
     * 获取我关注的所有人
     *
     * @return
     */
    @Override
    public List<Attention> findMyAttention(Long userId ) {
        List<Attention> attentionList = attentionDao.createLambdaQuery().andEq(Attention::getUserId, userId).andEq(Attention::getIsDeleted, false).select();
        return attentionList;
    }

    @Transactional(readOnly = false)
    @Override
    public void saveOrCaleStick(Long userId, Long fansId) {
        Attention attention = attentionDao.findByFocusId(userId, fansId);
        if (attention!=null){
            if(attention.getIsStick()==null){
                attention.setIsStick(true);
            }else{
                if (attention.getIsStick()){
                    attention.setIsStick(false);
                }else {
                    attention.setIsStick(true);
                }
            }
            attention.setUpdateTime(new Date());
            attention.setUpdateBy(userId.toString());
            attentionDao.updateTemplateById(attention);
        }
    }
}
