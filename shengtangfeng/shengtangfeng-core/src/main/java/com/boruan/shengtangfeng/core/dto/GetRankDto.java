package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 教师端排行榜的参数
 */
@Data
public class GetRankDto {
    @ApiModelProperty("班级id")
    private Long classId;

    @ApiModelProperty("作业ID")
    private Long jobId ;

    @ApiModelProperty("排行榜类型：1分数2速度3积极度")
    private Integer type;

    @ApiModelProperty("页码")
    private Integer pageNo;
}
