package com.boruan.shengtangfeng.core.dto.mapper;

import com.boruan.shengtangfeng.core.dto.ArticleCategoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.ArticleCategory;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface ArticleCategoryDtoMapper extends DtoMapper<ArticleCategoryDto, ArticleCategory> {
	ArticleCategoryDtoMapper MAPPER = Mappers.getMapper(ArticleCategoryDtoMapper.class);
}
