package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 模块做题记录
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "module_record")
//@EqualsAndHashCode(callSuper = true)
public class ModuleRecord extends IdEntity{

	private static final long serialVersionUID = -4470222369622255781L;

	/**
	 * 用户ID
	 **/
	private Long userId;
	/**
	 * 用户头像
	 **/
	private String userIcon;
	/**
	 * 用户名
	 **/
	private String userName;
	/**
	 * 父模块ID
	 **/
	private Long parentModuleId;
	/**
	 * 模块ID
	 **/
	private Long moduleId;
	
	/**
	 * 耗费时间,秒数
	 **/
	private Integer time;

	/**
	 * 得分
	 */
	private Integer score;
	/**
	 * 积分
	 */
	private Integer integral;
}
