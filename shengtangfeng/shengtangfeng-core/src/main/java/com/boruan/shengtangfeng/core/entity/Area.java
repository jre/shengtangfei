package com.boruan.shengtangfeng.core.entity;
import lombok.*;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AutoID;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import org.beetl.sql.core.annotatoin.Table;



@Table(name="area")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Area extends IdEntity {

    /**
    * 类型 1 省 2市 3区 4街道 
    **/
    private Integer level ;
            
    /**
    * 地区编号 
    **/
    private String adcode ;

    /**
    * 描述 
    **/
    private String description ;
            
    /**
    * 配置key 
    **/
    private String name ;
            
    /**
    * 父ID 默认为0 
    **/
    private Long parentId ;

}