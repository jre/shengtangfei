package com.boruan.shengtangfeng.core.tencentEntity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateGroupParam {

    /**
     *群主 ID，自动添加到群成员中。如果不填，群没有群主
     * 选填
     */
    @JSONField(name = "Owner_Account")
    private String Owner_Account;

    /**
     *群组形态，包括 Public（陌生人社交群），Private（即 Work，好友工作群），ChatRoom（即 Meeting，会议群），AVChatRoom（直播群）
     * 必填
     */
    @JSONField(name = "Type")
    private String Type;

    /**
     * 为了使得群组 ID 更加简单，便于记忆传播，腾讯云支持 App 在通过 REST API 创建群组时 自定义群组 ID
     * 选填
     */
    @JSONField(name = "GroupId")
    private String GroupId;

    /**
     *群名称，最长30字节，使用 UTF-8 编码，1个汉字占3个字节
     * 选填
     */
    @JSONField(name = "Name")
    private String Name;

    /**
     *申请加群处理方式。包含 FreeAccess（自由加入），NeedPermission（需要验证），DisableApply（禁止加群），不填默认为 NeedPermission（需要验证）
     * 仅当创建支持申请加群的 群组 时，该字段有效
     * 选填
     */
    @JSONField(name = "ApplyJoinOption")
    private String ApplyJoinOption;

    /**
     *  群头像 URL（选填）
     * */
    @JSONField(name = "FaceUrl")
    private String FaceUrl;

}
