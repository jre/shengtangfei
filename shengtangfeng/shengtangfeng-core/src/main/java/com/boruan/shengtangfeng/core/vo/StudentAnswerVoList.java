package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class StudentAnswerVoList {

    @ApiModelProperty(value = "学生作业记录Id")
    private Long userJobId ;

    @ApiModelProperty(value = "教师头像")
    private String teacherHeadImage;

    @ApiModelProperty(value = "学生姓名")
    private String name;

    @ApiModelProperty(value = "学生头像")
    private String headImage;

    @ApiModelProperty(value = "老师评语")
    private String comment ;

    @ApiModelProperty(value = "成绩")
    private BigDecimal score ;

    @ApiModelProperty(value = "用时 毫秒")
    private Long time ;

    @ApiModelProperty(value = "题目")
    private List<StudentAnswerVo> list ;

}
