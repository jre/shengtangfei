package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 学校展开
 */
@Data
public class SchoolToGradeDto {

    @ApiModelProperty("年级")
    private String schoolYear ;

    @ApiModelProperty("班级数")
    private Integer classNumber ;

    @ApiModelProperty("状态")
    private Integer status ;
}
