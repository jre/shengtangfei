package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class LoginResult {
    @ApiModelProperty("用户登录的token，只有status为1才返回")
    private String  token;
    @ApiModelProperty("API用户，前端使用")
    private UserVo  user;
    @ApiModelProperty("管理用户，后台使用")
    private SystemUserVo  admin;
    @ApiModelProperty("登录结果 1:成功  2:微信未绑定手机")
    private Integer  status;
    @ApiModelProperty("登录结果为2时返回该用户的openId，用于绑定手机号")
    private String  openId;
    
}
