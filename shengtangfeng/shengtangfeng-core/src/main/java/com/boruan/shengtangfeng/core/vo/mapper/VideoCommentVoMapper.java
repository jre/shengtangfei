package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.VideoComment;
import com.boruan.shengtangfeng.core.vo.VideoCommentVo;

/**
@author: lihaicheng
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface VideoCommentVoMapper extends VoMapper<VideoCommentVo,  VideoComment>  {
	VideoCommentVoMapper MAPPER = Mappers.getMapper(VideoCommentVoMapper.class);
}