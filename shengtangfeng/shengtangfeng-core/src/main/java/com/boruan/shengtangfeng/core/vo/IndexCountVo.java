package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 首页数量vo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IndexCountVo{

	/**
	 * 错题数量
	 */
	@ApiModelProperty("错题数量")
	private int incorrectCount;
	/**
	 * 收藏数量
	 */
	@ApiModelProperty("收藏数量")
	private int favoritesCount;
	
}
