package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author KongXH
 * @title: UserCategoryDto
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1314:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserCategoryDto{

    /**
     * type 0视频 1讲一讲 2小视频
     **/
    @ApiModelProperty("type 0视频 1讲一讲 2小视频")
    private Integer type;

    /**
     * 存放类别id
     **/
    @ApiModelProperty("存放类别id,保存时用逗号隔开所有ID")
    private String categoryId;

}
