package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 @author: guojiang
 @Description:
 @date:2021年5月15日 下午14:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NewClassMessageVo {

    @ApiModelProperty("id，记录的ID")
    private Long id;

    @ApiModelProperty("0我申请加入其它老师创建的班级 1其他老师和学生申请加入我创建的班级 2自己被移出班级 4补交作业")
    private Integer apply;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户头像")
    private String userImage;

    @ApiModelProperty("用户昵称")
    private String userNickname;

    @ApiModelProperty("用户类型")
    private Integer userType;

    @ApiModelProperty("用户手机号")
    private String userMobile;

    @ApiModelProperty("班级Id")
    private Long classId;

    @ApiModelProperty("班级名称")
    private String className;

    @ApiModelProperty("班级头像")
    private List classImage;

    @ApiModelProperty("状态 0等待验证 1同意 2拒绝 ")
    private Integer status ;

    @ApiModelProperty("申请备注 ")
    private String applyRemark ;

    @ApiModelProperty("处理人Id")
    private Long handlerId;

    @ApiModelProperty("处理人头像")
    private String handlerImage  ;

    @ApiModelProperty("处理人昵称 ")
    private String handlerNickname ;

    @ApiModelProperty("处理人手机号")
    private String handlerMobile;

    @ApiModelProperty("处理人类型")
    private Integer handlerType;

    @ApiModelProperty("作业ID ")
    private Long jobId ;

    @ApiModelProperty("作业名称 ")
    private String jobName ;

    @ApiModelProperty("作业学科")
    private String jobSubject;

    @ApiModelProperty("作业截止时间时间戳")
    private Long deadlineTime ;

    @ApiModelProperty("是否超过截止时间")
    private Boolean isDeadline ;
}
