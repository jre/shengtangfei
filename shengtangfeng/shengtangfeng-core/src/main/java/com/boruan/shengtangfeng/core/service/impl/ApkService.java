package com.boruan.shengtangfeng.core.service.impl;

import org.beetl.sql.core.db.KeyHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IApkDao;
import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.service.IApkService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author liuguangqiang
 * @date 2016年8月10日 上午9:36:16
 */
@Slf4j
@Service
@Transactional(readOnly = true)
public class ApkService implements IApkService {
	@Autowired
	private IApkDao apkDao;
	@Override
    @Transactional(readOnly = false)
    public Long save(Apk apk) {
        KeyHolder kh = null;
        if (apk.getId() == null) {
            apk.setId(kh.getLong());
            return kh.getLong();
        } else {
            apkDao.updateById(apk);
            return apk.getId();
        }
    }
	@Override
    public GlobalReponse<Apk> getApk() {
        Apk apk = apkDao.all().get(0);
        return GlobalReponse.success(apk);
    }

}