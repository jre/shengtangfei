package com.boruan.shengtangfeng.core.entity;
import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Table(name="grade_subject")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GradeSubject{
    
            
            
    /**
    * 年级名ID
    **/
    private Long gradeId ;
    /**
     * 学科ID
     **/
    private Long subjectId ;
            

}