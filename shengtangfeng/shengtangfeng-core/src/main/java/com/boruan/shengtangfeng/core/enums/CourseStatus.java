package com.boruan.shengtangfeng.core.enums;

import org.beetl.sql.core.annotatoin.EnumMapping;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CourseStatus implements CommonEnum {
    WEISHANGJIA(0, "未上架"), YISHANGJIA(1, "已上架");

    private Integer value;
    private String name;

    private CourseStatus(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}