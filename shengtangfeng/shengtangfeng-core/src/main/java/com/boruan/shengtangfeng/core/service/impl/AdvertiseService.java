package com.boruan.shengtangfeng.core.service.impl;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.IAdvertiseDao;
import com.boruan.shengtangfeng.core.entity.Advertise;
import com.boruan.shengtangfeng.core.service.IAdvertiseService;

/**
 * @author: 刘光强
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Service
@Transactional(readOnly = true)
public class AdvertiseService implements IAdvertiseService {
	@Autowired
	private IAdvertiseDao advertiseDao;
	/**
	 * @author: 刘光强
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public void pageQuery(PageQuery<Advertise> pageQuery, Advertise advertise) {
		pageQuery.setParas(advertise);
		advertiseDao.pageQuery(pageQuery);
	}

	/**
	 * @author: 刘光强
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	public Advertise findById(Long id) {
		return advertiseDao.unique(id);
	}

	/**
	 * @author: 刘光强
	 * @Description: 添加修改
	 * @date:2020年3月10日 上午11:14:10
	 */
	@Override
	@Transactional(readOnly = false)
	public void save(Advertise advertise) {
		if (advertise.getId() == null) {
			KeyHolder kh = advertiseDao.insertReturnKey(advertise);
			advertise.setId(kh.getLong());
		} else {
			advertiseDao.updateById(advertise);
		}
	}

	@Override
	public List<Advertise> findByTemplate(Advertise advertise) {
		return advertiseDao.template(advertise);
	}


	@Override
	public List<Advertise> getList(Integer status,Integer position) {
	    Advertise search=new Advertise();
	    search.setStatus(status);
	    search.setPosition(position);
	    search.setIsDeleted(false);
	    return advertiseDao.template(search);
	}
	// ========= 后台 =======

    @Override
	@Transactional(readOnly = false)
	public void delAdvertise(String ids) {
		Advertise po = new Advertise();
		po.setIsDeleted(true);
		for (String id : ids.split(",")) {
			po.setId(Long.parseLong(id));
			advertiseDao.updateTemplateById(po);
		}
	}
    
}
