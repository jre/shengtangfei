package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ModuleRankDetailVo {
    
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;
    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String userIcon;
    /**
     * 用户得分
     */
    @ApiModelProperty("用户得分")
    private String userScore;
    /**
     * 用户排名
     */
    @ApiModelProperty("用户排名")
    private Integer userRank;
}
