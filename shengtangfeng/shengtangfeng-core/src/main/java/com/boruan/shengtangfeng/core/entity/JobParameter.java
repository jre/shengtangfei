package com.boruan.shengtangfeng.core.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;

/**
 * 作业参数
 */
@Table(name="job_parameter")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class JobParameter extends IdEntity {

    /**
    * 最低正确率
    **/
    private BigDecimal accuracy ;

    /**
     * 截止时间迟于提交时间多少分钟
     **/
    private String minute ;

    /**
     * 题目分数范围
     */
    private Integer score ;
}