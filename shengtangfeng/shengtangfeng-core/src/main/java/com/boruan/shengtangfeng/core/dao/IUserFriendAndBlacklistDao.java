package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.UserFriendAndBlacklist;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IUserFriendAndBlacklistDao extends BaseMapper<UserFriendAndBlacklist> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<UserFriendAndBlacklist> query);

    public UserFriendAndBlacklist isFriend(Long userId,Long friendUser);

    UserFriendAndBlacklist isFriend1(Long userId, Long friendUser);

    /**
     * 获取好友列表
     * */
    List<UserFriendAndBlacklist> getList(Long userId, Integer type, String keyword);

    /**
     * 获取我的好友列表
     * @param userId
     * @param keyword
     * @return
     */
    List<UserFriendAndBlacklist> getFriendList(Long userId, String keyword);
}