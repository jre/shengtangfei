package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.JobParameter;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

public interface IJobParameterDao extends BaseMapper<JobParameter> {

    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<JobParameter> query);
}
