package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.Music;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IMusicDao extends BaseMapper<Music> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<Music> query);

    public void pageCollection(PageQuery<Music> query);

    void pageQuery1(PageQuery<Music> pageQuery);
}