package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.enums.QuestionType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 作业下的题目列表
 */
@Data
public class JobQuestionDto {

    @ApiModelProperty("序号")
    private Long serialNumber ;

    @ApiModelProperty("题目Id")
    private Long id ;

    @ApiModelProperty("题目分数")
    private BigDecimal score ;

    @ApiModelProperty("题目类型")
    private QuestionType type;

    @ApiModelProperty("题库类型 1淘学题库 2共享题库")
    private Integer bankType;

    @ApiModelProperty("正确率")
    private BigDecimal accuracy ;

    @ApiModelProperty("题干")
    private String title;

    @ApiModelProperty("内容")
    private String content;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("是否批改")
    private Boolean isCheck;

}
