package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IPopularWordsDao;
import com.boruan.shengtangfeng.core.dto.PopularWordsDto;
import com.boruan.shengtangfeng.core.entity.PopularWords;
import com.boruan.shengtangfeng.core.service.IPopularWordsService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author KongXH
 * @title: PopularWordsService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1317:07
 */
@Service
@Transactional(readOnly = true)
public class PopularWordsService implements IPopularWordsService {

    @Autowired
    private IPopularWordsDao popularWordsDao;


    @Override
    public void pageQuery(PageQuery<PopularWords> query) {

    }

    /**
     * @description: 热门词查询
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 17:08
     */
    @Override
    public List<PopularWords> getList(Integer type) {
        return popularWordsDao.getList(type);
    }
}
