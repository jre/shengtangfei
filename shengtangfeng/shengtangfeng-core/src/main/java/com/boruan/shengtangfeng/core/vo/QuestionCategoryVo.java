package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QuestionCategoryVo {

    private Long id;

    @ApiModelProperty("保留字段（名称）")
    private String name ;

    @ApiModelProperty("排序")
    private Integer sort ;

}
