package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.entity.IdEntity;
import com.boruan.shengtangfeng.core.enums.QuestionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.List;

/**
 * 教师端添加题目
 */
@Data
public class AddQuestionDto{
	@ApiModelProperty(value="id")
	private Long id;

	@ApiModelProperty(value="题目类型 0选择题 1主观题 2线下题")
	private Integer type;

	@ApiModelProperty(value="学科Id")
	private Long subjectId;

	@ApiModelProperty(value="年级Id")
	private Long gradeId;

	@ApiModelProperty(value="题干")
	private String title;

	@ApiModelProperty(value="图片")
	private String images;

	@ApiModelProperty(value="选项内容")
	private List<ContentDto> contentDtos;

	@ApiModelProperty(value="分值")
	private BigDecimal score;

	@ApiModelProperty(value="答案内容")
	private String answer;

	@ApiModelProperty(value="解析")
	private String textExplain;

	@ApiModelProperty(value="是否加入共享题库 0否 1是")
	private Integer shareType;

}
