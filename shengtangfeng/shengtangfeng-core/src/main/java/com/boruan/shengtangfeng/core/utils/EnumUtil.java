package com.boruan.shengtangfeng.core.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 枚举工具类
 */
public class EnumUtil {

	/**
	 * 返回指定值的'枚举' @param value @return SharedObjTypeEnum @throws
	 */
	public static <T extends CommonEnum> T getEnumByValue(Class<T> clazz, Integer value) {
		if (value == null)
			return null;
		for (T _enum : clazz.getEnumConstants()) {
			if (value.equals(_enum.getValue())) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 返回指定值的'枚举' @param value @return SharedObjTypeEnum @throws
	 */
	public static <T extends CommonEnum> T getEnumByName(Class<T> clazz, String name) {
		for (T _enum : clazz.getEnumConstants()) {
			if (StringUtils.equals(name, _enum.getName()))
				;
			{
				return _enum;
			}
		}
		return null;
	}

}
