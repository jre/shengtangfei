package com.boruan.shengtangfeng.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public abstract class BaseDto {

	@ApiModelProperty(value = "id")
	protected Long id;

	@ApiModelProperty(value = "createBy，用于检索，前端不用传")
	protected String createBy;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
	protected Date createTime;

	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
