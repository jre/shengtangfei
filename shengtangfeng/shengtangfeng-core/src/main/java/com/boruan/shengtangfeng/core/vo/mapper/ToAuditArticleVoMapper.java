package com.boruan.shengtangfeng.core.vo.mapper;

import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.vo.ToAuditArticleVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 @author: guojiang
 @Description:
 @date:2021/2/25
 */
@Mapper
public interface ToAuditArticleVoMapper extends VoMapper<ToAuditArticleVO, Article> {

    ToAuditArticleVoMapper MAPPER = Mappers.getMapper(ToAuditArticleVoMapper.class);
}
