package com.boruan.shengtangfeng.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

/**
 * 小视频@好友
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video_friend")
@EqualsAndHashCode(callSuper = false)
public class VideoFriend extends IdEntity{

    /**
     * 视频id
     **/
    private Long videoId ;

    /**
     * 好友id
     **/
    private Long friendId ;

    /**
     * 是否已读
     **/
    private Integer isRead ;
}
