package com.boruan.shengtangfeng.core.utils;


public class TencentCloudData {

    public static final String friend_add = "sns/friend_add";//添加好友
    public static final String friend_delete = "sns/friend_delete";//删除好友
    public static final String black_list_add = "sns/black_list_add";//添加黑名单
    public static final String black_list_delete = "sns/black_list_delete";//删除黑名单

    public static final String create_group = "group_open_http_svc/create_group";//创建群聊
    public static final String add_group_member = "group_open_http_svc/add_group_member";//增加群成员
    public static final String delete_group_member = "group_open_http_svc/delete_group_member";//删除群成员
    public static final String modify_group_member_info = "group_open_http_svc/modify_group_member_info";//修改群成员资料
    public static final String destroy_group = "group_open_http_svc/destroy_group";//解散群
    public static final String change_group_owner = "group_open_http_svc/change_group_owner";//转让群主
    public static final String update_group_way = "group_open_http_svc/modify_group_base_info";//修改加群方式或者群资料
    public static final String openim_querystate = "/openim/querystate";//查询账号在线状态
    public static final String get_role_in_group = "group_open_http_svc/get_role_in_group";//查询用户在群组中的身份

}

