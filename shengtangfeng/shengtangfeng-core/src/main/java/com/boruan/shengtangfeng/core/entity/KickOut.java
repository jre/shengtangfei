package com.boruan.shengtangfeng.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.annotatoin.Table;

/**
 * 踢出群聊或者班级表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="kick_out")
public class KickOut extends IdEntity{

    @ApiModelProperty("用户Id")
    private Long userId ;

    @ApiModelProperty("关联Id")
    private Long relevanceId ;

    @ApiModelProperty("1踢出群聊 2踢出班级")
    private Integer type ;

    @ApiModelProperty("是否已读")
    private Integer isRead ;
}
