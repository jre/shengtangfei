package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.entity.VideoCategory;
import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 * @author KongXH
 * @title: IVideoCategoryService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1315:31
 */
public interface IVideoCategoryService {

    public void pageQuery(PageQuery<VideoCategory> query);

    public List<VideoCategory> getList(VideoCategory videoCategory);

    public List<VideoCategory> getVideoCategoryDefault(Integer type);

}
