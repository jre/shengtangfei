package com.boruan.shengtangfeng.core.dto.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.dto.ModuleRecordDto;
import com.boruan.shengtangfeng.core.entity.ModuleRecord;

/**
* @author:twy
* @Description:
*/
@Mapper
public interface ModuleRecordDtoMapper extends DtoMapper<ModuleRecordDto, ModuleRecord>{

	ModuleRecordDtoMapper MAPPER = Mappers.getMapper(ModuleRecordDtoMapper.class);

}
