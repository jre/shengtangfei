package com.boruan.shengtangfeng.core.dao;
import org.beetl.sql.core.mapper.BaseMapper;

import com.boruan.shengtangfeng.core.entity.GradeSubject;


public interface IGradeSubjectDao extends BaseMapper<GradeSubject> {
    public void deleteGrade(Long subjectId, Long gradeId);
}