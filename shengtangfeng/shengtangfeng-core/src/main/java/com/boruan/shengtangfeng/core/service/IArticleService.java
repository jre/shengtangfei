package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ArticleVo;
import com.boruan.shengtangfeng.core.vo.ToAuditArticleVO;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.dto.ArticleCategoryDto;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.ArticleCategory;
import com.boruan.shengtangfeng.core.entity.Attention;
import com.boruan.shengtangfeng.core.vo.ArticleCategoryVo;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface IArticleService {
	/**
	 * 通过id查找用户，后台使用
	 * @param id
	 * @return
	 */
	public Article findId(Long id);
	public Article findById (Long id);
	/**
	 * 分页查询
	 * @param pageQuery
	 * @param article
	 */
	public void pageQuery(PageQuery<Article> pageQuery,Article article);


	 /**
	     * @description: 分页查询关注人的文章
	     * @Param
	     * @return
	     * @author KongXH
	     * @date 2020/8/18 11:05
	     */
	public void pageAttentionArticle(PageQuery<Article> pageQuery,Long userId);
	/**
	 * 保存用户
	 * @param article
	 * @return
	 */
	public boolean save(Article article);
	/**
	 * 通过条件查询用户列表
	 * @param article
	 * @return
	 */
	public List<Article> findByTemplate(Article article);
	/**
	 * 通过条件查询用户列表
	 * @param article
	 * @return
	 */
	public List<Article> findClosely(Article article);
	
	public void deleteArticle(Long articleId,Long userId);
	
	/**
     * 分页查询
     * @param pageQuery
     * @param article
     */
    public void pageQueryCategory(PageQuery<ArticleCategory> pageQuery,ArticleCategory articleCategory);
    /**
     * 按条件查询分类
     * @param pageQuery
     * @param article
     */
    public List<ArticleCategory> getCategoryList(ArticleCategory articleCategory);

	void addArticle(Article dto, Long id);

	List<ArticleCategoryVo> getCategory();

	void updateArticle(Long articleId, Integer type, Long userId);
	/**
     * 按照id查询分类
     * @param categoryId
     */
    public ArticleCategory getCategory(Long categoryId);
    
    /**
     * 保存分类
     * @param course
     */
    public void saveCategory(ArticleCategory articleCategory);
    
    /**
     * 分页查询
     * @param pageQuery
     * @param article
     */
    public void pageQueryCategory(PageQuery<ArticleCategory> pageQuery,ArticleCategoryDto articleCategoryDto);

    public  Long templateCount(Article article);
    
    /**
     * 获取收藏的数量
     * @param userId
     * @return
     */
    public Long getFavoritesCount(Long userId);

    /**
     * 分页获取收藏的文章
     * @param pageQuery
     * @param userId
     */
    public void pageFavoritesArticle(PageQuery<Article> pageQuery,Long userId);
    /**
     * 分页获取点赞的文章
     * @param pageQuery
     * @param userId
     */
    public void pageAccoladeArticle(PageQuery<Article> pageQuery,Long userId);

	/**
	 * 分页查询待审核文章
	 * @param pageQuery
	 */
	void pageToAuditArticle(PageQuery<Article> pageQuery);


	public List<Article> newCheck(Article article);

	/**
	 * 审核端根据用户id查询用户的所有文章
	 * @param pageIndex
	 * @param pageSize
	 * @param userId
	 * @return
	 */
    GlobalReponse<PageQuery<ToAuditArticleVO>> getUserArticle(int pageIndex, int pageSize, Long userId,String keywords);

	/**
	 * 审核端根据文章id查看文章详情
	 * @param articleId
	 * @return
	 */
	GlobalReponse<ToAuditArticleVO> getArticleDetails(Long articleId);

}
