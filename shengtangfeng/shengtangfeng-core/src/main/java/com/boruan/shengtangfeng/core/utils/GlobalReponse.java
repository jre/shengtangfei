package com.boruan.shengtangfeng.core.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *  全局返回JSON实体
 * 
 */
@Data
@ApiModel(value="全局返回实体")
public class GlobalReponse<T> {
	@ApiModelProperty(value="" + 
			"    1000, 操作成功\r\n" + 
			"    9999, 操作失败\r\n" + 
			"    8009, 用户已存在\r\n" + 
			"    8008, 验证码错误或已过期!\r\n" + 
			"    8001, 用户不存在\r\n" + 
			"    8002, 密码错误\r\n" + 
			"    8003, 登陆成功\r\n" + 
			"    8004, token刷新成功\r\n" + 
			"    8005, token刷新失败\r\n" + 
			"    8006,账号被禁用,\r\n" + 
			"	 8007,无效的token")
    private Integer code;
    private String message;
    @ApiModelProperty(value="只有在code=1000的时候才会有data数据")
    private T data;
    private Object data1;
    private Object data2;
    private Object data3;
    
    
    public GlobalReponse() {
    	super();
    }
	public GlobalReponse(GlobalReponseResultEnum reponseResultEnum) {
		this.code=reponseResultEnum.getCode();
		this.message=reponseResultEnum.getMessage();
	}
	public GlobalReponse(GlobalReponseResultEnum reponseResultEnum,T data) {
		this.code=reponseResultEnum.getCode();
		this.message=reponseResultEnum.getMessage();
		this.data=data;
	}
	
	public static GlobalReponse fail() {
		return new GlobalReponse(GlobalReponseResultEnum.FAIL);
	}
	public static GlobalReponse fail(String message) {
		return fail().setMessage(message);
	}
	public static GlobalReponse fail(Object data) {
		return fail().setData(data);
	}
	public static GlobalReponse fail(String message,Object data) {
		return fail().setData(data).setMessage(message);
	}
	public static GlobalReponse success() {
		return new GlobalReponse(GlobalReponseResultEnum.SUCCESS);
	}
	public static GlobalReponse success(String message) {
		return success().setMessage(message);
	}
		public static GlobalReponse success(Object data) {
		return success().setData(data);
	}
	public static GlobalReponse success(String message,Object data) {
		return success().setData(data).setMessage(message);
	}
	
	
	public GlobalReponse setCode(Integer code) {
		this.code = code;
		return this;
	}
	public GlobalReponse setMessage(String message) {
		this.message = message;
		return this;
	}
	public GlobalReponse setData(T data) {
		this.data = data;
		return this;
	}
	public GlobalReponse setData1(Object data1) {
		this.data1 = data1;
		return this;
	}
	public GlobalReponse setData2(Object data2) {
		this.data2 = data2;
		return this;
	}
	public GlobalReponse setData3(Object data3) {
		this.data3 = data3;
		return this;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
