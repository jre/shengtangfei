package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.UserReport;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;


public interface IUserReportDao extends BaseMapper<UserReport> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<UserReport> query);

    void toAuditReport(PageQuery<UserReport> pageQuery);

    void toAdminReport(PageQuery<UserReport> pageQuery);

    void upStatusById(Long[] reportId, Integer status);
}