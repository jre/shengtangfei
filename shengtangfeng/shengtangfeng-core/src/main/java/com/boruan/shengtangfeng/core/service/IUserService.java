package com.boruan.shengtangfeng.core.service;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.ReportReasons;
import com.boruan.shengtangfeng.core.entity.UserReport;
import com.boruan.shengtangfeng.core.vo.GetClassInformVo;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ArticleVideoCommentVo;
import com.boruan.shengtangfeng.core.vo.UserVo;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface IUserService {
    /**
     * 通过openId查找用户
     * 
     * @param id
     * @return
     */
    public User findByOpenId(String openId);

    /**
     * 通过id查找用户，后台使用
     * 
     * @param id
     * @return
     */
    public User findById(Long id);

    /**
     * 分页查询
     * 
     * @param pageQuery
     * @param user
     */
    public void pageQuery(PageQuery<User> pageQuery, User user);

    /**
     * 保存用户
     * 
     * @param user
     * @return
     */
    public boolean save(User user);

    /**
     * 保存用户，附带保存用户密码
     * 
     * @param user
     */
    public void save(User user, String plainPassword);

    /**
     * 通过条件查询用户列表
     * 
     * @param user
     * @return
     */
    public List<User> findByTemplate(User user);

    /**
     * 根据用户手机号查找用户
     * 
     * @param mobile
     * @return
     */
    public User findByMobile(String mobile);

    /**
     * 用户修改密码
     * 
     * @param oldPassword
     * @param password
     * @return
     */
    public GlobalReponse changePassword(User user, String oldPassword, String password);

    /**
     * 根据学生id获取学生姓名
     */
    public User getStudentById(Long id);

    /**
     * 获取排名用户
     * 
     * @param type 排行类型 1 按照做题数 2 按照正确率
     * @return
     */
    public List<User> getRank(Integer type, Integer size);

    /**
     * 添加用户信息
     * 
     * @param user
     * @return
     */
    public void insertUser(User user);

    /**
     * 校验用户原手机
     * 
     * @param mobile
     * @param authCode
     * @return
     */
    public GlobalReponse checkOldMobile(String mobile, String authCode);

    /**
     * @author: 刘光强
     * @Description: 处理绑定手机号
     * @date:2020年3月10日 上午11:14:10
     */
    public GlobalReponse<UserVo> changeMobile(String mobile, String authCode, Long userId);

    GlobalReponse lockUser(Long id, Long userId, Integer type);

    GlobalReponse recommendComments(Long id, Integer type, Boolean recommend);

    void deleteComments(String ids, Integer type);
    void updateCommentStatus(Long commentId, Integer type,Integer status);

    /**
     * 获取我邀请的用户
     * 
     * @param userId
     * @param isVip
     * @return
     */
    List<User> getMySpread(Long userId, Boolean isVip);

    void addUser(User user);

    public void plusLikeNum(Long id);

    public void lessLikeNum(Long id);

    public User findByInvitationCode(Long id);

    /**
     * 获取评论，包含文章和视频
     * 
     * @param userId
     * @return
     */
    public void pageComment(PageQuery<ArticleVideoCommentVo> pageQuery);
    
    /**
     * 用户删除自己的平路n
     * @param userId
     * @param type
     * @param commentId
     * @return
     */
    public GlobalReponse deleteUserComment(Long userId,Integer type,Long commentId);
    /**
     * 后台删除评论
     * @param type
     * @param commentId
     * @return
     */
    public GlobalReponse deleteComment(Integer type,Long commentId);
    /**
     * @author: 刘光强
     * @Description: 向用户绑定的手机发送验证码
     * @date:2020年3月10日 上午11:14:10
     */
    public GlobalReponse<String> sendCode(Long userId);
    
    /**
     * @author: 刘光强
     * @Description: 修改手机号时校验验证码
     * @date:2020年3月10日 上午11:14:10
     */
    public GlobalReponse<String> checkCode(Long userId,String code);
    /**
     * @author: 刘光强
     * @Description: 修改手机号
     * @date:2020年3月10日 上午11:14:10
     */
    public GlobalReponse bindMobile(Long userId,String mobile,String code);
    
    public void blackUser(Long userId, Long blackUserId);


    public GlobalReponse<String> newCheck();

    /**
     * 审核端将用户封号或者解封
     * @param userId
     * @return
     */
    GlobalReponse blockOrUnblockUser(Long adminId,Long userId,Integer status);


    GlobalReponse<User> getUserStatus(Long userId);

    /**
     * 获取举报原因
     * @return
     */
    public GlobalReponse<List<ReportReasons>> getReportReason();

    /**
     * 保存举报
     * @param userReport
     */
    public void saveReport(UserReport userReport);


    public boolean isFriend(Long userId,Long friendUser);

    void toAuditReport(PageQuery<UserReport> pageQuery);

    UserReport findReportById(Long reportId);

    void toAdminReport(PageQuery<UserReport> pageQuery);

    void dealUserReport(UserReport userReport);

    Long[] getReportUids(Long[] reportId);

    void batchDealReport(Long[] reportId, Integer status);

    void pageReportReason(PageQuery<ReportReasons> pageQuery);

    void saveReportReason(ReportReasons reportReasons);

    ReportReasons findReasonById(Long id);

    void delReport(Long reportId);

    User findByIdTelName(Long userId);

    /**
     * 获取UserSig
     * @param
     * @param sdkappid
     * @param secretkey
     * @return
     */
    GlobalReponse getUserSig(Long userId, Long sdkappid, String secretkey);

    /**
     * 删除举报理由
     * @param id
     */
    GlobalReponse delReportReasons(Long id);

    GlobalReponse<GetClassInformVo> getClassInformById(Long classId);

    /**
     * 赠送用户粉丝
     * @param userId
     * @param fansNum
     * @param adminId
     * @return
     */
    GlobalReponse giveUserFans(Long userId, Integer fansNum, Long adminId);

    User createRobotUser();

}
