package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 普通用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SubjectiveVo extends BaseVo {

	/**
	 * 章节名
	 **/
	@ApiModelProperty("模块名")
	private String name;
	/**
	 * 题目数量
	 **/
	@ApiModelProperty("题目数量")
	private Integer questionCount;
	/**
	 * 模块描述
	 **/
	@ApiModelProperty("模块描述")
	private String description;
	/**
	 * 已做题目数量
	 **/
	@ApiModelProperty("已做题目数量")
	private Integer answerCount;
	/**
	 * 评分
	 **/
	@ApiModelProperty("评分")
	private Double star;

}
