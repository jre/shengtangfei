package com.boruan.shengtangfeng.core.utils;

/**
 * \
 */
public enum GlobalReponseResultEnum {

	/**操作成功**/
    SUCCESS(1000, "操作成功"),
    /**操作失败**/
    FAIL(9999, "操作失败"),
    
    /**用户已存在**/
    USER_EXIST(8009, "用户已存在"),
	
	
	/**用户不存在**/
    USER_NOT_EXIST(8001, "用户不存在"),

    /**密码错误**/
    PASSWORD_IS_WRONG(8002, "密码错误"),

    /**登陆成功**/
    LOGIN_IN_SUCCEED(8003, "登陆成功"),

    /**token刷新成功**/
    REFRESH_TOKEN_SUCCEED(8004, "token刷新成功"),

    /**token刷新失败**/
    REFRESH_TOKEN_FAILED(8005, "token刷新失败"),

    /**账号被禁用**/
    USER_IS_DISABLED(8006,"账号被禁用"),
    
	/**无效的token**/
	TOKEN_ERROR(8007,"请先去登录"),
	
	/**验证码失败**/
	CODE_ERROR(8008,"验证码错误或已过期!"),
	
    /**没有权限**/
    USER_LACK_PERMISSION(9001,"用户端认证还未通过，没有权限"),

    /**没有权限**/
    DRIVER_LACK_PERMISSION(9002,"司机端认证还未通过，没有权限");

    private String message;
    private Integer code;

    GlobalReponseResultEnum(Integer code,String message) {
        this.message = message;
        this.code = code;
    }

    public static String getName(int code) {
        for (GlobalReponseResultEnum e : GlobalReponseResultEnum.values()) {
            if (e.getCode() == code) {
                return e.getMessage();
            }
        }
        return null;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
