package com.boruan.shengtangfeng.core.vo;

import com.boruan.shengtangfeng.core.entity.VideoCategory;
import com.boruan.shengtangfeng.core.entity.UserCategory;
import com.boruan.shengtangfeng.core.entity.VideoCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author KongXH
 * @title: VideoUserCategoryVo
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1315:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="我的频道")
public class VideoUserCategoryVo extends BaseVo{


    @ApiModelProperty(value="视频未选类别")
    private List<VideoCategory> notChoiceVideoCategorie;
    @ApiModelProperty(value="视频已选类别")
    private List<VideoCategory> userVideoCategorie;
    @ApiModelProperty(value="全部类别")
    private List<VideoCategory> allVideoCategorie;
}
