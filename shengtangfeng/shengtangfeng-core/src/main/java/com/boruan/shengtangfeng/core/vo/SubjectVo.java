package com.boruan.shengtangfeng.core.vo;

import java.util.Date;

import com.boruan.shengtangfeng.core.enums.CourseStatus;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 学科
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("学科")
public class SubjectVo extends BaseVo {

	/**
	 *  名称
	 */
	@ApiModelProperty("名称")
	private String name;
	/**
     *  图标
     **/
    @ApiModelProperty(value="图标")
    private String icon ;
	/**
     * 排序 
     **/
    private Integer sort ;
}
