package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ToAuditCommentDTO {

    @ApiModelProperty("type 0文章 1视频 2试题 ")
    private Integer type;

    @ApiModelProperty("用户id,用户昵称，评论内容")
    private String content;

    @ApiModelProperty("第几页")
    private Integer pageNo;

    @ApiModelProperty("每页总条数")
    private Integer pageSize;

}
