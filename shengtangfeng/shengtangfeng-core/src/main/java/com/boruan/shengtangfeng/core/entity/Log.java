package com.boruan.shengtangfeng.core.entity;

import org.beetl.sql.core.annotatoin.EnumMapping;
import org.beetl.sql.core.annotatoin.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Log")
@EqualsAndHashCode(callSuper = false)
public class Log extends IdEntity {

	private static final long serialVersionUID = 3980909601626103168L;

	@EnumMapping(value = "value")
	public enum LogType {
		INFO(0), DEBUG(1), ERROR(2), BASIC(3),
		// 订单
		ORDER(4),
		//
		SYSTEM(5), LOGIN(6), ADMIN(7);

		private int value;

		private LogType(int value) {
			this.value = value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}

	@EnumMapping(value = "value")
	public enum OperateType {
		ADD(0), UPDATE(1), DELETE(2);

		private int value;

		private OperateType(int value) {
			this.value = value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}

	/**
	 * 日志内容
	 */
	private String content;

	/**
	 * 操作的对象ID
	 */
	private Long objectId;

	/**
	 * 操作人ID
	 */
	private Long operater;

	/**
	 * @see LogType 日志类型
	 */
	private LogType logType;
	/**
	 * @see LogType 操作类型
	 */
	private OperateType operateType;
}
