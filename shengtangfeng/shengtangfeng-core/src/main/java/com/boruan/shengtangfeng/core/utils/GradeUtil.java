package com.boruan.shengtangfeng.core.utils;

import com.boruan.shengtangfeng.core.dao.IClassParameterDao;
import com.boruan.shengtangfeng.core.entity.ClassParameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Component
public class GradeUtil {
    @Autowired
    private IClassParameterDao classParameterDao;

    private final  static String[] grade = {"一年级","二年级","三年级","四年级","五年级","六年级","七年级","八年级","九年级"};

    public String getGrade(int schoolYear,String spring,String autumn){
        //获取当前年份
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        //获取学年配置
        ClassParameter classParam = classParameterDao.createLambdaQuery().andEq(ClassParameter::getIsDeleted, false).single();
        //判断学制
        int num = classParam.getEducationSystem() == 1 ? 5 : 6;
        //判断上下学期
        String now = new SimpleDateFormat("MM-dd").format(new Date());
        int newYear=year+1;
        String nowTime = year+"-"+now;
        String springTime =year+"-"+spring;
        String autumnTime =newYear+"-"+autumn;

//        Date nowTime = null;
//        Date springTime = null;
//        Date autumnTime = null;
//        try {
//            nowTime = DateUtils.parseDate(now, "MM-dd");
//            springTime = DateUtils.parseDate(spring, "MM-dd");
//            autumnTime = DateUtils.parseDate(autumn, "MM-dd");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Date date = null;
        Date date1 = null;
        Date date2 = null;
        String item = null;
        try {
            date = DateUtils.parseDate(nowTime, "yyyy-MM-dd");
            date1 = DateUtils.parseDate(springTime, "yyyy-MM-dd");
            date2 = DateUtils.parseDate(autumnTime, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date.after(date1) && date.before(date2)) {
            item = "上";
        } else {
            item = "下";
        }
        int cha = year-schoolYear;
        if(cha>9){
            return "已毕业";
        }else{
            return grade[cha]+item;
        }
    }
}
