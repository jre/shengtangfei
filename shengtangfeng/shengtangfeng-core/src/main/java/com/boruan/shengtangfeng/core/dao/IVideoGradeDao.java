package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.VideoGrade;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.math.BigDecimal;


public interface IVideoGradeDao extends BaseMapper<VideoGrade> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<VideoGrade> query);

    BigDecimal avgScore(Long videoId);

    VideoGrade findUserGrade(Long userId, Long videoId);

}