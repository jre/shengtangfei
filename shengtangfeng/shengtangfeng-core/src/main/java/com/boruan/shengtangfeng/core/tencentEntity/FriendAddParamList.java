package com.boruan.shengtangfeng.core.tencentEntity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FriendAddParamList {

    /**
     *好友的 UserID
     * 必填
     */
    @JSONField(name = "To_Account")
    private String To_Account;

    /**
     *From_Account 对 To_Account 的好友备注
     * 1. 备注长度最长不得超过 96 个字节
     * 选填
     */
    @JSONField(name = "Remark")
    private String Remark;

    /**
     *From_Account 对 To_Account 的分组信息，添加好友时只允许设置一个分组，因此使用 String 类型即可
     * 1. 最多支持 32 个分组；
     * 2. 不允许分组名为空；
     * 3. 分组名长度不得超过 30 个字节；
     * 4. 同一个好友可以有多个不同的分组
     * 选填
     */
    @JSONField(name = "GroupName")
    private String GroupName;

    /**
     * 加好友来源字段，详情可参见 标配好友字段
     * 1. 加好友来源字段包含前缀和关键字两部分；
     * 2. 加好友来源字段的前缀是：AddSource_Type_ ；
     * 3. 关键字：必须是英文字母，且长度不得超过 8 字节，建议用一个英文单词或该英文单词的缩写；
     * 4. 示例：加好友来源的关键字是 Android，则加好友来源字段是：AddSource_Type_Android
     * 必填
     */
    @JSONField(name = "AddSource")
    private String AddSource;

    /**
     *From_Account 和 To_Account 形成好友关系时的附言信息，详情可参见 标配好友字段
     * 1. 加好友附言的长度最长不得超过 256 个字节
     * 选填
     */
    @JSONField(name = "AddWording")
    private String AddWording;
    
}
