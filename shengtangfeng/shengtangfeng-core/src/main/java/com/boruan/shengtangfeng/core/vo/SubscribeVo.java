package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @program: tutuyou-parent
 * @description: 订阅线路
 * @author: lihaicheng
 * @create: 2019-10-11 11:46
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SubscribeVo extends BaseVo {


    @ApiModelProperty(value = "始发地省")
    private String startProvince;

    @ApiModelProperty(value = "始发地城市")
    private String startCity;

    @ApiModelProperty(value = "始发地区")
    private String startRegion;

    @ApiModelProperty(value = "始发地街道")
    private String startStreet;

    @ApiModelProperty(value = "始发地坐标")
    private String startCoordinate;

    @ApiModelProperty(value = "重量")
    private String weight;

    @ApiModelProperty(value = "是否开启回程路线，1开启，0关闭")
    private Integer isOpen;

    @ApiModelProperty(value = "目的地省")
    private String endProvince;

    @ApiModelProperty(value = "目的地城市")
    private String endCity;

    @ApiModelProperty(value = "目的地区")
    private String endRegion;

    @ApiModelProperty(value = "目的地街道")
    private String endStreet;

    @ApiModelProperty(value = "卸货地址坐标")
    private String endCoordinate;
}
