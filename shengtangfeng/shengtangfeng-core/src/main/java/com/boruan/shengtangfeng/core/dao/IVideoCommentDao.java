package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.VideoComment;
import com.boruan.shengtangfeng.core.vo.CommentVo;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IVideoCommentDao extends BaseMapper<VideoComment> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<VideoComment> pageQuery);
	/**
	 * 根据问题ID获取所有的评论
	 */
	public List<VideoComment> findByVideoId(Long userId,Long videoId,Integer status);
	/**
	 * 根据问题ID获取所有不是用户的的评论
	 */
	public List<VideoComment> findByVideoIdNotMine(Long userId,Long videoId,Integer status);

	public long getTodayCount(Long userId);

	/**
	 * 分页查询待审核视频评论
	 * @param pageQuery
	 */
    void pageGetComment(PageQuery<CommentVo> pageQuery);

}
