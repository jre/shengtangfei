package com.boruan.shengtangfeng.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author:guojiang
 * @Description:查询待审核文章返回数据
 * @date:2021/2/25
 */
@Data
public class ToAuditArticleVO {

    /**
     * 文章id
     */
    @ApiModelProperty("文章id")
    private Long id;

    /**
     * 分类ID
     */
    @ApiModelProperty("分类id")
    private Long categoryId;
    /**
     * 新闻标题
     */
    @ApiModelProperty("新闻标题")
    private String title;
    /**
     * 新闻摘要，显示在列表
     */
    @ApiModelProperty("摘要")
    private String summary;

    /**
     * 新闻内容
     */
    @ApiModelProperty("新闻内容")
    private String content;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @ApiModelProperty("发布时间")
    private Date publishDate;
    /**
     *  发布者Id
     */
    @ApiModelProperty("发布者Id")
    private Long publishId;
    /**
     * 发布人
     */
    @ApiModelProperty("发布人")
    private String publisher;
    /**
     * 发布人头像
     */
    @ApiModelProperty("发布人头像")
    private String publisherIcon;
    /**
     * 原文链接
     */
    @ApiModelProperty("原文链接")
    private String link;
    /**
     * 状态 0 审核中 1 通过 2 不通过
     */
    @ApiModelProperty("状态 0 审核中 1 通过 2 不通过")
    private Integer status;

    /**
     *  内容中的图，用英文逗号分隔
     */
    @ApiModelProperty("图片")
    private String images;
    /**
     *  图数量 1 无图  2 一张图 3 多于一张
     */
    @ApiModelProperty("图数量 1 无图  2 一张图 3 多于一张")
    private Integer type;
    /**
     *  审核备注
     */
    @ApiModelProperty("审核备注")
    private String remark;
    /**
     * 相关评论
     */
    @ApiModelProperty(value="相关评论")
    private List<ArticleCommentVo> comment;

}
