package com.boruan.shengtangfeng.core.vo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.vo.GradeVo;

/**
@author: 刘光强
@Description:
@date:2020年3月10日 上午11:14:10
*/
@Mapper
public interface GradeVoMapper extends VoMapper<GradeVo,  Grade>  {
	GradeVoMapper MAPPER = Mappers.getMapper(GradeVoMapper.class);
}