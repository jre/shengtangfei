package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.JobBack;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

public interface IJobBackDao extends BaseMapper<JobBack> {

    /**
     * 学生补交作业
     * @param jobBack
     * @return
     */
    List<JobBack> getStudentJobBack(JobBack jobBack);

    /**
     * 老师提醒补交作业
     * @param jobBack
     * @return
     */
    List<JobBack> getTeacherJobBack(JobBack jobBack);
}
