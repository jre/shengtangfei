package com.boruan.shengtangfeng.core.service;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.MessageNumVo;
import org.beetl.sql.core.engine.PageQuery;

import com.boruan.shengtangfeng.core.dto.NoticeSearch;
import com.boruan.shengtangfeng.core.entity.Notice;
import com.boruan.shengtangfeng.core.vo.NoticeVo;

/**
 * @author liuguangqiang
 * @date 2016年6月22日 下午3:12:06
 */

public interface INoticeService {
	/**
	 * 分页查询通知
	 * @param pageQuery
	 * @param notice
	 */
	public void pageQueryVo(PageQuery<NoticeVo> pageQuery,NoticeSearch noticeSearch);
	/**
	 * 分页查询通知
	 * @param pageQuery
	 * @param notice
	 */
	public void pageQuery(PageQuery<Notice> pageQuery,NoticeSearch noticeSearch);
	/**
	 * 根据条件获取数量
	 * @param pageQuery
	 * @param notice
	 */
	public GlobalReponse<MessageNumVo> getCount(NoticeSearch noticeSearch);
	
	/**
     * 保存
     * @param Notice
     */
    public void save(Notice notice);
    /**
     * 保存
     * @param id
     */
    public Notice findById(Long id);
    /**
     *设置消息为已读 
     */
    public void setRead(Long noticeId,Long userId);
    /**
     * 新增通知
     * @param category 分类  1 平台通知  2 用户通知
     * @param type 类型，0：平台通知  1：文章审核通知 2： 视频审核通知 403:用户被锁下线 404 用户互踢下线
     * @param objectId 对象ID，根据type判断对应
     * @param userId 用户ID 
     * @param title 标题
     */
    public Notice addNotice(Integer category,Integer type,Long objectId,Long userId ,String title,String content);

	/**
	 * 删除通知
	 * @param id
	 * @param userId
	 * @return
	 */
	GlobalReponse deletedNotice(Long id, Long userId);

}
