package com.boruan.shengtangfeng.core.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video")
@EqualsAndHashCode(callSuper = false)
public class Video extends IdEntity {

	/**
	 * 分类ID
	 */
	@ApiModelProperty(value="分类ID")
	private Long categoryId;
	/**
	 * 标题
	 */
	@ApiModelProperty(value="标题")
	private String title;
	/**
	 * 添加的话题
	 */
	@ApiModelProperty(value="添加的话题")
	private String topic;

	/**
	 *  视频长度（秒）
	 */
	@ApiModelProperty(value="视频长度（秒）")
	private Integer duration;
	/**
	 *  首页图
	 */
	@ApiModelProperty(value="首页图")
	private String image;
	/**
	 * 视频内容地址
	 */
	@ApiModelProperty(value="视频内容地址")
	private String content;

	/**
	 * 发布时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
	@ApiModelProperty(value="发布时间")
	private Date publishDate;
	/**
	 * 发布人
	 */
	@ApiModelProperty(value="发布人")
	private String publisher;
	/**
	 * 发布人id
	 */
	@ApiModelProperty(value="发布人id")
	private Long publishId;//发布者id
	/**
	 * 发布人头像
	 */
	@ApiModelProperty(value="发布人头像")
	private String publisherIcon;
	/**
	 * 状态 0 审核中 1 通过 2 不通过
	 */
	@ApiModelProperty(value=" 状态 0 审核中 1 通过 2 不通过")
	private Integer status;
	/**
	 *  是否推荐
	 */
	@ApiModelProperty(value="是否推荐")
	private Boolean recommend;
	/**
	 * 已读人数
	 */
	@ApiModelProperty(value="已读人数")
	private Integer readNum;
	/**
	 * 评论人数
	 */
	@ApiModelProperty(value="评论人数")
	private Integer commentNum;
	/**
     * 点赞人数
     */
	@ApiModelProperty(value="点赞人数")
    private Integer thumpUpNum;
	/**
	 * 分享次数
	 */
	@ApiModelProperty(value="分享次数")
	private Integer shareNum;
	/**
	 *  审核备注
	 */
	@ApiModelProperty(value="审核备注")
	private String remark;
	/**
	 *  上传类型0 系统  1用户
	 */
	@ApiModelProperty(value="上传类型0 系统  1用户")
	private Integer uploadType;
	/**
	 *  发布地址
	 */
	@ApiModelProperty(value="发布地址")
	private String address;
	/**
	 *  经度
	 */
	@ApiModelProperty(value="经度")
	private String longitude;
	/**
	 *  纬度
	 */
	@ApiModelProperty(value=" 纬度")
	private String latitude;
	/**
	 *  发布范围 0公开可见 1我的好友可见 2我的班级可见 3仅自己可见
	 */
	@ApiModelProperty(value="发布范围 0公开可见 1我的好友可见 2我的班级可见 3仅自己可见")
	private Integer scope;

	/**
	 *  视频分类 0淘学视频 1讲一讲视频 2小视频
	 */
	@ApiModelProperty(value="视频分类 0淘学视频 1讲一讲视频 2小视频")
	private Integer type;
	/**
	 * 邀请码
	 */
	@ApiModelProperty(value="邀请码")
	private Long invitationCode;//邀请码
	/**
	 *放值字段
	 */
	@ApiModelProperty(value="放值字段")
	private Long userId;//用户id

	/**
	 * 发布城市高德编码
	 */
	@ApiModelProperty(value="发布城市高德编码")
	private String cityCode;

	@ApiModelProperty(value="阿里审核任务Id")
	private String taskId;//用户id
	
}
