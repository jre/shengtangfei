package com.boruan.shengtangfeng.core.enums;

import com.boruan.shengtangfeng.core.utils.CommonEnum;
import org.beetl.sql.core.annotatoin.EnumMapping;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author:Jessica
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@EnumMapping("value")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ObjectType implements CommonEnum {
	TOALL(1, "发给所有人"), TOTEACHER(2, "只发给老师"), TOSTUDENT(3, "只发给学生");

	@ApiModelProperty(value = "1发给所有人2只发给老师 3只发给学生 ")
	private Integer value;
	@ApiModelProperty(value = "")
	private String name;

	private ObjectType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
