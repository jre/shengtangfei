package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.VideoCategory;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;


public interface IVideoCategoryDao extends BaseMapper<VideoCategory> {
    /**
     * 分页查询
     * @param query
     */
    public void pageQuery(PageQuery<VideoCategory> query);

    /**
     * 查询
     *
     * @param videoCategory
     */
    public List<VideoCategory> getList(VideoCategory videoCategory);



    /**
     * @description:获取默认类别
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/13 10:54
     */
    public List<VideoCategory> getVideoCategoryDefault();
}