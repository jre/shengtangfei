package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IModuleVideoDao;
import com.boruan.shengtangfeng.core.dto.ModuleVideoDto;
import com.boruan.shengtangfeng.core.entity.ModuleVideo;
import com.boruan.shengtangfeng.core.service.IModuleVideoService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ModuleVideoService implements IModuleVideoService {
    @Autowired
    private IModuleVideoDao moduleVideoDao;


    @Override
    public void getModuleVideo(PageQuery<ModuleVideo> pageData) {
        moduleVideoDao.pageModuleQuery(pageData);
        List<ModuleVideo> resultList = pageData.getList();
        pageData.setList(resultList);
    }

    @Override
    public void pageQueryByModule(PageQuery<ModuleVideo> pageData, ModuleVideoDto moduleVideoDto) {
        moduleVideoDao.pageQueryByModule(pageData, moduleVideoDto);
        List<ModuleVideo> resultList = pageData.getList();
        pageData.setList(resultList);
    }

    @Override
    public void addVideo(ModuleVideoDto po) {
        ModuleVideo moduleVideo = new ModuleVideo();
        BeanUtils.copyProperties(po,moduleVideo);
        if (null != po.getId()) {
            moduleVideoDao.updateTemplateById(moduleVideo);
        } else {
            moduleVideoDao.insertTemplate(moduleVideo);
        }
    }


}
