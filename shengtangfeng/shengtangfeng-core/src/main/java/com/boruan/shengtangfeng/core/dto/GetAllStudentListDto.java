package com.boruan.shengtangfeng.core.dto;

import com.boruan.shengtangfeng.core.vo.UserJobVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GetAllStudentListDto {

    @ApiModelProperty("全部数据集合")
    private List<UserJobVo> allList ;

    @ApiModelProperty("已提交集合")
    private List<UserJobVo> submitList ;

    @ApiModelProperty("未提交集合")
    private List<UserJobVo> notSubmitList ;

    @ApiModelProperty("全对的集合")
    private List<UserJobVo> allRightList ;

    @ApiModelProperty("未全对集合")
    private List<UserJobVo> notRightList ;

}
