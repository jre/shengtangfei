package com.boruan.shengtangfeng.core.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class AddressDto implements Serializable {

    /**
     * id
     */
    @ApiModelProperty(value="id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 高德编码
     */
    @ApiModelProperty(value="高德编码")
    private String adcode;
}
