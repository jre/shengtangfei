package com.boruan.shengtangfeng.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MessageNumVo {

    @ApiModelProperty("消息总数量")
    private Long  zongNum;

    @ApiModelProperty("新好友消息")
    private Long  friendNum;

    @ApiModelProperty("群消息")
    private Long  groupNum;

    @ApiModelProperty("班级消息")
    private Long  classNum;

    @ApiModelProperty("互动消息")
    private Long  interactiveNum;
}
