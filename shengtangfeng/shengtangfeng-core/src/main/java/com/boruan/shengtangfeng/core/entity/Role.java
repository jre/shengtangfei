package com.boruan.shengtangfeng.core.entity;

import java.beans.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.misc.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.annotatoin.Table;

import com.google.common.collect.ImmutableList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role")
@EqualsAndHashCode(callSuper = false)
public class Role extends IdEntity implements Serializable {

	private static final long serialVersionUID = -5486532934021999747L;

	public static final String PERMISSION_DIVIDER_TOKEN = ";";

	/**
	 * 角色名 一般为英文（对于简单应用而言，最好不要修改）
	 */
	private String name;
	/**
	 * 角色的描述
	 */
	private String dscn;
	/**
	 * Shiro权限定义
	 */
	private String permissions;

	/**
	 * 是否为系统内置角色，区分系统角色还是自定义的角色
	 */
	@NotNull
	private Boolean isSystemRole;

	@Transient
	public List<String> getPermissionList() {
		if (StringUtils.isNotBlank(permissions)) {
			return ImmutableList.copyOf(StringUtils.split(permissions, PERMISSION_DIVIDER_TOKEN));
		} else {
			return new ArrayList<String>();
		}
	}
}
