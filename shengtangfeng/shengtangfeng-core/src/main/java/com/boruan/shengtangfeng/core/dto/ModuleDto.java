package com.boruan.shengtangfeng.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* @author:twy
* @Description:
*
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "章节")
public class ModuleDto extends BaseDto{
	/**
	 * 父级id
	 */
	@ApiModelProperty(value = "父级id，最高级为0")
	private Integer parentId;
	/**
	 * 章节名称
	 */
	@ApiModelProperty(value = "章节名称")
	private String name;
	/**
	 * 章节练习 题目数量
	 */
	@ApiModelProperty(value = "章节练习 题目数量")
	private Integer questionCount;
	/**
	 * 章节描述
	 **/
	@ApiModelProperty(value = "章节描述")
	private String description;
	/**
     * 年级ID
     **/
	@ApiModelProperty(value = "年级ID")
    private Long gradeId;
    /**
     * 学科ID
     **/
	@ApiModelProperty(value = "学科ID")
    private Long subjectId;
    
    /**
     * 分类图标
     **/
	@ApiModelProperty(value = "分类图标")
    private String icon;

    /**
     * 模块下单题分数
     */
	@ApiModelProperty(value = "模块下单题分数")
    private Integer score;
	
	/**
     * 难度 0-5
     */
	@ApiModelProperty(value = "难度 0-5")
    private Integer difficulty;

	/**
	 * 模块类型
	 */
	@ApiModelProperty(value="模块类型：1试题2视频")
	private Integer type;

}
