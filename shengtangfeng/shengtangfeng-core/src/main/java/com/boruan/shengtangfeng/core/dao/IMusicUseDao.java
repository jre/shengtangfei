package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.MusicUse;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.Date;

public interface IMusicUseDao extends BaseMapper<MusicUse>{

    Long getUseNum(Long musicId, Date startTime, Date endTime, Long type);
}
