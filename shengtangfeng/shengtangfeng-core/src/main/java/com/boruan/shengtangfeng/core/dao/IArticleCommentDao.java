package com.boruan.shengtangfeng.core.dao;

import java.util.List;

import com.boruan.shengtangfeng.core.entity.ArticleComment;
import com.boruan.shengtangfeng.core.vo.CommentVo;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import javax.xml.stream.events.Comment;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface IArticleCommentDao extends BaseMapper<ArticleComment> {

	/**
	 * 后台分页获取
	 */
	public void pageQuery(PageQuery<ArticleComment> pageQuery);
	/**
	 * 根据问题ID获取不是当前用户的评论
	 */
	public List<ArticleComment> findByArticleIdNotMine(Long userId,Long articleId,Integer status);
	/**
	 * 根据问题ID获取所有的评论,自己的评论在上面
	 */
	public List<ArticleComment> findByArticleId(Long userId,Long articleId,Integer status);

	public long getTodayCount(Long userId);

	/**
	 * 分页获取文章评论
	 * @param pageQuery
	 */
    void pageGetComment(PageQuery<CommentVo> pageQuery);
}
