package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.IFavoritesDao;
import com.boruan.shengtangfeng.core.entity.Favorites;
import com.boruan.shengtangfeng.core.service.IFavoritesService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import org.beetl.sql.core.db.KeyHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author KongXH
 * @title: FavoritesService
 * @projectName ShengTangFeng-V2-JAVA
 * @description: 收藏表
 * @date 2020/8/1714:11
 */
@Service
@Transactional(readOnly = true)
public class FavoritesService implements IFavoritesService {


    @Autowired
    private IFavoritesDao favoritesDao;



     /**
         * @description: 删除
         * @Param
         * @return
         * @author KongXH
         * @date 2020/8/18 15:02
         */
    @Override
    public void deleteFavorites(Long userId, Long articleId,Integer type) {
        favoritesDao.deleteFavorites(userId,articleId,type);
    }


    /**
     * @description: 保存收藏
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/17 14:22
     */
    @Override
    @Transactional(readOnly = false)
    public GlobalReponse saveFavorites(Favorites favorites) {
        Favorites favorite = favoritesDao.getFavorites(favorites.getUserId(),favorites.getArticleId(),favorites.getType());
        if(null == favorite){
            favoritesDao.insertReturnKey(favorites);
            return GlobalReponse.success(true).setMessage("收藏成功");
        }else{
            favoritesDao.deleteFavorites(favorites.getUserId(),favorites.getArticleId(),favorites.getType());
            return GlobalReponse.success(false).setMessage("取消收藏成功");
        }
    }

    /**
     * @description: 获取是否点赞
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/17 17:56
     */
    @Override
    public Favorites getFavorites(Long userId,Long articleId,Integer type) {
        return  favoritesDao.getFavorites(userId,articleId,type);
    }
}
