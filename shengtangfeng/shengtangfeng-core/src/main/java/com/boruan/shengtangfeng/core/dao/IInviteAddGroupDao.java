package com.boruan.shengtangfeng.core.dao;

import com.boruan.shengtangfeng.core.entity.InviteAddGroup;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

public interface IInviteAddGroupDao extends BaseMapper<InviteAddGroup> {

    /**
     * 获取别人邀请我加群
     * @param userId
     * @return
     */
    List<InviteAddGroup> getInviteAddGroup(Long userId);
}
