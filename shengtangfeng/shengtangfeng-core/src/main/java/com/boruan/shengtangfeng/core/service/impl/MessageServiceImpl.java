package com.boruan.shengtangfeng.core.service.impl;

import com.boruan.shengtangfeng.core.dao.*;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.service.IMessageService;
import com.boruan.shengtangfeng.core.tencentEntity.CreateGroupReturn;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.TencentUtil;
import com.boruan.shengtangfeng.core.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: 郭江
 * @Description: 消息
 * @date:2021/5/15
 */
@Service
@Transactional(readOnly = false)
public class MessageServiceImpl implements IMessageService {

    @Autowired
    private IApplyForDao applyForDao;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IGroupChatDao groupChatDao;
    @Autowired
    private IGroupMembersDao groupMembersDao;
    @Autowired
    private IUserFriendAndBlacklistDao userFriendAndBlacklistDao;
    @Autowired
    private IKickOutDao kickOutDao;
    @Autowired
    private IClassAndDao classAndDao;
    @Autowired
    private IClassMembersDao classMembersDao;
    @Autowired
    private IJobBackDao jobBackDao;
    @Autowired
    private IJobDao jobDao;
    @Autowired
    private ISubjectDao subjectDao;
    @Autowired
    private IVideoFriendDao videoFriendDao;
    @Autowired
    private IVideoDao videoDao;
    @Autowired
    private IInviteAddGroupDao inviteAddGroupDao;
    @Autowired
    private IApplyForDeletedDao applyForDeletedDao;
    @Autowired
    private IJobBackDeletedDao jobBackDeletedDao;
    @Autowired
    private TencentUtil tencentUtil;

    /**
     * 获取新好友列表
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse<NewFriendVo> getNewFriendList(Long userId) {
        ApplyFor applyFor = new ApplyFor();
        applyFor.setRelevanceId(userId);
        applyFor.setType(0);
        List<ApplyFor> bFors = applyForDao.getNewFriend(applyFor);
        List<NewFriendVo> vos = new ArrayList<>();
        if (!bFors.isEmpty()) {
            for (ApplyFor bFor : bFors) {
                ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, bFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                if (single != null) {
                    if (single.getIsRead() == null || single.getIsRead() == 0) ;
                    {
                        single.setIsRead(1);
                        single.setUpdateTime(new Date());
                        single.setUpdateBy(userId.toString());
                        applyForDeletedDao.updateTemplateById(single);
                    }
                    if (single.getIsDeletedApply() != null && single.getIsDeletedApply() == 1) {
                        continue;
                    }
                } else {
                    ApplyForDeleted forDeleted = new ApplyForDeleted();
                    forDeleted.setApplyTableId(bFor.getId());
                    forDeleted.setUserId(userId);
                    forDeleted.setIsRead(1);
                    forDeleted.setIsDeletedApply(0);
                    forDeleted.setCreateTime(new Date());
                    forDeleted.setCreateBy(userId.toString());
                    applyForDeletedDao.insertTemplate(forDeleted);
                }
                NewFriendVo vo = new NewFriendVo();
                vo.setId(bFor.getId());
                User user = userDao.unique(bFor.getApplyId());
                vo.setApply(1);
                vo.setRelevanceId(user.getId());
                vo.setRelevanceImage(user.getHeadImage());
                vo.setRelevanceNickname(user.getName());
                vo.setRelevanceMobile(user.getMobile());
                vo.setStatus(bFor.getStatus());
                vo.setApplyRemark(bFor.getApplyRemark());
                vo.setCreateTime(bFor.getCreateTime());
                vos.add(vo);
            }
        }
        applyFor.setRelevanceId(null);
        applyFor.setApplyId(userId);
        List<ApplyFor> zFors = applyForDao.getNewFriend(applyFor);
        if (!zFors.isEmpty()) {
            for (ApplyFor zFor : zFors) {
                ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, zFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                if (single != null) {
                    if (single.getIsRead() == null || single.getIsRead() == 0) ;
                    {
                        single.setIsRead(1);
                        single.setUpdateTime(new Date());
                        single.setUpdateBy(userId.toString());
                        applyForDeletedDao.updateTemplateById(single);
                    }
                    if (single.getIsDeletedApply() == 1) {
                        continue;
                    }
                } else {
                    ApplyForDeleted forDeleted = new ApplyForDeleted();
                    forDeleted.setApplyTableId(zFor.getId());
                    forDeleted.setUserId(userId);
                    forDeleted.setIsRead(1);
                    forDeleted.setIsDeletedApply(0);
                    forDeleted.setCreateTime(new Date());
                    forDeleted.setCreateBy(userId.toString());
                    applyForDeletedDao.insertTemplate(forDeleted);
                }
                NewFriendVo vo = new NewFriendVo();
                User user = userDao.unique(zFor.getRelevanceId());
                vo.setId(zFor.getId());
                vo.setApply(0);
                vo.setRelevanceId(user.getId());
                vo.setRelevanceImage(user.getHeadImage());
                vo.setRelevanceNickname(user.getName());
                vo.setRelevanceMobile(user.getMobile());
                vo.setStatus(zFor.getStatus());
                vo.setApplyRemark(zFor.getApplyRemark());
                vo.setCreateTime(zFor.getCreateTime());
                vos.add(vo);
            }
        }
        vos.sort(Comparator.comparing(NewFriendVo::getCreateTime).reversed());
        return GlobalReponse.success(vos);
    }

    /**
     * 删除新好友消息
     *
     * @param id
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedNewFriend(Long id, Long userId) {
        ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, id).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
        if (single != null) {
            single.setIsDeletedApply(1);
            single.setUpdateBy(userId.toString());
            single.setUpdateTime(new Date());
            applyForDeletedDao.updateTemplateById(single);
            return GlobalReponse.success("删除成功");
        }
        ApplyForDeleted forDeleted = new ApplyForDeleted();
        forDeleted.setApplyTableId(id);
        forDeleted.setUserId(userId);
        forDeleted.setIsRead(1);
        forDeleted.setIsDeletedApply(1);
        forDeleted.setCreateTime(new Date());
        forDeleted.setCreateBy(userId.toString());
        forDeleted.setIsDeleted(false);
        applyForDeletedDao.insertTemplate(forDeleted);
        return GlobalReponse.success("删除成功");
    }

    /**
     * 同意或拒绝好友申请status 1同意 2拒绝
     *
     * @param userId
     * @param id
     * @param status
     * @return
     */
    @Override
    public GlobalReponse agreeOrRefuseApplyFriend(Long userId, Long id, Integer status) {

        ApplyFor applyFor = applyForDao.unique(id);
        if (status == 1) {
            applyFor.setStatus(1);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            UserFriendAndBlacklist blacklist = new UserFriendAndBlacklist();
            blacklist.setRelevanceId(applyFor.getRelevanceId());
            blacklist.setType(0);
            blacklist.setUserId(applyFor.getApplyId());
            blacklist.setIsDeleted(false);
            blacklist.setCreateBy(userId.toString());
            blacklist.setCreateTime(new Date());
            userFriendAndBlacklistDao.insertTemplate(blacklist);

            UserFriendAndBlacklist andBlacklist = new UserFriendAndBlacklist();
            andBlacklist.setRelevanceId(applyFor.getApplyId());
            andBlacklist.setType(0);
            andBlacklist.setUserId(applyFor.getRelevanceId());
            andBlacklist.setIsDeleted(false);
            andBlacklist.setCreateBy(userId.toString());
            andBlacklist.setCreateTime(new Date());
            userFriendAndBlacklistDao.insertTemplate(andBlacklist);
            return GlobalReponse.success("已同意");
        } else if (status == 2) {
            applyFor.setStatus(2);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            return GlobalReponse.success("已拒绝");
        }
        return GlobalReponse.fail("提交失败");
    }

    /**
     * 通过申请人的手机号同意或拒绝好友申请 status 1同意 2拒绝
     *
     * @param userId
     * @param mobile
     * @param status
     * @return
     */
    @Override
    public GlobalReponse agreeByMobile(Long userId, String mobile, Integer status) {
        User user = userDao.createLambdaQuery().andEq(User::getMobile, mobile).andEq(User::getIsDeleted, false).single();
        ApplyFor aFor = new ApplyFor();
        aFor.setApplyId(user.getId());
        aFor.setRelevanceId(userId);
        aFor.setIsDeleted(false);
        aFor.setType(0);
        aFor.setStatus(0);
        ApplyFor applyFor = applyForDao.templateOne(aFor);
        if (status == 1) {
            applyFor.setStatus(1);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            UserFriendAndBlacklist blacklist = new UserFriendAndBlacklist();
            blacklist.setRelevanceId(applyFor.getRelevanceId());
            blacklist.setType(0);
            blacklist.setUserId(applyFor.getApplyId());
            blacklist.setIsDeleted(false);
            blacklist.setCreateBy(userId.toString());
            blacklist.setCreateTime(new Date());
            userFriendAndBlacklistDao.insertTemplate(blacklist);

            UserFriendAndBlacklist andBlacklist = new UserFriendAndBlacklist();
            andBlacklist.setRelevanceId(applyFor.getApplyId());
            andBlacklist.setType(0);
            andBlacklist.setUserId(applyFor.getRelevanceId());
            andBlacklist.setIsDeleted(false);
            andBlacklist.setCreateBy(userId.toString());
            andBlacklist.setCreateTime(new Date());
            userFriendAndBlacklistDao.insertTemplate(andBlacklist);
            return GlobalReponse.success("已同意");
        } else if (status == 2) {
            applyFor.setStatus(2);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            return GlobalReponse.success("已拒绝");
        }
        return GlobalReponse.fail("提交失败");
    }

    /**
     * 获取加群消息列表
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse<NewGroupVo> getNewGroupList(Long userId) {
        List<NewGroupVo> vos = new ArrayList<>();
        ApplyFor applyFor = new ApplyFor();
        applyFor.setApplyId(userId);
        applyFor.setType(1);
        List<ApplyFor> zFors = applyForDao.getNewGroup(applyFor);
        if (!zFors.isEmpty()) {
            for (ApplyFor zFor : zFors) {
                ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, zFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                if (single != null) {
                    if (single.getIsRead() == null || single.getIsRead() == 0) {
                        single.setIsRead(1);
                        single.setUpdateTime(new Date());
                        single.setUpdateBy(userId.toString());
                        applyForDeletedDao.updateTemplateById(single);
                    }
                    if (single.getIsDeletedApply() != null && single.getIsDeletedApply() == 1) {
                        continue;
                    }
                } else {
                    ApplyForDeleted forDeleted = new ApplyForDeleted();
                    forDeleted.setApplyTableId(zFor.getId());
                    forDeleted.setUserId(userId);
                    forDeleted.setIsRead(1);
                    forDeleted.setIsDeletedApply(0);
                    forDeleted.setCreateTime(new Date());
                    forDeleted.setCreateBy(userId.toString());
                    applyForDeletedDao.insertTemplate(forDeleted);
                }
                NewGroupVo vo = new NewGroupVo();
                vo.setId(zFor.getId());
                GroupChat chat = groupChatDao.unique(zFor.getRelevanceId());
                vo.setApply(0);
                vo.setGroupId(chat.getId());
                vo.setGroupNum(chat.getGroupNum());
                vo.setTencentId(chat.getId().toString());
                if (chat.getHead() != null) {
                    vo.setGroupImage(chat.getHead());
                } else {
                    String head = "";
                    List<GroupMembers> groupMembers = groupMembersDao.createLambdaQuery().andEq(GroupMembers::getGroupChatId, chat.getId()).andEq(GroupMembers::getIsDeleted, 0).desc(GroupMembers::getType).limit(1, 9).select();
                    for (GroupMembers groupMember : groupMembers) {
                        User unique = userDao.unique(groupMember.getUserId());
                        if (unique.getHeadImage() != null) {
                            if (head.equals("")) {
                                head = unique.getHeadImage();
                            } else {
                                head = head + "," + unique.getHeadImage();
                            }
                        }
                    }
                    vo.setGroupImage(head);
                }
                vo.setGroupName(chat.getName());
                vo.setStatus(zFor.getStatus());
                vo.setApplyRemark(zFor.getApplyRemark());
                vo.setCreateTime(zFor.getCreateTime());

                User user = userDao.unique(userId);
                vo.setUserId(user.getId());
                vo.setUserNickname(user.getName());
                vo.setUserImage(user.getHeadImage());
                vo.setUserType(user.getUserType());
                vo.setUserMobile(user.getMobile());
                vos.add(vo);
            }
        }

        applyFor.setApplyId(null);
        List<ApplyFor> bFors = applyForDao.getNewGroup(applyFor);
        List<Long> rIds = bFors.stream().map(ApplyFor::getRelevanceId).distinct().collect(Collectors.toList());
        List<Integer> list = Arrays.asList(1, 2);
        for (ApplyFor bFor : bFors) {
            for (Long rId : rIds) {
                GroupMembers single = groupMembersDao.createLambdaQuery().andEq(GroupMembers::getUserId, userId).andEq(GroupMembers::getGroupChatId, rId).andIn(GroupMembers::getType, list).andEq(GroupMembers::getIsDeleted, false).single();
                if (single != null && bFor.getRelevanceId().equals(single.getGroupChatId())) {
                    ApplyForDeleted apply = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, bFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                    if (apply != null) {
                        if (apply.getIsRead() == null || apply.getIsRead() == 0) {
                            apply.setIsRead(1);
                            apply.setUpdateTime(new Date());
                            apply.setUpdateBy(userId.toString());
                            applyForDeletedDao.updateTemplateById(apply);
                        }
                        if (apply.getIsDeletedApply() != null && apply.getIsDeletedApply() == 1) {
                            continue;
                        }
                    } else {
                        ApplyForDeleted forDeleted = new ApplyForDeleted();
                        forDeleted.setApplyTableId(bFor.getId());
                        forDeleted.setUserId(userId);
                        forDeleted.setIsRead(1);
                        forDeleted.setIsDeletedApply(0);
                        forDeleted.setCreateTime(new Date());
                        forDeleted.setCreateBy(userId.toString());
                        applyForDeletedDao.insertTemplate(forDeleted);
                    }
                    NewGroupVo vo = new NewGroupVo();
                    vo.setId(bFor.getId());
                    GroupChat groupChat = groupChatDao.createLambdaQuery().andEq(GroupChat::getId, single.getGroupChatId()).andEq(GroupChat::getIsDeleted, false).single();
                    User user = userDao.unique(bFor.getApplyId());
                    vo.setCreateTime(bFor.getCreateTime());
                    vo.setApply(1);
                    vo.setUserId(user.getId());
                    vo.setUserNickname(user.getName());
                    vo.setUserImage(user.getHeadImage());
                    vo.setUserType(user.getUserType());
                    vo.setUserMobile(user.getMobile());
                    vo.setGroupId(groupChat.getId());
                    if (groupChat.getHead() != null) {
                        vo.setGroupImage(groupChat.getHead());
                    } else {
                        String head = "";
                        List<GroupMembers> groupMembers = groupMembersDao.createLambdaQuery().andEq(GroupMembers::getGroupChatId, groupChat.getId()).andEq(GroupMembers::getIsDeleted, 0).desc(GroupMembers::getType).limit(1, 9).select();
                        for (GroupMembers groupMember : groupMembers) {
                            User unique = userDao.unique(groupMember.getUserId());
                            if (unique.getHeadImage() != null) {
                                if (head.equals("")) {
                                    head = unique.getHeadImage();
                                } else {
                                    head = head + "," + unique.getHeadImage();
                                }
                            }
                        }
                        vo.setGroupImage(head);
                    }
                    vo.setGroupNum(groupChat.getGroupNum());
                    vo.setTencentId(groupChat.getId().toString());
                    vo.setGroupId(groupChat.getId());
                    vo.setGroupName(groupChat.getName());
                    vo.setStatus(bFor.getStatus());
                    vo.setApplyRemark(bFor.getApplyRemark());
                    vos.add(vo);
                }
            }
        }
        List<KickOut> kickOuts = kickOutDao.createLambdaQuery().andEq(KickOut::getUserId, userId).andEq(KickOut::getType, 1).andEq(KickOut::getIsDeleted, false).select();
        if (!kickOuts.isEmpty()) {
            for (KickOut kickOut : kickOuts) {
                kickOut.setIsRead(1);
                kickOut.setUpdateTime(new Date());
                kickOut.setUpdateBy(userId.toString());
                kickOutDao.updateTemplateById(kickOut);
                NewGroupVo vo = new NewGroupVo();
                User user = userDao.unique(kickOut.getUserId());
                vo.setId(kickOut.getId());
                vo.setApply(2);
                vo.setUserId(user.getId());
                vo.setUserNickname(user.getName());
                vo.setUserImage(user.getHeadImage());
                vo.setUserType(user.getUserType());
                vo.setUserMobile(user.getMobile());
                GroupChat groupChat = groupChatDao.unique(kickOut.getRelevanceId());
                if (groupChat.getHead() != null) {
                    vo.setGroupImage(groupChat.getHead());
                } else {
                    String head = "";
                    List<GroupMembers> groupMembers = groupMembersDao.createLambdaQuery().andEq(GroupMembers::getGroupChatId, groupChat.getId()).andEq(GroupMembers::getIsDeleted, 0).desc(GroupMembers::getType).limit(1, 9).select();
                    for (GroupMembers groupMember : groupMembers) {
                        User unique = userDao.unique(groupMember.getUserId());
                        if (unique.getHeadImage() != null) {
                            if (head.equals("")) {
                                head = unique.getHeadImage();
                            } else {
                                head = head + "," + unique.getHeadImage();
                            }
                        }
                    }
                    vo.setGroupImage(head);
                }
                vo.setTencentId(groupChat.getTencentId());
                vo.setGroupId(groupChat.getId());
                vo.setGroupName(groupChat.getName());
                User kickUser = userDao.unique(kickOut.getCreateBy());
                vo.setHandlerId(kickUser.getId());
                vo.setHandlerImage(kickUser.getHeadImage());
                vo.setHandlerNickname(kickUser.getName());
                vo.setHandlerType(kickUser.getUserType());
                vo.setHandlerMobile(kickUser.getMobile());
                vo.setCreateTime(kickOut.getCreateTime());
                vos.add(vo);
            }
        }
        List<InviteAddGroup> inviteAddGroups = inviteAddGroupDao.getInviteAddGroup(userId);
        for (InviteAddGroup addGroup : inviteAddGroups) {
            addGroup.setIsRead(1);
            addGroup.setUpdateTime(new Date());
            addGroup.setUpdateBy(userId.toString());
            inviteAddGroupDao.updateTemplateById(addGroup);
            NewGroupVo vo = new NewGroupVo();
            User user = userDao.unique(addGroup.getInviteeId());
            vo.setId(addGroup.getId());
            vo.setApply(3);
            vo.setStatus(addGroup.getStatus());
            vo.setUserId(user.getId());
            vo.setUserNickname(user.getName());
            vo.setUserImage(user.getHeadImage());
            vo.setUserType(user.getUserType());
            vo.setUserMobile(user.getMobile());
            GroupChat groupChat = groupChatDao.unique(addGroup.getGroupId());
            if (groupChat.getHead() != null) {
                vo.setGroupImage(groupChat.getHead());
            } else {
                String head = "";
                List<GroupMembers> groupMembers = groupMembersDao.createLambdaQuery().andEq(GroupMembers::getGroupChatId, groupChat.getId()).andEq(GroupMembers::getIsDeleted, 0).desc(GroupMembers::getType).limit(1, 9).select();
                for (GroupMembers groupMember : groupMembers) {
                    User unique = userDao.unique(groupMember.getUserId());
                    if (unique.getHeadImage() != null) {
                        if (head.equals("")) {
                            head = unique.getHeadImage();
                        } else {
                            head = head + "," + unique.getHeadImage();
                        }
                    }
                }
                vo.setGroupImage(head);
            }
            vo.setTencentId(groupChat.getTencentId());
            vo.setGroupId(groupChat.getId());
            vo.setGroupNum(groupChat.getGroupNum());
            vo.setGroupName(groupChat.getName());
            User unique = userDao.unique(addGroup.getInviterId());
            vo.setHandlerId(unique.getId());
            vo.setHandlerImage(unique.getHeadImage());
            vo.setHandlerNickname(unique.getName());
            vo.setHandlerMobile(unique.getMobile());
            vo.setHandlerType(unique.getUserType());
            vo.setCreateTime(addGroup.getCreateTime());
            vos.add(vo);
        }
        vos.sort(Comparator.comparing(NewGroupVo::getCreateTime).reversed());
        return GlobalReponse.success(vos);
    }

    /**
     * 删除加群消息
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedNewGroup(Long id, Long userId, Integer apply) {
        if (apply.equals(1) || apply.equals(0)) {
            ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, id).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
            if (single != null) {
                single.setIsDeletedApply(1);
                single.setUpdateBy(userId.toString());
                single.setUpdateTime(new Date());
                applyForDeletedDao.updateTemplateById(single);
                return GlobalReponse.success("删除成功");
            }
            ApplyForDeleted forDeleted = new ApplyForDeleted();
            forDeleted.setApplyTableId(id);
            forDeleted.setUserId(userId);
            forDeleted.setIsRead(1);
            forDeleted.setIsDeletedApply(1);
            forDeleted.setCreateTime(new Date());
            forDeleted.setCreateBy(userId.toString());
            forDeleted.setIsDeleted(false);
            applyForDeletedDao.insertTemplate(forDeleted);
            return GlobalReponse.success("删除成功");
        }
        if (apply.equals(2)) {
            KickOut kickOut = kickOutDao.unique(id);
            kickOut.setIsDeleted(true);
            kickOut.setUpdateTime(new Date());
            kickOut.setUpdateBy(userId.toString());
            kickOutDao.updateTemplateById(kickOut);
            return GlobalReponse.success("删除成功");
        }
        if (apply.equals(3)) {
            InviteAddGroup addGroup = inviteAddGroupDao.unique(id);
            addGroup.setIsDeleted(true);
            addGroup.setUpdateTime(new Date());
            addGroup.setUpdateBy(userId.toString());
            inviteAddGroupDao.updateTemplateById(addGroup);
            return GlobalReponse.success("删除成功");
        }
        return GlobalReponse.fail("删除失败");
    }

    /**
     * 同意或拒绝群聊申请 status 1同意 2拒绝
     *
     * @param userId
     * @param id
     * @param status
     * @return
     */
    @Override
    public GlobalReponse agreeOrRefuseApplyGroup(Long userId, Long id, Integer status) {
        if (status.equals(3)) {
            InviteAddGroup addGroup = inviteAddGroupDao.unique(id);
            addGroup.setStatus(1);
            addGroup.setUpdateTime(new Date());
            addGroup.setUpdateBy(userId.toString());
            inviteAddGroupDao.updateTemplateById(addGroup);
            return GlobalReponse.success("已忽略");
        }
        ApplyFor applyFor = applyForDao.unique(id);
        if (status.equals(1)) {
            List<CreateGroupReturn> returns = tencentUtil.addGroupMember(applyFor.getRelevanceId(), applyFor.getApplyId().toString(), 0);
            for (CreateGroupReturn aReturn : returns) {
                if (aReturn.getResult().equals("1")) {
                    User user = userDao.unique(applyFor.getApplyId());
                    applyFor.setStatus(1);
                    applyFor.setUpdateBy(userId.toString());
                    applyFor.setUpdateTime(new Date());
                    applyForDao.updateTemplateById(applyFor);
                    GroupMembers groupMembers = new GroupMembers();
                    groupMembers.setGroupChatId(applyFor.getRelevanceId());
                    groupMembers.setUserId(applyFor.getApplyId());
                    groupMembers.setNickName(user.getName());
                    groupMembers.setType(0);
                    groupMembers.setCreateTime(new Date());
                    groupMembers.setCreateBy(userId.toString());
                    groupMembers.setIsDeleted(false);
                    groupMembersDao.insertTemplate(groupMembers);
                    UserFriendAndBlacklist blacklist = new UserFriendAndBlacklist();
                    blacklist.setRelevanceId(applyFor.getRelevanceId());
                    blacklist.setType(1);
                    blacklist.setUserId(applyFor.getApplyId());
                    blacklist.setIsDeleted(false);
                    blacklist.setCreateBy(userId.toString());
                    blacklist.setCreateTime(new Date());
                    userFriendAndBlacklistDao.insertTemplate(blacklist);
                    return GlobalReponse.success("已同意");
                }
            }
        } else if (status.equals(2)) {
            applyFor.setStatus(2);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            return GlobalReponse.success("已拒绝");
        }
        return GlobalReponse.fail("提交失败");
    }

    /**
     * 获取班级消息教师端
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse<NewClassMessageVo> getClassMessageTeacher(Long userId) {
        User unique = userDao.unique(userId);
        if (unique.getUserType().equals(2)) {
            List<NewClassMessageVo> vos = new ArrayList<>();
            //我申请加入其他的班级
            ApplyFor applyFor = new ApplyFor();
            applyFor.setApplyId(userId);
            applyFor.setType(2);
            List<ApplyFor> zFors = applyForDao.getNewClass(applyFor);
            if (!zFors.isEmpty()) {
                for (ApplyFor zFor : zFors) {
                    ApplyForDeleted apply = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, zFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                    if (apply != null) {
                        if (apply.getIsRead() == null || apply.getIsRead() == 0) ;
                        {
                            apply.setIsRead(1);
                            apply.setUpdateTime(new Date());
                            apply.setUpdateBy(userId.toString());
                            applyForDeletedDao.updateTemplateById(apply);
                        }
                        if (apply.getIsDeletedApply() != null && apply.getIsDeletedApply() == 1) {
                            continue;
                        }
                    } else {
                        ApplyForDeleted forDeleted = new ApplyForDeleted();
                        forDeleted.setApplyTableId(zFor.getId());
                        forDeleted.setUserId(userId);
                        forDeleted.setIsRead(1);
                        forDeleted.setIsDeletedApply(0);
                        forDeleted.setCreateTime(new Date());
                        forDeleted.setCreateBy(userId.toString());
                        applyForDeletedDao.insertTemplate(forDeleted);
                    }
                    NewClassMessageVo vo = new NewClassMessageVo();
                    vo.setId(zFor.getId());
                    vo.setApply(0);
                    User user = userDao.unique(zFor.getApplyId());
                    vo.setUserId(user.getId());
                    vo.setUserNickname(user.getName());
                    vo.setUserImage(user.getHeadImage());
                    vo.setUserType(user.getUserType());
                    vo.setUserMobile(user.getMobile());
                    ClassAnd classAnd = classAndDao.unique(zFor.getRelevanceId());
                    List<ClassMembers> members = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId, classAnd.getId()).andEq(ClassMembers::getIsDeleted, false).limit(1, 9).select();
                    List<Long> collect = members.stream().map(ClassMembers::getUserId).collect(Collectors.toList());
                    if (collect.size() > 0) {
                        List<User> users = userDao.createLambdaQuery().andEq(User::getIsDeleted, false).andIn(User::getId, collect).select();
                        List<String> images = users.stream().map(User::getHeadImage).collect(Collectors.toList());
                        vo.setClassImage(images);
                    }
                    vo.setClassId(classAnd.getId());
                    vo.setClassName(classAnd.getName());
                    vo.setStatus(zFor.getStatus());
                    vo.setApplyRemark(zFor.getApplyRemark());
                    vos.add(vo);
                }
            }
            //其他人申请加入我创建的班级
            List<ClassAnd> ands = classAndDao.createLambdaQuery().andEq(ClassAnd::getCreateBy, userId).andEq(ClassAnd::getIsDeleted, false).select();
            if (ands.size() > 0) {
                for (ClassAnd and : ands) {
                    applyFor.setApplyId(null);
                    applyFor.setRelevanceId(and.getId());
                    List<ApplyFor> bFors = applyForDao.getNewClass(applyFor);
                    for (ApplyFor bFor : bFors) {
                        ApplyForDeleted apply = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, bFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
                        if (apply != null) {
                            if (apply.getIsRead() == null || apply.getIsRead() == 0) ;
                            {
                                apply.setIsRead(1);
                                apply.setUpdateTime(new Date());
                                apply.setUpdateBy(userId.toString());
                                applyForDeletedDao.updateTemplateById(apply);
                            }
                            if (apply.getIsDeletedApply() == 1) {
                                continue;
                            }
                        } else {
                            ApplyForDeleted forDeleted = new ApplyForDeleted();
                            forDeleted.setApplyTableId(bFor.getId());
                            forDeleted.setUserId(userId);
                            forDeleted.setIsRead(1);
                            forDeleted.setIsDeletedApply(0);
                            forDeleted.setCreateTime(new Date());
                            forDeleted.setCreateBy(userId.toString());
                            applyForDeletedDao.insertTemplate(forDeleted);
                        }
                        NewClassMessageVo vo = new NewClassMessageVo();
                        vo.setId(bFor.getId());
                        vo.setApply(1);
                        User user = userDao.unique(bFor.getApplyId());
                        vo.setUserId(user.getId());
                        vo.setUserNickname(user.getName());
                        vo.setUserImage(user.getHeadImage());
                        vo.setUserType(user.getUserType());
                        vo.setUserMobile(user.getMobile());
                        ClassAnd classAnd = classAndDao.unique(bFor.getRelevanceId());
                        List<ClassMembers> members = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId, classAnd.getId()).andEq(ClassMembers::getIsDeleted, false).limit(1, 9).select();
                        List<Long> collect = members.stream().map(ClassMembers::getUserId).collect(Collectors.toList());
                        if (collect.size() > 0) {
                            List<User> users = userDao.createLambdaQuery().andEq(User::getIsDeleted, false).andIn(User::getId, collect).select();
                            List<String> images = users.stream().map(User::getHeadImage).collect(Collectors.toList());
                            vo.setClassImage(images);
                        }
                        vo.setClassId(classAnd.getId());
                        vo.setClassName(classAnd.getName());
                        vo.setStatus(bFor.getStatus());
                        vo.setApplyRemark(bFor.getApplyRemark());
                        vos.add(vo);
                    }
                }
            }
            //我被移出班级
            KickOut out = new KickOut();
            out.setUserId(userId);
            out.setType(2);
            List<KickOut> kickOuts = kickOutDao.outClass(out);
            for (KickOut kickOut : kickOuts) {
                kickOut.setIsRead(1);
                kickOut.setUpdateTime(new Date());
                kickOut.setUpdateBy(userId.toString());
                kickOutDao.updateTemplateById(kickOut);
                NewClassMessageVo vo = new NewClassMessageVo();
                User user = userDao.unique(kickOut.getUserId());
                vo.setId(kickOut.getId());
                vo.setApply(2);
                vo.setUserId(user.getId());
                vo.setUserNickname(user.getName());
                vo.setUserImage(user.getHeadImage());
                vo.setUserMobile(user.getMobile());
                ClassAnd classAnd = classAndDao.unique(kickOut.getRelevanceId());
                List<ClassMembers> members = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId, classAnd.getId()).andEq(ClassMembers::getIsDeleted, false).limit(1, 9).select();
                List<Long> collect = members.stream().map(ClassMembers::getUserId).collect(Collectors.toList());
                if (collect.size() > 0) {
                    List<User> users = userDao.createLambdaQuery().andEq(User::getIsDeleted, false).andIn(User::getId, collect).select();
                    List<String> images = users.stream().map(User::getHeadImage).collect(Collectors.toList());
                    vo.setClassImage(images);
                }
                vo.setClassId(classAnd.getId());
                vo.setClassName(classAnd.getName());
                User kickUser = userDao.unique(kickOut.getCreateBy());
                vo.setHandlerId(kickUser.getId());
                vo.setHandlerImage(kickUser.getHeadImage());
                vo.setHandlerNickname(kickUser.getName());
                vo.setHandlerType(kickUser.getUserType());
                vo.setHandlerMobile(kickUser.getMobile());
                vos.add(vo);
            }
            //学生补交作业
            JobBack jobBack = new JobBack();
            jobBack.setCreateBy(userId.toString());
            jobBack.setStatus(1);
            List<JobBack> jobBacks = jobBackDao.getStudentJobBack(jobBack);
            for (JobBack back : jobBacks) {
                JobBackDeleted single = jobBackDeletedDao.createLambdaQuery().andEq(JobBackDeleted::getJobBackId, back.getId()).andEq(JobBackDeleted::getUserId, userId).andEq(JobBackDeleted::getIsDeleted, false).single();
                if (single != null) {
                    if (single.getIsRead() == null || single.getIsRead() == 0) {
                        single.setIsRead(1);
                        single.setUpdateTime(new Date());
                        single.setUpdateBy(userId.toString());
                        jobBackDeletedDao.updateTemplateById(single);
                    }
                    if (single.getIsDeletedApply() == 1) {
                        continue;
                    }
                } else {
                    JobBackDeleted backDeleted = new JobBackDeleted();
                    backDeleted.setJobBackId(back.getId());
                    backDeleted.setUserId(userId);
                    backDeleted.setIsRead(1);
                    backDeleted.setIsDeletedApply(0);
                    backDeleted.setCreateTime(new Date());
                    backDeleted.setCreateBy(userId.toString());
                    jobBackDeletedDao.insertTemplate(backDeleted);
                }
                NewClassMessageVo vo = new NewClassMessageVo();
                User user = userDao.unique(back.getUserId());
                vo.setId(back.getId());
                vo.setApply(4);
                vo.setUserId(user.getId());
                vo.setUserNickname(user.getName());
                vo.setUserImage(user.getHeadImage());
                vo.setStatus(back.getStatus());
                vo.setUserType(user.getUserType());
                vo.setUserMobile(user.getMobile());
                Job job = jobDao.unique(back.getJobId());
                vo.setJobId(job.getId());
                vo.setDeadlineTime(job.getDeadline().getTime());
                if (new Date().getTime() > job.getDeadline().getTime()) {
                    vo.setIsDeadline(true);
                } else {
                    vo.setIsDeadline(false);
                }
                vo.setJobName(job.getName());
                Subject subject = subjectDao.unique(job.getSubjectId());
                vo.setJobSubject(subject.getName());
                vos.add(vo);
            }
            return GlobalReponse.success(vos);
        }

        List<NewClassMessageVo> vos = new ArrayList<>();
        //我申请加入其他的班级
        ApplyFor applyFor = new ApplyFor();
        applyFor.setApplyId(userId);
        applyFor.setType(2);
        List<ApplyFor> applyFors = applyForDao.getNewClass(applyFor);
        for (ApplyFor aFor : applyFors) {
            ApplyForDeleted apply = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, aFor.getId()).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
            if (apply != null) {
                if (apply.getIsRead() == null || apply.getIsRead() == 0){
                    apply.setIsRead(1);
                    apply.setUpdateTime(new Date());
                    apply.setUpdateBy(userId.toString());
                    applyForDeletedDao.updateTemplateById(apply);
                }
                if (apply.getIsDeletedApply() == 1) {
                    continue;
                }
            } else {
                ApplyForDeleted forDeleted = new ApplyForDeleted();
                forDeleted.setApplyTableId(aFor.getId());
                forDeleted.setUserId(userId);
                forDeleted.setIsRead(1);
                forDeleted.setIsDeletedApply(0);
                forDeleted.setCreateTime(new Date());
                forDeleted.setCreateBy(userId.toString());
                applyForDeletedDao.insertTemplate(forDeleted);
            }
            NewClassMessageVo vo = new NewClassMessageVo();
            User user = userDao.unique(aFor.getApplyId());
            vo.setId(aFor.getId());
            vo.setApply(0);
            vo.setUserId(user.getId());
            vo.setUserNickname(user.getName());
            vo.setUserImage(user.getHeadImage());
            vo.setUserMobile(user.getMobile());
            ClassAnd classAnd = classAndDao.unique(aFor.getRelevanceId());
            List<ClassMembers> members = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId, classAnd.getId()).andEq(ClassMembers::getIsDeleted, false).limit(1, 9).select();
            List<Long> collect = members.stream().map(ClassMembers::getUserId).collect(Collectors.toList());
            if (collect.size() > 0) {
                List<User> users = userDao.createLambdaQuery().andEq(User::getIsDeleted, false).andIn(User::getId, collect).select();
                List<String> images = users.stream().map(User::getHeadImage).collect(Collectors.toList());
                vo.setClassImage(images);
            }
            vo.setStatus(aFor.getStatus());
            vo.setClassId(classAnd.getId());
            vo.setClassName(classAnd.getName());
            vos.add(vo);
        }
        //我被移出班级
        KickOut out = new KickOut();
        out.setUserId(userId);
        out.setType(2);
        List<KickOut> kickOuts = kickOutDao.outClass(out);
        for (KickOut kickOut : kickOuts) {
            NewClassMessageVo vo = new NewClassMessageVo();
            User user = userDao.unique(kickOut.getUserId());
            vo.setId(kickOut.getId());
            vo.setApply(2);
            vo.setUserId(user.getId());
            vo.setUserNickname(user.getName());
            vo.setUserImage(user.getHeadImage());
            vo.setUserMobile(user.getMobile());
            ClassAnd classAnd = classAndDao.unique(kickOut.getRelevanceId());
            List<ClassMembers> members = classMembersDao.createLambdaQuery().andEq(ClassMembers::getClassId, classAnd.getId()).andEq(ClassMembers::getIsDeleted, false).limit(1, 9).select();
            List<Long> collect = members.stream().map(ClassMembers::getUserId).collect(Collectors.toList());
            if (collect.size() > 0) {
                List<User> users = userDao.createLambdaQuery().andEq(User::getIsDeleted, false).andIn(User::getId, collect).select();
                List<String> images = users.stream().map(User::getHeadImage).collect(Collectors.toList());
                vo.setClassImage(images);
            }
            vo.setClassId(classAnd.getId());
            vo.setClassName(classAnd.getName());
            User kickUser = userDao.unique(kickOut.getCreateBy());
            vo.setHandlerId(kickUser.getId());
            vo.setHandlerImage(kickUser.getHeadImage());
            vo.setHandlerNickname(kickUser.getName());
            vo.setHandlerType(kickUser.getUserType());
            vo.setHandlerMobile(kickUser.getMobile());
            vos.add(vo);
        }
        //老师提醒补交作业
        JobBack jobBack = new JobBack();
        jobBack.setUserId(userId);
        jobBack.setStatus(0);
        List<JobBack> jobBacks = jobBackDao.getTeacherJobBack(jobBack);
        for (JobBack back : jobBacks) {
            JobBackDeleted single = jobBackDeletedDao.createLambdaQuery().andEq(JobBackDeleted::getJobBackId, back.getId()).andEq(JobBackDeleted::getUserId, userId).andEq(JobBackDeleted::getIsDeleted, false).single();
            if (single != null) {
                if (single.getIsRead() == null || single.getIsRead() == 0) {
                    single.setIsRead(1);
                    single.setUpdateTime(new Date());
                    single.setUpdateBy(userId.toString());
                    jobBackDeletedDao.updateTemplateById(single);
                }
                if (single.getIsDeletedApply() == 1) {
                    continue;
                }
            } else {
                JobBackDeleted backDeleted = new JobBackDeleted();
                backDeleted.setJobBackId(back.getId());
                backDeleted.setUserId(userId);
                backDeleted.setIsRead(1);
                backDeleted.setIsDeletedApply(0);
                backDeleted.setCreateTime(new Date());
                backDeleted.setCreateBy(userId.toString());
                jobBackDeletedDao.insertTemplate(backDeleted);
            }
            NewClassMessageVo vo = new NewClassMessageVo();
            User user = userDao.unique(back.getCreateBy());
            vo.setId(back.getId());
            vo.setApply(4);
            vo.setJobId(jobBack.getJobId());
            vo.setUserId(user.getId());
            vo.setStatus(back.getStatus());
            vo.setUserNickname(user.getName());
            vo.setUserImage(user.getHeadImage());
            vo.setUserType(user.getUserType());
            vo.setUserMobile(user.getMobile());
            Job job = jobDao.unique(back.getJobId());
            vo.setJobId(job.getId());
            vo.setJobName(job.getName());
            Subject subject = subjectDao.unique(job.getSubjectId());
            vo.setJobSubject(subject.getName());
            vo.setDeadlineTime(job.getDeadline().getTime());
            vos.add(vo);
        }
        return GlobalReponse.success(vos);
    }

    /**
     * 删除班级消息
     *
     * @param id
     * @param userId
     * @param apply
     * @return
     */
    @Override
    public GlobalReponse deletedClassMessage(Long id, Long userId, Integer apply) {
        if (apply.equals(0) || apply.equals(1)) {
            ApplyForDeleted single = applyForDeletedDao.createLambdaQuery().andEq(ApplyForDeleted::getApplyTableId, id).andEq(ApplyForDeleted::getUserId, userId).andEq(ApplyForDeleted::getIsDeleted, false).single();
            if (single != null) {
                single.setIsDeletedApply(1);
                single.setUpdateBy(userId.toString());
                single.setUpdateTime(new Date());
                applyForDeletedDao.updateTemplateById(single);
                return GlobalReponse.success("删除成功");
            }
            ApplyForDeleted forDeleted = new ApplyForDeleted();
            forDeleted.setApplyTableId(id);
            forDeleted.setUserId(userId);
            forDeleted.setIsRead(1);
            forDeleted.setIsDeletedApply(1);
            forDeleted.setCreateTime(new Date());
            forDeleted.setCreateBy(userId.toString());
            forDeleted.setIsDeleted(false);
            applyForDeletedDao.insertTemplate(forDeleted);
            return GlobalReponse.success("删除成功");
        }
        if (apply.equals(2)) {
            KickOut kickOut = kickOutDao.unique(id);
            kickOut.setIsDeleted(true);
            kickOut.setUpdateTime(new Date());
            kickOut.setUpdateBy(userId.toString());
            kickOutDao.updateTemplateById(kickOut);
            return GlobalReponse.success("删除成功");
        }
        if (apply.equals(4)) {
            JobBackDeleted single = jobBackDeletedDao.createLambdaQuery().andEq(JobBackDeleted::getJobBackId, id).andEq(JobBackDeleted::getUserId, userId).andEq(JobBackDeleted::getIsDeleted, false).single();
            if (single != null) {
                single.setIsDeletedApply(1);
                single.setUpdateBy(userId.toString());
                single.setUpdateTime(new Date());
                jobBackDeletedDao.updateTemplateById(single);
            }
            JobBackDeleted backDeleted = new JobBackDeleted();
            backDeleted.setJobBackId(id);
            backDeleted.setUserId(userId);
            backDeleted.setIsRead(1);
            backDeleted.setIsDeletedApply(1);
            backDeleted.setCreateTime(new Date());
            backDeleted.setCreateBy(userId.toString());
            backDeleted.setIsDeleted(false);
            jobBackDeletedDao.insertTemplate(backDeleted);
            return GlobalReponse.success("删除成功");
        }
        return GlobalReponse.fail("删除失败");
    }

    /**
     * 同意或拒绝班级申请 1同意 2拒绝
     *
     * @param userId
     * @param id
     * @param status
     * @return
     */
    @Override
    public GlobalReponse agreeOrRefuseApplyClass(Long userId, Long id, Integer status) {
        ApplyFor applyFor = applyForDao.unique(id);
        if (status == 1) {
            applyFor.setStatus(1);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            User user = userDao.unique(applyFor.getApplyId());
            ClassMembers members = new ClassMembers();
            members.setIsDeleted(false);
            members.setClassId(applyFor.getRelevanceId());
            members.setUserId(applyFor.getApplyId());
            members.setNickname(user.getName());
            members.setType(user.getUserType());
            members.setCreateTime(new Date());
            members.setCreateBy(userId.toString());
            classMembersDao.insertTemplate(members);
            GroupChat groupChat = groupChatDao.createLambdaQuery().andEq(GroupChat::getClassId, applyFor.getRelevanceId()).andEq(GroupChat::getIsDeleted, false).single();
            GroupMembers groupMembers = new GroupMembers();
            groupMembers.setIsDeleted(false);
            groupMembers.setGroupChatId(groupChat.getId());
            groupMembers.setUserId(applyFor.getApplyId());
            groupMembers.setNickName(user.getName());
            groupMembers.setType(user.getUserType());
            groupMembers.setCreateTime(new Date());
            groupMembers.setCreateBy(user.toString());
            groupMembersDao.insertTemplate(groupMembers);
            List<CreateGroupReturn> returns = tencentUtil.addGroupMember(groupChat.getId(), applyFor.getApplyId().toString(), 0);
            for (CreateGroupReturn aReturn : returns) {
                if (aReturn.getResult().equals("1")) {
                    applyFor.setStatus(1);
                    applyFor.setUpdateBy(userId.toString());
                    applyFor.setUpdateTime(new Date());
                    applyForDao.updateTemplateById(applyFor);
                    UserFriendAndBlacklist blacklist = new UserFriendAndBlacklist();
                    blacklist.setRelevanceId(groupChat.getId());
                    blacklist.setType(1);
                    blacklist.setUserId(applyFor.getApplyId());
                    blacklist.setIsDeleted(false);
                    blacklist.setCreateBy(userId.toString());
                    blacklist.setCreateTime(new Date());
                    userFriendAndBlacklistDao.insertTemplate(blacklist);
                    return GlobalReponse.success("已同意").setData(groupChat.getTencentId()).setData1(user);
                } else if (aReturn.getResult().equals("0")) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return GlobalReponse.fail("提交失败");
                }
            }
        } else if (status == 2) {
            applyFor.setStatus(2);
            applyFor.setUpdateBy(userId.toString());
            applyFor.setUpdateTime(new Date());
            applyForDao.updateTemplateById(applyFor);
            return GlobalReponse.success("已拒绝");
        }
        return GlobalReponse.fail("提交失败");
    }

    /**
     * 获取互动消息
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse<VideoFriendVo> getInteractMessage(Long userId) {
        List<VideoFriend> friends = videoFriendDao.getInteractMessage(userId);
        ArrayList<VideoFriendVo> vos = new ArrayList<>();
        for (VideoFriend friend : friends) {
            friend.setIsRead(1);
            friend.setUpdateTime(new Date());
            friend.setUpdateBy(userId.toString());
            videoFriendDao.updateTemplateById(friend);
            VideoFriendVo vo = new VideoFriendVo();
            vo.setId(friend.getId());
            Video video = videoDao.unique(friend.getVideoId());
            vo.setVideoId(video.getId());
            vo.setContent(video.getCityCode());
            vo.setImage(video.getImage());
            User user = userDao.unique(friend.getCreateBy());
            vo.setUserId(user.getId());
            vo.setUserImage(user.getHeadImage());
            vo.setUserNickname(user.getName());
            vo.setUserMobile(user.getMobile());
            vos.add(vo);
        }
        return GlobalReponse.success(vos);
    }

    /**
     * 删除互动消息
     *
     * @param userId
     * @return
     */
    @Override
    public GlobalReponse deletedInteractMessage(Long id, Long userId) {
        VideoFriend friends = videoFriendDao.unique(id);
        friends.setIsDeleted(true);
        friends.setUpdateBy(userId.toString());
        friends.setUpdateTime(new Date());
        videoFriendDao.updateTemplateById(friends);
        return GlobalReponse.success("删除成功");
    }
}
