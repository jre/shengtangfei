package com.boruan.shengtangfeng.api.jwt.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @program: tutuyou-parent
 * @description: 用户端验证身份，如果身份没有通过，抛异常
 * @author: lihaicheng
 * @create: 2019-10-15 15:00
 **/
public class DriverVerificationException extends AuthenticationException {

	private static final long serialVersionUID = -4228001791444665538L;

	public DriverVerificationException(String message) {
		super(message);
	}
}
