package com.boruan.shengtangfeng.api.controller;



import java.util.Date;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;
import com.boruan.shengtangfeng.core.dao.IVideoDao;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.utils.ALiYunUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.boruan.shengtangfeng.api.jwt.UserSubject;
import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.service.impl.OssServiceImpl;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.OssPolicyResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;

/**
 * Oss相关操作接口
 */
@RestController
@Api(tags = {"NO:12 OSS接口"}, description = "Oss管理")
@RequestMapping("/aliyun/oss")
public class OssController {
    @Autowired
    private OssServiceImpl ossService;
    @Autowired
    private IVideoDao videoDao;
    @Autowired
    private ALiYunUtils aLiYunUtils;

    @ApiOperation(value = "NO:12-1 oss web上传签名生成")
    @RequestMapping(value = "/policy", method = RequestMethod.GET)
    @ResponseBody
    public GlobalReponse<OssPolicyResult> policy() {
        OssPolicyResult result = ossService.policy();
        return GlobalReponse.success(result);
    }

    @ApiOperation(value = "NO:12-2 oss-sts，ios，安卓")
    @RequestMapping(value = "/sts", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> sts() {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Map<String, String> sts = ossService.getSts(userSubject.getId());
        return sts;
    }

    @PostMapping("/uploadImage")
    @ApiOperation(value = "NO 12-2 oss图片上传", notes = "单个图片上传，返回地址")
    @ResponseBody
    public GlobalReponse<String> uploadImage(@RequestParam("file") MultipartFile file) {
        return ossService.uploadFile(file);
    }

    @PostMapping("/uploadImages")
    @ApiOperation(value = "NO 12-3 oss多图片上传", notes = "多图片上传，返回逗号分隔的地址")
    @ResponseBody
    public GlobalReponse<String> uploadImages(@RequestParam("file") MultipartFile[] file) {
        return ossService.uploadFiles(file);
    }

    @PostMapping("/aliYunVideoAuditReturn")
    @ApiOperation(value = "aliyun视频检测回调")
    public GlobalReponse aliYunVideoAuditReturn(HttpServletRequest httpServletRequest) {
        try {
            String content = httpServletRequest.getParameter("content");
            JSONObject jsonObject = JSONObject.parseObject(content);
            String code = jsonObject.getString("code");
            if (code.equals("200")){
                String taskId = jsonObject.getString("taskId");
                Video video = videoDao.createLambdaQuery().andEq(Video::getTaskId, taskId).single();
                String suggestion = "";
                for (Object results : JSONObject.parseArray(jsonObject.getString("results"))) {
                    suggestion = JSONObject.parseObject(results.toString()).getString("suggestion");
                }
                if (video != null && !suggestion.equals("") && suggestion.equals("pass")){
                    video.setStatus(1);
                    video.setUpdateTime(new Date());
                    videoDao.updateTemplateById(video);
                }else if (video != null && !suggestion.equals("") && suggestion.equals("block")){
                    video.setStatus(2);
                    video.setUpdateTime(new Date());
                    videoDao.updateTemplateById(video);
                }
                return GlobalReponse.success("200");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return GlobalReponse.fail();
    }

    @GetMapping("/test")
    @ApiOperation(value = "NO99")
    public void test() {
        String url = "http://1400100725.vod2.myqcloud.com/8b7d5993vodgzp1400100725/c49e2533387702292246654920/j65fD7fiiB4A.mp4";
        aLiYunUtils.aliyunVideoSyncCheck(url, 10207L);
    }

}
