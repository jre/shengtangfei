package com.boruan.shengtangfeng.api.controller;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.tencentcloudapi.vm.v20201229.models.TaskData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/api/notify")
@Api(value = "回调",tags = "回调接口")
public class NotifyApi {

    @PostMapping("/vmnotify")
    @ApiOperation(value = "视频审核任务回调")
    public GlobalReponse vmNotify(TaskData taskData){

        return GlobalReponse.success();
    }
}
