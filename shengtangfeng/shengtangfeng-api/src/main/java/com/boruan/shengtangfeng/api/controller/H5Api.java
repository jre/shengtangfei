package com.boruan.shengtangfeng.api.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.core.entity.Accolade;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.ArticleComment;
import com.boruan.shengtangfeng.core.entity.Attention;
import com.boruan.shengtangfeng.core.entity.Favorites;
import com.boruan.shengtangfeng.core.entity.Question;
import com.boruan.shengtangfeng.core.entity.QuestionComment;
import com.boruan.shengtangfeng.core.entity.Video;
import com.boruan.shengtangfeng.core.entity.VideoComment;
import com.boruan.shengtangfeng.core.service.IArticleCommentService;
import com.boruan.shengtangfeng.core.service.IArticleService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.IQuestionCommentService;
import com.boruan.shengtangfeng.core.service.IQuestionService;
import com.boruan.shengtangfeng.core.service.ISearchWordsService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.service.IVideoCommentService;
import com.boruan.shengtangfeng.core.service.IVideoService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ArticleCommentVo;
import com.boruan.shengtangfeng.core.vo.ArticleVo;
import com.boruan.shengtangfeng.core.vo.QuestionCommentVo;
import com.boruan.shengtangfeng.core.vo.QuestionVo;
import com.boruan.shengtangfeng.core.vo.VideoCommentVo;
import com.boruan.shengtangfeng.core.vo.VideoVo;
import com.boruan.shengtangfeng.core.vo.mapper.ArticleCommentVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.ArticleVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.QuestionCommentVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.QuestionVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoCommentVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoVoMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author KongXH
 * @title: sss
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1210:50
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/h5")
@Api(value = "", tags = { "NO:9 H5用的相关接口" })
public class H5Api {

    @Autowired
    private IArticleService articleService;
    @Autowired
    private IArticleCommentService articleCommentService;
    @Autowired
    private IVideoCommentService videoCommentService;
    @Autowired
    private IQuestionCommentService questionCommentService;
    @Autowired
    private IVideoService videoService;
    @Autowired
    private IUserService userService;

    @Autowired
    private ILogService sysLogService;
    @Autowired
    private IQuestionService questionService;
    @Autowired
    private ISearchWordsService searchWordsService;



    /**
     * @description:获取文章详情和评论
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @GetMapping("/articleDetail/{articleId}")
    @ApiOperation(value = "NO:9-1 获取文章详情", notes = "")
    public GlobalReponse<ArticleVo> detailArticle(@PathVariable("articleId") Long articleId) {

        Article article = articleService.findById(articleId);
        //获取一次详情加一次阅读量
        article.setReadNum((article.getReadNum()==null?0:article.getReadNum())+1);
        articleService.save(article);
        //是否关注
        Attention attention = null;
        //是否收藏
        Favorites favorites = null;
        //是否点赞
        Accolade accolade = null;
        List<ArticleComment> comments = articleCommentService.findByArticleId(null,articleId, 1);
        List<ArticleCommentVo> commentVos= ArticleCommentVoMapper.MAPPER.toVo(comments);
//        commentVos.forEach(vo->{
//            vo.setThumbUpEd(articleCommentService.isThumbUp(vo.getId(), userSubject.getId()));
//        });

        ArticleVo vo= ArticleVoMapper.MAPPER.toVo(article);
        vo.setComment(commentVos);
        return GlobalReponse.success(vo);
    }
    
    /**
     * @description:获取视频详情和评论
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @GetMapping("/videoDetail/{videoId}")
    @ApiOperation(value = "NO:9-2 获取视频详情", notes = "")
    public GlobalReponse<VideoVo> detailVideo(@PathVariable("videoId") Long videoId) {

        Video video = videoService.findById(videoId);
        //获取一次详情加一次阅读量
        video.setReadNum((video.getReadNum()==null?0:video.getReadNum())+1);
        videoService.save(video);
        //是否关注
        Attention attention = null;
        //是否收藏
        Favorites favorites = null;
        //是否点赞
        Accolade accolade = null;


        List<VideoComment> comments = videoCommentService.findByVideoId(null,videoId, 1);
        List<VideoCommentVo> commentVos= VideoCommentVoMapper.MAPPER.toVo(comments);
//        commentVos.forEach(vo->{
//            vo.setThumbUpEd(videoCommentService.isThumbUp(vo.getId(), userSubject.getId()));
//        });

        VideoVo vo= VideoVoMapper.MAPPER.toVo(video);
        vo.setComment(commentVos);
        return GlobalReponse.success(vo);
    }
    /**
     * @description:获取视频详情和评论
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @GetMapping("/questionDetail/{questionId}")
    @ApiOperation(value = "NO:9-3 获取试题详情", notes = "")
    public GlobalReponse<VideoVo> questionDetail(@PathVariable("questionId") Long questionId) {
        
        Question question = questionService.findById(questionId);
        //是否收藏
        Favorites favorites = null;
        
        List<QuestionComment> comments = questionCommentService.findByQuestionId(questionId, 1,null);
        List<QuestionCommentVo> commentVos= QuestionCommentVoMapper.MAPPER.toVo(comments);
//        commentVos.forEach(vo->{
//            vo.setThumbUpEd(videoCommentService.isThumbUp(vo.getId(), userSubject.getId()));
//        });
        
        QuestionVo vo= QuestionVoMapper.MAPPER.toVo(question);
        vo.setComment(commentVos);
        return GlobalReponse.success(vo);
    }
    
    @RequestMapping("/webhooks")
    public String issueChangeHook(HttpServletRequest request) throws IOException {
        System.out.println("===============================");

        BufferedReader reader = null;
        try {
            reader = request.getReader();
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            System.out.println(result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            reader.close();
        }
        return null;
    }

}
