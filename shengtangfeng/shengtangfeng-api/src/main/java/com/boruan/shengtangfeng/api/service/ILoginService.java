package com.boruan.shengtangfeng.api.service;

import com.boruan.shengtangfeng.core.dto.BindMobileDto;
import com.boruan.shengtangfeng.core.dto.GetMobileCodeDto;
import com.boruan.shengtangfeng.core.dto.PasswordLoginDto;
import com.boruan.shengtangfeng.core.dto.RegisterDto;
import com.boruan.shengtangfeng.core.dto.SmsLoginDto;
import com.boruan.shengtangfeng.core.dto.UpdatePassDto;
import com.boruan.shengtangfeng.core.dto.WxLoginDto;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.LoginResult;
import com.boruan.shengtangfeng.core.vo.UserVo;

/**
 * @author: lihaicheng
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface ILoginService {

	/**
	 * 获取验证码
	 * 
	 * @param mobile
	 * @return
	 */
	public GlobalReponse<String> getVerficationCode(GetMobileCodeDto getMobileCodeDto);

	/**
	 * 校验手机号码是否存在
	 * 
	 * @param mobile
	 * @return
	 */
	public GlobalReponse<Boolean> verificationMobile(String mobile);

	/**
	 * 密码登录
	 * 
	 * @param mobile
	 * @param password
	 * @return
	 */
	public GlobalReponse<LoginResult> passwordLogin(PasswordLoginDto passwordLoginDto);

	/**
	 * 微信登录
	 * 
	 * @param code
	 * @return
	 */
	public GlobalReponse<LoginResult> wxLogin(WxLoginDto wxLoginDto);

	/**
	 * 验证码登录
	 * 
	 * @param mobile
	 * @param authCode
	 * @return
	 */
	public GlobalReponse<LoginResult> smsLogin(SmsLoginDto smsLoginDto);

	/**
	 * 微信登录后绑定手机
	 * 
	 * @param mobile
	 * @param code
	 * @param wxAccessToken
	 * @param openid
	 * @return
	 */
	GlobalReponse<LoginResult> bindingMobile(BindMobileDto bindMobileDto);

	/**
	 * @author: 刘光强
	 * @Description: 更新密码或者忘记密码
	 * @date:2020年3月10日 上午11:14:10
	 */
	public GlobalReponse updatePassword(UpdatePassDto updatePassDto);

	/**
	 * 注册用户
	 * 
	 * @param mobile
	 * @param password
	 * @param authCode
	 * @param openId
	 * @return
	 */
	public GlobalReponse<LoginResult> register(RegisterDto registerDto);
	/**
	 * 注册用户
	 * 
	 * @param mobile
	 * @param authCode
	 * @param userId 推广
	 * @return
	 */
	public GlobalReponse registerH5(String mobile, String authCode, Long spreadId);

	/**
	 * 用户登出
	 * @param userId
	 * @return
	 */
	GlobalReponse logout(Long userId);

	/**
	 * 判断用户是否注册，注册了则加入班级
	 * @param mobile
	 * @param classId
	 * @return
	 */
    GlobalReponse userIsRegistered(String mobile, Long classId);
}
