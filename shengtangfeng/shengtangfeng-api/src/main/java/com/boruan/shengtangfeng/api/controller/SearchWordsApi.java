package com.boruan.shengtangfeng.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.api.jwt.UserSubject;
import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.SearchWords;
import com.boruan.shengtangfeng.core.service.ISearchWordsService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.SearchWordsVo;
import com.boruan.shengtangfeng.core.vo.mapper.SearchWordsVoMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author KongXH
 * @title: sss
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1210:50
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/searchWords")
@Api(value = "", tags = { "NO:7 搜索历史相关接口" })
public class SearchWordsApi {

    @Autowired
    private ISearchWordsService searchWordsService;



    /**
     * @description: 搜索结果页
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/18 13:39
     */
    @GetMapping("/list/{type}")
    @ApiOperation(value = "NO:7-1 获取用户的搜索历史 type  1 试题 2 文章 3 视频 ", notes = "")
    public GlobalReponse<List<SearchWordsVo>> list(@PathVariable Integer type) {
        UserSubject userSubject=JwtUtil.getCurrentJwtUser();
        List<SearchWords> list=searchWordsService.getList(userSubject.getId(), type);
        return GlobalReponse.success(SearchWordsVoMapper.MAPPER.toVo(list));
    }
}
