package com.boruan.shengtangfeng.api.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.api.service.ILoginService;
import com.boruan.shengtangfeng.core.dto.BindMobileDto;
import com.boruan.shengtangfeng.core.dto.GetMobileCodeDto;
import com.boruan.shengtangfeng.core.dto.PasswordLoginDto;
import com.boruan.shengtangfeng.core.dto.RegisterDto;
import com.boruan.shengtangfeng.core.dto.SmsLoginDto;
import com.boruan.shengtangfeng.core.dto.UpdatePassDto;
import com.boruan.shengtangfeng.core.dto.WxLoginDto;
import com.boruan.shengtangfeng.core.service.IUploadFileService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import com.boruan.shengtangfeng.core.vo.LoginResult;
import com.boruan.shengtangfeng.core.vo.UserVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: lihaicheng
 * @Description: 登录
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/login")
@Api(value = "", tags = { "NO:2 登录相关接口" })
public class LoginApi {
	@Autowired
	private ILoginService loginService;
	@Autowired
	private JwtUtil jwtUtil;
	@Value("${jwt.tokenHead}")
	private String tokenHead;
	@Autowired
	private IUserService studentService;
	@Autowired
	private IUploadFileService uploadFileService;

	@PostMapping("/passwordLogin")
	@ApiOperation(value = "NO:2-1 用户名密码登录，time，互踢校验码", notes = "")
	public GlobalReponse<LoginResult> passwordLogin(@RequestBody PasswordLoginDto passwordLoginDto) {
	    if(!passwordLoginDto.checkParam()) {
            return GlobalReponse.fail("用户名或密码不能为空");
        }
		GlobalReponse globalReponse = loginService.passwordLogin(passwordLoginDto);
		return globalReponse;
	}

	@PostMapping("/weChatLogin")
	@ApiOperation(value = "NO:2-2 微信登录授权", notes = "具体返回的各种情况请看LoginResult")
	public GlobalReponse<LoginResult> weChatLogin(HttpServletResponse res,@RequestBody WxLoginDto wxLoginDto) {
	    if(!wxLoginDto.checkParam()) {
            return GlobalReponse.fail("code不能为空");
        }
	    GlobalReponse globalReponse = loginService.wxLogin(wxLoginDto);
		return globalReponse;
	}

	@PostMapping("/smsLogin")
	@ApiOperation(value = "NO:2-3 使用手机+验证码登录，time，互踢校验码", notes = "")
	public GlobalReponse<LoginResult> smsLogin(@RequestBody SmsLoginDto smsLoginDto) {
	    if(!smsLoginDto.checkParam()) {
	        return GlobalReponse.fail("参数不能为空");
	    }
		GlobalReponse globalReponse = loginService.smsLogin(smsLoginDto);
		return globalReponse;
	}

	@PostMapping("/register")
	@ApiOperation(value = "NO:2-4 使用手机+验证码+密码注册，time，互踢校验码", notes = "")
	public GlobalReponse<LoginResult> register(@RequestBody RegisterDto registerDto) {
		if(registerDto.getUserType()==null){
			return GlobalReponse.fail("请选择用户身份");
		}
	    if(!registerDto.checkParam()) {
            return GlobalReponse.fail("手机号不能为空");
        }
        if(!StringUtils.checkPassword(registerDto.getPassword())) {
            return GlobalReponse.fail("密码只能包含数字和大小写字母");
        }
		GlobalReponse globalReponse = loginService.register(registerDto);
		return globalReponse;
	}

	/**
	 * @author: 刘光强
	 * @Description: 获取手机验证码
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping("/getVerficationCode")
	@ApiOperation(value = "NO:2-5 获取手机验证码", notes = "获取手机验证码")
	public GlobalReponse<String> getVerficationCode(@RequestBody GetMobileCodeDto getMobileCodeDto) {
	    if(!getMobileCodeDto.checkParam()) {
            return GlobalReponse.fail("参数不能为空");
        }
		return loginService.getVerficationCode(getMobileCodeDto);
	}

	/**
	 * @author: 刘光强
	 * @Description: 绑定手机号
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping("/bindingMobile")
	@ApiOperation(value = "NO:2-6 微信登录绑定手机号", notes = "绑定手机号,返回code不是1000则会返回错误信息")
	public GlobalReponse<LoginResult> bindingMobile(@RequestBody BindMobileDto bindMobileDto) {
	    if(!bindMobileDto.checkParam()) {
            return GlobalReponse.fail("参数不能为空");
        }
		return loginService.bindingMobile(bindMobileDto);

	}

	/**
	 * @author: 刘光强
	 * @Description: 校验手机号是否存在
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping("/verificationMobile/{mobile}")
	@ApiOperation(value = "NO:2-7 校验手机号是否存在", notes = "返回true 不存在，false存在")
	public GlobalReponse<Boolean> verificationMobile(@PathVariable("mobile") String mobile) {
	    if(StringUtils.isBlank(mobile)) {
            return GlobalReponse.fail("手机号不能为空");
        }
		return loginService.verificationMobile(mobile);

	}
//
//	@ApiOperation("NO:2-4 修改密码")
//	@PostMapping(value = "/updatePassword")
//	public GlobalReponse updatePassword(@RequestParam String mobile, @RequestParam String password,
//			@RequestParam String authCode) {
//		return loginService.updatePassword(mobile, password, authCode);
//	}

	/**
	 * @author: 刘光强
	 * @Description: 修改密码
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@PostMapping(value = "/updatePassword")
	@ApiOperation(value = "NO:2-8 修改密码", notes = "")
	public GlobalReponse updatePassword(@RequestBody UpdatePassDto updatePassDto) {
	    if(!updatePassDto.checkParam()) {
            return GlobalReponse.fail("参数不能为空");
        }
	    if(!StringUtils.checkPassword(updatePassDto.getPassword())) {
	        return GlobalReponse.fail("密码只能包含数字和大小写字母");
	    }
		return loginService.updatePassword(updatePassDto);
	}

//	/**
//	 *
//	 * @author:刘光强
//	 * @Descrition:学生端获取登录者信息
//	 * @date:2020年3月10日 上午11:14:10
//	 * @param id
//	 * @return
//	 */
//	@GetMapping("/getUser")
//	@ApiOperation(value = "NO:2-9 通过id获取用户详情")
//	public GlobalReponse getUser(Long id) {
//		UserVo studentVo = null;
//		try {
//			studentVo = studentService.findById(id);
//		} catch (Exception e) {
//			return GlobalReponse.success("信息获取失败或者该用户不存在");
//		}
//
//		return GlobalReponse.success(studentVo);
//	}

	@PostMapping("/registerH5")
    @ApiOperation(value = "NO:2-9 使用手机+验证码注册", notes = "")
    public GlobalReponse<UserVo> registerH5(String mobile, String authCode,Long spreadId) {
        if(StringUtils.isBlank(mobile)) {
            return GlobalReponse.fail("手机号不能为空");
        }
        return loginService.registerH5(mobile, authCode, spreadId);
    }

	@PostMapping("/logout")
	@ApiOperation(value = "用户登出", notes = "")
	public GlobalReponse logout() {
		Long userId = JwtUtil.getCurrentJwtUser().getId();
		return loginService.logout(userId);
	}

	@PostMapping("/userIsRegistered")
	@ApiOperation(value = "判断用户是否注册，注册了则加入班级", notes = "")
	public GlobalReponse userIsRegistered(String mobile,Long classId) {
		if(StringUtils.isBlank(mobile)) {
			return GlobalReponse.fail("手机号不能为空");
		}
		if(classId==null) {
			return GlobalReponse.fail("班级id不能为空");
		}
		return loginService.userIsRegistered(mobile,classId);
	}

}
