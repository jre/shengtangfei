package com.boruan.shengtangfeng.api.controller;

import java.util.List;
import java.util.Map;

import com.boruan.shengtangfeng.core.vo.MessageNumVo;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.api.jwt.UserSubject;
import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.BasePageSearch;
import com.boruan.shengtangfeng.core.dto.NoticeSearch;
import com.boruan.shengtangfeng.core.service.INoticeService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.NoticeVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: 刘光强
 * @Description: 头条相关接口
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/notice")
@Api(value = "", tags = { "NO:10 通知相关的接口" })
public class NoticeApi {

    @Autowired
    private INoticeService noticeService;
    @Autowired
    private SQLManager sqlManager;
    
    @PostMapping("/pageNotice")
    @ApiOperation(value = "NO:10-1 分页获取通知")
    public GlobalReponse<PageQuery<NoticeVo>> pageNotice(@RequestBody BasePageSearch basePageSearch) {
        UserSubject userSubject= JwtUtil.getCurrentJwtUser();
        PageQuery<NoticeVo> pageQuery = new PageQuery<NoticeVo>();
        pageQuery.setPageNumber(basePageSearch.getPageNo());
        NoticeSearch noticeSearch=new NoticeSearch();
        noticeSearch.setUserId(userSubject.getId());
        noticeService.pageQueryVo(pageQuery, noticeSearch);
        List<NoticeVo> list=pageQuery.getList();
        list.forEach(no->{
            if(!no.getReaded()) {
                noticeService.setRead(no.getId(), no.getUserId());
            }
        });
        return GlobalReponse.success(pageQuery);
    }


    @GetMapping("/getNoticeCount")
    @ApiOperation(value = "NO:10-2 获取未读消息数", notes = "")
    public GlobalReponse<MessageNumVo> getNoticeCount() {
        UserSubject userSubject=JwtUtil.getCurrentJwtUser();
        NoticeSearch noticeSearch=new NoticeSearch();
        noticeSearch.setUserId(userSubject.getId());
        noticeSearch.setReaded(false);
        return noticeService.getCount(noticeSearch);
    }

    @GetMapping("/deletedNotice")
    @ApiOperation(value = "NO:10-3 删除通知消息", notes = "")
    public GlobalReponse deletedNotice(Long id) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return noticeService.deletedNotice(id,userId);
    }
}
