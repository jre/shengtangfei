package com.boruan.shengtangfeng.api.jwt.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @program: tutuyou-parent
 * @description: 用户端验证身份，如果身份没有通过，抛异常
 * @author: lihaicheng
 * @create: 2019-10-15 14:04
 **/
public class UserVerificationException extends AuthenticationException {

	private static final long serialVersionUID = -1587749520770383392L;

	public UserVerificationException(String message) {
		super(message);
	}
}
