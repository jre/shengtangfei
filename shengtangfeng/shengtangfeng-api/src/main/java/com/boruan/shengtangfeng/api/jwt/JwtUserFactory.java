package com.boruan.shengtangfeng.api.jwt;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.boruan.shengtangfeng.core.entity.User;

public final class JwtUserFactory {

	private JwtUserFactory() {
	}

	public static UserSubject create(User user) {
		ArrayList<GrantedAuthority> authorities = new ArrayList<>();
//         if(StringUtils.equals("admin", user.getLoginName())) {
//         	authorities.add(new GrantedAuthorityImpl("ROLEADMIN") );
//         }
		return new UserSubject(user.getId(), user.getMobile(), user.getName(), user.getPassword(), user.getSalt(),
				user.getOpenId(), user.getHeadImage(), authorities);
	}

	private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> authorities) {
		if (authorities == null || authorities.size() == 0) {
			return null;
		}
		return authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

}
