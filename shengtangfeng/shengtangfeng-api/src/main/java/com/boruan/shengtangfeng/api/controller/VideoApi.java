package com.boruan.shengtangfeng.api.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import com.boruan.shengtangfeng.core.dao.ISubjectDao;
import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.utils.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RegExUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.boruan.shengtangfeng.api.jwt.UserSubject;
import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.AttentionDto;
import com.boruan.shengtangfeng.core.dto.UserCategoryDto;
import com.boruan.shengtangfeng.core.dto.VideoCommentDto;
import com.boruan.shengtangfeng.core.dto.VideoDto;
import com.boruan.shengtangfeng.core.dto.VideoPageSearch;
import com.boruan.shengtangfeng.core.dto.mapper.VideoDtoMapper;
import com.boruan.shengtangfeng.core.service.IAccoladeService;
import com.boruan.shengtangfeng.core.service.IAttentionService;
import com.boruan.shengtangfeng.core.service.IFavoritesService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.IPopularWordsService;
import com.boruan.shengtangfeng.core.service.ISearchWordsService;
import com.boruan.shengtangfeng.core.service.IUserCategoryService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.service.IVideoCategoryService;
import com.boruan.shengtangfeng.core.service.IVideoCommentService;
import com.boruan.shengtangfeng.core.service.IVideoService;
import com.boruan.shengtangfeng.core.vo.PopularWordsVo;
import com.boruan.shengtangfeng.core.vo.VideoCategoryVo;
import com.boruan.shengtangfeng.core.vo.VideoCommentVo;
import com.boruan.shengtangfeng.core.vo.VideoUserCategoryVo;
import com.boruan.shengtangfeng.core.vo.VideoVo;
import com.boruan.shengtangfeng.core.vo.mapper.PopularWordsMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoCategoryVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoCommentVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoVoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author KongXH
 * @title: sss
 * @projectName ShengTangFeng-V2-JAVA
 * @description: TODO
 * @date 2020/8/1210:50
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/video")
@Api(value = "", tags = {"NO:6 视频相关接口"})
public class VideoApi {

    @Autowired
    private IVideoService videoService;
    @Autowired
    private IVideoCommentService videoCommentService;
    @Autowired
    private IUserCategoryService userCategoryService;
    @Autowired
    private IVideoCategoryService videoCategoryService;
    @Autowired
    private IPopularWordsService popularWordsService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IAttentionService attentionService;
    @Autowired
    private IFavoritesService favoritesService;
    @Autowired
    private IAccoladeService accoladeService;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private ILogService sysLogService;
    @Autowired
    private ISearchWordsService searchWordsService;
    @Autowired
    private ISubjectDao subjectDao;


    /**
     * @return
     * @description: 搜索结果页
     * @Param
     * @author KongXH
     * @date 2020/8/18 13:39
     */
    @GetMapping("/getUserCategory")
    @ApiOperation(value = "NO:6-1 获取用户的视频类别", notes = "")
    public GlobalReponse<PageQuery<List<VideoCategoryVo>>> getUserCategory(Integer type) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        List<VideoCategoryVo> vos = null;
        if (type != 1) {
            if (userSubject == null) {
                VideoCategory videoCategory = new VideoCategory();
                videoCategory.setType(type);
                List<VideoCategory> videoCategoryList = videoCategoryService.getList(videoCategory);
                vos = VideoCategoryVoMapper.MAPPER.toVo(videoCategoryList);
            } else {
                List<VideoCategory> userCategoryList = userCategoryService.getUserVideoCategory(userSubject.getId(), type);
                vos = VideoCategoryVoMapper.MAPPER.toVo(userCategoryList);
            }
        } else {
            if (userSubject == null) {
                List<Subject> subjectList = subjectDao.createLambdaQuery().andEq(Subject::getIsDeleted, false).select();
                for (Subject subject : subjectList) {
                    VideoCategoryVo vo = new VideoCategoryVo();
                    vo.setId(subject.getId());
                    vo.setName(subject.getName());
                    vo.setIsDefault(false);
                    if (subject.getSort() != null) {
                        vo.setSort(subject.getSort());
                    }
                    vos.add(vo);
                }
            } else {
                List<VideoCategory> userCategoryList = null;
                if (type != null && type != 1) {
                    userCategoryList = userCategoryService.getUserVideoCategory(userSubject.getId(), type);//已选全部类别
                    if (userCategoryList.isEmpty()) {
                        userCategoryList = videoCategoryService.getVideoCategoryDefault(type);
                    }
                } else {
                    List<Subject> list = userCategoryService.getUserJYJCategory(userSubject.getId(), type);//已选全部类别
                    ArrayList<VideoCategory> categories = new ArrayList<VideoCategory>();
                    for (Subject subject : list) {
                        VideoCategory vc = new VideoCategory();
                        vc.setName(subject.getName());
                        vc.setIsDefault(subject.getIsDefault());
                        categories.add(vc);
                    }
                    userCategoryList = categories;


                }
                vos = VideoCategoryVoMapper.MAPPER.toVo(userCategoryList);

            }
        }
        VideoCategoryVo vo = new VideoCategoryVo();
        vo.setId(0L);
        vo.setName("关注");
        vos.add(0, vo);
        return GlobalReponse.success(vos);
    }

    /**
     * @return
     * @description: 国学视频展示页
     * @Param
     * @author KongXH
     * @date 2020/8/12 15:57
     */
    @PostMapping("/videoPage")
    @ApiOperation(value = "NO:6-2 视频分页展示,搜索也可用这个接口", notes = "")
    public GlobalReponse<PageQuery<VideoVo>> videoPage(@RequestBody VideoPageSearch videoPageSearch) {
        Video search = new Video();
        search.setStatus(1);
        search.setType(videoPageSearch.getType());

        PageQuery<Video> pageQuery = new PageQuery<Video>();
        List<Video> list = new ArrayList<>();
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        if (userSubject != null) {
            search.set("searchNoBlack", userSubject.getId());
        }
        search.set("keyword", videoPageSearch.getKeyword());
        if (userSubject != null && StringUtils.isNotBlank(videoPageSearch.getKeyword())) {
            searchWordsService.addSearchWord(userSubject.getId(), 3, videoPageSearch.getKeyword());
        }
        search.setStatus(1);
        pageQuery.setPageNumber(videoPageSearch.getPageNo());
        //查看我已经关注的
        if (videoPageSearch.getCategoryId() != null && 0 == videoPageSearch.getCategoryId()) {
            //用户没登录显示全部
            if (userSubject == null) {
                search.setCategoryId(null);
                videoService.pageQuery(pageQuery, search);
                list = pageQuery.getList();
            } else {
                videoService.pageAttentionVideo(pageQuery, userSubject.getId());
                list = pageQuery.getList();
                if (list.size() <= 0) {//没有关注显示全部
                    pageQuery = new PageQuery<Video>();
                    pageQuery.setPageNumber(videoPageSearch.getPageNo());
                    search.setCategoryId(null);
                    videoService.pageQuery(pageQuery, search);
                    list = pageQuery.getList();
                }
            }
        } else {
            search.setCategoryId(videoPageSearch.getCategoryId());
            videoService.pageQuery(pageQuery, search);
            list = pageQuery.getList();
        }
        if (videoPageSearch.getAddress()!=null && !videoPageSearch.getAddress().equals("")){
            for (Video video : list) {
                String add = GaoDeUtils.getAdd(video.getLongitude(), video.getLatitude());
                if (video.getAddress()!=null){
                    if (!add.contains(videoPageSearch.getAddress())){
                        list.remove(video);
                    }
                }
            }
        }
        ArrayList<VideoVo> vos = new ArrayList<>();
        for (Video video : list) {
            VideoVo vo = new VideoVo();
            BeanUtils.copyProperties(video,vo);
            if (videoPageSearch.getAddress()!=null && !videoPageSearch.getAddress().equals("")){
                String add = GaoDeUtils.getAdd(video.getLongitude(), video.getLatitude());
                if (video.getAddress()!=null){
                    if (!add.contains(videoPageSearch.getAddress())){
                        list.remove(video);
                        continue;
                    }
                }
            }
            if (videoPageSearch.getLongitude()!=null && videoPageSearch.getLatitude()!=null && vo.getLongitude()!=null && vo.getLatitude()!=null){
                double distance = GaoDeUtils.getDistance(Double.valueOf(videoPageSearch.getLongitude()), Double.valueOf(videoPageSearch.getLatitude()), Double.valueOf(vo.getLongitude()), Double.valueOf(vo.getLatitude()));
                vo.setDistance(new BigDecimal(distance).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            }
            //是否关注
            Attention attention = null;
            //是否点赞
            Accolade accolade = null;
            if (userSubject != null) {
                //是否关注
                attention = attentionService.findByFocusId(userSubject.getId(), vo.getPublishId());
                //是否点赞
                accolade = accoladeService.getAccolade(userSubject.getId(), vo.getId(), 2);
            }
            vo.setIsAttention(null == attention ? false : true);
            vo.setIsLike(null == accolade ? false : true);
            User publisher = userService.findById(vo.getPublishId());
            vo.setInvitationCode(publisher.getInvitationCode());
            vos.add(vo);
        }
        pageQuery.setList(vos);
        return GlobalReponse.success(pageQuery);
    }

//    /**
//     * @description: 搜索结果页
//     * @Param
//     * @return
//     * @author KongXH
//     * @date 2020/8/18 13:39
//     */
//    @GetMapping("/search")
//    @ApiOperation(value = "m3 搜索结果页", notes = "")
//    public GlobalReponse<PageQuery<VideoVo>> search(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
//                                                      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,Video video) {
//        PageQuery<Video> pageQuery = new PageQuery<Video>();
//        pageQuery.setPageNumber(pageNo);
//        videoService.pageQuery(pageQuery, video);
//        List<Video> list=pageQuery.getList();
//        List<VideoVo> vos=VideoVoMapper.MAPPER.toVo(list);
//        vos.forEach(vo->{
//            if(StringUtils.isNotBlank(vo.getImages())) {
//                vo.setImageList(StringUtils.split(vo.getImages(),","));
//            }else {
//                vo.setImageList(new String[] {});
//            }
//        });
//
//
//        pageQuery.setList(vos);
//
//        return GlobalReponse.success(pageQuery);
//    }


    /**
     * @return
     * @description:获取视频详情和评论
     * @Param
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @GetMapping("/videoDetail/{videoId}")
    @ApiOperation(value = "NO:6-3 获取视频详情", notes = "")
    public GlobalReponse<VideoVo> detailVideo(@PathVariable("videoId") Long videoId) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();

        Video video = videoService.findById(videoId);
        //获取一次详情加一次阅读量
        video.setReadNum((video.getReadNum() == null ? 0 : video.getReadNum()) + 1);
        videoService.save(video);
        //是否关注
        Attention attention = null;
        //是否收藏
        Favorites favorites = null;
        //是否点赞
        Accolade accolade = null;
        List<VideoComment> comments;
        if (userSubject != null) {
            //是否关注
            attention = attentionService.findByFocusId(userSubject.getId(), video.getPublishId());
            //是否收藏
            favorites = favoritesService.getFavorites(userSubject.getId(), videoId, 2);
            //是否点赞
            accolade = accoladeService.getAccolade(userSubject.getId(), videoId, 2);
            comments = videoCommentService.findByVideoId(userSubject.getId(), videoId, 1);
        } else {
            comments = videoCommentService.findByVideoId(null, videoId, 1);
        }


        List<VideoCommentVo> commentVos = VideoCommentVoMapper.MAPPER.toVo(comments);
//        commentVos.forEach(vo->{
//            vo.setThumbUpEd(videoCommentService.isThumbUp(vo.getId(), userSubject.getId()));
//        });

        VideoVo vo = VideoVoMapper.MAPPER.toVo(video);
        if (vo.getUploadType()==1){
            User user = userDao.unique(vo.getPublishId());
            vo.setAccount(user.getAccount());
        }
        vo.setIsAttention(null == attention ? false : true);
        vo.setIsFavorites(null == favorites ? false : true);
        vo.setIsLike(null == accolade ? false : true);
        vo.setComment(commentVos);
        return GlobalReponse.success(vo);
    }


    /**
     * @return
     * @description: 发布视频加载类别
     * @Param
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @GetMapping("getVideoCategory")
    @ApiOperation(value = "NO:6-4 发布时获取所有视频类别", notes = "")
    public GlobalReponse<List<VideoCategoryVo>> getVideoCategory() {
        VideoCategory videoCategory = new VideoCategory();
        videoCategory.setType(0);
        List<VideoCategory> list = videoService.getCategoryList(videoCategory);
        ArrayList<VideoCategory> videoCategories = new ArrayList<>();
        for (VideoCategory category : list) {
            if (!category.getName().equals("全部")) {
                videoCategories.add(category);
            }
        }
        return GlobalReponse.success(VideoCategoryVoMapper.MAPPER.toVo(videoCategories));
    }

    /**
     * @return
     * @description: 保存视频
     * @Param
     * @author KongXH
     * @date 2020/8/12 15:52
     */
    @PostMapping("saveVideo")
    @ApiOperation(value = "NO:6-4 发布视频", notes = "新增、修改保存")
    public GlobalReponse saveVideo(@RequestBody VideoDto videoDto) throws IOException {

        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        //临时替换地址
        videoDto.setContent(RegExUtils.replaceAll(videoDto.getContent(),"https://public.stftt.com","http://accelerate.boruankeji.net"));
        if (videoDto.getId() != null) {
            Video video = videoService.findId(videoDto.getId());
            video.setStatus(0);
            video.setCategoryId(videoDto.getCategoryId());
            video.setContent(videoDto.getContent());
            video.setTitle(videoDto.getTitle());
            video.setUploadType(1);
            video.setImage(video.getContent() + "?x-oss-process=video/snapshot,t_7000,f_jpg,w_800,h_600,m_fast");
            videoService.save(video);
        } else {
            Video video = VideoDtoMapper.MAPPER.toEntity(videoDto);
            video.setPublishId(userSubject.getId());
            video.setPublisher(userSubject.getName());
            video.setPublisherIcon(userSubject.getIcon());
            video.setCreateBy(userSubject.getName());
            video.setCreateTime(new Date());
            video.setPublishDate(new Date());
            video.setStatus(0);
            video.setType(0);
            video.setUploadType(1);
            video.setReadNum(0);
            video.setCommentNum(0);
            video.setThumpUpNum(0);
            video.setRecommend(false);
            video.setIsDeleted(false);
            video.setImage(video.getContent() + "?x-oss-process=video/snapshot,t_7000,f_jpg,w_800,h_600,m_fast");
            videoService.save(video);
        }
        return GlobalReponse.success();
    }

    /**
     * @return
     * @description: 展示我的类别
     * @Param
     * @author KongXH
     * @date 2020/8/13 14:15
     */
    @PostMapping("getMyCategory")
    @ApiOperation(value = "NO:6-4 编辑我的类别选择详情 0普通视频1讲一讲分类2小视频分类", notes = "")
    public GlobalReponse<List<VideoUserCategoryVo>> getMyCategory(Integer type) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        ArrayList<VideoCategory> videoCategories = new ArrayList<>();
        if (type != 1) {
            VideoCategory videoCategory = new VideoCategory();
            if (type != null) {
                videoCategory.setType(type);
            }
            List<VideoCategory> allVideoCategoryList = videoService.getCategoryList(videoCategory);//视频全部类别
            for (VideoCategory category : allVideoCategoryList) {
                if (category.getName() != null && !category.getName().equals("全部")) {
                    videoCategories.add(category);
                }
            }
        } else {
            List<Subject> subjectList = subjectDao.createLambdaQuery().andEq(Subject::getIsDeleted, false).select();
            for (Subject subject : subjectList) {
                VideoCategory vo = new VideoCategory();
                vo.setId(subject.getId());
                vo.setName(subject.getName());
                vo.setIsDefault(false);
                if (subject.getSort() != null) {
                    vo.setSort(subject.getSort());
                }
                videoCategories.add(vo);
            }
        }
        List<VideoCategory> userVideoCategoryList = null;
        if (type != null && type != 1) {
            userVideoCategoryList = userCategoryService.getUserVideoCategory(userSubject.getId(), type);//已选全部类别
        } else {
            List<Subject> list = userCategoryService.getUserJYJCategory(userSubject.getId(), type);//已选全部类别
            ArrayList<VideoCategory> categories = new ArrayList<VideoCategory>();
            for (Subject subject : list) {
                VideoCategory vc = new VideoCategory();
                vc.setName(subject.getName());
                vc.setIsDefault(subject.getIsDefault());
                categories.add(vc);
            }
            userVideoCategoryList = categories;
        }

        List<VideoCategory> notChoiceVideoCategoryList = new ArrayList<>();
        CollectionUtils.addAll(notChoiceVideoCategoryList, new Object[videoCategories.size()]);
        Collections.copy(notChoiceVideoCategoryList, videoCategories);
        notChoiceVideoCategoryList.removeAll(userVideoCategoryList);
        VideoUserCategoryVo vo = new VideoUserCategoryVo();
        vo.setAllVideoCategorie(videoCategories);
        VideoCategory vc = new VideoCategory();
        vc.setId(0L);
        vc.setName("关注");
        userVideoCategoryList.add(0, vc);
        vo.setUserVideoCategorie(userVideoCategoryList);
        vo.setNotChoiceVideoCategorie(notChoiceVideoCategoryList);
        return GlobalReponse.success(vo);
    }

    /**
     * @return
     * @description: 保存我的类别
     * @Param
     * @author KongXH
     * @date 2020/8/13 14:01
     */
    @PostMapping("saveUserCategory")
    @ApiOperation(value = "NO:6-7 保存我的类别", notes = "")
    public GlobalReponse saveUserCategory(@RequestBody UserCategoryDto userCategoryDto) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        if (userCategoryDto.getType() != 1) {
            userCategoryService.saveUserCategory(userCategoryDto, userCategoryDto.getType(), userSubject.getId());
        } else {
            userCategoryService.saveUserCategory(userCategoryDto, 3, userSubject.getId());
        }

        return GlobalReponse.success();
    }


    /**
     * @return
     * @description: 保存收藏
     * @Param
     * @author KongXH
     * @date 2020/8/17 14:15
     */
    @PostMapping("/saveVideoFavorites/{videoId}")
    @ApiOperation(value = "NO:6-8 收藏/取消收藏视频，data是true是收藏成功，false是取消收藏成功", notes = "")
    public GlobalReponse<Boolean> saveFavorites(@PathVariable Long videoId) {
        UserSubject user = JwtUtil.getCurrentJwtUser();
        Favorites favorites = new Favorites();
        favorites.setArticleId(videoId);
        favorites.setType(2);
        favorites.setUserId(user.getId());
        favorites.setCreateBy(user.getName());
        favorites.setCreateTime(new Date());
        return favoritesService.saveFavorites(favorites);
    }


    /**
     * @return
     * @description: 保存点赞
     * @Param
     * @author KongXH
     * @date 2020/8/17 17:32
     */
    @PostMapping("saveAccolade/{videoId}")
    @ApiOperation(value = "NO:6-9 点赞/取消点赞视频，data是true是点赞成功，false是取消点赞成功", notes = "")
    public GlobalReponse<Boolean> saveAccolade(@PathVariable Long videoId) {
        UserSubject user = JwtUtil.getCurrentJwtUser();
        Accolade accolade = new Accolade();
        accolade.setArticleId(videoId);
        accolade.setType(2);
        accolade.setUserId(user.getId());
        accolade.setCreateBy(user.getName());
        accolade.setCreateTime(new Date());
        return accoladeService.saveAccolade(accolade);
    }


    /**
     * @return
     * @description: 获取关键词
     * @Param
     * @author KongXH
     * @date 2020/8/13 17:10
     */
    @PostMapping("getPopularWords")
    @ApiOperation(value = "NO:6-10 获取视频热门词 ", notes = "")
    public GlobalReponse<List<PopularWordsVo>> getPopularWords() {
//        关键字类型1试题2文3视频
        List<PopularWords> popularWordsList = popularWordsService.getList(2);
        return GlobalReponse.success(PopularWordsMapper.MAPPER.toVo(popularWordsList));
    }


    /*

     **
     * @description: 获取邀请码
     * @Param
     * @return
     * @author KongXH
     * @date 2020/8/14 14:18
     *//*

    @GetMapping("getAttention")
    @ApiOperation(value = "m6-获取邀请码", notes = "")
    public GlobalReponse<List<UserVo>> getAttention(Long id){
        User user = userService.findByInvitationCode(id);
        return GlobalReponse.success(UserVoMapper.MAPPER.toVo(user));

    }
*/


    /**
     * @return
     * @description: 保存关注
     * @Param
     * @author KongXH
     * @date 2020/8/14 13:53
     */
    @PostMapping("saveAttention")
    @ApiOperation(value = "NO:6-11 关注/取消关注，data是true是关注成功，false是取消关注成功", notes = "")
    public GlobalReponse<Boolean> saveAttention(@RequestBody AttentionDto dto) {
        Attention attention = new Attention();
        UserSubject user = JwtUtil.getCurrentJwtUser();
        //关注信息保存
        attention.setUserId(user.getId());
        attention.setFocusUserId(dto.getFocusUserId());
        attention.setIsDeleted(false);
        return attentionService.saveAttention(attention, dto.getInvitationCode());
    }


    /**
     * @return
     * @description: 视频评论
     * @Param
     * @author KongXH
     * @date 2020/8/14 16:34
     */
    @PostMapping(value = "/videoComment")
    @ApiOperation(value = "NO:6-12 视频评论", notes = "")
    public GlobalReponse<VideoCommentVo> videoComment(@RequestBody VideoCommentDto videoCommentDto) {
        GlobalReponse globalReponse = null;
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Video video = videoService.findId(videoCommentDto.getVideoId());
        VideoComment comment = new VideoComment();

        Integer check = CommentUtil.checkComment(videoCommentDto.getContent());
        if (check == 2) {
            comment.setStatus(1);
            globalReponse = GlobalReponse.success(VideoCommentVoMapper.MAPPER.toVo(comment));
            //TODO 评论数审核过再加1
            video.setCommentNum((video.getCommentNum() == null ? 0 : video.getCommentNum()) + 1);
            videoService.save(video);
        } else if (check == 1) {
            comment.setStatus(0);
            globalReponse = GlobalReponse.fail("您的评论包含敏感词，需要等待客服审核");
        } else if (check == 0) {
            return GlobalReponse.fail("您的评论包含敏感词禁止评论");
        }
        comment.setContent(videoCommentDto.getContent());
        comment.setCreateBy(userSubject.getMobile());
        comment.setCreateTime(new Date());
        comment.setParentId(0L);
        comment.setThumpUpNum(0);
        comment.setIsDeleted(false);
        comment.setVideoId(videoCommentDto.getVideoId());
        comment.setUserId(userSubject.getId());
        comment.setUserName(userSubject.getName());
        comment.setUserIcon(userSubject.getIcon());
        videoCommentService.save(comment);
        return globalReponse.setData(VideoCommentVoMapper.MAPPER.toVo(comment));
    }

}
