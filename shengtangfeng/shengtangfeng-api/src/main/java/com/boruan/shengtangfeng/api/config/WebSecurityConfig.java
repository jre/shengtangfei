package com.boruan.shengtangfeng.api.config;

import com.boruan.shengtangfeng.api.jwt.exception.JwtAuthenticationEntryPoint;
import com.boruan.shengtangfeng.api.jwt.filter.CustomLogoutSuccessHandler;
import com.boruan.shengtangfeng.api.jwt.filter.JWTAuthenticationFilter;
import com.boruan.shengtangfeng.api.jwt.filter.JWTLoginFilter;
import com.boruan.shengtangfeng.api.jwt.handler.LoginFailureHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.boruan.shengtangfeng.api.jwt.UserAuthenticationProvider;

/**
 * SpringSecurity的配置
 * 通过SpringSecurity的配置，将JWTLoginFilter，JWTAuthenticationFilter组合在一起
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${jwt.exceptUrl}")
	private String exceptUrl;

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private UserAuthenticationProvider userAuthenticationProvider;

	@Bean
	public JWTAuthenticationFilter authenticationTokenFilterBean() throws Exception {
		return new JWTAuthenticationFilter(authenticationManager(), unauthorizedHandler);
	}

	@Bean
	public JWTLoginFilter loginFilterBean() throws Exception {
		JWTLoginFilter jwtLoginFilter = new JWTLoginFilter(authenticationManager());
		// 只有登录的时候才去走过滤器
		jwtLoginFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/api/login"));
		jwtLoginFilter.setAuthenticationFailureHandler(loginFailureHandler());
		return jwtLoginFilter;
	}

	@Bean
	public LoginFailureHandler loginFailureHandler() {
		return new LoginFailureHandler();
	}

	@Bean
	public CustomLogoutSuccessHandler customLogoutSuccessHandlerBean() throws Exception {
		return new CustomLogoutSuccessHandler();
	}

	// 设置 HTTP 验证规则
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.cors().and().csrf().disable() // 登录失败
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()// 未授权处理
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				// 所有 /users/signup 的POST请求 都放行
				// 注册方法用到的请求需要放行
				.anyRequest().authenticated() // 所有请求需要身份认证
				.and().addFilter(loginFilterBean()).addFilter(authenticationTokenFilterBean()).logout()
				.logoutSuccessHandler(customLogoutSuccessHandlerBean()) // 默认注销行为为logout，可以通过下面的方式来修改
				.logoutUrl("/logout")
//            .logoutSuccessUrl("/login")
				.permitAll();// 设置注销成功后跳转页面，默认是跳转到登录页面;
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers(HttpMethod.POST, exceptUrl).antMatchers("/api/login/**") // 微信登录
//    	   .antMatchers("/api/changePassword") //忘记密码
//    	   .antMatchers("/api/verficationCode/**") //验证码
//				.antMatchers("/api/mall/**") // 商城类
				.antMatchers("/api/config/getAgreement") // 隐私协议，注册协议
				.antMatchers("/aliyun/oss/aliYunVideoAuditReturn") // 阿里视频审核回调
				.antMatchers("/api/login/userIsRegistered") // 分享页判断用户是否祖册
				.antMatchers("/api/user/getClassInformById") // 分享页获取班级信息
				.antMatchers("/api/apk/get") // 更新
				.antMatchers("/api/config/getUserGuide") // 更新
				.antMatchers("/api/h5/**") // H5用户的接口，全部匿名
				.antMatchers("/api/config/getOnline") // ios上架
				.antMatchers("/api/article/getArticleCommentH5") // 隐私协议，注册协议
				.antMatchers("/api/home/getHomeAdvertise") //首页轮播
				.antMatchers("/api/home/getSplashAdvertise") //闪屏广告
				.antMatchers("/api/home/getGrade") //首页年级列表
				.antMatchers("/api/home/getSubject/**") //首页学科列表
				.antMatchers("/api/home/getGrade/**") //首页年级列表
				.antMatchers("/api/home/getSubject") //首页学科列表
				.antMatchers("/api/home/getModule/**") //首页模块列表
				.antMatchers("/api/home/getModuleVideo/**") //首页模块列表
				.antMatchers("/api/home/getModuleDetail/**") //首页模块列表
				.antMatchers("/api/home/getModuleQuestion/**") //获取模块试题
				.antMatchers("/api/article/getUserCategory") // 获取文章分类
				.antMatchers("/api/article/articlePage") // 获取文章列表
				.antMatchers("/api/article/articleDetail/**") // 获取文章详情
				.antMatchers("/api/article/getPopularWords") // 获取热门词
				.antMatchers("/api/video/getUserCategory") // 获取文章分类
				.antMatchers("/api/video/videoPage") // 获取文章列表
				.antMatchers("/api/video/videoDetail/**") // 获取文章详情
				.antMatchers("/api/video/getPopularWords") // 获取热门词
				.antMatchers("/api/question/getPopularWords") // 获取热门词
				.antMatchers("/api/question/getQuestionDetail") // 获取题目详情
				.antMatchers("/api/common/address/list") // 获取省市区
				.antMatchers("/api/authentication/getImage/**")
				.antMatchers("/swagger-ui.html")
				.antMatchers("/swagger-resources/**")
				.antMatchers("/images/**")
				.antMatchers("/webjars/**")
				.antMatchers("/v2/api-docs")
				.antMatchers("/configuration/ui")
				.antMatchers("/configuration/security")
				.antMatchers("/error")
				.antMatchers("/api/notify/vmNotify")
				.antMatchers("/api/smallVideo/videoPage")
				.antMatchers("/api/smallVideo/getSmallVideoComment")
				.antMatchers("/api/smallVideo/smallVideoDetailForId");
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		//使用自定义身份验证组件
		auth.authenticationProvider(userAuthenticationProvider);
	}

}
