package com.boruan.shengtangfeng.api.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.boruan.shengtangfeng.core.dao.IUserDao;
import com.boruan.shengtangfeng.core.entity.User;

/**
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private IUserDao userDao;

	@Override
	public UserSubject loadUserByUsername(String mobile) throws UsernameNotFoundException {
		User user = userDao.findByMobile(mobile);
		if (user == null) {
			throw new UsernameNotFoundException(mobile);
		}
		return JwtUserFactory.create(user);
	}

}
