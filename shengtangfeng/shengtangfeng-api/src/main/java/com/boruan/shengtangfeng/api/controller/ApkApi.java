package com.boruan.shengtangfeng.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.service.IApkService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings(value = { "all" })
@Slf4j
@Controller
@RequestMapping("/api/apk")
@Api(value = "", tags = { "NO:19 安卓更新接口" })
public class ApkApi {
	@Autowired
	private IApkService apkService;

	/**
	 * 
	 */
	@GetMapping(value = "/get")
	@ApiOperation(value = "NO:19-1 获取更新信息", notes = "")
	@ResponseBody
	public GlobalReponse<Apk> get(Model model) {
	    return apkService.getApk();
	}
}
