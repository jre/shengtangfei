//package com.boruan.shengtangfeng.api.controller;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.boruan.shengtangfeng.core.utils.GlobalReponse;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//
///**
// * @author: 刘光强
// * @Description: 头条相关接口
// * @date:2020年3月10日 上午11:14:10
// */
//@Slf4j
//@RestController
//@SuppressWarnings("all")
//@RequestMapping("/api/news")
//@Api(value = "", tags = { "NO:6 头条相关接口，弃用了" })
//public class NewsApi {
//
//	@GetMapping("/detail")
//	@ApiOperation(value = "NO:6-1 获取头条详情，弃用了", notes = "")
//	public GlobalReponse detail(Long newsId) {
//		return GlobalReponse.success();
//	}
//	@GetMapping("/pageNews")
//	@ApiOperation(value = "NO:6-2 分页获取头条新闻(status默认为1)，弃用了", notes = "")
//	public GlobalReponse pageNews(
//			@RequestParam(value = "pageNo", defaultValue = "1") int pageNo) {
//		return GlobalReponse.success();
//	}
//}
