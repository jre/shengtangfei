package com.boruan.shengtangfeng.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.service.IConfigService;
import com.boruan.shengtangfeng.core.utils.Consts;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.OssConfigVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: 刘光强
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@RequestMapping("/api/config")
@Api(value = "", tags = { "NO:1 系统配置相关接口" })
@SuppressWarnings("all")
public class ConfigApi {

	@Autowired
	private IConfigService configService;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private JwtUtil jwtUtil;
	@Value("${aliyun.oss.publicEndpoint}")
    private String publicEndpoint;
	@Value("${aliyun.oss.bucketName}")
	private String bucketName;
	@Value("${aliyun.oss.publicAction}")
	private String domain;
	@GetMapping("/getOssConfig")
    @ApiOperation(value = "NO:1-1 获取oss 的配置信息")
    public GlobalReponse<OssConfigVo> getOssConfig() {
        OssConfigVo vo=new OssConfigVo();
        vo.setBucketName(bucketName);
        vo.setEndPoint(publicEndpoint);
        vo.setDomain(domain);
        return GlobalReponse.success(vo);
    }
	@GetMapping("/getApk")
	@ApiOperation(value = "NO:1-4 安卓最新版本号，更新时间用updateTime")
	public GlobalReponse<Apk> getApk() {
	    Apk apk= configService.getApk(1L);
	    return GlobalReponse.success(apk);
	}
	@GetMapping("/getAgreement")
	@ApiOperation(value = "NO:1-5 获取相关协议  1隐私协议  2用户注册协议 3帮助文档")
	public GlobalReponse<String> getAgreement(Integer type) {
	    switch (type) {
        case 1:
            Config config=configService.findConfigByKey(Consts.CONFIG_PRIVACY_AGREEMENT);
            return GlobalReponse.success(((Object)config.getValue()));
        case 2:
            config=configService.findConfigByKey(Consts.CONFIG_REGISTER_AGREEMENT);
            return GlobalReponse.success(((Object)config.getValue()));
        case 3:
            config=configService.findConfigByKey(Consts.CONFIG_HELP_DOCUMENT);
            return GlobalReponse.success(((Object)config.getValue()));
        default :
            return GlobalReponse.fail();
        }
	}
	@GetMapping("/getOnline")
    @ApiOperation(value = "NO:1-9 苹果上架标记（true已上架 false 上架中）")
    public GlobalReponse<String> getOnline() {
        Config config=configService.findConfigByKey(Consts.CONFIG_APPLE_ONLINE);
        return GlobalReponse.success("",config.getValue());
    }
	@GetMapping("/getUserGuide")
	@ApiOperation(value = "NO:1-10 获取用户端使用教学视频地址")
	public GlobalReponse<String> getUserGuide() {
	    Config config=configService.findConfigByKey(Consts.CONFIG_LEARN_VIDEO_USER);
	    return GlobalReponse.success("",config.getValue());
	}

}
