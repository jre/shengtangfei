//package com.boruan.shengtangfeng.api.controller;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//import org.beetl.sql.core.engine.PageQuery;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.boruan.shengtangfeng.api.jwt.UserSubject;
//import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
//import com.boruan.shengtangfeng.core.entity.Advertise;
//import com.boruan.shengtangfeng.core.entity.User;
//import com.boruan.shengtangfeng.core.service.IAdvertiseService;
//import com.boruan.shengtangfeng.core.service.IUserService;
//import com.boruan.shengtangfeng.core.utils.GlobalReponse;
//import com.boruan.shengtangfeng.core.vo.AdvertiseVo;
//import com.boruan.shengtangfeng.core.vo.mapper.AdvertiseVoMapper;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//
///**
// * @author: 刘光强
// * @Description: 首页接口
// * @date:2020年3月10日 上午11:14:10
// */
//@Slf4j
//@RestController
//@SuppressWarnings("all")
//@RequestMapping("/api/advertise")
//@Api(value = "", tags = { "NO:9 广告相关接口" })
//public class AdvertiseApi {
//    @Autowired
//    private IAdvertiseService advertiseService;
//    @Autowired
//    private IUserService userService;
//
//    @GetMapping("/getAdvertise")
//    @ApiOperation(value = "NO:9-1 获取系统所有广告,返回map  key是类型a0->首页轮播；  ", notes = "")
//    public GlobalReponse<Map<Integer, List<AdvertiseVo>>> getAdvertise() {
//        List<Advertise> result = advertiseService.getList(1,null);
//        List<AdvertiseVo> vos = AdvertiseVoMapper.MAPPER.toVo(result);
//        Map<Integer, List<AdvertiseVo>> map = vos.stream().collect(Collectors.groupingBy(AdvertiseVo::getPosition));
//        Map<String, List<AdvertiseVo>> resultMap =new HashMap<String, List<AdvertiseVo>>();
//        map.forEach((key,value)->{
//            resultMap.put("a"+key.toString(), value);
//        });
//        return GlobalReponse.success(resultMap);
//    }
//    
//    @GetMapping("/pageAdvertise")
//    @ApiOperation(value = "NO:9-2 按照条件分页查询广告", notes = "")
//    public GlobalReponse<Map<Integer, List<AdvertiseVo>>> pageAdvertise(
//            @RequestParam(value = "pageNo", defaultValue = "1") int pageNo, Advertise advertise) {
//        PageQuery<Advertise> pageQuery = new PageQuery<Advertise>();
//        pageQuery.setPageNumber(pageNo);
//        advertise.setIsDeleted(false);
//        advertise.setStatus(1);
//        advertiseService.pageQuery(pageQuery, advertise);
//        return GlobalReponse.success(pageQuery);
//    }
//
//    @GetMapping("/getAdvertiseClick")
//    @ApiOperation(value = "NO:9-3 添加广告点击次数,传广告Id", notes = "")
//    public GlobalReponse getAdvertiseClick(Long id) {
//        UserSubject userSubject=JwtUtil.getCurrentJwtUser();
//        User user=userService.findById(userSubject.getId());
//        Advertise advertise=advertiseService.findById(id);
//        advertise.setClickCount((advertise.getClickCount()==null?0:advertise.getClickCount())+1);
//        advertiseService.save(advertise);
//        return GlobalReponse.success();
//    }
//}
