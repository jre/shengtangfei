package com.boruan.shengtangfeng.api.jwt.exception;

/**
 * 描述：
 * <p>
 *
 * @date:2020年3月10日 上午11:14:10
 */
public class BaseException extends RuntimeException {

	public BaseException(String message) {
		super(message);
	}

	public BaseException(String message, Throwable cause) {
		super(message, cause);
	}
}
