package com.boruan.shengtangfeng.api.jwt;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserSubject implements UserDetails {

	private Long id;
	private String mobile; // 设置为account
	private String name;
	private String password;
	private String salt;
	private String icon;
	private String openId;
	private Collection<? extends GrantedAuthority> authorities;

	public UserSubject() {

	}

	public UserSubject(Long id, String mobile, String name, String password, String salt, String openId, String userIcon,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.mobile = mobile;
		this.name = name;
		this.password = password;
		this.salt = salt;
		this.openId = openId;
		this.icon = userIcon;
		this.authorities = authorities;
	}

	/**
	 * 返回分配给用户的角色列表
	 * 
	 * @return
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.mobile;
	}

	public Long getId() {
		return id;
	}

	public String getMobile() {
		return mobile;
	}

	public String getName() {
		return name;
	}

	public String getSalt() {
		return salt;
	}

	public String getIcon() {
		return icon;
	}

	/**
	 * 账户是否未过期
	 * 
	 * @return
	 */
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	/**
	 * 账户是否未锁定
	 * 
	 * @return
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	public String getOpenId() {
		return openId;
	}

	/**
	 * 密码是否未过期
	 * 
	 * @return
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/**
	 * 账户是否激活
	 * 
	 * @return
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}

}