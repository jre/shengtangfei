package com.boruan.shengtangfeng.api.controller;

import com.boruan.shengtangfeng.api.jwt.UserSubject;
import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dao.IClassParameterDao;
import com.boruan.shengtangfeng.core.dto.*;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.service.IClassTeacherService;
import com.boruan.shengtangfeng.core.utils.DateUtils;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.jsoup.helper.DataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.unit.DataUnit;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @description: v4E 班级-教师端
 * @date 2021/4/7 9:20
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/classTeacher")
@Api(value = "", tags = {"v4E 班级-教师端"})
public class ClassTeacherController {


    @Autowired
    private IClassTeacherService classTeacherService;
    @Autowired
    private IClassParameterDao classParameterDao;

    @PostMapping("/getProvince")
    @ApiOperation(value = "获取省", notes = "")
    public GlobalReponse<AddressDto> getProvince() {
        return classTeacherService.getProvince();
    }

    @PostMapping("/getCity")
    @ApiOperation(value = "获取市", notes = "")
    public GlobalReponse<AddressDto> getCity(Long provinceId) {
        return classTeacherService.getCity(provinceId);
    }

    @PostMapping("/getCounty")
    @ApiOperation(value = "获取区", notes = "")
    public GlobalReponse<AddressDto> getCounty(Long cityId) {
        return classTeacherService.getCounty(cityId);
    }

    @PostMapping("findSchoolForAddress")
    @ApiOperation(value = "根据地址和名称查询学校", notes = "")
    public GlobalReponse<PageQuery<School>> findSchool(@RequestBody FindSchoolDto dto) {
        return classTeacherService.findSchool(dto);
    }


    @PostMapping("createSchool")
    @ApiOperation(value = "创建学校", notes = "")
    public GlobalReponse<School> createSchool(@RequestBody SchoolDto schoolDto) {
        if (schoolDto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.createSchool(userId, schoolDto);
    }


    @PostMapping("/schoolYear")
    @ApiOperation(value = "学年配置", notes = "学年配置")
    public GlobalReponse schoolYear() {

        String[] grade = {"一年级", "二年级", "三年级", "四年级", "五年级", "六年级", "七年级", "八年级", "九年级"};
        //获取学年配置
        ClassParameter classParam = classParameterDao.createLambdaQuery().andEq(ClassParameter::getIsDeleted,false).single();
        //获取当前年份
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        //判断学制
        int num = classParam.getEducationSystem() == 1 ? 5 : 6;
        //判断上下学期
        String now = new SimpleDateFormat("MM-dd").format(new Date());
        String spring = classParam.getSpringTime();
        String autumn = classParam.getAutumnTime();
        int newYear=year+1;
        String nowTime = year+"-"+now;
        String springTime =year+"-"+spring;
        String autumnTime =newYear+"-"+autumn;

//        Date nowTime = null;
//        Date springTime = null;
//        Date autumnTime = null;
//        try {
//            nowTime = DateUtils.parseDate(now, "MM-dd");
//            springTime = DateUtils.parseDate(spring, "MM-dd");
//            autumnTime = DateUtils.parseDate(autumn, "MM-dd");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Date date = null;
        Date date1 = null;
        Date date2 = null;
        String item = null;
        try {
             date = DateUtils.parseDate(nowTime, "yyyy-MM-dd");
             date1 = DateUtils.parseDate(springTime, "yyyy-MM-dd");
             date2 = DateUtils.parseDate(autumnTime, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date.after(date1) && date.before(date2)) {
            item = "上";
        } else {
            item = "下";
        }
        ArrayList<String> primary = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            String s = (year - i) + "级（" + grade[i] + item + "）";
            primary.add(s);
        }

        ArrayList<String> middle = new ArrayList<>();
        for (int y = num; y < 9; y++) {
            String s = (year - y) + "级（" + grade[y] + item + "）";
            middle.add(s);
        }
        return GlobalReponse.success("success").setData(primary).setData1(middle);
    }


    @PostMapping("createClass")
    @ApiOperation(value = "创建班级", notes = "")
    public GlobalReponse<ClassAndVo> createClass(@RequestBody ClassAndDto classDto) {
        if (classDto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.createClass(userId, classDto);
    }


    @PostMapping("tencentGroupId")
    @ApiOperation(value = "接收前端传递的腾讯群Id", notes = "")
    public GlobalReponse tencentGroupId(String tencentId,Long classId) {
        if (tencentId == null || classId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.tencentGroupId(tencentId,classId,userId);
    }

    @PostMapping("/getTeacherClassAnd")
    @ApiOperation(value = "查询教师的班级列表 在发布作业请求时传1")
    public GlobalReponse<ClassAndVo> getTeacherClassAnd(Integer type) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        return classTeacherService.getTeacherClassAnd(userSubject.getId(),type);
    }

    @PostMapping("getClassById")
    @ApiOperation(value = "根据班级id查询班级详情", notes = "")
    public GlobalReponse<ClassAndVo> getClassById(Long classId) {
        if (classId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getClassById(classId,userId);
    }

    @PostMapping("/updateNickname")
    @ApiOperation(value = "修改班级内昵称")
    public GlobalReponse changeNickname(@ApiParam(value = "昵称") @RequestParam(value = "nickname") String nickname, @ApiParam(value = "班级ID") @RequestParam(value = "classId") Long classId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.changeNickname(nickname, classId, userId);
    }

    @PostMapping("/dissolveClass")
    @ApiOperation(value = "解散班级")
    public GlobalReponse dissolveClass(@ApiParam(value = "班级ID") @RequestParam(value = "classId") Long classId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.dissolveClass(classId, userId);
    }

    @PostMapping("/getclassInformById")
    @ApiOperation(value = "根据班级id查看班级通知")
    public GlobalReponse<PageQuery<InformDto>> classInform(@ApiParam(value = "班级Id") @RequestParam(value = "classId") Long classId, @RequestParam(value = "pageNo", defaultValue = "1") int pageNo) {
        if (classId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        PageQuery<Inform> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(10);
        pageQuery.setPara("classId", classId);
        return classTeacherService.getClassInformById(userId, pageQuery,classId);
    }

    @PostMapping("/getMyInform")
    @ApiOperation(value = "查看我发布的通知")
    public GlobalReponse<PageQuery<Inform>> getMyInform(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        PageQuery<Inform> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(10);
        pageQuery.setPara("userId", userId);
        return classTeacherService.getMyInform(userId, pageQuery);
    }

    @PostMapping("/deleteMyInform")
    @ApiOperation(value = "删除我发布的通知")
    public GlobalReponse deleteMyInform(Long informId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.deleteMyInform(userId, informId);
    }

    @PostMapping("/pushInform")
    @ApiOperation(value = "发布通知")
    public GlobalReponse pushInform(@RequestBody InformPushDto informPushDto) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.pushInform(userId, informPushDto);
    }

    @GetMapping("getFriendList")
    @ApiOperation(value = "班级获取我的好友列表", notes = "")
    public GlobalReponse<FriendDto> getFriendList(Long classId,String keyword) {
        if (classId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getFriendList(userId, classId,keyword);
    }

    @PostMapping("addClassMembers")
    @ApiOperation(value = "邀请好友加入班级", notes = "")
    public GlobalReponse<ClassAndVo> addClassMembers(@RequestBody AddDeleteClassMembersDto dto) {
        if (dto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.addClassMembers(dto, userId);
    }

    @PostMapping("deletedClassMembers")
    @ApiOperation(value = "删除班级成员", notes = "")
    public GlobalReponse deletedClassMembers(@RequestBody AddDeleteClassMembersDto dto) {
        if (dto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.deletedClassMembers(dto, userId);
    }

    @PostMapping("getsubJect")
    @ApiOperation(value = "获取全部的学科", notes = "")
    public GlobalReponse<Subject> getsubJect() {
        return classTeacherService.getSubject();
    }

    @PostMapping("getGrade")
    @ApiOperation(value = "获取全部的年级", notes = "")
    public GlobalReponse<Grade> getGrade() {
        return classTeacherService.getGrade();
    }

    @PostMapping("getUserJobGrade")
    @ApiOperation(value = "获取老师发布过作业的学科", notes = "")
    public GlobalReponse<Subject> getUserJobGrade() {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getUserJobGrade(userId);
    }


    @PostMapping("getQuestionCount")
    @ApiOperation(value = "查询题库数量 type:0我的题库,1淘学题库,2共享题库 subjectId:学科id, gradeId:年级id", notes = "")
    public GlobalReponse<QuestionCountDto> getQuestionCount(Integer type,Long subjectId,Long gradeId) {
        if (type == null ) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        if (subjectId==null){
            subjectId=0L;
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getQuestionCount(type, subjectId, gradeId, userId);
    }


    @PostMapping("getQuestionPage")
    @ApiOperation(value = "查询题库分页", notes = "")
    public GlobalReponse<PageQuery<GetQuestionPageVo>> getQuestionPage(@RequestBody QuestionPageDto dto) {
        if (dto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getQuestionPage(dto, userId);
    }

    @PostMapping("addOrCancelFavorites")
    @ApiOperation(value = "加入或取消收藏", notes = "")
    public GlobalReponse addOrCancelFavorites(@ApiParam(value = "0取消收藏,1加入收藏") @RequestParam(value = "type") Integer type, Long questionId) {
        if (type == null || questionId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.addOrCancelFavorites(type, questionId, userId);
    }

    @PostMapping("addQuestion")
    @ApiOperation(value = "添加题目或者编辑题目", notes = "")
    public GlobalReponse addQuestion(@RequestBody AddQuestionDto dto) {
        if (dto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.addQuestion(dto, userId);
    }

    @PostMapping("deletedQuestion")
    @ApiOperation(value = "删除题目", notes = "")
    public GlobalReponse deletedQuestion(Long questionId) {
        if (questionId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.deletedQuestion(questionId, userId);
    }

    @PostMapping("getQuestionDetail")
    @ApiOperation(value = "获取题目详情", notes = "")
    public GlobalReponse<QuestionDetailDto> getQuestionDetail(Long questionId) {
        if (questionId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getQuestionDetail(questionId, userId);
    }

    @PostMapping("getDeadline")
    @ApiOperation(value = "获取截止时间", notes = "")
    public GlobalReponse getDeadline() {
        return classTeacherService.getDeadline();
    }

    @PostMapping("getJobName")
    @ApiOperation(value = "获取作业名称", notes = "")
    public GlobalReponse<JobNameDto> getJobName() {
        return classTeacherService.getJobName();
    }

    @PostMapping("putHomework")
    @ApiOperation(value = "布置作业", notes = "")
    public GlobalReponse putHomework(@RequestBody HomeworkDto homeworkDto) {
        if (homeworkDto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.putHomework(homeworkDto, userId);
    }

    @PostMapping("getScore")
    @ApiOperation(value = "获取题目的最大分数", notes = "")
    public GlobalReponse getScore() {
        return classTeacherService.getScore();
    }

    @PostMapping("getJobListById")
    @ApiOperation(value = "通过班级id多条件查看作业列表", notes = "")
    public GlobalReponse<PageQuery<JobDto>> getJobList(@RequestBody JobConditionDto jobConditionDto) {
        if (jobConditionDto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getJobListById(jobConditionDto,userId);
    }

    @PostMapping("getJobDetails")
    @ApiOperation(value = "通过id查看作业详情", notes = "")
    public GlobalReponse<JobDto> getJobDetails(Long classId,Long jobId) {
        if (jobId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getJobDetails(classId,jobId,userId);
    }

    @PostMapping("getRank")
    @ApiOperation(value = "教师端作业排行榜")
    public GlobalReponse<PageQuery<UserJobVo>> getRank(@RequestBody GetRankDto dto){
        if (dto==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getRankTeacher(dto);
    }

    @PostMapping("getJobQuestionCount")
    @ApiOperation(value = "查询作业题目数量", notes = "")
    public GlobalReponse<QuestionCountDto> getJobQuestionCount(@ApiParam(value = "作业id") @RequestParam(value = "jobId") Long jobId) {
        if (jobId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getJobQuestionCount(jobId);
    }

    @PostMapping("getJobQuestionPage")
    @ApiOperation(value = "多条件查询作业题目列表", notes = "")
    public GlobalReponse<PageQuery<JobQuestionDto>> getJobQuestionPage(@RequestBody GetJobQuestionPageDto dto) {
        if (dto == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getJobQuestionPage(dto);
    }

    @PostMapping("getQuestionDetailFromJob")
    @ApiOperation(value = "通过班级,作业和题目Id获取题目详情", notes = "")
    public GlobalReponse<QuestionDetailDto> getQuestionDetailFromJob(Long classId,Long jobId,Long questionId) {
        if (classId == null || jobId==null ||questionId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getQuestionDetailFromJob(classId,jobId,questionId);
    }


    @PostMapping("getAllClassForJobId")
    @ApiOperation(value = "通过作业Id查看全部班级", notes = "")
    public GlobalReponse<JobDto> getAllClassForJobId(Long jobId) {
        if (jobId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.getAllClassForJobId(jobId,userId);
    }

    @PostMapping("getAllStudentList")
    @ApiOperation(value = "通过班级Id和作业Id查看学生列表", notes = "")
    public GlobalReponse<GetAllStudentListDto> getAllStudentList(Long classId,Long jobId) {
        if (classId==null || jobId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getAllStudentList(classId,jobId);
    }

    @PostMapping("addRemark")
    @ApiOperation(value = "老师添加评语", notes = "")
    public GlobalReponse addRemark(Long userJobId,String remark) {
        if (userJobId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.addRemark(userJobId,remark,userId);
    }

    @PostMapping("getStudentAnswer")
    @ApiOperation(value = "查询学生个人答题记录", notes = "")
    public GlobalReponse<StudentAnswerVoList> getStudentAnswer(Long jobId,Long userId,Long classId) {
        if (jobId==null || userId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classTeacherService.getStudentAnswer(jobId,userId,classId);
    }

    @PostMapping("pullStudentscore")
    @ApiOperation(value = "给学生打分", notes = "")
    public GlobalReponse pullStudentScore(@RequestBody PullStudentScoreDto dto) {
        if (dto==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long teacherId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.pullStudentScore(dto,teacherId);
    }

    @PostMapping("callHomeWork")
    @ApiOperation(value = "催交作业", notes = "")
    public GlobalReponse callHomeWork(Long jobId,Long studentId) {
        if (jobId==null || studentId==null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long teacherId = JwtUtil.getCurrentJwtUser().getId();
        return classTeacherService.callHomeWork(jobId,studentId,teacherId);
    }

}
