package com.boruan.shengtangfeng.api.controller;

import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.GroupDataDto;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.service.IChitchatService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.GroupChatDetailVo;
import com.boruan.shengtangfeng.core.vo.UserRelevanceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @description: v4F聊天入口
 * @date 2021/4/1 10:50
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/chitchat")
@Api(value = "", tags = { "v4F聊天相关接口" })
public class ChitchatApi {

    @Autowired
    private IChitchatService chitchatService;

    @GetMapping("getList")
    @ApiOperation(value = "获取我的好友群聊和黑名单列表 type 0好友 1群聊 2黑名单", notes = "")
    public GlobalReponse<UserRelevanceVo> getList(@ApiParam(value = "type 0好友 1群聊 2黑名单")@RequestParam Integer type, String keyword) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return chitchatService.getList(userId,type,keyword);
    }

    @PutMapping("addOrRemoveBlack")
    @ApiOperation(value = "加入或移出黑名单 type 0移出 1加入", notes = "")
    public GlobalReponse addOrRemoveBlack(Long friendId,@ApiParam(value = " 0移出 1加入")@RequestParam Integer type) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (friendId==null || type==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.addOrRemoveBlack(userId,friendId,type);
    }

    @PostMapping("deletedFriend")
    @ApiOperation(value = "删除好友", notes = "")
    public GlobalReponse deletedFriend(@ApiParam(value = " 好友的Id")@RequestParam Long friendId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (friendId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.deletedFriend(userId,friendId);
    }

    @GetMapping("searchFriendOrGroup")
    @ApiOperation(value = "根据id或名称搜索好友或群聊 type 0好友 1群聊", notes = "")
    public GlobalReponse searchFriendOrGroup(String parameter,@ApiParam(value = " 0好友 1群聊")@RequestParam Integer type) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return chitchatService.searchFriendOrGroup(userId,parameter,type);
    }

    @PostMapping("addFriendOrGroup")
    @ApiOperation(value = "添加好友或申请加群 type 0好友 1群聊", notes = "")
    public GlobalReponse addFriendOrGroup(Long id,String remark,@ApiParam(value = " 0好友 1群聊")@RequestParam Integer type) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (id==null || type==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.addFriendOrGroup(userId,id,remark,type);
    }

    @PostMapping("createGroup")
    @ApiOperation(value = "创建群聊", notes = "")
    public GlobalReponse createGroup(@ApiParam(value = " 好友id的集合")@RequestParam(value = "ids") String ids) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (ids==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.createGroup(userId,ids);
    }

    @PostMapping("getGroupChatDetail")
    @ApiOperation(value = "查看群详情", notes = "")
    public GlobalReponse<GroupChatDetailVo> getGroupChatDetail(@ApiParam(value = " 群id")@RequestParam Long groupId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.getGroupChatDetail(userId,groupId);
    }

    @PostMapping("updateNickName")
    @ApiOperation(value = "修改昵称", notes = "")
    public GlobalReponse updateNickName(@ApiParam(value = "群id")@RequestParam Long groupId,@ApiParam(value = " 昵称")@RequestParam String nickName) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (nickName==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.updateNickName(userId,groupId,nickName);
    }

    @PostMapping("updateGroupData")
    @ApiOperation(value = "查看资料或编辑群资料", notes = "")
    public GlobalReponse updateGroupData(@ApiParam(value = " 群id")@RequestParam Long groupId,@RequestBody GroupDataDto groupDataDto) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.updateGroupData(userId,groupId,groupDataDto);
    }

    @PostMapping("getAllGroupMembers")
    @ApiOperation(value = "获取所有群成员列表", notes = "")
    public GlobalReponse getAllGroupMembers(@ApiParam(value = " 群id")@RequestParam(value = "groupId") Long groupId,String keyword) {
         if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }

        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return chitchatService.getAllGroupMembers(userId,groupId,keyword);
    }

    @PostMapping("deletedGroupMembers")
    @ApiOperation(value = "批量删除群成员", notes = "")
    public GlobalReponse deletedGroupMembers(@ApiParam(value = " 群id")@RequestParam Long groupId,@ApiParam(value = " 群成员id的集合")@RequestParam(value = "ids") String ids) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null || ids==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.deletedGroupMembers(userId,groupId,ids);
    }

    @PostMapping("getFriendList")
    @ApiOperation(value = "获取未加入群的好友列表", notes = "")
    public GlobalReponse<ArrayList<User>> getFriendList(Long groupId,String keyword) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.getFriendList(userId,groupId,keyword);
    }

    @PostMapping("addGroupMembers")
    @ApiOperation(value = "批量邀请成员/邀请好友加入群聊", notes = "")
    public GlobalReponse addGroupMembers(Long groupId,@ApiParam(value = " 好友id的集合")@RequestParam(value = "ids") String ids) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null || ids==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        String[] splitids = ids.split(",");
        Long[] idArr = (Long[]) ConvertUtils.convert(splitids,Long.class);
        return chitchatService.addGroupMembers(userId,groupId,idArr);
    }

    @PostMapping("setGroupAdministrator")
    @ApiOperation(value = "设置群管理员", notes = "")
    public GlobalReponse setGroupAdministrator(@ApiParam(value = " 群id")Long groupId,@ApiParam(value = "添加为管理员好友id的集合")@RequestParam(value = "addIds")String addIds,@ApiParam(value = "删除管理员好友id的集合")@RequestParam(value = "deletedIds")String deletedIds) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        if (addIds==null || deletedIds ==null){
            return GlobalReponse.fail("群成员选择失败");
        }
        return chitchatService.setGroupAdministrator(userId,groupId,addIds,deletedIds);
    }

    @PostMapping("updateAddGroupWay")
    @ApiOperation(value = "设置加群方式", notes = "")
    public GlobalReponse updateAddGroupWay(@ApiParam(value = " 群id")@RequestParam Long groupId,@ApiParam(value = "加群方式  0禁止加群 ，1管理员审核，2不需要审核 ")@RequestParam Integer type) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null || type==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.updateAddGroupWay(userId,groupId,type);
    }

    @PostMapping("transferGroup")
    @ApiOperation(value = "转让群", notes = "")
    public GlobalReponse transferGroup(@ApiParam(value = " 群id")@RequestParam Long groupId,@ApiParam(value = " 个人id")@RequestParam Long memberId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null || memberId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.transferGroup(userId,groupId,memberId);
    }

    @PostMapping("dissolveGroup")
    @ApiOperation(value = "解散群", notes = "")
    public GlobalReponse dissolveGroup(@ApiParam(value = " 群id")@RequestParam Long groupId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.dissolveGroup(userId,groupId);
    }

    @PostMapping("quitGroup")
    @ApiOperation(value = "退出群聊", notes = "")
    public GlobalReponse quitGroup(@ApiParam(value = " 群id")@RequestParam Long groupId) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (groupId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return chitchatService.quitGroup(userId,groupId);
    }
}
