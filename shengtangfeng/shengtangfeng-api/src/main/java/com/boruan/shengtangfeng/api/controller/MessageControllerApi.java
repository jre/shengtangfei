package com.boruan.shengtangfeng.api.controller;


import com.boruan.shengtangfeng.api.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.service.IMessageService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author guojiang
 * @projectName ShengTangFeng-V2-JAVA
 * @description: v4E 消息
 * @date 2021/5/15 9:20
 */

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/api/classTeacher")
@Api(value = "", tags = {"v4E 消息"})
public class MessageControllerApi {

    @Autowired
    private IMessageService messageService;

    @GetMapping("getNewFriendList")
    @ApiOperation(value = "获取新好友消息列表", notes = "")
    public GlobalReponse<NewFriendVo> getNewFriendList() {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.getNewFriendList(userId);
    }

    @PostMapping("deletedNewFriend")
    @ApiOperation(value = "删除新好友消息", notes = "")
    public GlobalReponse deletedNewFriend(@ApiParam(value = "记录的id")@RequestParam Long id) {
        if (id==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.deletedNewFriend(id,userId);
    }

    @PostMapping("agreeOrRefuseApplyFriend")
    @ApiOperation(value = "同意或拒绝好友申请 status 1同意 2拒绝", notes = "")
    public GlobalReponse agreeOrRefuseApplyFriend(@ApiParam(value = "记录的id")@RequestParam Long id, @ApiParam(name = "status",value = "1同意 2拒绝")@RequestParam Integer status) {
        if (id==null || status == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.agreeOrRefuseApplyFriend(userId,id,status);
    }

    @PostMapping("agreeByMobile")
    @ApiOperation(value = "通过申请人的手机号同意或拒绝好友申请 status 1同意 2拒绝", notes = "")
    public GlobalReponse agreeByMobile(@ApiParam(value = "申请人的手机号")@RequestParam String mobile, @ApiParam(name = "status",value = "1同意 2拒绝")@RequestParam Integer status) {
        if (mobile==null || status == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.agreeByMobile(userId,mobile,status);
    }

    @GetMapping("getNewGroupList")
    @ApiOperation(value = "获取加群消息列表", notes = "")
    public GlobalReponse<NewGroupVo> getNewGroupList() {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.getNewGroupList(userId);
    }

    @GetMapping("deletedNewGroup")
    @ApiOperation(value = "删除加群消息", notes = "")
    public GlobalReponse deletedNewGroup(@ApiParam(value = "记录的id")@RequestParam Long id,@ApiParam(name = "apply",value = "列表数据中的apply")@RequestParam Integer apply) {
        if (apply==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.deletedNewGroup(id,userId,apply);
    }

    @PostMapping("agreeOrRefuseApplyGroup")
    @ApiOperation(value = "同意或拒绝群聊申请 点击忽略时候status传3", notes = "")
    public GlobalReponse agreeOrRefuseApplyGroup(@ApiParam(value = "记录的id")@RequestParam Long id,@ApiParam(value = "1同意 2拒绝 3忽略")@RequestParam Integer status) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (id==null || status == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return messageService.agreeOrRefuseApplyGroup(userId,id,status);
    }

    @GetMapping("getClassMessageTeacher")
    @ApiOperation(value = "获取班级消息老师和学生", notes = "")
    public GlobalReponse<NewClassMessageVo> getClassMessageTeacher() {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.getClassMessageTeacher(userId);
    }

    @GetMapping("deletedClassMessage")
    @ApiOperation(value = "删除班级消息", notes = "")
    public GlobalReponse deletedClassMessage(@ApiParam(value = "记录的id")@RequestParam Long id,@ApiParam(name = "apply",value = "apply")@RequestParam Integer apply) {
        if (apply==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.deletedClassMessage(id,userId,apply);
    }

    @PostMapping("agreeOrRefuseApplyClass")
    @ApiOperation(value = "同意或拒绝班级申请 status 1同意 2拒绝", notes = "")
    public GlobalReponse agreeOrRefuseApplyClass(@ApiParam(value = "记录的id")@RequestParam Long id, @ApiParam(name = "status",value = "1同意 2拒绝")@RequestParam Integer status) {
        if (id==null || status == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.agreeOrRefuseApplyClass(userId,id,status);
    }

    @GetMapping("getInteractMessage")
    @ApiOperation(value = "获取互动消息", notes = "")
    public GlobalReponse<VideoFriendVo> getInteractMessage() {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.getInteractMessage(userId);
    }

    @GetMapping("deletedInteractMessage")
    @ApiOperation(value = "删除互动消息", notes = "")
    public GlobalReponse deletedInteractMessage(@ApiParam(value = "记录的id")@RequestParam Long id) {
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return messageService.deletedInteractMessage(id,userId);
    }

}
