package com.boruan.shengtangfeng.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.boruan.shengtangfeng.core.dao.IBadWordDao;
import com.boruan.shengtangfeng.core.entity.BadWord;
import com.boruan.shengtangfeng.core.utils.CommentUtil;

@Component
public class MyCommandLineRunner implements CommandLineRunner {

    @Autowired
    private IBadWordDao badWordDao;

    @Override
    public void run(String... args) throws Exception {
        BadWord search = new BadWord();
        search.setIsDeleted(false);
        List<BadWord> list = badWordDao.template(search);

        //完全禁止的词汇
        List<String> level0Names = list.stream().filter(e -> (e.getLevel() == 0)).map(BadWord::getName).collect(Collectors.toList());
        //可放行审核的词汇
        List<String> level1Names = list.stream().filter(e -> (e.getLevel() == 1)).map(BadWord::getName).collect(Collectors.toList());

        String[] level0Arr = new String[level0Names.size()];
        level0Names.toArray(level0Arr);
        CommentUtil.level0BadWord = level0Arr;

        String[] level1Arr = new String[level1Names.size()];
        level1Names.toArray(level1Arr);
        CommentUtil.level1BadWord = level1Arr;
    }

}
