//package com.boruan.shengtangfeng.admin.controller;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.ResourceLoader;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.alibaba.fastjson.JSON;
//import com.boruan.shengtangfeng.admin.jwt.UserSubject;
//import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
//import com.boruan.shengtangfeng.core.service.IUploadFileService;
//import com.boruan.shengtangfeng.core.utils.GlobalReponse;
//import com.google.common.base.Strings;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//
///**
// * @author liuguangqiang
// */
//@RestController
//@RequestMapping("/admin/upload")
//@Api(value = "", tags = { "NO:8 文件上传相关接口" })
//public class UploadApi {
//
//	@Autowired
//	private IUploadFileService uploadFileService;
//	@Autowired
//	ResourceLoader resourceLoader;
//
//	@PostMapping("/uploadPublicCk")
//	@ApiOperation(value = "NO:8-1 上传公共图片（暂时先不用）", notes = "")
//	public Map<String, Object> uploadPublic(@RequestParam MultipartFile upload, HttpServletRequest request,
//			HttpServletResponse response) {
//		Map<String, Object> result = new HashMap<>();
//		UserSubject userSubject = JwtUtil.getCurrentJwtUser();
//		String uploadContentType = upload.getContentType();
//		if (uploadContentType.equals("image/pjpeg") || uploadContentType.equals("image/jpeg")) {
//		} else if (uploadContentType.equals("image/png") || uploadContentType.equals("image/x-png")) {
//			// IE6上传的png图片的headimageContentType是"image/x-png"
//		} else if (uploadContentType.equals("image/gif")) {
//		} else if (uploadContentType.equals("image/bmp")) {
//		} else {
//			result.put("uploaded", 0);
//			result.put("error", "{'message':'文件格式不正确'}");
//			return result;
//		}
//		GlobalReponse rep = uploadFileService.uploadImageFile(upload, userSubject.getId());
//		result.put("uploaded", 1);
//		result.put("fileName", rep.getData3());
//		result.put("url", rep.getData());
//		return result;
//	}
//
//	/**
//	 * 
//	 * @author:Jessica
//	 * @Descrition:文件上传
//	 * @date:2020年3月10日 上午11:14:10
//	 * @return
//	 */
//	@PostMapping("/upLoadFile")
//	@ApiOperation(value = "NO:8-2 上传文件", notes = "")
//	public GlobalReponse upLoadFile(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request,
//			HttpServletResponse response) {
//		Map<String, Object> result = new HashMap<>();
//		UserSubject userSubject = JwtUtil.getCurrentJwtUser();
//		String uploadContentType = file.getContentType();
//
//		GlobalReponse rep = uploadFileService.uploadImageFile(file, userSubject.getId());
//		result.put("uploaded", 1);
//		result.put("fileName", rep.getData3());
//		result.put("url", rep.getData());
//		return GlobalReponse.success(result);
//	}
//	
//	/**
//    *
//    * @author:Cookie
//    * @Descrition:ueditor文件上传
//    * @return
//    */
//   @RequestMapping("/ueditor")
//   @ResponseBody
//   @ApiOperation(value = "NO:8-3", notes = "")
//   public Object ueditorUpLoadFile(@RequestParam(required = false, name = "upfile") MultipartFile file, @RequestParam(required = false) String action) throws IOException {
//       UserSubject userSubject = JwtUtil.getCurrentJwtUser();
//       if (!Strings.isNullOrEmpty(action) && action.equals("config")) {
//           String jsonStr = "";
//           Resource resource = resourceLoader.getResource("classpath:ueditor.json");
//           InputStream is = resource.getInputStream();
//           InputStreamReader reader = new InputStreamReader(is);
//           int ch = 0;
//           StringBuffer sb = new StringBuffer();
//           while ((ch = reader.read()) != -1) {
//               sb.append((char) ch);
//           }
//           is.close();
//           reader.close();
//           jsonStr = sb.toString();
//
//           Map jsonMap = (Map) JSON.parse(jsonStr);
//           return jsonMap;
//       } else {
//           try {
//               GlobalReponse reponse = uploadFileService.uploadImageFile(file,userSubject==null?0L:userSubject.getId());
//               if (reponse.getData()!=null) {
//                   Map<String, Object> result = new HashMap<>();
//                   result.put("url", reponse.getData());
//                   result.put("state", "SUCCESS");
//                   result.put("title", file.getOriginalFilename());
//                   result.put("imageFileName", file.getOriginalFilename());
//                   result.put("original", file.getOriginalFilename());
//                   result.put("size", file.getSize());
//                   result.put("type", file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."), file.getOriginalFilename().length()));
//                   return result;
//               }
//           } catch (Exception e) {
//               e.printStackTrace();
//           }
//       }
//
//       return GlobalReponse.fail();
//   }
//
//}
