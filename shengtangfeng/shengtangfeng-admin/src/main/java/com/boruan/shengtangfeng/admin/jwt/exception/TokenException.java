package com.boruan.shengtangfeng.admin.jwt.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date:2020年3月10日 上午11:14:10
 */
public class TokenException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public TokenException(String message) {
		super(message);
	}
}
