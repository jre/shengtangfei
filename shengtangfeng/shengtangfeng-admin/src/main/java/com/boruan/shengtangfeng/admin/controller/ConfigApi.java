package com.boruan.shengtangfeng.admin.controller;

import java.util.Date;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Config;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Log.LogType;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.IConfigService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: lihaicheng
 * @Description: 配置管理
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@RequestMapping("/admin/config")
@SuppressWarnings("all")
@Api(value = "", tags = { "NO:1 配置相关接口" })
public class ConfigApi {

	@Autowired
	private IConfigService configService;
	@Autowired
	private ILogService logService;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @author: lihaicheng
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	@ApiOperation(value = "NO:1-1 分页获取所有配置信息", notes = "")
	public GlobalReponse<PageQuery<Config>> page(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
												 @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
			Config config, Model model) {
		PageQuery<Config> query = new PageQuery<Config>();
		query.setPageNumber(pageIndex);
		query.setPageSize(pageSize);
		configService.pageQuery(query, config);
		return GlobalReponse.success(query);
	}

	/**
	 * @author: lihaicheng
	 * @Description: 详情
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@ApiOperation(value = "NO:1-2 根绝id获取配置详情", notes = "")
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public GlobalReponse<Config> get(Long id, Model model) {
		Config config = configService.findById(id);
		return GlobalReponse.success(config);
	}

	/**
	 * @author: lihaicheng
	 * @Description: 产品分类只能修改上架天数
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@ApiOperation(value = "NO:1-3 添加或者更新配置信息", notes = "")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public GlobalReponse add(@RequestBody Config config) {
		try {
			UserSubject userSubject = JwtUtil.getCurrentJwtUser();
			Log sysLog = new Log();
			// 更新
			Config old = configService.findById(config.getId());
			old.setValue(config.getValue());
			old.setUpdateTime(new Date());
			old.setUpdateBy(userSubject.getName());
			configService.save(old);
			sysLog.setObjectId(old.getId());
			sysLog.setContent("管理员修改" + old.getDescription());
			sysLog.setOperateType(OperateType.UPDATE);
			sysLog.setCreateTime(new Date());
			sysLog.setOperater(userSubject.getId());
			sysLog.setLogType(LogType.ADMIN);
			logService.save(sysLog);
			return GlobalReponse.success();
		} catch (Exception e) {
			log.error("管理员修改配置信息失败:{}", e);
			return GlobalReponse.fail();
		}
	}
}
