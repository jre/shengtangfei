package com.boruan.shengtangfeng.admin.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/* *
 * @Description swagger2配置 默认地址http://localhost:8080/swagger-ui.html
 * @Date 21:05 2018/3/17
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
//		        .host("hedu720.cn")
				.select().apis(RequestHandlerSelectors.basePackage("com.boruan.shengtangfeng.admin"))
				.paths(PathSelectors.any()).build().globalOperationParameters(setHeaderToken());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("盛唐风后台:" + " Restful APIs").description("接口文档")
//				.termsOfServiceUrl("https://hedu720.cn")
				.version("1.0").build();
	}

	private List<Parameter> setHeaderToken() {
		ParameterBuilder tokenPar = new ParameterBuilder();
		List<Parameter> pars = new ArrayList<>();
		tokenPar.name("Authorization").description("token").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();
		pars.add(tokenPar.build());
		return pars;
	}

}
