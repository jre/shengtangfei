package com.boruan.shengtangfeng.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ErrorProperties.IncludeStacktrace;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("${server.error.path:${error.path:/error}}")
public class GlobalExceptionController extends AbstractErrorController {
	private final ErrorProperties errorProperties;

	@Autowired
	public GlobalExceptionController(ErrorAttributes errorAttributes, ServerProperties serverProperties) {
		super(errorAttributes);
		this.errorProperties = serverProperties.getError();
	}

	@Override
	public String getErrorPath() {
		return errorProperties.getPath();
	}

	@RequestMapping(produces = "text/html")
	public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("error");
		Map<String, Object> errorMap = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
		if (errorMap != null) {
			/* timestamp status error message path */
			modelAndView.addObject("msg", errorMap.get("error"));
			modelAndView.addObject("statusCode", errorMap.get("status"));
			logHandler(errorMap);
		}
		return modelAndView;
	}

	@RequestMapping
	@ResponseBody
	public GlobalReponse error(HttpServletRequest request) {
		Map<String, Object> errorMap = getErrorAttributes(request,
				isIncludeStackTrace(request, MediaType.APPLICATION_JSON));
		logHandler(errorMap);
		return GlobalReponse.success(errorMap);
	}

	private void logHandler(Map<String, Object> errorMap) {
		log.error("url:{},status{},time:{},errorMsg:{}", errorMap.get("path"), errorMap.get("status"),
				errorMap.get("timestamp"), errorMap.get("message"));
	}

	protected boolean isIncludeStackTrace(HttpServletRequest request, MediaType produces) {
		IncludeStacktrace include = getErrorProperties().getIncludeStacktrace();
		if (include == IncludeStacktrace.ALWAYS) {
			return true;
		}
		if (include == IncludeStacktrace.ON_TRACE_PARAM) {
			return getTraceParameter(request);
		}
		return false;
	}

	private ErrorProperties getErrorProperties() {
		return this.errorProperties;
	}
}
