package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Question;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.entity.VideoGrade;
import com.boruan.shengtangfeng.core.service.*;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import com.boruan.shengtangfeng.core.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author: guojiang
 * @Description: 查看用户全部评论相关接口
 * @date:2020/2/25
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/checkUserComment")
@Api(value = "", tags = {"v3C审核端--查看用户全部文章视频评论相关接口"})
public class CheckUserMessageApi {
    @Autowired
    private IUserService iUserService;
    @Autowired
    private IArticleService iArticleService;
    @Autowired
    private IArticleCommentService iArticleCommentService;
    @Autowired
    private IVideoService iVideoService;
    @Autowired
    private IVideoCommentService iVideoCommentService;
    @Autowired
    private ICommentService iCommentService;
    @Autowired
    private IQuestionService iQuestionService;
    @Autowired
    private IQuestionCommentService iQuestionCommentService;

    @GetMapping("/blockOrUnblockUser")
    @ApiOperation(value = "将用户 封号1 或 解封0", notes = "")
    public GlobalReponse blockOrUnblockUser(Long userId, Integer status) {
        if (userId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long adminId= JwtUtil.getCurrentJwtUser().getId();
        return iUserService.blockOrUnblockUser(adminId,userId,status);
    }

    @GetMapping("/getUserStatus")
    @ApiOperation(value = "查看用户状态", notes = "")
    public GlobalReponse<User> getUserStatus(Long userId) {
        if (userId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iUserService.getUserStatus(userId);
    }


    @GetMapping("/getUserArticle")
    @ApiOperation(value = "根据用户id查看用户文章", notes = "")
    public GlobalReponse<PageQuery<ToAuditArticleVO>> getUserArticle(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                                                                     @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                                     @ApiParam(value = "关键词")@RequestParam(value = "keywords",required = false)String keywords,
                                                                     Long userId) {
        if (userId==null){
            return GlobalReponse.fail("请传入有效传参数");
        }
        return iArticleService.getUserArticle(pageIndex,pageSize,userId,keywords);
    }

    @GetMapping("/getArticleDetails")
    @ApiOperation(value = "根据文章id获取文章详情", notes = "")
    public GlobalReponse<ToAuditArticleVO> getArticleDetails(Long articleId) {
        if (articleId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iArticleService.getArticleDetails(articleId);
    }

    @DeleteMapping("/deleteArticleCommentById")
    @ApiOperation(value = "根据文章评论id删除评论", notes = "")
    public GlobalReponse deleteArticleCommentById(Long commentId) {
        if (commentId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long adminId= JwtUtil.getCurrentJwtUser().getId();
        iArticleCommentService.deleteComment(commentId,adminId);
        return GlobalReponse.success("删除成功");
    }

    @GetMapping("/getUserVideo")
    @ApiOperation(value = "根据用户id查看用户视频", notes = "")
    public GlobalReponse<PageQuery<ToAuditVideoVO>> getUserVideo(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                                                                 @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                                 @RequestParam(value = "userId") Long userId,
                                                                 @ApiParam(value = "类型：0普通视频1讲一讲2小视频") @RequestParam(value = "type",required = false) Integer type,
                                                                 @RequestParam(value = "keywords",required = false)String keywords) {
        if (userId==null){
            return GlobalReponse.fail("请传入有效传参数");
        }
        return iVideoService.getUserVideo(pageIndex,pageSize,userId,type,keywords);
    }


    @GetMapping("/getVideoDetails")
    @ApiOperation(value = "根据视频id获取视频详情", notes = "")
    public GlobalReponse<ToAuditVideoVO> getVideoDetails(Long videoId) {
        if (videoId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iVideoService.getVideoDetails(videoId);
    }

    @DeleteMapping("/deleteVideoCommentById")
    @ApiOperation(value = "根据视频评论id删除评论", notes = "")
    public GlobalReponse deleteVideoCommentById(Long commentId) {
        if (commentId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long adminId= JwtUtil.getCurrentJwtUser().getId();
        iVideoCommentService.deleteComment(commentId,adminId);
        return GlobalReponse.success("删除成功");
    }

    @GetMapping("/getUserComment")
    @ApiOperation(value = "根据用户id查看用户评论", notes = "")
    public GlobalReponse<PageQuery<CommentVo>> getUserComment(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                                                              @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                                              @ApiParam(value = "关键词")@RequestParam(value = "keywords",required = false)String keywords,
                                                              Long userId) {
        if (userId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iCommentService.getUserComment(pageIndex,pageSize,userId,keywords);
    }

    @GetMapping("/getQuestionDetail")
    @ApiOperation(value = "通过试题评论id查看题目", notes = "")
    public GlobalReponse<QuestionVo> getQuestionDetail(Long commentId) {
        if (commentId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iQuestionService.getQuestionDetail(commentId);
    }

    @GetMapping("/getQuestionCommentById")
    @ApiOperation(value = "根据题目id查看全部评论", notes = "")
    public GlobalReponse<QuestionCommentVo> getQuestionCommentById(Long questionId) {
        if (questionId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return iQuestionCommentService.getQuestionDetail(questionId);
    }

    @DeleteMapping("/deleteQuestionCommentById")
    @ApiOperation(value = "根据试题评论id删除评论", notes = "")
    public GlobalReponse deleteQuestionCommentById(Long commentId) {
        if (commentId == null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long adminId= JwtUtil.getCurrentJwtUser().getId();
        return iQuestionCommentService.deleteComment(commentId,adminId);
    }

    @PostMapping("/pageUserVideoGrade")
    @ApiOperation(value = "查看用户全部点评")
    public GlobalReponse<PageQuery<VideoGrade>> pageUserVideoGrade(@ApiParam(value = "页码")@RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo,
                                                                   @ApiParam(value = "每页数量")@RequestParam(value = "pageSize")Integer pageSize,
                                                                   @ApiParam(value = "用户ID")@RequestParam(value = "userId")Long userId,
                                                                   @ApiParam(value = "关键词")@RequestParam(value = "keywords")String keywords){
        PageQuery<VideoGrade> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);
        pageQuery.setPara("userId",userId);
        if(StringUtils.isNotBlank(keywords)){
            pageQuery.setPara("remark",keywords);
        }
        iVideoService.pageVideoGrade(pageQuery);
        return GlobalReponse.success(pageQuery);
    }
}
