package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.IncorrentQuestionListDto;
import com.boruan.shengtangfeng.core.entity.IncorrectQuestion;
import com.boruan.shengtangfeng.core.service.IQuestionService;
import com.boruan.shengtangfeng.core.utils.Global;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/admin/auditQuestion")
@Api(value = "V4D审核端",tags = {"V4D审核端--审核管理用户题目相关接口"})
public class AuditQuestionApi {

    @Autowired
    private IQuestionService questionService;

    @PostMapping("/selfQuestion")
    @ApiOperation(value = "V4D-01 待审核的错题列表")
    public GlobalReponse<PageQuery<IncorrectQuestion>> getSelfUpQuestion(@RequestBody IncorrentQuestionListDto incorrentQuestionListDto, @RequestParam(value = "pageNo",defaultValue = "1") int pageNo){
        //实例化分页类
        PageQuery<IncorrectQuestion> pageQuery = new PageQuery<IncorrectQuestion>();
        pageQuery.setPageNumber(pageNo);//设置分页
        pageQuery.setParas(incorrentQuestionListDto);
        questionService.auditSelfQuestion(pageQuery);
        return GlobalReponse.success(pageQuery);
    }


    @PostMapping("/confirmSelfQuestion")
    @ApiOperation(value = "V4D-02 审核错题集")
    public GlobalReponse confirmSelfQuestion(@ApiParam(value = "错题集Id") @RequestParam(value = "inQuestionId") Long inQuestionId, @ApiParam(value = "1通过；2失败")@RequestParam(value = "status") int status){
        UserSubject userSubject= JwtUtil.getCurrentJwtUser();
        IncorrectQuestion incorrectQuestion = questionService.findInQuestion(inQuestionId);
        incorrectQuestion.setStatus(status);
        questionService.auditInQuestion(incorrectQuestion);
        return GlobalReponse.success("审核成功");
    }
}
