package com.boruan.shengtangfeng.admin.controller;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.service.impl.OssServiceImpl;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.OssPolicyResult;
import com.google.common.base.Strings;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Oss相关操作接口
 */
@RestController
@Api(tags = { "NO:12 OSS接口" }, description = "Oss管理")
@RequestMapping("/admin/oss")
public class OssController {
    @Autowired
    private OssServiceImpl ossService;
    @Autowired
    ResourceLoader resourceLoader;
    @PostMapping("/uploadImage")
    @ApiOperation(value = "NO 12-2 oss图片上传", notes = "单个图片上传，返回地址")
    @ResponseBody
    @CrossOrigin
    public GlobalReponse<String> uploadImage(@RequestParam("file") MultipartFile file) {
        return ossService.uploadFile(file);
    }
//    
//    @PostMapping("/uploadImages")
//    @ApiOperation(value = "NO 12-3 oss多图片上传", notes = "多图片上传，返回逗号分隔的地址")
//    @ResponseBody
//    public GlobalReponse<String> uploadImages(@RequestParam("file") MultipartFile[]  file) {
//        return ossService.uploadFiles(file);
//    }
//
////    @ApiOperation(value = "oss上传成功回调")
//    @ApiIgnore
//    @RequestMapping(value = "callback", method = RequestMethod.POST)
//    @ResponseBody
//    public GlobalReponse<OssCallbackResult> callback(HttpServletRequest request) {
//        OssCallbackResult ossCallbackResult = ossService.callback(request);
//        return GlobalReponse.success(ossCallbackResult);
//    }
    @GetMapping(value = "/policy")
    public GlobalReponse<OssPolicyResult> policy() {
        Object result = ossService.policy();
        return GlobalReponse.success(result);
    }
    /**
    *
    * @author:Cookie
    * @Descrition:ueditor文件上传
    * @return
    */
   @RequestMapping("/ueditor")
   @ResponseBody
   @ApiOperation(value = "NO:8-3", notes = "")
   public Object ueditorUpLoadFile(@RequestParam(required = false, name = "upfile") MultipartFile file, @RequestParam(required = false) String action) throws IOException {
       UserSubject userSubject = JwtUtil.getCurrentJwtUser();
       if (!Strings.isNullOrEmpty(action) && action.equals("config")) {
           String jsonStr = "";
           Resource resource = resourceLoader.getResource("classpath:ueditor.json");
           InputStream is = resource.getInputStream();
           InputStreamReader reader = new InputStreamReader(is);
           int ch = 0;
           StringBuffer sb = new StringBuffer();
           while ((ch = reader.read()) != -1) {
               sb.append((char) ch);
           }
           is.close();
           reader.close();
           jsonStr = sb.toString();

           Map jsonMap = (Map) JSON.parse(jsonStr);
           return jsonMap;
       } else {
           try {
               GlobalReponse reponse = ossService.uploadFile(file);
               if (reponse.getData()!=null) {
                   Map<String, Object> result = new HashMap<>();
                   result.put("url", reponse.getData());
                   result.put("state", "SUCCESS");
                   result.put("title", file.getOriginalFilename());
                   result.put("imageFileName", file.getOriginalFilename());
                   result.put("original", file.getOriginalFilename());
                   result.put("size", file.getSize());
                   result.put("type", file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."), file.getOriginalFilename().length()));
                   return result;
               }
           } catch (Exception e) {
               e.printStackTrace();
           }
       }

       return GlobalReponse.fail();
   }
}
