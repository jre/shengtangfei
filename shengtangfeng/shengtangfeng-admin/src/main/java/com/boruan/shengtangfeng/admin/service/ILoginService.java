package com.boruan.shengtangfeng.admin.service;

import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.LoginResult;
import com.boruan.shengtangfeng.core.vo.SystemUserVo;

/**
 * @author: lihaicheng
 * @Description:
 * @date:2020年3月10日 上午11:14:10
 */
public interface ILoginService {

	/**
	 * 获取验证码
	 * 
	 * @param mobile
	 * @return
	 */
	public GlobalReponse<String> getVerficationCode(String mobile, Integer type);

	/**
	 * 校验手机号码是否存在
	 * 
	 * @param mobile
	 * @return
	 */
	public GlobalReponse<Boolean> verificationMobile(String mobile);

	/**
	 * 密码登录
	 * 
	 * @param mobile
	 * @param password
	 * @return
	 */
	public GlobalReponse<LoginResult> passwordLogin(String loginName, String password);

	/**
	 * 微信登录后绑定手机
	 * 
	 * @param mobile
	 * @param code
	 * @param wxAccessToken
	 * @param openid
	 * @return
	 */
	GlobalReponse<SystemUserVo> bindingMobile(String mobile, String code, Long adminId);

	/**
	 * @author: 刘光强
	 * @Description: 更新密码或者忘记密码
	 * @date:2020年3月10日 上午11:14:10
	 */
	public GlobalReponse updatePassword(String mobile, String password, String authCode);

	/**
	 * 审核端修改密码
	 * @param userId
	 * @param password
	 * @param newPassword
	 * @param newPasswordAgain
	 * @return
	 */
    GlobalReponse updatePasswordForNew(Long userId, String password, String newPassword, String newPasswordAgain);

}
