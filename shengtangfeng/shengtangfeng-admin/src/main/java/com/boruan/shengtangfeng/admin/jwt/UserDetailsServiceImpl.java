package com.boruan.shengtangfeng.admin.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.boruan.shengtangfeng.core.dao.ISystemUserDao;
import com.boruan.shengtangfeng.core.entity.SystemUser;

/**
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private ISystemUserDao systemUserDao;

	@Override
	public UserSubject loadUserByUsername(String loginName) throws UsernameNotFoundException {
		SystemUser user = systemUserDao.findByLoginName(loginName);
		if (user == null) {
			throw new UsernameNotFoundException(loginName);
		}
		return JwtUserFactory.create(user);
	}

}
