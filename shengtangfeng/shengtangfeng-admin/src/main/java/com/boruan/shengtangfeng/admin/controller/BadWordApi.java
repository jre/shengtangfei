package com.boruan.shengtangfeng.admin.controller;

import java.util.Date;
import java.util.List;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.core.entity.BadWord;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.IBadWordService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/badWord")
@Api(value = "", tags = {"NO:27 敏感词相关接口"})
public class BadWordApi {
    @Autowired
    private IBadWordService badWordService;
    @Autowired
    private ILogService sysLogService;

    @GetMapping("/page")
    @ApiOperation(value = "NO:7-2 分页获取敏感词", notes = "")
    public GlobalReponse<PageQuery<BadWord>> page(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
													@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
													BadWord badWord) {
        PageQuery<BadWord> pageQuery = new PageQuery<BadWord>();
        pageQuery.setPageNumber(pageIndex);
        pageQuery.setPageSize(pageSize);
        badWord.setIsDeleted(false);
        badWordService.pageQuery(pageQuery, badWord);
        List<BadWord> list = pageQuery.getList();
        return GlobalReponse.success(pageQuery);
    }

    @CrossOrigin
    @PostMapping(value = "/saveBadWord")
    @ApiOperation(value = "NO:7-8 添加敏感词", notes = "")
    public GlobalReponse saveBadWord(@RequestBody BadWord badWord) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();
            Log sysLog = new Log();
            if (badWord.getId() == null) {
                // 添加
                badWord.setCreateBy(userSubject.getName());
                badWord.setCreateTime(new Date());
                badWord.setIsDeleted(false);
                badWordService.save(badWord);
                sysLog.setObjectId(badWord.getId());
                sysLog.setContent("管理员添加敏感词信息");
                sysLog.setOperateType(OperateType.ADD);
            } else {
                // 更新
                BadWord old = badWordService.findById(badWord.getId());
                old.setName(badWord.getName());
                old.setUpdateBy(userSubject.getName());
                old.setUpdateTime(new Date());
                badWordService.save(old);
                sysLog.setObjectId(old.getId());
                sysLog.setContent("管理员修改敏感词信息");
                sysLog.setOperateType(OperateType.UPDATE);
            }
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            sysLogService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员编辑敏感词失败:{}", e);
            return GlobalReponse.fail();
        }
        
    }
    @GetMapping("delete")
    @ApiOperation(value = "NO:15-1 删除敏感词", notes = "")
    public GlobalReponse delete(Long id) {
        
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        BadWord badWord = badWordService.findById(id);
        badWord.setIsDeleted(true);
        badWord.setUpdateBy(userSubject.getId().toString());
        badWord.setUpdateTime(new Date());
        try {
            badWordService.save(badWord);
            Log sysLog = new Log();
            sysLog.setContent("删除敏感词信息");
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setObjectId(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            sysLog.setOperateType(OperateType.DELETE);
            sysLogService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("删除敏感词信息失败:{}", e.getMessage());
            return GlobalReponse.fail();
        }
    }

    @CrossOrigin
    @PostMapping(value = "/isLevel")
    @ApiOperation(value = "NO:22-4 禁止或者取消禁止敏感词", notes = "")
    public GlobalReponse releaseExamPager(@RequestBody BadWord badWord) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        BadWord badWordNew = badWordService.findById(badWord.getId());
        badWordNew.setLevel(badWord.getLevel());
        badWordNew.setUpdateBy(userSubject.getId().toString());
        badWordNew.setUpdateTime(new Date());
        try {
            badWordService.save(badWordNew);
            Log sysLog = new Log();
            sysLog.setContent("敏感词管理禁止敏感词信息");
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setObjectId(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            sysLog.setOperateType(Log.OperateType.DELETE);
            sysLogService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("敏感词管理禁止或者取消敏感词信息失败:{}", e.getMessage());
            return GlobalReponse.fail();
        }
    }
}
