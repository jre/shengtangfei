package com.boruan.shengtangfeng.admin.controller;

import com.boruan.shengtangfeng.core.dto.FeedbackDto;
import com.boruan.shengtangfeng.core.dto.mapper.FeedbackDtoMapper;
import com.boruan.shengtangfeng.core.entity.Feedback;
import com.boruan.shengtangfeng.core.service.IFeedBackService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.FeedbackVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/admin/feedBack")
@Api(value = "", tags = {"NO:30 意见反馈"})
public class FeedBackApi {
    @Autowired
    private IFeedBackService feedBackService;
    @CrossOrigin
    @GetMapping(value = "/pageFeedBack")
    @ApiOperation(value = "NO:30-1 查看意见反馈列表", notes = "")
    public GlobalReponse<FeedbackVo> pageFeedBack(FeedbackDto feedbackDto, @RequestParam(value = "pageNumber", defaultValue = "1") int pageNumber,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        PageQuery<Feedback> pageData = new PageQuery<Feedback>();
        pageData.setPageNumber(pageNumber);
        pageData.setPageSize(pageSize);
        Feedback feedback = FeedbackDtoMapper.MAPPER.toEntity(feedbackDto);
        feedBackService.pageQuery(pageData, feedback);
        return GlobalReponse.success().setData(pageData);
    }
}
