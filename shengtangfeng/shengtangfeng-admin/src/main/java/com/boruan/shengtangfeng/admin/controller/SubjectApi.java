package com.boruan.shengtangfeng.admin.controller;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Advertise;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Module;
import com.boruan.shengtangfeng.core.entity.ModuleRecord;
import com.boruan.shengtangfeng.core.entity.Question;
import com.boruan.shengtangfeng.core.entity.Subject;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.IAdvertiseService;
import com.boruan.shengtangfeng.core.service.IArticleService;
import com.boruan.shengtangfeng.core.service.IConfigService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.ISubjectService;
import com.boruan.shengtangfeng.core.service.IModuleRecordService;
import com.boruan.shengtangfeng.core.service.IModuleService;
import com.boruan.shengtangfeng.core.service.IQuestionService;
import com.boruan.shengtangfeng.core.service.ISubjectService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.AdvertiseVo;
import com.boruan.shengtangfeng.core.vo.SubjectVo;
import com.boruan.shengtangfeng.core.vo.ModuleVo;
import com.boruan.shengtangfeng.core.vo.QuestionVo;
import com.boruan.shengtangfeng.core.vo.SubjectVo;
import com.boruan.shengtangfeng.core.vo.mapper.AdvertiseVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.SubjectVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.ModuleVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.QuestionVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.SubjectVoMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: 刘光强
 * @Description: 首页接口
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/subject")
@Api(value = "", tags = { "NO:5 淘学主页相关接口" })
public class SubjectApi {
    @Autowired
    private IAdvertiseService advertiseService;
    @Autowired
    private IArticleService articleService;
    @Autowired
    private IQuestionService questionService;
    @Autowired
    private IConfigService configService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISubjectService subjectService;
    @Autowired
    private IModuleService moduleService;
    @Autowired
    private ILogService logService;
    @Autowired
    private JwtUtil jwtUtil;
    
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @GetMapping("/getSubject/{gradeId}")
    @ApiOperation(value = "NO:5-3 获取年级下的所有学科 不需要登录", notes = "")
    public GlobalReponse<List<SubjectVo>> getSubject(@PathVariable("gradeId") Long gradeId) {
        List<Subject> result = subjectService.getListByGradeId(gradeId);
        return GlobalReponse.success(SubjectVoMapper.MAPPER.toVo(result));
    }
    @GetMapping("/getSubject")
    @ApiOperation(value = "NO:5-3 获取年级下的所有学科 不需要登录", notes = "")
    public GlobalReponse<List<SubjectVo>> getSubject(Long[] gradeIds) {
        List<Subject> result = subjectService.getListByGradeIds(gradeIds);
        return GlobalReponse.success(SubjectVoMapper.MAPPER.toVo(result));
    }
    @GetMapping("/getAllSubject")
    @ApiOperation(value = "NO:5-3 获取所有学科 不需要登录", notes = "")
    public GlobalReponse<List<SubjectVo>> getAllSubject() {
        List<Subject> result = subjectService.getList(new Subject());
        return GlobalReponse.success(SubjectVoMapper.MAPPER.toVo(result));
    }
    
    
    @GetMapping("pageSubject")
    @ApiOperation(value = "NO:7-7 获取学科", notes = "")
    public GlobalReponse<SubjectVo> pageSubject(Subject subject,
                                                 @RequestParam(value = "pageIndex", defaultValue = "1") int

                                                         pageIndex,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {

        PageQuery<Subject> pageData = new PageQuery<Subject>();
        pageData.setPageNumber(pageIndex);
        pageData.setPageSize(pageSize);
        subjectService.pageQuery(pageData,subject);
//        pageData.setList(SubjectVoMapper.MAPPER.toVo(pageData.getList()));
        return GlobalReponse.success(pageData);
    }
    @CrossOrigin
    @PostMapping(value = "/saveSubject")
    @ApiOperation(value = "NO:7-8 添加学科", notes = "")
    public GlobalReponse saveSubject(@RequestBody Subject subject) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();
            Log sysLog = new Log();
            if (subject.getId() == null) {
                // 添加
                subject.setCreateBy(userSubject.getName());
                subject.setCreateTime(new Date());
                subject.setIsDeleted(false);
                subjectService.save(subject);
                sysLog.setObjectId(subject.getId());
                sysLog.setContent("管理员添加学科信息");
                sysLog.setOperateType(OperateType.ADD);
            } else {
                // 更新
                Subject old = subjectService.findById(subject.getId());
                old.setName(subject.getName());
                old.setSort(subject.getSort());
                old.setIcon(subject.getIcon());
                old.setIsDefault(subject.getIsDefault());
                old.setUpdateBy(userSubject.getName());
                old.setUpdateTime(new Date());
                subjectService.save(old);
                sysLog.setObjectId(old.getId());
                sysLog.setContent("管理员修改学科信息");
                sysLog.setOperateType(OperateType.UPDATE);
            }
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            logService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员编辑学科失败:{}", e);
            return GlobalReponse.fail();
        }
        
    }
    @CrossOrigin
    @PostMapping(value = "/addGrade")
    @ApiOperation(value = "NO:7-8 添加学科", notes = "")
    public GlobalReponse addGrade(Long subjectId,Long[] gradeIds) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();
            subjectService.addGrade(subjectId,gradeIds);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员编辑学科失败:{}", e);
            return GlobalReponse.fail();
        }
        
    }
    @CrossOrigin
    @PostMapping(value = "/deleteGrade")
    @ApiOperation(value = "NO:7-8 删除学科年级", notes = "")
    public GlobalReponse deleteGrade(Long subjectId,Long gradeId) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();
            subjectService.deleteGrade(subjectId,gradeId);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员删除学科年级失败:{}", e);
            return GlobalReponse.fail();
        }
        
    }
    @GetMapping("deleteSubject")
    @ApiOperation(value = "NO:15-1 删除学科", notes = "")
    public GlobalReponse deleteSubject(Long id) {
        
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Subject subject = subjectService.findById(id);
        subject.setIsDeleted(true);
        subject.setUpdateBy(userSubject.getId().toString());
        subject.setUpdateTime(new Date());
        try {
            subjectService.save(subject);
            Log sysLog = new Log();
            sysLog.setContent("试题管理删除学科信息");
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setObjectId(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            sysLog.setOperateType(OperateType.DELETE);
            logService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("试题管理删除学科信息失败:{}", e.getMessage());
            return GlobalReponse.fail();
        }

    }
}
