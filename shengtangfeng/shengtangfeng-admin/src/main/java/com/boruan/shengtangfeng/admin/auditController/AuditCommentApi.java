package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.QuestionCommentPageSearch;
import com.boruan.shengtangfeng.core.dto.ToAuditCommentDTO;
import com.boruan.shengtangfeng.core.entity.Question;
import com.boruan.shengtangfeng.core.entity.QuestionComment;
import com.boruan.shengtangfeng.core.service.ICommentService;
import com.boruan.shengtangfeng.core.service.IQuestionCommentService;
import com.boruan.shengtangfeng.core.service.IQuestionService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.CommentVo;
import com.boruan.shengtangfeng.core.vo.QuestionCommentVo;
import com.boruan.shengtangfeng.core.vo.mapper.QuestionCommentVoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: guojiang
 * @Description: 审核管理用户视频相关接口
 * @date:2020/2/25
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/auditComment")
@Api(value = "", tags = {"v3C审核端--审核管理用户评论相关接口"})
public class AuditCommentApi {

    @Autowired
    private ICommentService iCommentService;
    @Autowired
    private IQuestionService questionService;
    @Autowired
    private IQuestionCommentService questionCommentService;


    @PostMapping("/getComment")
    @ApiOperation(value = "多条件查询评论 type 0文章 1视频 2试题", notes = "")
    public GlobalReponse<PageQuery<CommentVo>> getComment(@RequestBody ToAuditCommentDTO toAuditCommentDTO) {
        return iCommentService.getComment(toAuditCommentDTO);
    }

    @DeleteMapping("/deleteComment")
    @ApiOperation(value = "根据id和评论类型删除评论 type 0文章 1视频 2试题", notes = "")
    public GlobalReponse deleteComment(Long commentId, Integer type) {
        if (commentId==null || type==null){
            return GlobalReponse.fail("请传入正确的参数");
        }

        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        return iCommentService.deleteComment(userSubject.getId(),commentId,type);
    }

    @PutMapping("/updateComment")
    @ApiOperation(value = "根据id和评论类型修改评论 type 0文章 1视频 2试题 status 1通过 2失败", notes = "")
    public GlobalReponse updateComment(Long commentId, Integer type, String status,@RequestParam(required = false) String remark) {
        if (commentId==null || type==null || status==null){
            return GlobalReponse.fail("请传入正确的参数");
        }

        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        return iCommentService.updateComment(userSubject.getId(),commentId,type,Integer.valueOf(status),remark);
    }

    @GetMapping("/questionInfo")
    @ApiOperation(value = "V3C-001 查询题目详情")
    public GlobalReponse<Question> questionInfo(@ApiParam(value = "题目ID")@RequestParam(value = "questionId")Long questionId){
        Question question = questionService.findById(questionId);
        return GlobalReponse.success(question);
    }

    @PostMapping("/getQuestionComment")
    @ApiOperation(value = "V3C-002 获取题目下的评论", notes = "")
    public GlobalReponse<PageQuery<QuestionCommentVo>> getQuestionComment(@RequestBody QuestionCommentPageSearch pageSearch) {
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();

        PageQuery<QuestionComment> pageQuery = new PageQuery<QuestionComment>();
        pageQuery.setPageNumber(pageSearch.getPageNo());
        pageQuery.setPageSize(pageSearch.getPageSize());
        QuestionComment search=new QuestionComment();
        search.setQuestionId(pageSearch.getQuestionId());
        search.setStatus(1);
        search.setIsDeleted(false);
        questionCommentService.pageQueryNotMine(pageQuery, search);
        pageQuery.setList(QuestionCommentVoMapper.MAPPER.toVo(pageQuery.getList()));
        return GlobalReponse.success(pageQuery);
    }
}
