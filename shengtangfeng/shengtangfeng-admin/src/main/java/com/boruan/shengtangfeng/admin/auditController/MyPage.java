package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.admin.service.ILoginService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: guojiang
 * @Description: 我的页面相关接口
 * @date:2020/2/26
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/myPage")
@Api(value = "", tags = {"v3C审核端--我的页面相关接口"})
public class MyPage {

    @Autowired
    private ILoginService iLoginService;

    @ApiOperation("修改密码")
    @GetMapping(value = "/updatePassword")
    public GlobalReponse updatePassword(@RequestParam String password, @RequestParam String newPassword,
                                        @RequestParam String newPasswordAgain) {
        if (password==null || newPassword==null || newPasswordAgain==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return iLoginService.updatePasswordForNew(userId,password,newPassword, newPasswordAgain);
    }
}
