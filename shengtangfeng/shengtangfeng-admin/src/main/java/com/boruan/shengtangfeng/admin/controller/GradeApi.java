package com.boruan.shengtangfeng.admin.controller;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Grade;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.IAdvertiseService;
import com.boruan.shengtangfeng.core.service.IArticleService;
import com.boruan.shengtangfeng.core.service.IConfigService;
import com.boruan.shengtangfeng.core.service.IGradeService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.IModuleRecordService;
import com.boruan.shengtangfeng.core.service.IModuleService;
import com.boruan.shengtangfeng.core.service.IQuestionService;
import com.boruan.shengtangfeng.core.service.ISubjectService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.GradeVo;
import com.boruan.shengtangfeng.core.vo.mapper.GradeVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.GradeVoMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: 刘光强
 * @Description: 首页接口
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/grade")
@Api(value = "", tags = { "NO:5 淘学主页相关接口" })
public class GradeApi {
	@Autowired
	private IAdvertiseService advertiseService;
	@Autowired
	private IQuestionService questionService;
	@Autowired
	private IConfigService configService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IGradeService gradeService;
	@Autowired
	private ISubjectService subjectService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private IModuleRecordService moduleRecordService;
	@Autowired
	private ILogService logService;
	@Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    
	@GetMapping("/getGrade")
	@ApiOperation(value = "NO:5-2 获取所有的年级 不需要登录", notes = "")
	public GlobalReponse<List<GradeVo>> getGrade() {
	    Grade search=new Grade();
	    search.setIsDeleted(false);
	    List<Grade> result = gradeService.getList(search);
	    return GlobalReponse.success(GradeVoMapper.MAPPER.toVo(result));
	}
	
	@GetMapping("pageGrade")
    @ApiOperation(value = "NO:7-7 获取年级", notes = "")
    public GlobalReponse<GradeVo> pageGrade(Grade grade,
                                                 @RequestParam(value = "pageIndex", defaultValue = "1") int

                                                         pageIndex,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {

        PageQuery<Grade> pageData = new PageQuery<Grade>();
        pageData.setPageNumber(pageIndex);
        pageData.setPageSize(pageSize);
        gradeService.pageQuery(pageData,grade);
        pageData.setList(GradeVoMapper.MAPPER.toVo(pageData.getList()));
        return GlobalReponse.success(pageData);
    }
    @CrossOrigin
    @PostMapping(value = "/saveGrade")
    @ApiOperation(value = "NO:7-8 添加年级", notes = "")
    public GlobalReponse saveGrade(@RequestBody Grade grade) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();
            Log sysLog = new Log();
            if (grade.getId() == null) {
                // 添加
                grade.setCreateBy(userSubject.getName());
                grade.setCreateTime(new Date());
                grade.setIsDeleted(false);
                gradeService.save(grade);
                sysLog.setObjectId(grade.getId());
                sysLog.setContent("管理员添加年级信息");
                sysLog.setOperateType(OperateType.ADD);
            } else {
                // 更新
                Grade old = gradeService.findById(grade.getId());
                old.setName(grade.getName());
                old.setIcon(grade.getIcon());
                old.setSort(grade.getSort());
                old.setUpdateBy(userSubject.getName());
                old.setUpdateTime(new Date());
                gradeService.save(old);
                sysLog.setObjectId(old.getId());
                sysLog.setContent("管理员修改年级信息");
                sysLog.setOperateType(OperateType.UPDATE);
            }
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            logService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员编辑年级失败:{}", e);
            return GlobalReponse.fail();
        }
        
    }
    @GetMapping("deleteGrade")
    @ApiOperation(value = "NO:15-1 删除年级", notes = "")
    public GlobalReponse deleteCategory(Long id) {
        
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Grade grade = gradeService.findById(id);
        grade.setIsDeleted(true);
        grade.setUpdateBy(userSubject.getId().toString());
        grade.setUpdateTime(new Date());
        try {
            gradeService.save(grade);
            Log sysLog = new Log();
            sysLog.setContent("试题管理删除年级信息");
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setObjectId(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);
            sysLog.setOperateType(OperateType.DELETE);
            logService.save(sysLog);
            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("试题管理删除年级信息失败:{}", e.getMessage());
            return GlobalReponse.fail();
        }

    }
}
