package com.boruan.shengtangfeng.admin.controller;


import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dao.IMusicUseDao;
import com.boruan.shengtangfeng.core.dto.MusicPageSearchDto;
import com.boruan.shengtangfeng.core.entity.Music;
import com.boruan.shengtangfeng.core.entity.Singer;
import com.boruan.shengtangfeng.core.service.IMusicService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/admin/music")
@Api(value = "", tags = {"V4G 音乐相关接口"})
public class MusicApi{

    @Autowired
    private IMusicService musicService;
    @Autowired
    private IMusicUseDao musicUseDao;



    @PostMapping("/getMusic")
    @ApiOperation(value = "V4G-001 获取音乐列表", notes = "")
    public GlobalReponse<PageQuery<Music>> getMusic(@RequestBody MusicPageSearchDto musicPageSearch){
       // Music search=new Music();
        PageQuery<Music> pageQuery = new PageQuery<Music>();
        pageQuery.setPara("keyword", musicPageSearch.getKeyword());
        pageQuery.setPara("singerId", musicPageSearch.getSingerId());
        pageQuery.setPara("startTime", musicPageSearch.getStartTime());
        pageQuery.setPara("endTime", musicPageSearch.getEndTime());
        pageQuery.setPara("type",musicPageSearch.getType());
        pageQuery.setPageNumber(musicPageSearch.getPageNo());
        pageQuery.setPageSize(musicPageSearch.getPageSize());
        musicService.pageQuery1(pageQuery);
        List<Music> list = pageQuery.getList();
        for (Music music : list) {
            Long num = musicUseDao.getUseNum(music.getId(),musicPageSearch.getStartTime(),musicPageSearch.getEndTime(),musicPageSearch.getType());
            music.setClickNum(num);
        }
        pageQuery.setList(list);
        return GlobalReponse.success(pageQuery);
    }


    @PostMapping("/saveMusic")
    @ApiOperation(value = "V4G-002 保存音乐信息")
    public GlobalReponse saveMusic(@RequestBody Music music){
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Long singerId = music.getSingerId();
        Singer singer = musicService.findSingerById(singerId);
        if(music.getId()!=null){
            Music old = musicService.findById(music.getId());
            old.setName(music.getName());
            old.setSinger(singer.getName());
            old.setSingerId(music.getSingerId());
            old.setUpdateTime(new Date());
            old.setUpdateBy(userSubject.getName());
            if (music.getIsHot()!=null && music.getIsHot().equals(0)){
                old.setSort(null);
            }else if (music.getIsHot()!=null && music.getIsHot().equals(1)){
                old.setSort(music.getSort());
            }
            old.setIsHot(music.getIsHot());
            musicService.save(old);
        }else{
            music.setSinger(singer.getName());
            music.setCreateTime(new Date());
            music.setCreateBy(userSubject.getName());
            music.setIsDeleted(false);
            musicService.save(music);
        }

        return GlobalReponse.success("保存成功");
    }

    @GetMapping("/delMusic")
    @ApiOperation(value = "V4G-003 删除音乐 type 删除音乐传0，删除热门音乐传1")
    public GlobalReponse delMusic(@RequestParam(value = "musicId")@ApiParam(value = "音乐ID")String musicId,Integer type){
        for (String s : musicId.split(",")) {
            Long id = Long.valueOf(s).longValue();
            musicService.delMusic(id,type);
        }
        return GlobalReponse.success("删除成功");
    }

    @GetMapping("/singerList")
    @ApiOperation(value = "V4G-004 歌手列表")
    public GlobalReponse<PageQuery<Singer>> singerList(@ApiParam(value = "关键词")@RequestParam(value = "keywords",required = false)String keywords, @ApiParam(value = "页码")@RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo){
        PageQuery<Singer> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(20);
        Singer singer = new Singer();
        singer.setIsDeleted(false);
        singer.setName(keywords);
        pageQuery.setParas(singer);
        musicService.pageQuerySinger(pageQuery);
        return GlobalReponse.success(pageQuery);
    }


    @PostMapping("/saveSinger")
    @ApiOperation(value = "V4G-005 保存歌手信息")
    public GlobalReponse saveSinger(@RequestBody Singer singer){
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        if(singer.getIsDeleted()!=null){
            Singer old = musicService.findSingerById(singer.getId());
            old.setName(singer.getName());
            old.setUpdateBy(userSubject.getName());
            old.setUpdateTime(new Date());
            musicService.saveSinger(old);
        }else{
            singer.setCreateBy(userSubject.getName());
            singer.setCreateTime(new Date());
            singer.setIsDeleted(false);
            musicService.saveSinger(singer);
        }
        return GlobalReponse.success("保存成功");
    }

    @GetMapping("/delSinger")
    @ApiOperation(value = "V4G-003 删除歌手")
    public GlobalReponse delSinger(@RequestParam(value = "singerId")@ApiParam(value = "歌手ID")String singerId){
        for (String s : singerId.split(",")) {
            Long id = Long.valueOf(s).longValue();
            musicService.delSinger(id);
        }
        return GlobalReponse.success("删除成功");
    }

}
