package com.boruan.shengtangfeng.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = { "com.boruan.shengtangfeng", "com.boruan.shengtangfeng.core" })
public class StartShengtangfengAdmin {
	public static void main(String[] args) {
		try {
			SpringApplication.run(StartShengtangfengAdmin.class, args);
		}catch (Exception e){
			e.printStackTrace();
		}

	}
}
