package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.ToAuditArticleDTO;
import com.boruan.shengtangfeng.core.entity.Article;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.IArticleCommentService;
import com.boruan.shengtangfeng.core.service.IArticleService;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.INoticeService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ArticleCategoryVo;
import com.boruan.shengtangfeng.core.vo.ToAuditArticleVO;
import com.boruan.shengtangfeng.core.vo.mapper.ToAuditArticleVoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author: guojiang
 * @Description: 审核管理用户文章相关接口
 * @date:2020/2/25
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/auditArticle")
@Api(value = "", tags = {"v3C审核端--审核管理用户文章相关接口"})
public class AuditArticleApi {
    @Autowired
    private IArticleService articleService;
    @Autowired
    private IArticleCommentService articleCommentService;
    @Autowired
    private ILogService sysLogService;
    @Autowired
    private INoticeService noticeService;

    @GetMapping("/getCategory")
    @ApiOperation(value = "获取文章分类", notes = "")
    public GlobalReponse getCategory() {
        List<ArticleCategoryVo> list = articleService.getCategory();
        return GlobalReponse.success(list);
    }

    @PostMapping("/getToAuditArticle")
    @ApiOperation(value = "分页获取文章", notes = "")
    public GlobalReponse<PageQuery<ToAuditArticleVO>> getToAuditArticle(@RequestBody ToAuditArticleDTO toAuditDTO) {
        PageQuery<Article> pageQuery = new PageQuery<Article>();
        Integer pageNo = toAuditDTO.getPageNo();
        Integer pageSize = toAuditDTO.getPageSize();
        if (pageNo==null){
            pageNo=1;
        }
        if (pageSize==null){
            pageSize=10;
        }
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);
        pageQuery.setPara("categoryId",toAuditDTO.getCategoryId());
        if (toAuditDTO.getContent() != null){
            pageQuery.setPara("content",toAuditDTO.getContent());
        }
        articleService.pageToAuditArticle(pageQuery);
        List<Article> list = pageQuery.getList();
        List<ToAuditArticleVO> vos = ToAuditArticleVoMapper.MAPPER.toVo(list);
        pageQuery.setList(vos);
        return GlobalReponse.success(pageQuery);
    }

    @GetMapping(value = "/confirmArticle")
    @ApiOperation(value = "审核文章", notes = "")
    public GlobalReponse confirmArticle(Long id, Integer status, String remark) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        UserSubject userSubject= JwtUtil.getCurrentJwtUser();
        Article article=articleService.findById(id);
        article.setStatus(status);
        article.setRemark(remark);
        articleService.save(article);
//            0 未审核 1 已审核 2 审核失败
        //用户上传的审核通过给用户发通知
        if(article.getUploadType()==1) {
            if(status==1) {
                noticeService.addNotice(2, 1, id, article.getPublishId(), "内容审核通知","您于"+dateFormat.format(article.getCreateTime())+"发表过的文章"+"《"+article.getTitle()+"》，审核通过。");
            }else if(status==2){
                if (remark!=null && !remark.equals("")){
                    noticeService.addNotice(2, 1, id, article.getPublishId(), "内容审核通知","您于"+dateFormat.format(article.getCreateTime())+"发表过的文章"+"《"+article.getTitle()+"》，"+"因"+remark+"审核不通过。");
                }else {
                    noticeService.addNotice(2, 1, id, article.getPublishId(), "内容审核通知","您于"+dateFormat.format(article.getCreateTime())+"发表过的文章"+"《"+article.getTitle()+"》，"+"因存在敏感信息审核不通过。");
                }
            }
        }
        Log sysLog = new Log();
        sysLog.setObjectId(article.getId());
        sysLog.setContent("管理员审核文章信息："+status);
        sysLog.setOperateType(OperateType.UPDATE);
        sysLog.setCreateTime(new Date());
        sysLog.setOperater(userSubject.getId());
        sysLog.setLogType(Log.LogType.ADMIN);
        sysLogService.save(sysLog);
        return GlobalReponse.success();
    }

    @DeleteMapping(value = "/deletedArticle")
    @ApiOperation(value = "删除文章", notes = "")
    public GlobalReponse deletedArticle(Long id) {
        UserSubject userSubject= JwtUtil.getCurrentJwtUser();
        Article article=articleService.findById(id);
        articleService.deleteArticle(id,userSubject.getId());
        return GlobalReponse.success();
    }

    @GetMapping(value = "/getArticleDetail")
    @ApiOperation(value = "根据文章id查看文章", notes = "")
    public GlobalReponse<ToAuditArticleVO> getArticleDetail(Long id) {
        Article article=articleService.findById(id);
        ToAuditArticleVO vo = ToAuditArticleVoMapper.MAPPER.toVo(article);
        return GlobalReponse.success(vo);
    }
}
