package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.ReportReasons;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.entity.UserReport;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/auditReport")
@Api(value = "", tags = {"v3C审核端--审核用户举报相关接口"})
public class AuditReportApi {

    @Autowired
    private IUserService userService;

    @GetMapping("/getReportReason")
    @ApiOperation(value = "V3C-001 获取举报原因")
    public GlobalReponse<List<ReportReasons>> getReportReason(){
        return userService.getReportReason();
    }


    @PostMapping("/getUserReport")
    @ApiOperation(value = "V3C-002 分页获取待审核举报")
    public GlobalReponse<PageQuery<UserReport>> getUserReport(
            @ApiParam(value = "举报理由ID")@RequestParam(value = "reasonId",required = false)Long reasonId,
            @ApiParam(value = "关键词")@RequestParam(value = "keywords",required = false)String keywords,
            @RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo){
        PageQuery<UserReport> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        if(reasonId!=null){
            pageQuery.setPara("reasonId",reasonId);
        }
        if(keywords!=null && !keywords.equals("")){
            pageQuery.setPara("keywords",keywords);
        }
        userService.toAuditReport(pageQuery);
        List<UserReport> list = pageQuery.getList();
        for (UserReport v : list) {
            User informer = userService.findById(v.getInformerId());
            User person = userService.findById(v.getPersonId());
            v.setInformerAvatar(informer.getHeadImage());
            v.setPersonAvatar(person.getHeadImage());
        }
        pageQuery.setList(list);
        return GlobalReponse.success(pageQuery);
    }

    @PostMapping("/dealReport")
    @ApiOperation(value = "V3C-003 处理举报")
    public GlobalReponse dealReport(@ApiParam(value = "举报记录ID")@RequestParam(value = "reportId")Long reportId,@ApiParam(value = "1忽略2已处理3封号")@RequestParam(value = "status")Integer status){
        UserSubject admin = JwtUtil.getCurrentJwtUser();
        try{
            UserReport userReport = userService.findReportById(reportId);
            userReport.setStatus(status);
            userService.dealUserReport(userReport);
            if(status==3){
                return userService.blockOrUnblockUser(admin.getId(),userReport.getPersonId(),1);
            }
            if(status==4){
                return userService.blockOrUnblockUser(admin.getId(),userReport.getPersonId(),0);
            }
            return GlobalReponse.success("处理成功");
        }catch (Exception $e){
            return GlobalReponse.fail("处理失败");
        }
    }
}
