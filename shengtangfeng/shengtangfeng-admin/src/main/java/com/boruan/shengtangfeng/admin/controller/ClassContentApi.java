package com.boruan.shengtangfeng.admin.controller;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.ClassContentDto;
import com.boruan.shengtangfeng.core.dto.InformDto;
import com.boruan.shengtangfeng.core.dto.QuestionDetailDto;
import com.boruan.shengtangfeng.core.dto.StudentAnswerDto;
import com.boruan.shengtangfeng.core.service.IClassContentService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 @author: guojiang
 @Description:班级内容
 @date:2021/4/28
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/classContent")
@Api(value = "", tags = {"v4E班级内容相关接口"})
public class ClassContentApi {

    @Autowired
    private IClassContentService classContentService;

    @PostMapping(value = "/getQuestionPage")
    @ApiOperation(value = "多条件查询题目列表", notes = "")
    public GlobalReponse<PageQuery<QuestionDetailDto>> getQuestionPage(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                                       @RequestBody ClassContentDto dto) {
        return classContentService.getQuestionPage(pageNo,pageSize,dto);
    }

    @PostMapping(value = "/auditQuestion")
    @ApiOperation(value = "审核题目", notes = "")
    public GlobalReponse auditQuestion(Long questionId,@ApiParam(value = "1审核通过,2审核拒绝") @RequestParam(value = "status")Integer status) {
        if (questionId==null || status==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classContentService.auditQuestion(questionId,status,userId);
    }

    @PostMapping(value = "/deletedQuestion")
    @ApiOperation(value = "批量或者单个删除题目", notes = "")
    public GlobalReponse deletedQuestion(@RequestParam(value = "questionIds") String questionIds) {
        if (questionIds==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        String[] split = questionIds.split(",");
        Long[] convert = (Long[]) ConvertUtils.convert(split, Long.class);
        return classContentService.deletedQuestion(convert,userId);
    }

    @PostMapping(value = "/isLockedUser")
    @ApiOperation(value = "批量封号或者批量解封", notes = "")
    public GlobalReponse isLockedUser(@RequestParam(value = "createIds") String createIds,@ApiParam(value = "0解封,1封号") @RequestParam(value = "status")Integer status) {
        if (createIds==null || status==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        String[] ids = createIds.split(",");
        Long[] convert = (Long[]) ConvertUtils.convert(ids, Long.class);
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classContentService.isLockedUser(convert,status,userId);
    }

    @PostMapping(value = "/getInformPage")
    @ApiOperation(value = "多条件查询班级通知列表", notes = "")
    public GlobalReponse<PageQuery<InformDto>> getInformPage(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                             @RequestBody ClassContentDto dto) {

        return classContentService.getInformPage(pageNo,pageSize,dto);
    }

    @PostMapping(value = "/auditInform")
    @ApiOperation(value = "审核通知", notes = "")
    public GlobalReponse auditInform(Long informId,@ApiParam(value = "1审核通过,2审核拒绝") @RequestParam(value = "status")Integer status) {
        if (informId==null || status==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classContentService.auditInform(informId,status,userId);
    }

    @PostMapping(value = "/deletedInform")
    @ApiOperation(value = "批量或者单个删除班级通知", notes = "")
    public GlobalReponse deletedInform(@RequestParam(value = "informIds") String informIds) {
        if (informIds==null || informIds.equals("")){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        String[] split = informIds.split(",");
        Long[] convert = (Long[]) ConvertUtils.convert(split, Long.class);
        return classContentService.deletedInform(convert,userId);
    }

    @PostMapping(value = "/getStudentAnswer")
    @ApiOperation(value = "多条件查询学生答题列表", notes = "")
    public GlobalReponse<PageQuery<StudentAnswerDto>> getStudentAnswer(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                                       @RequestBody ClassContentDto dto) {
        return classContentService.getStudentAnswer(pageNo,pageSize,dto);
    }

    @PostMapping(value = "/auditStudentAnswer")
    @ApiOperation(value = "审核学生答题", notes = "")
    public GlobalReponse auditStudentAnswer(Long questionRecordId,@ApiParam(value = "1审核通过,2审核拒绝") @RequestParam(value = "status")Integer status) {
        if (questionRecordId==null || status==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classContentService.auditStudentAnswer(questionRecordId,status,userId);
    }

    @PostMapping(value = "/deletedStudentAnswer")
    @ApiOperation(value = "批量或者单个删除学生答题", notes = "")
    public GlobalReponse deletedStudentAnswer(@RequestParam(value = "questionRecordIds") Long[] questionRecordIds) {
        if (questionRecordIds.length==0){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classContentService.deletedStudentAnswer(questionRecordIds,userId);
    }

}
