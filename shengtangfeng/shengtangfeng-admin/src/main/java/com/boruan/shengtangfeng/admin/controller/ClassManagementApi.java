package com.boruan.shengtangfeng.admin.controller;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.*;
import com.boruan.shengtangfeng.core.service.IClassManagementService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.ClassAndVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 @author: guojiang
 @Description:班级管理
 @date:2021/4/28
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/classManagement")
@Api(value = "", tags = {"v4E班级管理相关接口"})
public class ClassManagementApi {

    @Autowired
    private IClassManagementService classManagementService;

    @PostMapping("/getAddress")
    @ApiOperation(value = "获取省市区", notes = "")
    public GlobalReponse<List<ProvinceDto>> getAddress() {
        return classManagementService.getAddress();
    }

    @PostMapping("/createSchool")
    @ApiOperation(value = "新建学校", notes = "")
    public GlobalReponse createSchool(@RequestBody SchoolDto schoolDto) {
        if (schoolDto==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.createSchool(schoolDto,userId);
    }

    @PostMapping(value = "/schoolPage")
    @ApiOperation(value = "多条件查询学校列表", notes = "")
    public GlobalReponse<PageQuery<SchoolListDto>> schoolPage(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                              @RequestBody SchoolDto schoolDto) {
        return classManagementService.schoolPage(pageNo,pageSize,schoolDto);

    }

    @PostMapping(value = "/deletedSchool")
    @ApiOperation(value = "批量或单个删除学校", notes = "")
    public GlobalReponse deletedSchool(@RequestParam(value = "schoolIds") String schoolIds) {
        if (schoolIds==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.deletedSchool(schoolIds,userId);
    }

    @PostMapping(value = "/getGradeBySchoolId")
    @ApiOperation(value = "学校展开，即查询学校的年级", notes = "")
    public GlobalReponse getGradeBySchoolId(Long schoolId) {
        if (schoolId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classManagementService.getGradeBySchoolId(schoolId);
    }

    @PostMapping(value = "/getClassBySchoolYear")
    @ApiOperation(value = "年级展开，即查询年级下的班级", notes = "")
    public GlobalReponse<GradeToClassDto> getClassBySchoolYear(Long schoolId,String schoolYear) {
        if (schoolYear==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classManagementService.getClassBySchoolYear(schoolId,schoolYear);
    }

    @PostMapping(value = "/deletedClass")
    @ApiOperation(value = "后台删除班级", notes = "")
    public GlobalReponse deletedClass(Long classId) {
        if (classId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.deletedClass(classId,userId);
    }

    @PostMapping(value = "/getClassDetails")
    @ApiOperation(value = "查看班级详情", notes = "")
    public GlobalReponse<ClassAndVo> getClassDetails(Long classId) {
        if (classId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return classManagementService.getClassDetails(classId);
    }

    @PostMapping(value = "/getClassParameter")
    @ApiOperation(value = "获取班级参数", notes = "")
    public GlobalReponse<ClassParameterDto> getClassParameter() {
        return classManagementService.getClassParameter();
    }

    @PostMapping(value = "/setClassParameter")
    @ApiOperation(value = "班级参数设置", notes = "")
    public GlobalReponse setClassParameter(@RequestBody ClassParameterDto dto) {
        if (dto==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.setClassParameter(dto,userId);
    }

    @GetMapping(value = "/getJobName")
    @ApiOperation(value = "获取作业名称", notes = "")
    public GlobalReponse<PageQuery<JobNameDto>> getJobName(Integer pageNo, Integer pageSize) {
        if (pageNo==null){
            pageNo=1;
        }
        if (pageSize==null){
            pageSize=10;
        }
        return classManagementService.getJobName(pageNo,pageSize);
    }

    @PostMapping(value = "/createJobName")
    @ApiOperation(value = "新增或编辑作业名称", notes = "")
    public GlobalReponse createJobName(@RequestBody JobNameDto dto) {
        if (dto==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.createJobName(dto,userId);
    }

    @GetMapping(value = "/deletedJobName")
    @ApiOperation(value = "删除作业名称", notes = "")
    public GlobalReponse deletedJobName(String jobNameId) {
        if (jobNameId==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.deletedJobName(jobNameId,userId);
    }

    @PostMapping(value = "/getJobParameter")
    @ApiOperation(value = "获取作业参数", notes = "")
    public GlobalReponse<JobParameterDto> getJobParameter() {
        return classManagementService.getJobParameter();
    }

    @PostMapping(value = "/setJobParameter")
    @ApiOperation(value = "作业参数设置", notes = "")
    public GlobalReponse setJobParameter(@RequestBody JobParameterDto dto) {
        if (dto==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        Long userId = JwtUtil.getCurrentJwtUser().getId();
        return classManagementService.setJobParameter(dto,userId);
    }
}
