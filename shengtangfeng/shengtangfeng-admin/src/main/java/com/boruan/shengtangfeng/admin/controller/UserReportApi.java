package com.boruan.shengtangfeng.admin.controller;

import cn.hutool.core.util.StrUtil;
import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.ReportReasons;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.entity.UserReport;
import com.boruan.shengtangfeng.core.service.IClassContentService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/userReport")
@Api(value = "", tags = {"V4E--后台用户举报相关接口"})
public class UserReportApi {

    @Autowired
    private IUserService userService;
    @Autowired
    private IClassContentService classContentService;


    @GetMapping("/getReportReason")
    @ApiOperation(value = "V4E-001 获取举报原因")
    public GlobalReponse<List<ReportReasons>> getReportReason(){
        return userService.getReportReason();
    }

    @PostMapping("/getUserReport")
    @ApiOperation(value = "V4E-002 分页获取待审核举报")
    public GlobalReponse<PageQuery<UserReport>> getUserReport(
            @ApiParam(value = "举报理由ID")@RequestParam(value = "reasonId",required = false)Long reasonId,
            @ApiParam(value = "用户昵称（ID）")@RequestParam(value = "user",required = false)String user,
            @ApiParam(value = "内容关键词")@RequestParam(value = "keywords",required = false)String keywords,
            @RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo){
        PageQuery<UserReport> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        if(reasonId!=null){
            pageQuery.setPara("reasonId",reasonId);
        }
        if(StringUtils.isNotBlank(user)){
            pageQuery.setPara("user",user);
        }
        if(StringUtils.isNotBlank(keywords)){
            pageQuery.setPara("keywords",keywords);
        }
        userService.toAdminReport(pageQuery);
        List<UserReport> list = pageQuery.getList();
        for (UserReport v : list) {
            User informer = userService.findById(v.getInformerId());
            User person = userService.findById(v.getPersonId());
            v.setInformerAvatar(informer.getHeadImage());
            v.setPersonAvatar(person.getHeadImage());
            v.setPersonStatus(person.getIsLocked());
            v.set("user",person);
        }
        pageQuery.setList(list);
        return GlobalReponse.success(pageQuery);
    }

    @PostMapping("/dealReport")
    @ApiOperation(value = "V4E-003 处理举报")
    public GlobalReponse dealReport(@ApiParam(value = "举报记录ID")@RequestParam(value = "reportId")String reportId,@ApiParam(value = "1忽略2已处理3封号4解封")@RequestParam(value = "status")Integer status){
        UserSubject admin = JwtUtil.getCurrentJwtUser();
        String[] split = reportId.split(",");
        Long[] convert = (Long[]) ConvertUtils.convert(split, Long.class);
        Long[] createIds = userService.getReportUids(convert);
        try{
            for (String s : reportId.split(",")) {
                UserReport userReport = userService.findReportById(Long.valueOf(s).longValue());
                if(status==3){
                    userReport.setStatus(3);
                    userService.dealUserReport(userReport);
                    userService.blockOrUnblockUser(admin.getId(),userReport.getPersonId(),1);
                }
                if(status==4){
                    userReport.setStatus(2);
                    userService.dealUserReport(userReport);
                    userService.blockOrUnblockUser(admin.getId(),userReport.getPersonId(),0);
                }else{
                    userReport.setStatus(status);
                    userService.dealUserReport(userReport);
                }
            }
            return GlobalReponse.success("处理成功");
        }catch (Exception e){
            return GlobalReponse.fail("处理失败");
        }

    }

    @GetMapping("/delReport")
    @ApiOperation(value = "删除举报")
    public GlobalReponse delReport(@RequestParam(value = "reportId")@ApiParam(value = "点评ID")String reportId){
        for (String s : reportId.split(",")) {
            userService.delReport(Long.valueOf(s).longValue());
        }
        return GlobalReponse.success("删除成功");
    }

    @PostMapping("/batchDealReport")
    @ApiOperation(value = "V4E-004 批量处理举报")
    public GlobalReponse batchDealReport(@ApiParam(value = "举报记录ID")@RequestParam(value = "reportId")String reportId,@ApiParam(value = "1忽略2已处理3封号4解封")@RequestParam(value = "status")Integer status){
        UserSubject admin = JwtUtil.getCurrentJwtUser();
        String[] split = reportId.split(",");
        Long[] convert = (Long[]) ConvertUtils.convert(split, Long.class);
        Long[] createIds = userService.getReportUids(convert);
        try {
            if(status==4){
                classContentService.isLockedUser(createIds,0,admin.getId());
            }else{
                if(status==3){
                    classContentService.isLockedUser(createIds,1,admin.getId());
                }
                userService.batchDealReport(convert,status);
            }
            return GlobalReponse.success("处理成功");
        }catch (Exception e){
            return GlobalReponse.fail("处理失败");
        }
    }

    @GetMapping("/pageReportReason")
    @ApiOperation(value = "V4E-005 举报理由列表")
    public GlobalReponse<PageQuery<ReportReasons>> pageReportReason(@ApiParam(value = "页码")@RequestParam(value = "pageNo",defaultValue = "1")Integer pageNo, @ApiParam(value = "每页数量")@RequestParam(value = "pageSize")Integer pageSize){
        PageQuery<ReportReasons> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);
        ReportReasons reportReasons = new ReportReasons();
        reportReasons.setIsDeleted(false);
        pageQuery.setParas(reportReasons);
        userService.pageReportReason(pageQuery);
        return GlobalReponse.success(pageQuery);
    }

    @PostMapping("/saveReason")
    @ApiOperation(value = "V4E-006 保存举报原因")
    public GlobalReponse saveReason(@RequestBody ReportReasons reportReasons){
        UserSubject admin = JwtUtil.getCurrentJwtUser();
        if(reportReasons.getId()!=null){
            ReportReasons old = userService.findReasonById(reportReasons.getId());
            old.setReasons(reportReasons.getReasons());
            old.setUpdateBy(admin.getName());
            old.setUpdateTime(new Date());
            userService.saveReportReason(old);
        }else{
            reportReasons.setCreateBy(admin.getName());
            reportReasons.setCreateTime(new Date());
            reportReasons.setIsDeleted(false);
            userService.saveReportReason(reportReasons);
        }
        return GlobalReponse.success("保存成成功");
    }

    @GetMapping("/delReportReasons")
    @ApiOperation(value = "删除举报理由")
    public GlobalReponse delReportReasons(Long id){
        if (id==null){
            return GlobalReponse.fail("请传入正确的参数");
        }
        return userService.delReportReasons(id);
    }

}
