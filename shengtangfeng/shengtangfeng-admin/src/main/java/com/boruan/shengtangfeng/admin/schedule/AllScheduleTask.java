package com.boruan.shengtangfeng.admin.schedule;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boruan.shengtangfeng.core.dao.ISystemUserDao;
import com.boruan.shengtangfeng.core.redis.utils.RedisAtomicClient;
import com.boruan.shengtangfeng.core.service.IConfigService;

import cn.jpush.api.JPushClient;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Transactional(readOnly = false)
public class AllScheduleTask {
	@Autowired
	private RedisAtomicClient redisAtomicClient;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private IConfigService configService;
	@Autowired
	private ISystemUserDao systemUserDao;
	@Resource(name = "jPushClient")
	private JPushClient jPushClient;

	/**
	 * 每隔一分钟启动
	 */
	// 定时任务
//  @Scheduled(cron = "0 * * * * ?")
	public void changeTaskAppShowStatus() {
	}

}
