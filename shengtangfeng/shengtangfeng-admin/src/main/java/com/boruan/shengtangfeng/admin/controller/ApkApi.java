package com.boruan.shengtangfeng.admin.controller;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Apk;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.service.IApkService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/admin/apk")
@Api(value = "", tags = { "NO:8 apk" })
@SuppressWarnings("all")
public class ApkApi {

    @Autowired
    private IApkService apkService;

    @GetMapping("getList")
    @ApiOperation(value = "NO:15-2 获取列表", notes = "")
    public GlobalReponse<Apk> getList() {

        return apkService.getApk();

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public GlobalReponse add(@RequestBody Apk apk) {
        try {
            UserSubject userSubject = JwtUtil.getCurrentJwtUser();

            apkService.save(apk);

            Log sysLog = new Log();
            // 更新
            sysLog.setObjectId(apk.getId());
            sysLog.setContent(userSubject.getName() + "上传程序包：" + apk.getIntroduce());
            sysLog.setOperateType(Log.OperateType.UPDATE);
            sysLog.setCreateTime(new Date());
            sysLog.setOperater(userSubject.getId());
            sysLog.setLogType(Log.LogType.ADMIN);

            return GlobalReponse.success();
        } catch (Exception e) {
            log.error("管理员修改配置信息失败:{}", e);
            return GlobalReponse.fail();
        }
    }

}
