package com.boruan.shengtangfeng.admin.controller;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.core.entity.Advertise;
import com.boruan.shengtangfeng.core.service.IAdvertiseService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/admin/setting")
@Api(value = "", tags = {"NO:7 系统管理相关"})
public class SettingManageApi {

    @Autowired
    private IAdvertiseService advertiseService;




    // ============== 广告设置 ==============

    @GetMapping("getAdvertise")
    public GlobalReponse getAdvertise(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                Advertise ads) {

        PageQuery<Advertise> pageQuery = new PageQuery<Advertise>();
        pageQuery.setPageNumber(pageIndex);
        pageQuery.setPageSize(pageSize);

        advertiseService.pageQuery(pageQuery, ads);

        return GlobalReponse.success().setData(pageQuery);
    }

    @PostMapping("addAdvertise")
    public GlobalReponse addAdvertise(@RequestBody Advertise po) {
        po.setIsDeleted(false);
        advertiseService.save(po);

        return GlobalReponse.success();
    }

    @GetMapping("delAdvertise")
    public GlobalReponse delAdvertise(@RequestParam String ids) {

        advertiseService.delAdvertise(ids);

        return GlobalReponse.success();
    }

}
