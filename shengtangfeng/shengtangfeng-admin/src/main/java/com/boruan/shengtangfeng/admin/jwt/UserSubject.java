package com.boruan.shengtangfeng.admin.jwt;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserSubject implements UserDetails {

	private Long id;
	private String loginName; // 设置为account
	private String name;
	private String headImage;
	private String password;
	private String salt;
	private Collection<? extends GrantedAuthority> authorities;

	public UserSubject() {

	}

	public UserSubject(Long id, String loginName, String name, String password, String salt,String headImage,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.loginName = loginName;
		this.headImage = headImage;
		this.name = name;
		this.password = password;
		this.salt = salt;
		this.authorities = authorities;
	}

	/**
	 * 返回分配给用户的角色列表
	 * 
	 * @return
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.loginName;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSalt() {
		return salt;
	}

	/**
	 * 账户是否未过期
	 * 
	 * @return
	 */
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	/**
	 * 账户是否未锁定
	 * 
	 * @return
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	/**
	 * 密码是否未过期
	 * 
	 * @return
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/**
	 * 账户是否激活
	 * 
	 * @return
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getLoginName() {
		return loginName;
	}

    public String getHeadImage() {
        return headImage;
    }
	

}