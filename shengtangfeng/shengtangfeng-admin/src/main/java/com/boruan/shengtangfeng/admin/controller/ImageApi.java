package com.boruan.shengtangfeng.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.IUserService;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: lihaicheng
 * @Description: 私有图片管理
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@RequestMapping("/admin/image")
@SuppressWarnings("all")
@Api(value = "", tags = { "NO:2 图片相关接口" })
public class ImageApi {

	@Autowired
	private IUserService userService;
	@Autowired
	private ILogService logService;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

}
