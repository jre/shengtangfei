package com.boruan.shengtangfeng.admin.controller;

import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.entity.Permission;
import com.boruan.shengtangfeng.core.entity.Role;
import com.boruan.shengtangfeng.core.service.IPermissionService;
import com.boruan.shengtangfeng.core.service.IRoleService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/menu")
@SuppressWarnings("all")
@Api(value = "", tags = { "NO:00 权限" })
public class MenuApi {

    @Autowired
    IPermissionService permissionService;
    @Autowired
    IRoleService roleService;

    @ApiOperation(value = "权限树", notes = "")
    @GetMapping("getMenu")
    public GlobalReponse getMenu() {

        List<Permission> result = permissionService.buildMenuTree(JwtUtil.getCurrentJwtUser().getId());

        return GlobalReponse.success().setData(result);
    }

    @ApiOperation(value = "权限列表", notes = "")
    @GetMapping("getRole")
    public GlobalReponse getRole(@RequestParam(required = false) String name,
                                @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                                @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {

        PageQuery<Role> pageData = new PageQuery<Role>();
        pageData.setPageNumber(pageIndex);
        pageData.setPageSize(pageSize);
        pageData.setPara("name", name);
        permissionService.pageQuery(pageData);

        return GlobalReponse.success().setData(pageData);
    }

    @ApiOperation(value = "增加角色", notes = "")
    @PostMapping("addRole")
    public GlobalReponse addRole(@RequestBody Role role) {

        Long userId = JwtUtil.getCurrentJwtUser().getId();
        if (null != role.getId()) {
            role.setUpdateBy(userId.toString());
            role.setUpdateTime(new Date());
        } else {
            role.setIsDeleted(false);
            role.setCreateBy(userId.toString());
            role.setCreateTime(new Date());
        }
        roleService.save(role);

        return GlobalReponse.success();

    }


    @ApiOperation(value = "删除角色", notes = "")
    @GetMapping("delRole")
    public GlobalReponse delRole(@RequestParam Long id) {

        if (id == 1) {
            return GlobalReponse.fail("超级管理员角色不允许删除");
        }

        Long userId = JwtUtil.getCurrentJwtUser().getId();

        return roleService.delRole(id, userId);

    }

    @ApiOperation(value = "角色权限树", notes = "")
    @GetMapping("getRolePermissionTree")
    public GlobalReponse getRolePermissionTree(@RequestParam Long roleId) {

        return permissionService.getRolePermissionTree(roleId);

    }

    @ApiOperation(value = "编辑角色权限", notes = "")
    @PostMapping("editRolePermission")
    public GlobalReponse editRolePermission(@RequestBody EditParams editParams) {

        if (editParams.getRoleId() == 1) {
            return GlobalReponse.fail("超级管理员权限不允许修改");
        }

        return permissionService.editRolePermission(editParams.getRoleId(), editParams.getAddPermissions(), editParams.getDelPermissions());
    }


}


@Data
class EditParams {

    private Long roleId;

    private List<Long> delPermissions;

    private List<Long> addPermissions;

}