//package com.boruan.shengtangfeng.admin.controller;
//
//import java.util.Date;
//
//import org.beetl.sql.core.engine.PageQuery;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import UserSubject;
//import JwtUtil;
//import Log;
//import com.boruan.shengtangfeng.core.entity.News;
//import Log.OperateType;
//import ILogService;
//import com.boruan.shengtangfeng.core.service.INewsService;
//import GlobalReponse;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//
///**
// * @author liuguangqiang
// */
//@RestController
//@Slf4j
//@RequestMapping("/admin/news")
//@Api(value = "", tags = { "NO:13 资讯相关接口" })
//public class NewsApi {
//	@Autowired
//	private INewsService newsService;
//	@Autowired
//	private ILogService logService;
//
//	/**
//	 * @author: lihaicheng
//	 * @Description: 分页获取
//	 * @date:2020年3月10日 上午11:14:10
//	 **/
//	@PostMapping(value = "/pageNews")
//	@ApiOperation(value = "NO:13-1 分页查询资讯", notes = "")
//	public GlobalReponse<PageQuery<News>> pageNews(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
//			News news, Model model) {
//		PageQuery<News> query = new PageQuery<News>();
//		query.setPageNumber(pageNo);
//		newsService.pageQuery(query, news);
//		return GlobalReponse.success(query);
//	}
//
//	@GetMapping(value = "/getNews")
//	@ApiOperation(value = "NO:13-2 查询资讯详情", notes = "")
//	public GlobalReponse<News> getNews(Long id, Model model) {
//		if (id == null) {
//			return GlobalReponse.success(new News());
//		} else {
//			News news = newsService.findId(id);
//			return GlobalReponse.success(news);
//		}
//	}
//
//	@PostMapping(value = "/addNews")
//	@ApiOperation(value = "NO:13-3 添加资讯", notes = "")
//	public GlobalReponse addNews(News news) {
//		try {
//			UserSubject userSubject = JwtUtil.getCurrentJwtUser();
//			Log sysLog = new Log();
//			if (news.getId() == null) {
//				// 添加
//				news.setCreateBy(userSubject.getName());
//				news.setCreateTime(new Date());
//				news.setPublishDate(new Date());
//				news.setIsDeleted(false);
//				newsService.save(news);
//				sysLog.setObjectId(news.getId());
//				sysLog.setContent("管理员添加资讯");
//				sysLog.setOperateType(OperateType.ADD);
//			} else {
//				// 更新
//				News news2 = newsService.findId(news.getId());
//				news2.setStatus(news.getStatus());
//				news2.setTitle(news.getTitle());
//				news2.setLink(news.getLink());
//				news2.setContent(news.getContent());
//				news2.setUpdateBy(userSubject.getName());
//				news2.setUpdateTime(new Date());
//				newsService.save(news2);
//				sysLog.setObjectId(news2.getId());
//				sysLog.setContent("管理员修改资讯");
//				sysLog.setOperateType(OperateType.UPDATE);
//			}
//			sysLog.setCreateTime(new Date());
//			sysLog.setOperater(userSubject.getId());
//			sysLog.setLogType(Log.LogType.ADMIN);
//			logService.save(sysLog);
//			return GlobalReponse.success();
//		} catch (Exception e) {
//			log.error("管理员修改资讯失败:{}", e);
//			return GlobalReponse.fail();
//		}
//	}
//
//	/**
//	 * @author: 刘光强
//	 * @Description: 删除新闻
//	 * @date:2020年3月10日 上午11:14:10
//	 **/
//	@PostMapping(value = "/deleteNews")
//	@ApiOperation(value = "NO:13-4 删除资讯", notes = "")
//	public GlobalReponse deleteNews(Long id, Model model) {
//		UserSubject userSubject = JwtUtil.getCurrentJwtUser();
//
//		newsService.deleteNews(id, userSubject.getId());
//
//		return GlobalReponse.success();
//	}
//
//}
