package com.boruan.shengtangfeng.admin.auditController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.core.dto.IncorrentQuestionListDto;
import com.boruan.shengtangfeng.core.dto.ToAuditVideoDTO;
import com.boruan.shengtangfeng.core.entity.*;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.INoticeService;
import com.boruan.shengtangfeng.core.service.IVideoCommentService;
import com.boruan.shengtangfeng.core.service.IVideoService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.utils.StringUtils;
import com.boruan.shengtangfeng.core.vo.*;
import com.boruan.shengtangfeng.core.vo.mapper.ToAuditVideoVoMapper;
import com.boruan.shengtangfeng.core.vo.mapper.VideoVoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author: guojiang
 * @Description: 审核管理用户视频相关接口
 * @date:2020/2/25
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/auditVideo")
@Api(value = "", tags = {"v3C审核端--审核管理用户视频相关接口"})
public class AuditVideoApi {
    @Autowired
    private IVideoService videoService;
    @Autowired
    private IVideoCommentService videoCommentService;
    @Autowired
    private ILogService sysLogService;
    @Autowired
    private INoticeService noticeService;


    @GetMapping("/getCategory")
    @ApiOperation(value = "获取视频分类", notes = "")
    public GlobalReponse getCategory(@ApiParam(value = "类型：0普通视频1讲一讲2小视频") @RequestParam(value = "type") Integer type) {
        List<VideoCategoryVo> list = videoService.getCategory(type);
        VideoCategoryVo all = new VideoCategoryVo();
        all.setId(0l);
        all.setName("全部");
        list.add(0,all);
        return GlobalReponse.success(list);
    }

    @PostMapping("/getToAuditVideo")
    @ApiOperation(value = "多条件查询待审核视频", notes = "")
    public GlobalReponse<PageQuery<ToAuditVideoVO>> getToAuditVideo(@RequestBody ToAuditVideoDTO toAuditVideoDTO) {
        PageQuery<Video> pageQuery = new PageQuery<>();
        Integer pageNo = toAuditVideoDTO.getPageNo();
        Integer pageSize = toAuditVideoDTO.getPageSize();
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);
        if (toAuditVideoDTO.getCategoryId() != null && toAuditVideoDTO.getCategoryId() != 0) {
            pageQuery.setPara("categoryId", toAuditVideoDTO.getCategoryId());
        }
        if (toAuditVideoDTO.getContent() != null && !toAuditVideoDTO.getContent().equals("")) {
            pageQuery.setPara("content", toAuditVideoDTO.getContent());
        }
        if (toAuditVideoDTO.getType() != null) {
            pageQuery.setPara("type", toAuditVideoDTO.getType());
        }

        videoService.pageToAuditVideo(pageQuery);

        List<Video> list = pageQuery.getList();
        List<ToAuditVideoVO> vos = ToAuditVideoVoMapper.MAPPER.toVo(list);
        pageQuery.setList(vos);
        return GlobalReponse.success(pageQuery);
    }

    @DeleteMapping("deleteVideo")
    @ApiOperation(value = "删除视频", notes = "")
    public GlobalReponse deleteVideo(@RequestParam Long[] ids) {
        UserSubject user = JwtUtil.getCurrentJwtUser();
        for (Long id : ids) {
            videoService.deleteVideo(id, user.getId());
        }
        return GlobalReponse.success();
    }

    @GetMapping(value = "/confirmVideo")
    @ApiOperation(value = "审核视频", notes = "")
    public GlobalReponse confirmVideo(Long id, Integer status, String remark) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        Video video = videoService.findById(id);
        video.setStatus(status);
        video.setRemark(remark);
        videoService.save(video);
//        0 未审核 1 已审核 2 审核失败
        //用户上传的审核通过给用户发通知
        if (video.getUploadType() == 1) {
            if (status == 1) {
                noticeService.addNotice(2, 1, id, video.getPublishId(), "内容审核通知", "您于" + dateFormat.format(video.getCreateTime()) + "发表过的视频" + "《" + video.getTitle() + "》，审核通过。");
            } else if (status == 2) {
                if (remark != null && !remark.equals("")) {
                    noticeService.addNotice(2, 1, id, video.getPublishId(), "内容审核通知", "您于" + dateFormat.format(video.getCreateTime()) + "发表过的视频" + "《" + video.getTitle() + "》，" + "因" + remark + "审核不通过。");
                } else {
                    noticeService.addNotice(2, 1, id, video.getPublishId(), "内容审核通知", "您于" + dateFormat.format(video.getCreateTime()) + "发表过的视频" + "《" + video.getTitle() + "》，" + "存在敏感信息审核不通过。");
                }
            }
        }
        Log sysLog = new Log();
        sysLog.setObjectId(video.getId());
        sysLog.setContent("管理员审核视频信息：" + status);
        sysLog.setOperateType(OperateType.UPDATE);
        sysLog.setCreateTime(new Date());
        sysLog.setOperater(userSubject.getId());
        sysLog.setLogType(Log.LogType.ADMIN);
        sysLogService.save(sysLog);
        return GlobalReponse.success();
    }

    @GetMapping(value = "/getVideoDetail")
    @ApiOperation(value = "根据视频id查看视频", notes = "")
    public GlobalReponse<ToAuditVideoVO> getArticleDetail(Long id) {
        Video video = videoService.findById(id);
        ToAuditVideoVO vo = ToAuditVideoVoMapper.MAPPER.toVo(video);
        return GlobalReponse.success(vo);
    }


    @PostMapping("/pageVideoGrade")
    @ApiOperation(value = "查看讲一讲点评")
    public GlobalReponse<PageQuery<VideoGrade>> pageUserVideoGrade(@ApiParam(value = "页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                                   @ApiParam(value = "每页数量") @RequestParam(value = "pageSize") Integer pageSize,
                                                                   @ApiParam(value = "用户ID") @RequestParam(value = "userId") Long userId,
                                                                   @ApiParam(value = "关键词") @RequestParam(value = "keywords") String keywords,
                                                                   @ApiParam(value = "状态") @RequestParam(value = "status") Integer status) {
        PageQuery<VideoGrade> pageQuery = new PageQuery<>();
        pageQuery.setPageNumber(pageNo);
        pageQuery.setPageSize(pageSize);
        if (status != null) {
            pageQuery.setPara("status", status);
        }
        if (userId != null) {
            pageQuery.setPara("userId", userId);
        }
        if (StringUtils.isNotBlank(keywords)) {
            pageQuery.setPara("remark", keywords);
        }
        videoService.pageVideoGrade(pageQuery);
        return GlobalReponse.success(pageQuery);
    }


    @PostMapping("/updateVideoGrade")
    @ApiOperation(value = "修改讲一讲点评 status 1通过 2拒绝 remark：拒绝原因", notes = "")
    public GlobalReponse updateVideoGrade(@RequestBody UpdateVideoGradeVo vo) {
        if (vo == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }

        UserSubject userSubject = JwtUtil.getCurrentJwtUser();
        return videoService.updateVideoGrade(userSubject.getId(), vo.getVideoGradeId(), vo.getStatus(), vo.getRemark());
    }

    @GetMapping("/getJyJComment")
    @ApiOperation(value = "根据讲一讲id获取点评", notes = "")
    public GlobalReponse<List<VideoGrade>> getVideoDetails(Long videoId) {
        if (videoId == null) {
            return GlobalReponse.fail("请传入正确的参数");
        }
        return videoService.getJyJComment(videoId);
    }


}
