package com.boruan.shengtangfeng.admin.controller;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import com.boruan.shengtangfeng.admin.service.ILoginService;
import com.boruan.shengtangfeng.core.entity.SystemUser;
import com.boruan.shengtangfeng.core.service.ISystemUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;
import com.boruan.shengtangfeng.core.vo.LoginResult;
import com.boruan.shengtangfeng.core.vo.SystemUserVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: lihaicheng
 * @Description: 登录
 * @date:2020年3月10日 上午11:14:10
 */
@Slf4j
@RestController
@SuppressWarnings("all")
@RequestMapping("/admin/login")
@Api(value = "", tags = { "NO:3 登录相关接口" })
public class LoginApi {
	@Autowired
	private ILoginService loginService;
	@Autowired
	private ISystemUserService systemUserService;
	@Autowired
	private JwtUtil jwtUtil;
	@Value("${jwt.tokenHead}")
	private String tokenHead;

	@GetMapping("/passwordLogin")
	@ApiOperation(value = "NO:3-2用户名密码登录", notes = "")
	public GlobalReponse<LoginResult> passwordLogin(String loginName, String password) {
		GlobalReponse globalReponse = loginService.passwordLogin(loginName, password);
		return globalReponse;
	}

	/**
	 * @author: 刘光强
	 * @Description: 获取手机验证码
	 * @date:2020年3月10日 上午11:14:10
	 */
	@GetMapping("/getVerficationCode")
	@ApiOperation(value = "NO:3-4 获取手机验证码, type:1用户注册验证码, 2登录确认验证码, 3修改密码验证码", notes = "获取手机验证码")
	public GlobalReponse<String> getVerficationCode(String mobile, Integer type) {
		return loginService.getVerficationCode(mobile, type);
	}

	/**
	 * @author: 刘光强
	 * @Description: 绑定手机号
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping("/bindingMobile")
	@ApiOperation(value = "NO:3-5 绑定手机号", notes = "绑定手机号,返回code不是1000则会返回错误信息")
	public GlobalReponse<SystemUserVo> bindingMobile(String mobile, String code) {
		UserSubject user = jwtUtil.getCurrentJwtUser();
		return loginService.bindingMobile(mobile, code, user.getId());

	}

	/**
	 * @author: 刘光强
	 * @Description: 绑定手机号
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping("/verificationMobile")
	@ApiOperation(value = "NO:3-6 校验手机号是否存在", notes = "返回true 不存在，false存在")
	public GlobalReponse<Boolean> verificationMobile(String mobile) {
		return loginService.verificationMobile(mobile);

	}

	@ApiOperation("NO:3-7 修改密码")
	@PostMapping(value = "/updatePassword")
	public GlobalReponse updatePassword(@RequestParam String mobile, @RequestParam String password,
			@RequestParam String authCode) {
		return loginService.updatePassword(mobile, password, authCode);
	}

	/**
	 * @author: 刘光强
	 * @Description: 获取登录者信息
	 * @date:2020年3月10日 上午11:14:10
	 */
//	@ApiOperation("NO:3-8 通过id获取登录者详情")
//	@PostMapping(value = "/getUser")
//	public GlobalReponse getUser(Long id) {
//		SystemUser user = null;
//		try {
//			user = systemUserService.findById(id);
//		} catch (Exception e) {
//			return GlobalReponse.fail("获取失败，该用户不存在");
//		}
//
//		return GlobalReponse.success(user);
//	}

	/**
	 * @author: 刘光强
	 * @Description: 分页获取所有的用户
	 * @date:2020年3月10日 上午11:14:10
	 */
	@PostMapping(value = "/page")
	@ApiOperation(value = "NO:3-8 分页获取所有的用户", notes = "")
	public GlobalReponse<PageQuery<SystemUser>> page(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
			SystemUser systemUser, Model model) {
		PageQuery<SystemUser> query = new PageQuery<SystemUser>();
		query.setPageNumber(pageNo);
		systemUserService.pageQuery(query, systemUser);
		model.addAttribute("page", query);
		return GlobalReponse.success(query);
	}

}
