package com.boruan.shengtangfeng.admin.controller;

import java.util.Date;

import com.boruan.shengtangfeng.admin.jwt.UserSubject;
import com.boruan.shengtangfeng.admin.jwt.util.JwtUtil;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boruan.shengtangfeng.core.dto.NoticeSearch;
import com.boruan.shengtangfeng.core.entity.Log;
import com.boruan.shengtangfeng.core.entity.Log.OperateType;
import com.boruan.shengtangfeng.core.entity.Notice;
import com.boruan.shengtangfeng.core.entity.User;
import com.boruan.shengtangfeng.core.service.ILogService;
import com.boruan.shengtangfeng.core.service.INoticeService;
import com.boruan.shengtangfeng.core.service.IUserService;
import com.boruan.shengtangfeng.core.utils.GlobalReponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author liuguangqiang
 */
@RestController
@Slf4j
@RequestMapping("/admin/notice")
@Api(value = "", tags = { "NO:13 通知相关接口" })
public class NoticeApi {
	@Autowired
	private INoticeService noticeService;
	@Autowired
	private ILogService logService;
	@Autowired
	private IUserService userService;

	/**
	 * @author: lihaicheng
	 * @Description: 分页获取
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@GetMapping(value = "/pageNotice")
	@ApiOperation(value = "NO:13-1 分页查询通知", notes = "")
	public GlobalReponse<PageQuery<Notice>> pageNotice(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
	        NoticeSearch noticeSearch, Model model) {
		PageQuery<Notice> query = new PageQuery<Notice>();
		query.setPageNumber(pageNo);
		noticeService.pageQuery(query, noticeSearch);
		return GlobalReponse.success(query);
	}

	@GetMapping(value = "/getNotice")
	@ApiOperation(value = "NO:13-2 查询通知详情", notes = "")
	public GlobalReponse<Notice> getNotice(Long id, Model model) {
		if (id == null) {
			return GlobalReponse.success(new Notice());
		} else {
			Notice notice = noticeService.findById(id);
			return GlobalReponse.success(notice);
		}
	}

	@PostMapping(value = "/addNotice")
	@ApiOperation(value = "NO:13-3 添加通知", notes = "")
	public GlobalReponse addNotice(@RequestBody Notice notice) {
		try {
			UserSubject userSubject = JwtUtil.getCurrentJwtUser();
			Log sysLog = new Log();
			if (notice.getId() == null) {
				// 添加
				notice=noticeService.addNotice(1, 0, null, userSubject.getId(), notice.getTitle(), notice.getContent());
				sysLog.setObjectId(notice.getId());
				sysLog.setContent("管理员添加通知");
				sysLog.setOperateType(OperateType.ADD);
			} else {
				// 更新
				Notice notice2 = noticeService.findById(notice.getId());
				notice2.setTitle(notice.getTitle());
				notice2.setUpdateBy(userSubject.getName());
				notice2.setUpdateTime(new Date());
				noticeService.save(notice2);
				sysLog.setObjectId(notice2.getId());
				sysLog.setContent("管理员修改通知");
				sysLog.setOperateType(OperateType.UPDATE);
			}
			sysLog.setCreateTime(new Date());
			sysLog.setOperater(userSubject.getId());
			sysLog.setLogType(Log.LogType.ADMIN);
			logService.save(sysLog);
			return GlobalReponse.success();
		} catch (Exception e) {
			log.error("管理员修改通知失败:{}", e);
			return GlobalReponse.fail();
		}
	}

	/**
	 * @author: 刘光强
	 * @Description: 删除新闻
	 * @date:2020年3月10日 上午11:14:10
	 **/
	@PostMapping(value = "/deleteNotice")
	@ApiOperation(value = "NO:13-4 删除通知", notes = "")
	public GlobalReponse deleteNotice(Long id, Model model) {
		UserSubject userSubject = JwtUtil.getCurrentJwtUser();
		Notice notice = noticeService.findById(id);
		notice.setUpdateBy(userSubject.getName());
		notice.setUpdateTime(new Date());
		notice.setIsDeleted(true);
        noticeService.save(notice);
        Log sysLog = new Log();
        sysLog.setObjectId(notice.getId());
        sysLog.setContent("管理员删除通知");
        sysLog.setOperateType(OperateType.DELETE);
        logService.save(sysLog);
		return GlobalReponse.success();
	}

}
