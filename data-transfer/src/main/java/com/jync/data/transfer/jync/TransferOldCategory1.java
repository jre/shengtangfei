package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldCategory1 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://192.168.6.102:3306/toutiao?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","boruankeji@2018");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement oldQuestionQueryStatement =oldConn.createStatement();
            //题库
            ResultSet questionQueryRs=oldQuestionQueryStatement.executeQuery(
                    "select * from sys_node");
            while(questionQueryRs.next()) {
                String id=questionQueryRs.getString("id");
                String name =questionQueryRs.getString("name");
                String position =questionQueryRs.getString("position");
                System.out.println("开始处理分类："+id);
                newConn.setAutoCommit(true);
                switch (position) {
                //文章分类
                case "0":
                    PreparedStatement newQuestionStatement =newConn.prepareStatement(
                            "INSERT INTO `article_category` (`id`,`name`, `is_deleted`, `sort`, `is_default`) VALUES (?, ?, '0', ?, '0')");
                    newQuestionStatement.setString(1, id);
                    newQuestionStatement.setString(2, name);
                    newQuestionStatement.setString(3, id);
                    newQuestionStatement.executeUpdate();
                    newQuestionStatement.close();
                    break;
                //视频分类
                case "1":
                    PreparedStatement newQuestionStatement1 =newConn.prepareStatement(
                            "INSERT INTO `video_category` (`id`,`name`, `is_deleted`, `sort`, `is_default`) VALUES (?, ?, '0', ?, '0')");
                    newQuestionStatement1.setString(1, id);
                    newQuestionStatement1.setString(2, name);
                    newQuestionStatement1.setString(3, id);
                    newQuestionStatement1.executeUpdate();
                    newQuestionStatement1.close();
                    break;
                //试题年级
                case "2":
                    PreparedStatement newQuestionStatement2 =newConn.prepareStatement(
                            "INSERT INTO `grade` (`id`,`name`, `is_deleted`, `sort`) VALUES (?, ?, '0', ?)");
                    newQuestionStatement2.setString(1, id);
                    newQuestionStatement2.setString(2, name);
                    newQuestionStatement2.setString(3, id);
                    newQuestionStatement2.executeUpdate();
                    newQuestionStatement2.close();
                    break;
                }
            }
            
            newConn.close();
            oldQuestionQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
}
