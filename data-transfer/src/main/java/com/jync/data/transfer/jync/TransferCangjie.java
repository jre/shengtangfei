package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class TransferCangjie 
{
    public static void main( String[] args )
    {
        

        Connection newConn=null;
        String newUrl = "jdbc:mysql://39.99.143.255:3306/cjxt?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            newConn = DriverManager.getConnection(newUrl,"root","cjxt@2020");
            Statement newSubjectQueryStatement =newConn.createStatement();
            //题库
            ResultSet subjectQueryRs=newSubjectQueryStatement.executeQuery(
                    "select * from culture_section");
            while(subjectQueryRs.next()) {
                String id=subjectQueryRs.getString("id");
                String content=subjectQueryRs.getString("content");
                Document document=Jsoup.parse(content);
                Elements elements=document.getElementsByTag("p");
                String description="";
                String all="";
                for (int i = 0; i < elements.size(); i++) {
                    Element element=elements.get(i);
                    String str=element.text();
                    all=all+str;
                }
                description=StringUtils.substring(all, 0, 100);
                System.out.println("开始处理模块："+id);
                newConn.setAutoCommit(true);
                Statement newSubjectStatement =newConn.createStatement();
                newSubjectStatement.executeUpdate("update culture_section set description='"+description+"' where id="+id);
                newSubjectStatement.close();
                
            }
            
            newSubjectQueryStatement.close();
            newConn.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
