package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldModule6 
{
    /**
     * @param args
     */
    public static void main( String[] args )
    {
    	

        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            
            Statement newSubjectQueryStatement =newConn.createStatement();
            //题库
            ResultSet subjectQueryRs=newSubjectQueryStatement.executeQuery(
                    "select * from module where parent_id is null");
            int i=0;
            while(subjectQueryRs.next()) {
                //题库
                Statement idQueryStatement =newConn.createStatement();
                ResultSet idQueryRs=idQueryStatement.executeQuery(
                        "select max(id) from module");
                idQueryRs.next();
                Long parentId= idQueryRs.getLong(1);
                parentId++;
                idQueryRs.close();
                idQueryStatement.close();
                
                String id=subjectQueryRs.getString("id");
                System.out.println("开始处理模块："+id);
                String subject_id=subjectQueryRs.getString("subject_id");
                String grade_id=subjectQueryRs.getString("grade_id");
                String name=subjectQueryRs.getString("name");
                
                PreparedStatement newSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `module` (`id`, `name`, `question_count`,  `create_time`,`sort`, `is_deleted`, `subject_id`, `grade_id`, `icon`, `difficulty`, `score`,`parent_id`) "
                                + "VALUES (?,?, '0', ?, ?, '0', ?, ?, ?, '0', '1','0')");
                newSubjectStatement.setString(1, parentId+"");
                newSubjectStatement.setString(2, name);
                newSubjectStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                newSubjectStatement.setString(4, parentId+"");
                newSubjectStatement.setString(5, subject_id);
                newSubjectStatement.setString(6, grade_id);
                newSubjectStatement.setString(7, "");
                newSubjectStatement.executeUpdate();
                newSubjectStatement.close();
                
                Statement newSubjectStatement1 =newConn.createStatement();
                newSubjectStatement1.executeUpdate("update module set parent_id= "+parentId+" where id="+id);
                newSubjectStatement1.close();
                i++;
            }
            subjectQueryRs.close();
            newSubjectQueryStatement.close();
            newConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
//    /**
//     * @param args
//     */
//    public static void main( String[] args )
//    {
//        
//        
//        Connection newConn=null;
//        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
//                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
//        try {
//            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
//            
//            System.out.println("成功加载MySQL驱动程序");
//            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
//            
//            Statement idQueryStatement =newConn.createStatement();
//            //题库
//            ResultSet idQueryRs=idQueryStatement.executeQuery(
//                    "select max(id) from module");
//            idQueryRs.next();
//            Long parentId= idQueryRs.getLong(1);
//            parentId++;
//            idQueryRs.close();
//            idQueryStatement.close();
//            Statement newSubjectQueryStatement =newConn.createStatement();
//            String name="两位数除三位数";
//            //题库
//            ResultSet subjectQueryRs=newSubjectQueryStatement.executeQuery(
//                    "select * from module where parent_id is null and name like '"+name+"%'");
//            int i=0;
//            while(subjectQueryRs.next()) {
//                String id=subjectQueryRs.getString("id");
//                System.out.println("开始处理模块："+id);
//                String subject_id=subjectQueryRs.getString("subject_id");
//                String grade_id=subjectQueryRs.getString("grade_id");
//                
//                if(i==0) {
//                    PreparedStatement newSubjectStatement =newConn.prepareStatement(
//                            "INSERT INTO `module` (`id`, `name`, `question_count`,  `create_time`,`sort`, `is_deleted`, `subject_id`, `grade_id`, `icon`, `difficulty`, `score`,`parent_id`) "
//                                    + "VALUES (?,?, '0', ?, ?, '0', ?, ?, ?, '0', '1','0')");
//                    newSubjectStatement.setString(1, parentId+"");
//                    newSubjectStatement.setString(2, name);
//                    newSubjectStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
//                    newSubjectStatement.setString(4, parentId+"");
//                    newSubjectStatement.setString(5, subject_id);
//                    newSubjectStatement.setString(6, grade_id);
//                    newSubjectStatement.setString(7, "");
//                    newSubjectStatement.executeUpdate();
//                    newSubjectStatement.close();
//                }
//                Statement newSubjectStatement =newConn.createStatement();
//                newSubjectStatement.executeUpdate("update module set parent_id= "+parentId+" where id="+id);
//                newSubjectStatement.close();
//                i++;
//            }
//            subjectQueryRs.close();
//            newSubjectQueryStatement.close();
//            newConn.close();
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
