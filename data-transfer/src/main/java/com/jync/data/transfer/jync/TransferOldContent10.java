package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldContent10 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://39.104.73.67:3306/shengtangfeng?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://39.104.73.67:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","Bang_toutiao_@");
            newConn = DriverManager.getConnection(newUrl,"root","Bang_toutiao_@");
            Statement oldQuestionQueryStatement =oldConn.createStatement();
            //题库
            ResultSet questionQueryRs=oldQuestionQueryStatement.executeQuery(
                    "select * from article where summary is null");
            while(questionQueryRs.next()) {
                Long id=questionQueryRs.getLong("id");
                String content=questionQueryRs.getString("content");
                System.out.println("开始处理内容："+id);
                
                String  summary=dealContent(content);
                newConn.setAutoCommit(true);
                PreparedStatement newQuestionStatement =newConn.prepareStatement(
                        "update `article` set summary=? where id=?");
                
                newQuestionStatement.setString(1, summary);
                newQuestionStatement.setLong(2, id);
                newQuestionStatement.executeUpdate();
                newQuestionStatement.close();
            }
            
            newConn.close();
            oldQuestionQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
    /**
     * 去除富文本的标签
     * @param content
     * @return
     */
    public static String dealContent(String content){
        String regx = "(<.+?>)|(</.+?>)";
        Matcher matcher = Pattern.compile(regx).matcher(content);
        while (matcher.find()){
            // 替换图片
            content= matcher.replaceAll("").replace(" ", "");
        }

        return content;
    }
}
