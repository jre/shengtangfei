package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldQuestion5 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://192.168.6.102:3306/toutiao?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","boruankeji@2018");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement oldSubjectQueryStatement =oldConn.createStatement();
            //题库
            ResultSet subjectQueryRs=oldSubjectQueryStatement.executeQuery(
                    "select * from taoxue_timu where type=2 and single=1");
            while(subjectQueryRs.next()) {
                String id=subjectQueryRs.getString("id");
                String title =subjectQueryRs.getString("title");
                String course_id =subjectQueryRs.getString("course_id");
                //1单选 2 多选
                String single =subjectQueryRs.getString("single");
                Integer type=0;
                if(StringUtils.equals(single, "2")) {
                    continue;
                }
                String answer =subjectQueryRs.getString("answer");
                List<QuestionOption> options=new ArrayList<>();
                String item_a =subjectQueryRs.getString("item_a");
                item_a=dealContent(item_a);
                QuestionOption a=new QuestionOption();
                a.setType("text");
                a.setContent(item_a);
                if(StringUtils.equalsIgnoreCase(answer, "a")) {
                    a.setIsCorrect(true);
                }else {
                    a.setIsCorrect(false);
                }
                options.add(a);
                String item_b =subjectQueryRs.getString("item_b");
                item_b=dealContent(item_b);
                QuestionOption b=new QuestionOption();
                b.setType("text");
                b.setContent(item_b);
                if(StringUtils.equalsIgnoreCase(answer, "b")) {
                    b.setIsCorrect(true);
                }else {
                    b.setIsCorrect(false);
                }
                options.add(b);
                String item_c =subjectQueryRs.getString("item_c");
                item_c=dealContent(item_c);
                QuestionOption c=new QuestionOption();
                c.setType("text");
                c.setContent(item_c);
                if(StringUtils.equalsIgnoreCase(answer, "c")) {
                    c.setIsCorrect(true);
                }else {
                    c.setIsCorrect(false);
                }
                options.add(c);
                String item_d =subjectQueryRs.getString("item_d");
                item_d=dealContent(item_d);
                QuestionOption d=new QuestionOption();
                d.setType("text");
                d.setContent(item_d);
                if(StringUtils.equalsIgnoreCase(answer, "d")) {
                    d.setIsCorrect(true);
                }else {
                    d.setIsCorrect(false);
                }
                options.add(d);
                String content=JSON.toJSONString(options);
                
                Timestamp create_time =subjectQueryRs.getTimestamp("create_time");
                
                Statement newContentQueryStatement =newConn.createStatement();
                ResultSet contentQueryRs=newContentQueryStatement.executeQuery(
                        "select * from module where id='"+course_id+"'");
                if(!contentQueryRs.next()) {
                    continue;
                }
                String subject_id=contentQueryRs.getString("subject_id");
                String grade_id=contentQueryRs.getString("grade_id");
                contentQueryRs.close();
                newContentQueryStatement.close();
                
                
                System.out.println("开始处理试题："+id);
                newConn.setAutoCommit(true);
                PreparedStatement newSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `question` (`id`, `title`, `content`, `answer`, `text_explain`, `subject_id`, `type`, `is_deleted`, `create_time`, `accurate`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, '0', ?,'0.00')");
                newSubjectStatement.setString(1, id);
                newSubjectStatement.setString(2, title);
                newSubjectStatement.setString(3, content);
                newSubjectStatement.setString(4, answer);
                newSubjectStatement.setString(5, "");
                newSubjectStatement.setString(6, subject_id);
                newSubjectStatement.setInt(7, type);
                newSubjectStatement.setTimestamp(8, create_time);
                newSubjectStatement.executeUpdate();
                newSubjectStatement.close();
                
                PreparedStatement newGradeSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `question_grade` (`question_id`,`grade_id`) VALUES (?, ?)");
                newGradeSubjectStatement.setString(1, id);
                newGradeSubjectStatement.setString(2, grade_id);
                newGradeSubjectStatement.executeUpdate();
                newGradeSubjectStatement.close();
                
            }
            
            newConn.close();
            oldSubjectQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
    /**
     * 去除富文本的标签
     * @param content
     * @return
     */
    public static String dealContent(String content){
        if(StringUtils.isBlank(content)) {
            return "";
        }
        String regx = "(<.+?>)|(</.+?>)";
        Matcher matcher = Pattern.compile(regx).matcher(content);
        while (matcher.find()){
            // 替换图片
            content= matcher.replaceAll("").replace(" ", "");
        }

        return content;
    }
}
