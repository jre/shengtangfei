package com.jync.data.transfer.utils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by  on 2017/6/10.
 */
public class UUIDUtil {

    public static final String allChar = "123456789abcdefghijkmnpqrstuvwxyz";

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }


    /**
     * 返回时间样式的id (201706121614xxxxx)
     * @Title: getTimeId
     * @Description:
     * @author 宋天成
     * @date 2017年6月12日 下午4:11:20
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     * @version V1.0
     */
    public static String getTimeId(){
        SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
        Date da = new Date();
        return si.format(da)+(int)((Math.random()*9+1)*100000);
    }

    /**
     * 返回一个定长的随机字符串(只包含大小写字母、数字)
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String getCode(int length) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(allChar.charAt(random.nextInt(allChar.length())));
        }
        return sb.toString();
    }

}
