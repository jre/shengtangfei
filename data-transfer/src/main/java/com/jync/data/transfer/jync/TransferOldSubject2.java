package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldSubject2 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://39.99.143.255:3306/cjxt?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://39.99.143.255:3306/cjxt?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","boruankeji@2018");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement oldSubjectQueryStatement =oldConn.createStatement();
            //题库
            ResultSet subjectQueryRs=oldSubjectQueryStatement.executeQuery(
                    "select * from taoxue_nianji");
            while(subjectQueryRs.next()) {
                String id=subjectQueryRs.getString("id");
                String name =subjectQueryRs.getString("name");
                String category_id =subjectQueryRs.getString("category_id");
                System.out.println("开始处理学科："+id);
                newConn.setAutoCommit(true);
                PreparedStatement newSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `subject` (`id`,`name`, `is_deleted`, `sort`) VALUES (?, ?, '0', ?)");
                newSubjectStatement.setString(1, id);
                newSubjectStatement.setString(2, name);
                newSubjectStatement.setString(3, id);
                newSubjectStatement.executeUpdate();
                newSubjectStatement.close();
                
                PreparedStatement newGradeSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `grade_subject` (`grade_id`,`subject_id`) VALUES (?, ?)");
                newGradeSubjectStatement.setString(1, category_id);
                newGradeSubjectStatement.setString(2, id);
                newGradeSubjectStatement.executeUpdate();
                newGradeSubjectStatement.close();
            }
            
            newConn.close();
            oldSubjectQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
}
