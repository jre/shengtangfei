package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldContent3 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://192.168.6.102:3306/toutiao?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","boruankeji@2018");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement oldQuestionQueryStatement =oldConn.createStatement();
            //题库
            ResultSet questionQueryRs=oldQuestionQueryStatement.executeQuery(
                    "select * from content");
            while(questionQueryRs.next()) {
                String id=questionQueryRs.getString("id");
                int item_type =questionQueryRs.getInt("item_type");
                String source=questionQueryRs.getString("source");
                String title=questionQueryRs.getString("title");
                String category=questionQueryRs.getString("category");
                String thumb=questionQueryRs.getString("thumb");
                String img_1=questionQueryRs.getString("img_1");
                String img_2=questionQueryRs.getString("img_2");
                String img_3=questionQueryRs.getString("img_3");
                String video_thumb=questionQueryRs.getString("video_thumb");
                String video_url=questionQueryRs.getString("video_url");
                Timestamp create_time=questionQueryRs.getTimestamp("create_time");
                System.out.println("开始处理内容："+id);
                newConn.setAutoCommit(true);
                //视频
                if(item_type==4) {
                    PreparedStatement newQuestionStatement =newConn.prepareStatement(
                            "INSERT INTO `video` ("
                            + "`id`, `create_time`, `create_by`, `is_deleted`, `content`, `title`, `publish_date`, `publisher`, `status`, `publisher_icon`, `recommend`, `read_num`, `comment_num`, `category_id`, `image`, `publish_id`, `thump_up_num`, `upload_type`"
                            + ") VALUES ("
                            + "?, ?, '1',  '0', ?, ?, ?, '盛唐风', '1', ?, '0', '0', '0', ?, ?, '1', '0', 0)");
                    newQuestionStatement.setString(1, id);
                    newQuestionStatement.setTimestamp(2, create_time);
                    newQuestionStatement.setString(3, video_url);
                    newQuestionStatement.setString(4, title);
                    newQuestionStatement.setTimestamp(5, create_time);
                    newQuestionStatement.setString(6, "https://public.stftt.com/shengtangfeng/public/user_icon.png");
                    newQuestionStatement.setString(7, category);
                    newQuestionStatement.setString(8, video_thumb);
                    newQuestionStatement.executeUpdate();
                    newQuestionStatement.close();
                }else {
                    PreparedStatement newQuestionStatement =newConn.prepareStatement(
                            "INSERT INTO `article` ("
                                    + "`id`, `create_time`, `create_by`, `is_deleted`, `content`, `title`, `publish_date`, `publisher`, `status`, `publisher_icon`, `recommend`, `read_num`, `comment_num`, `category_id`, `publish_id`, `thump_up_num`, `upload_type`, `type`, `images`"
                                    + ") VALUES ("
                                    + "?, ?, '1',  '0', ?, ?, ?, '盛唐风', '1', ?, '0', '0', '0', ?,'1', '0', 0,?,?)");
                    newQuestionStatement.setString(1, id);
                    newQuestionStatement.setTimestamp(2, create_time);
                    
                    Statement oldContentQueryStatement =oldConn.createStatement();
                    //内容
                    ResultSet contentQueryRs=oldContentQueryStatement.executeQuery(
                            "select * from content_data where content_id='"+id+"'");
                    contentQueryRs.next();
                    String content=contentQueryRs.getString("description");
                    contentQueryRs.close();
                    oldContentQueryStatement.close();
                    
                    
                    newQuestionStatement.setString(3, content);
                    
                    newQuestionStatement.setString(4, title);
                    newQuestionStatement.setTimestamp(5, create_time);
                    newQuestionStatement.setString(6, "https://public.stftt.com/shengtangfeng/public/user_icon.png");
                    newQuestionStatement.setString(7, category);
                    
                    
                    
//                    图数量 1 无图  2 一张图 3 多于一张
                    if(!StringUtils.isAnyBlank(thumb,img_1,img_2,img_3)) {
                        newQuestionStatement.setString(8, "3");
                        newQuestionStatement.setString(9, thumb+","+img_1+","+img_2+","+img_3);
                    }else if(!StringUtils.isAnyBlank(thumb,img_1,img_2)){
                        newQuestionStatement.setString(8, "3");
                        newQuestionStatement.setString(9, thumb+","+img_1+","+img_2);
                    }else if(!StringUtils.isAnyBlank(thumb,img_1)){
                        newQuestionStatement.setString(8, "3");
                        newQuestionStatement.setString(9, thumb+","+img_1);
                    }else if(!StringUtils.isAnyBlank(thumb)){
                        newQuestionStatement.setString(8, "2");
                        newQuestionStatement.setString(9, thumb);
                    }else {
                        newQuestionStatement.setString(8, "1");
                        newQuestionStatement.setString(9, "");
                    }
                    newQuestionStatement.executeUpdate();
                    newQuestionStatement.close();
                    
                }
            }
            
            newConn.close();
            oldQuestionQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
}
