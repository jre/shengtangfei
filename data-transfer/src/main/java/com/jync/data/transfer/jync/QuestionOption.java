package com.jync.data.transfer.jync;

public class QuestionOption {
//    [{"type":"text","content":"6400","isCorrect":true},{"type":"text","content":"6900","isCorrect":false},{"type":"text","content":"5800","isCorrect":false},{"type":"text","content":"6500","isCorrect":false}]
	private String type;
	private String content;
	private Boolean isCorrect;
//	private Boolean ischecked;
//	private Integer optionNo;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public Boolean getIsCorrect() {
        return isCorrect;
    }
    public void setIsCorrect(Boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
//    public Boolean getIschecked() {
//        return ischecked;
//    }
//    public void setIschecked(Boolean ischecked) {
//        this.ischecked = ischecked;
//    }
//    public Integer getOptionNo() {
//        return optionNo;
//    }
//    public void setOptionNo(Integer optionNo) {
//        this.optionNo = optionNo;
//    }

}
