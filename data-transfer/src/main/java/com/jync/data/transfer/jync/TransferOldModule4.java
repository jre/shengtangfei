package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldModule4 
{
    public static void main( String[] args )
    {
    	

    	String oldUrl = "jdbc:mysql://192.168.6.102:3306/toutiao?"
    			+ "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        Connection oldConn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            oldConn = DriverManager.getConnection(oldUrl,"root","boruankeji@2018");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement oldSubjectQueryStatement =oldConn.createStatement();
            //题库
            ResultSet subjectQueryRs=oldSubjectQueryStatement.executeQuery(
                    "select * from taoxue_course");
            while(subjectQueryRs.next()) {
                String id=subjectQueryRs.getString("id");
                String nianji =subjectQueryRs.getString("nianji");
                String category_id =subjectQueryRs.getString("category_id");
                String title =subjectQueryRs.getString("title");
                String thumb =subjectQueryRs.getString("thumb");
                Timestamp create_time =subjectQueryRs.getTimestamp("create_time");
                System.out.println("开始处理学科："+id);
                newConn.setAutoCommit(true);
                PreparedStatement newSubjectStatement =newConn.prepareStatement(
                        "INSERT INTO `module` (`id`, `name`, `question_count`,  `create_time`,`sort`, `is_deleted`, `subject_id`, `grade_id`, `icon`, `difficulty`, `score`) "
                        + "VALUES (?,?, '0', ?, ?, '0', ?, ?, ?, '0', '1')");
                newSubjectStatement.setString(1, id);
                newSubjectStatement.setString(2, title);
                newSubjectStatement.setTimestamp(3, create_time);
                newSubjectStatement.setString(4, id);
                newSubjectStatement.setString(5, nianji);
                newSubjectStatement.setString(6, category_id);
                newSubjectStatement.setString(7, thumb);
                newSubjectStatement.executeUpdate();
                newSubjectStatement.close();
                
            }
            
            newConn.close();
            oldSubjectQueryStatement.close();
            oldConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
}
