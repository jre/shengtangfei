package com.jync.data.transfer.jync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * Hello world!
 *
 */
public class TransferOldQuestion9 
{
    public static void main( String[] args )
    {
    	

        Connection newConn=null;
        String newUrl = "jdbc:mysql://192.168.6.102:3306/shengtangfeng?"
                + "useUnicode=true&characterEncoding=UTF8&useSSL=false";
        try {
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动
 
            System.out.println("成功加载MySQL驱动程序");
            newConn = DriverManager.getConnection(newUrl,"root","boruankeji@2018");
            Statement newSubjectQueryStatement =newConn.createStatement();
            //题库
            ResultSet subjectQueryRs=newSubjectQueryStatement.executeQuery(
                    "select * from question");
            while(subjectQueryRs.next()) {
                String id=subjectQueryRs.getString("id");
                String title=subjectQueryRs.getString("title");
                title=dealContent(title);
                title=StringUtils.replace(title, "&nbsp;", "");
                System.out.println("开始处理模块："+id);
                newConn.setAutoCommit(true);
                Statement newSubjectStatement =newConn.createStatement();
                newSubjectStatement.executeUpdate("update question set title='"+title+"' where id="+id);
                newSubjectStatement.close();
                
            }
            
            newSubjectQueryStatement.close();
            newConn.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
    }
    /**
     * 去除富文本的标签
     * @param content
     * @return
     */
    public static String dealContent(String content){
        if(StringUtils.isBlank(content)) {
            return "";
        }
        String regx = "(<.+?>)|(</.+?>)";
        Matcher matcher = Pattern.compile(regx).matcher(content);
        while (matcher.find()){
            // 替换图片
            content= matcher.replaceAll("").replace(" ", "");
        }

        return content;
    }
}
